<?php

namespace App\Http\Controllers\Api;

use App\Models\Project;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FilesController extends Controller
{
    public function index() {

        if(request()->user()->be_writer) {
            $projects = Project::where('writer_id', request()->user()->id)->select('id', 'topic', 'completed')->with('projectfiles')->get();
        } else {
            $projects = request()->user()->projects()->select('id', 'topic', 'completed')->with('projectfiles')->get();
        }

        return response(['status' => 'success', 'data' => $projects], 200);

    }
}
