<?php

namespace App\Http\Controllers\Api;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Auth\Passwords\PasswordBroker;

class ProfileController extends Controller
{

    public function getProfile(User $user)
    {
        $user['completed_projects_count'] = $user->completed_projects_count;
        $user['ongoing_projects_count'] = $user->ongoing_projects_count;
        return response(['status' => 'success', 'data' => $user], 200);
    }

    public function updateProfile(Request $request)
    {
        $this->validate($request, ['fname' => 'required', 'lname' => 'required', 'phone' => 'required']);

        auth()->user()->update([
            'fname' => request('fname'),
            'lname' => request('lname'),
            'phone' => request('phone'),
            'subjects' => array_key_exists("subjects", $request->all()) ? implode(',', request('subjects')) : ''
        ]);

        return response(['status' => 'success', 'message' => 'Profile Updated!'], 200);
    }

    public function updateAvatar(Request $request)
    {

        request()->validate(['profile_photo' => 'required']);

        $data = request()->profile_photo;

        $image = request()->profile_photo;  // your base64 encoded
        $image = preg_replace('#^data:[^;]/[^;]+;base64,#', '', $image);
        $image = str_replace(' ', '+', $image);
        $imageData = base64_decode($image);
        $f = finfo_open();
        $mime_type = finfo_buffer($f, $imageData, FILEINFO_MIME_TYPE);
        //return response($mime_type);
        $imageName = str_random(10) . '.' . mime2ext($mime_type);

        if(!\File::exists('/'.'storage/profile_photo/')) {
            \Storage::makeDirectory('profile_photo');
        }

        $path =  "/profile_photo/" . $imageName;

        \Storage::put('profile_photo/' . $imageName, $imageData);

        request()->user()->profile_photo = $path;
        request()->user()->save();

        return response(['status' => 'success', 'url' => \Storage::url($path), 'message' => 'Profile Photo Updated!'], 200);


       /* $file = request()->file('profile_photo');
        $url = '';
        if(!empty($file)) {
           $url = \Storage::url(request()->file('profile_photo')->store('files'));
        }

        request()->user()->profile_photo = $url;
        request()->user()->save();
        return response(['url' => $url], 200);*/
    }

    public function updatePassword(Request $request)
    {
        $this->validate($request, [
            'old_password' => 'required',
            'password' => 'required|string|min:8|strong_password|confirmed'
        ]);

        $old_password = $request->get('old_password');
        $password = $request->get('password');

        if(!\Hash::check($old_password, request()->user()->password))
        {
            return response(['status' => 'fail', 'message' => 'Your old password is incorrect!'], 200);
        }

        request()->user()->password = bcrypt($password);
        request()->user()->save();
        return response(['status' => 'success', 'message' => 'Password Updated'], 200);
    }

    public function availability(){
        return response(['status' => 'success', 'data' => request()->user()->availability], 200);
    }


    public function saveavailability(Request $request)
    {
        if (request()->user()->availability) {
            request()->user()->availability()->update(['available_dates' => request('available_dates')]);
            return response(['status' => 'success', 'message' => 'Availability Updated!'], 200);
        }else{
            $availability = request()->user()->availability()->create(['available_dates' => request('available_dates')]);
            return response(['status' => 'success', 'message' => 'Availability Created!', 'data' => $availability], 200);
        }
        return back();
    }

   /* public function CompletedProjectsCount()
    {
        return response(['data' => request()->user()->completed_projects_count], 200);
    }

    public function OngoingProjectsCount()
    {
        return response(['data' => request()->user()->ongoing_projects_count], 200);
    }*/

    public function sendResetLinkEmail(Request $request, PasswordBroker $broker)
    {
        if( $request->wantsJson() )
        {
            $this->validate($request, ['email' => 'required|email']);
            $response = $broker->sendResetLink($request->only('email'));
            return $response == PasswordBroker::RESET_LINK_SENT
                    ? response(['status' => 'success', 'message' => 'Password reset instructions sent!', 'data' => []])
                    : response(['status' => 'failed', 'message' => 'There was some problem loading profile data!', 'data' => []]);
        }
        return false;
    }
}
