<?php

namespace App\Http\Controllers\Api;

use App\User;
use App\Models\Project;
use Illuminate\Http\Request;
use App\Notifications\NewMessage;
use App\Http\Controllers\Controller;

class ProjectChatsController extends Controller
{
    public function store(Request $request, Project $project)
    {

        $request->validate([
            'message' => 'required'
        ]);
        // $text = preg_replace("(?:(?:\+?([1-9]|[0-9][0-9]|[0-9][0-9][0-9])\s*(?:[.-]\s*)?)?(?:\(\s*([2-9]1[02-9]|[2-9][02-8]1|[2-9][02-8][02-9])\s*\)|([0-9][1-9]|[0-9]1[02-9]|[2-9][02-8]1|[2-9][02-8][02-9]))\s*(?:[.-]\s*)?)?([2-9]1[02-9]|[2-9][02-9]1|[2-9][02-9]{2})\s*(?:[.-]\s*)?([0-9]{4})(?:\s*(?:#|x\.?|ext\.?|extension)\s*(\d+))?", '[blocked]', request('message'));

        $text = preg_replace('/\+?[0-9][0-9a-z()\-\s+]{4,20}[0-9][a-z]*/', '[blocked]', request('message'));
        // $text = preg_replace('(?:[a-z0-9!#$%&+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&+/=?^_`{|}~-]+)|(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f]))@(?:(?:[a-z0-9](?:[a-z0-9-][a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-][a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])', '[blocked]', $text);

        $text = preg_replace('^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})^', '[email blocked]', $text);

        if(request()->user()->id == $project->user->id){
            $message = $project->chats()->create(
                [
                    'message' => $text,
                    'sender' => $project->user->id,
                    'receiver'=>  $project->writer->id
                ]
            );
            $project->writer->notify(new NewMessage($message));
        }else{
            $message = $project->chats()->create(
                [
                    'message' => $text,
                    'sender' => $project->writer->id,
                    'receiver'=>  $project->user->id
                ]
            );
            $project->user->notify(new NewMessage($message));
        }

        return response(['status' => 'success', 'message' => $message], 200);
    }
}
