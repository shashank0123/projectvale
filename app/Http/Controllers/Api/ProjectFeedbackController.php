<?php

namespace App\Http\Controllers\Api;

use App\User;
use App\Models\Project;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Notifications\CompleteProject;

class ProjectFeedbackController extends Controller
{
    public function store(Request $request, Project $project)
    {
        $project->completed = 1;
        $project->status = 4;
        $project->save();

        $data = request()->all();
        $data['writer_id'] = $project->writer_id;
        if(request('score')){
            $data['rating'] = request('score');
        }else{
            $data['rating'] = 3;
        }
        $project->feedback()->create($data);

        $writer = User::where('id', $project->writer_id)->first();

        $userMessage = 'Congratulations! The project that you had posted on ProjectVala was successfully completed.';
        $response = sendSMS($project->user->phone, $userMessage);

        $writerMessage = 'Congratulations! A project you were working on ProjectVala is marked complete by the client.';
        $response = sendSMS($writer->phone, $writerMessage);

        $writer->notify(new CompleteProject($project, $writer));
        return response(['status' => 'success', 'message' => 'Project Completed!'], 200);
    }

    public function skip(Project $project)
    {
        $project->completed = 1;
        $project->status = 4;
        $project->save();

        $writer = User::where('id', $project->writer_id)->first();

        $userMessage = 'Congratulations! The project that you had posted on ProjectVala was successfully completed.';
        $response = sendSMS($project->user->phone, $userMessage);

        $writerMessage = 'Congratulations! A project you were working on ProjectVala is marked complete by the client.';
        $response = sendSMS($writer->phone, $writerMessage);

        $writer->notify(new CompleteProject($project, $writer));

        return response(['status' => 'success', 'message' => 'Project Completed!'], 200);
    }
}
