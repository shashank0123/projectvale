<?php

namespace App\Http\Controllers\Api;

use App\User;
use App\Models\Project;
use App\Models\ProjectFile;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Notifications\UploadProjectFile;
use App\Notifications\UploadWriterProjectFile;

class ProjectFilesController extends Controller
{
    public function store(Project $project, User $user, Request $request)
    {
        $request->validate([
            'name' => 'required',
            'file' => 'required',
        ]);

        $data = request()->file;

        $file = request()->file;  // your base64 encoded
        $file = preg_replace('#^data:[^;]/[^;]+;base64,#', '', $file);
        $file = str_replace(' ', '+', $file);
        $filedata = base64_decode($file);
        $f = finfo_open();
        $mime_type = finfo_buffer($f, $filedata, FILEINFO_MIME_TYPE);
        $fileName = str_random(10) . '.' . mime2ext($mime_type);

         if(!\File::exists('/'.'storage/files/')) {
            \Storage::makeDirectory('files');
        }

        $path =  "/files/" . $fileName;

        \Storage::put('files/' . $fileName, $filedata);


        if(auth()->id() == $project->user->id){
            $file = $project->projectfiles()->create(['name' => request('name'), 'description' => request('description'), 'user_id' => $user->id, 'file' => $path, 'is_verify' => 1]);
            $message = 'A new file was uploaded on ProjectVala. Visit your account to check the file.';
            $response = sendSMS($project->writer->phone, $message);
            $project->writer->notify(new UploadWriterProjectFile($project, $project->writer));
        }else{
            $file = $project->projectfiles()->create(['name' => request('name'), 'description' => request('description'), 'user_id' => $user->id, 'file' => $path, 'is_verify' => 0]);
            /*$message = 'A new file was uploaded on ProjectVala. Visit your account to check the file.';
            $response = sendSMS($project->user->phone, $message);*/
            $project->user->notify(new UploadProjectFile($project, $project->user));
        }
        return response(['status' => 'success', 'message' => 'File Uploaded!', 'data' => $file], 200);
    }

    public function destroy(ProjectFile $file, Project $project)
    {
        $file->delete();
        return response(['status' => 'success', 'message' => 'File Deleted!'], 200);
    }
}
