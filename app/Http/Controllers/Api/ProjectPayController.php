<?php

namespace App\Http\Controllers\Api;

use App\User;
use App\Models\Project;
use Illuminate\Http\Request;
use App\Notifications\HireWriter;
use App\Http\Controllers\Controller;

class ProjectPayController extends Controller
{
    public function store(Project $project, User $writer)
    {
        $project->update(['writer_id' => $writer->id,'status' => 3, 'payment_status' => 1]);
        $writerMessage = 'You have received a new project on ProjectVala. Visit your account to start working on it.';
        $response = sendSMS($writer->phone, $writerMessage);

        $userMessage = 'Thank you for making the payment on ProjectVala. Please visit your account to stay updated.';
        $response = sendSMS($project->user->phone, $userMessage);

        $writer->notify(new HireWriter($project, $writer));

        return response(['status' => 'success', 'message' => 'Payment Successfully Done!'], 200);
    }
}
