<?php

namespace App\Http\Controllers\Api;

use App\User;
use App\Models\Project;
use App\Models\ProjectFile;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Notifications\UploadProjectFile;
use App\Notifications\UploadWriterProjectFile;

class ProjectReferencesController extends Controller
{
    public function store(Project $project,  Request $request)
    {
        $request->validate([
            'file' => 'required',
        ]);


        $file = request()->file;  // your base64 encoded
        $file = preg_replace('#^data:[^;]/[^;]+;base64,#', '', $file);
        $file = str_replace(' ', '+', $file);
        $filedata = base64_decode($file);
        $f = finfo_open();
        $mime_type = finfo_buffer($f, $filedata, FILEINFO_MIME_TYPE);
        $fileName = str_random(10) . '.' . mime2ext($mime_type);

        if(!\File::exists('/'.'storage/references/')) {
            \Storage::makeDirectory('references');
        }

        $path =  "/references/" . $fileName;

        \Storage::put('references/' . $fileName, $filedata);

        $filesList = $project->files;

        $filesList[] = $path;

        $project->files = $filesList;
        $project->save();

        return response(['status' => 'success', 'message' => 'File Uploaded!', 'data' => $filesList], 200);
    }


}
