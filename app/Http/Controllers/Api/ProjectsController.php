<?php

namespace App\Http\Controllers\Api;

use App\User;
use App\Models\Project;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Notifications\ProjectChanges;
use App\Notifications\AcceptHireWriter;
use Illuminate\Support\Facades\Storage;
use App\Notifications\DeclineHireWriter;
use App\Notifications\RequestHireWriter;
use App\Notifications\CancelRequestHireWriter;

class ProjectsController extends Controller
{
    public function index(Request $request)
    {
        if (request()->user()->be_writer) {
            $projects = request()->user()->writerProjects;
            foreach ($projects as $key => $project) {
                $project['writer_user_name'] = isset($project->writer) ? $project->writer->username : null;
                $project['owner_user_name'] = isset($project->user) ? $project->user->username : null;
            }
            return response(['status' => 'success', 'data' =>  $projects], 200);
        }else{
            $projects = request()->user()->userProjects;
            foreach ($projects as $key => $project) {
                $project['writer_user_name'] = isset($project->writer) ? $project->writer->username : null;
                $project['owner_user_name'] = isset($project->user) ? $project->user->username : null;
            }
            return response(['status' => 'success', 'data' =>  $projects], 200);
        }
    }

    public function show(Project $project)
    {
        $project['history'] = $project->user->api_writer_history;
        return response(['status' => 'success', 'data' =>  $project->toArray()], 200);
    }

    public function store(Request $request)
    {
        $newData = $request->all();
        if (isset($newData->file)) {
            $newData->file = $request->file_path;
        }
        $project = request()->user()->projects()->create($newData);

        $message = 'You have posted a new project. Please visit your account to stay updated.';
        $response = sendSMS(request()->user()->phone, $message);

        return response(['status' => 'success', 'message' => 'Project Posted!', 'data' => $project], 200);
    }



    public function update(Request $request, Project $project)
    {
        $newData = $request->all();
        if (isset($newData->file)) {
            $newData->file = $request->file_path;
        }
        $project->update($newData);
        if($project->status == 3){
            $message = 'The user has updated instructions on ProjectVala. Visit your account to know the changes.';
            $response = sendSMS($project->writer->phone, $message);
            $project->writer->notify(new ProjectChanges($project, $project->writer));
        }
        return response(['status' => 'success', 'message' => 'Project Updated!'], 200);
    }

    public function destroy(Project $project)
    {
        $project->projectfiles()->delete();
        $project->chats()->delete();
        $project->feedback()->delete();
        $project->delete();
        return response(['status' => 'success', 'message' => 'Project Deleted!'], 200);
    }

    public function hire(Project $project, User $writer)
    {

        $project->update(['writer_id' => $writer->id,'status' => 1]);
        $message = 'You have received a new project request on ProjectVala. Visit your account to respond.';
        $response = sendSMS($writer->phone, $message);
        $writer->notify(new RequestHireWriter($project, $writer));
        return response(['status' => 'success', 'message' => 'Hire Request Sent!', 'data' => ['project_id'=> $project->id, 'writer_id' => $writer->id]], 200);

        /*$dates=[];
        if(isset($writer->availability)){
            $availability = str_replace(' ', '', $writer->availability->available_dates);
            $dates = explode(',' , $availability);

            if (in_array(date('Y-m-d'), $dates)) {
                $project->update(['writer_id' => $writer->id,'status' => 1]);
                $message = 'You have received a new project request on ProjectVala. Visit your account to respond.';
                $response = sendSMS($writer->phone, $message);
                $writer->notify(new RequestHireWriter($project, $writer));

                return response(['status' => 'success', 'message' => 'Hire Request Sent!', 'data' => ['project_id'=> $project->id, 'writer_id' => $writer->id]], 200);
            }else{
                $project->update(['writer_id' => $writer->id,'status' => 2]);
                return response(['status' => 'success', 'message' => 'Writer Hired, Please Make a Payment!', 'data' => ['project_id'=> $project->id, 'writer_id' => $writer->id]], 200);
            }
        }else{
            $project->update(['writer_id' => $writer->id,'status' => 2]);
            return response(['status' => 'success', 'message' => 'Writer Hired, Please Make a Payment!', 'data' => ['project_id'=> $project->id, 'writer_id' => $writer->id]], 200);
        }*/

    }

    public function accept(Project $project, User $writer)
    {
        $project->update(['status' => 2]);
        $message = 'The writer has accepted your project. Please visit your account to make the payment.';
        $response = sendSMS($project->user->phone, $message);
        $project->user->notify(new AcceptHireWriter($project, $project->user));
        return response(['status' => 'success', 'message' => 'Request Accepted!', 'data' => ['project_id'=> $project->id, 'writer_id' => $writer->id]], 200);
    }

    public function decline(Project $project, User $writer)
    {
        $message = 'We are sorry to inform you that the writer has rejected your project. Please visit your account and find another writer for your project.';
        $response = sendSMS($project->user->phone, $message);
        $project->user->notify(new DeclineHireWriter($project, $project->user));
        $project->update(['writer_id' => 0, 'status' => 0]);
        return response(['status' => 'success', 'message' => 'Request Declined!'], 200);
    }


    public function cancelRequest(Project $project, User $writer)
    {
        $project->update(['writer_id' => 0, 'status' => 0]);
        $writer->notify(new CancelRequestHireWriter($project, $writer));
        return response(['status' => 'success', 'message' => 'Request Cancelled!', 'data' => ['project_id'=> $project->id]], 200);
    }


    public function chats(Project $project)
    {
        return response(['data' =>  $project->chats], 200);
    }

    public function files(Project $project)
    {
        return response(['data' =>  $project->projectfiles], 200);
    }

    public function getProjectUsersname(Project $project)
    {
        $writer = isset($project->writer) ? $project->writer->username : null;
        $owner = isset($project->user) ? $project->user->username : null;
        return response(['data' =>  ['writer_user_name' => $writer, 'owner_user_name' => $owner]], 200);
    }
}
