<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\User;
use App\Models\Project;
use App\Models\ProjectFeedback;
use App\Http\Controllers\Controller;

class WriterProfileController extends Controller
{
    public function feedback(){
        $feedback = ProjectFeedback::where('writer_id', request()->user()->id)->get();
        return response(['status' => 'success', 'data' => $feedback], 200);
    }


}
