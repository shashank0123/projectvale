<?php

namespace App\Http\Controllers\Api;

use App\User;
use App\Models\Project;
use Illuminate\Http\Request;
use App\Models\ProjectFeedback;
use App\Http\Controllers\Controller;

class WritersController extends Controller
{
    public function index(Request $request, Project $project)
    {
        $writers = User::where('be_writer', 1)->where('is_verify', 1)->get();
        foreach ($writers as $key => $writer) {
            $writer['per_page_rate'] = $writer->project_rate($project);
        }
        return response(['status' => 'success', 'data' =>  $writers], 200);
    }

    public function show(User $writer)
    {
        return response(['status' => 'success', 'data' =>  $writer->toArray()], 200);
    }

    public function profile(Project $project, User $writer){
        $history = $writer->history;
        $feedback = ProjectFeedback::where('writer_id', $writer->id)->get();
        $writer['per_page_rate'] = $writer->project_rate($project);
        return response(['status' => 'success', 'data' =>  ['project' => $project, 'writer' => $writer, 'history' => $history, 'feedback' => $feedback]], 200);
    }

    public function detail(User $writer){
        return response(['status' => 'success', 'data' =>  $writer], 200);
    }

    public function wallet(User $writer){
        return response(['status' => 'success', 'data' =>  $writer], 200);
    }

    public function feedbacks(User $writer){
        $feedback = ProjectFeedback::where('writer_id', $writer->id)->get();
        foreach ($feedback as $key => $item) {
            $feedback[$key]['user_name'] = $item->project->user->username;
            $feedback[$key]['project_topic'] = $item->project->topic;
        }
        return response(['status' => 'success', 'data' => $feedback], 200);
    }

    public function history(User $writer){
        $history = $writer->history;
            return response(['status' => 'success', 'data' => $history], 200);
    }
}
