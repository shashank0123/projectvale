<?php

namespace App\Http\Controllers\Backend;

use App\User;
use App\Models\Project;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BackendDashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('admin_login');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $projects = Project::count();
        $ongoing = Project::where('status', 3)->where('completed', 0)->count();
        $completed = Project::where('status', 4)->where('completed', 1)->count();
        $writers = User::where('be_writer', 1)->count();
        $users = User::where('be_writer', 0)->count();
        $verify = User::where('be_writer', 1)->where('is_verify', 1)->count();
        $unverify = User::where('be_writer', 1)->where('is_verify', 0)->count();
        return view('backend.dashboard.index', compact('writers', 'users', 'projects', 'verify', 'unverify', 'ongoing', 'completed'));
    }
}
