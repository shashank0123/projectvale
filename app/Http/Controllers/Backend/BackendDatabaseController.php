<?php

namespace App\Http\Controllers\Backend;
use App\User;
use App\Models\Project;
use App\Models\ProjectFile;
use App\Models\ProjectChat;
use App\Models\Availability;
use App\Models\WriterDocument;
use App\Models\ProjectFeedback;
use App\Models\Backend\Subject;
use App\Models\Backend\Citation;
use App\Models\Backend\PaperType;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BackendDatabaseController extends Controller
{

    public function __construct()
    {
        $this->middleware('admin_login');
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function clear()
    {
        User::truncate();
        Project::truncate();
        ProjectFile::truncate();
        ProjectChat::truncate();
        Availability::truncate();
        WriterDocument::truncate();
        ProjectFeedback::truncate();
        flash('Database clear successfully!');
        return back();
    }

    public function subjectsClear()
    {
        Subject::truncate();
        flash('Subject table clear successfully!');
        return back();
    }

    public function papertypeClear()
    {
        PaperType::truncate();
        flash('Papertype table clear successfully!');
        return back();
    }

    public function citationClear()
    {
        Citation::truncate();
        flash('Citation table clear successfully!');
        return back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
