<?php

namespace App\Http\Controllers\Backend;
use App\User;
use App\Models\Project;
use App\Models\ProjectFile;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BackendFilesController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin_login');
    }

    public function index(){
        $projects = Project::paginate(10);
        return view('backend.files.index', compact('projects'));
    }

    public function verify(Request $request, ProjectFile $file){
        $message = 'A new file was uploaded on ProjectVala. Visit your account to check the file.';
        $verifyMessage = 'Your file has been successfully verified by admin on ProjectVala. Visit your account to check the file.';

        if(isset($request->plagiarism_report)){
            $fileName = "PlagiarismPeportFile".time().'.'.request()->plagiarism_report->getClientOriginalExtension();
            $request->plagiarism_report->storeAs('file',$fileName);
            $path = \Storage::putFileAs('plagiarism_report', $request->file('plagiarism_report'), $fileName, 'public');
            $file->update(['is_verify' => 1, 'plagiarism_report' => $path, 'unverify_reason' => '']);
            $response = sendSMS($file->project->user->phone, $message);
            $response = sendSMS($file->project->writer->phone, $verifyMessage);

        }else{
            $file->update(['is_verify' => 1, 'unverify_reason' => '']);
            $response = sendSMS($file->project->user->phone, $message);
            $response = sendSMS($file->project->writer->phone, $verifyMessage);
        }
        flash('File verify successfully!');
        return back();
    }

    public function unverify(Request $request, ProjectFile $file){
        $file->update(['is_verify' => -1, 'plagiarism_report' => '', 'unverify_reason' => request('unverify_reason')]);

        $unverifyMessage = 'Your file has been unverified by admin on ProjectVala. Visit your account to check the file.';
        $response = sendSMS($file->project->writer->phone, $unverifyMessage);

        flash('File unverify successfully!');
        return back();
    }
}
