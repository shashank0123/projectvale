<?php

namespace App\Http\Controllers\Backend;
use App\Models\Backend;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BackendLoginController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        // $this->middleware('admin_guest')->except('logout');
    }

    public function index()
    {
        return view('backend.login');
    }
    
    public function login(Request $request)
    {
        $request->validate([
            'username' => 'required',
            'password' => 'required'
        ]);
        if(request('username') =="admin" && request('password')=="admin")
        {
            session(['admin_logged_in' => true]);
            return redirect('/admin');
        }
        else{
            flash('Your login credential incorrect, Please enter valid credentials!')->error();
            return back();
        }
    }
    
    public function logout()
    {
        session(['admin_logged_in' => false]);
        return redirect('/admin/login');
    }
}
