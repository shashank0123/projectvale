<?php

namespace App\Http\Controllers\Backend;
use Illuminate\Http\Request;
use App\Models\Backend\PaperType;
use App\Http\Controllers\Controller;

class BackendPaperTypeController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin_login');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $papertypes = PaperType::paginate(10);
        return view('backend.papertypes.index', compact('papertypes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        PaperType::create($request->all());
        flash('Paper type added successfully!');
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PaperType $papertype)
    {
        $papertype->update($request->all());
        flash('Paper type updated successfully!');
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(PaperType $papertype)
    {
        $papertype->delete();
        flash('Paper type deleted successfully!');
        return back();
    }
}
