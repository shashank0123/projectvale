<?php

namespace App\Http\Controllers\Backend;
use App\User;
use App\Models\Project;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BackendProjectsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('admin_login');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $projects = Project::paginate(10);
        return view('backend.projects.index', compact('projects'));
    }

    public function show(Project $project)
    {
        return view('backend.projects.show', compact('project'));
    }

    public function destroy(Project $project)
    {
        $project->projectfiles()->delete();
        $project->chats()->delete();
        $project->feedback()->delete();

        $project->delete();
        flash('Project deleted successfully!')->success();
        return back();
    }
}
