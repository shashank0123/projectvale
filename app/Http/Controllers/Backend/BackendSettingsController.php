<?php

namespace App\Http\Controllers\Backend;
use App\Models\Backend\Setting;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BackendSettingsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('admin_login');
    }

    public function index()
    {
        $settings = Setting::first();
        return view('backend.settings.index', compact('settings'));
    }

    public function store(Request $request)
    {
        $settings = Setting::first();
        if ($settings) {
             $settings->update($request->all());
        }else{
            Setting::create($request->all());
        }

        flash('Settings saved successfully!')->success();
        return back();
    }
}
