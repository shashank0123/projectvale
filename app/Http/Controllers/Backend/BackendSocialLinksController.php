<?php

namespace App\Http\Controllers\Backend;

use App\Models\SocialLink;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
class BackendSocialLinksController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin_login');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sociallinks = SocialLink::first();
        return view('backend.social.index', compact('sociallinks'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $socialLink = SocialLink::first();
        if ($socialLink) {

             $socialLink->update(
                [
                    'facebook' => request('facebook'),
                    'instagram' => request('instagram'),
                    'linkedin' => request('linkedin'),
                    'twitter' => request('twitter'),
                    'googlepluse' => request('googlepluse')
                ]
            );
        }else{
            SocialLink::create(
                [
                    'facebook' => request('facebook'),
                    'instagram' => request('instagram'),
                    'linkedin' => request('linkedin'),
                    'twitter' => request('twitter'),
                    'googlepluse' => request('googlepluse')
                ]
            );
        }

        flash('Social links saved successfully!')->success();
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\SocialLink  $socialLink
     * @return \Illuminate\Http\Response
     */
    public function show(SocialLink $socialLink)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\SocialLink  $socialLink
     * @return \Illuminate\Http\Response
     */
    public function edit(SocialLink $socialLink)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\SocialLink  $socialLink
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SocialLink $socialLink)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\SocialLink  $socialLink
     * @return \Illuminate\Http\Response
     */
    public function destroy(SocialLink $socialLink)
    {
        //
    }
}
