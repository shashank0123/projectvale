<?php

namespace App\Http\Controllers\Backend;
use App\User;
use App\Models\Project;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BackendUsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('admin_login');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::where('be_writer', 0)->get();
        return view('backend.users.index', compact('users'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        $projects = Project::where('user_id', $user->id)->paginate(10);
        return view('backend.users.show', compact('user', 'projects'));
    }

    public function project(User $user, Project $project)
    {
        return view('backend.users.project', compact('user', 'project'));
    }

    public function destroy(User $user)
    {
        Project::where('user_id', $user->id)->delete();
        $user->delete();
        flash('User deleted successfully!');
        return back();
    }

}
