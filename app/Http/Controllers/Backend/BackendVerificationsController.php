<?php

namespace App\Http\Controllers\Backend;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BackendVerificationsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('admin_login');
    }

    public function pending()
    {
        $writers = User::where('be_writer', 1)->where('is_verify', 0)->paginate(10);
        return view('backend.verifications.pending', compact('writers'));
    }

    public function writer(User $writer)
    {
        return view('backend.verifications.writer', compact('writer'));
    }
}
