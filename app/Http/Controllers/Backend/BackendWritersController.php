<?php

namespace App\Http\Controllers\Backend;
use App\User;
use App\Models\Project;
use Illuminate\Http\Request;
use App\Notifications\VerifyWriter;
use App\Http\Controllers\Controller;
use App\Notifications\UnverifyWriter;

class BackendWritersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('admin_login');
    }

    public function index()
    {
        $writers = User::where('be_writer', 1)->paginate(10);
        return view('backend.writers.index', compact('writers'));
    }

    public function pending()
    {
        $writers = User::where('be_writer', 1)->where('is_verify', 0)->paginate(10);
        return view('backend.writers.pending', compact('writers'));
    }

    public function verified()
    {
        $writers = User::where('be_writer', 1)->where('is_verify', 1)->paginate(10);
        return view('backend.writers.verified', compact('writers'));
    }

    public function unverified()
    {
        $writers = User::where('be_writer', 1)->where('is_verify', 2)->paginate(10);
        return view('backend.writers.unverified', compact('writers'));
    }

    public function show(User $writer)
    {
        $projects = Project::where('writer_id', $writer->id)->paginate(10);
        return view('backend.writers.show', compact('writer', 'projects'));
    }

    public function project(User $user, Project $project)
    {
        return view('backend.writers.project', compact('user', 'project'));
    }

    public function verify(Request $request, User $writer)
    {
        $writer->update(['quality' => request('quality'), 'is_verify' => 1, 'is_available' => 1]);
        $message = 'Your writer account on ProjectVala is verified. You can now start using the ProjectVala platform.';
        $response = sendSMS($writer->phone, $message);
        $writer->notify(new VerifyWriter($writer));
        flash('Writer was verified successfully!')->success();
        return redirect('/admin/writers/'.$writer->id.'#pills-rate');

    }

    public function unverify(User $writer)
    {
        $writer->update(['is_verify' => 2]);
        $message = 'We are sorry to inform you that your writer account on ProjectVala has been declined.';
        $response = sendSMS($writer->phone, $message);
        $writer->notify(new UnverifyWriter($writer));
        flash('Writer was unverified successfully!')->success();
        return back();
    }

    public function rate(Request $request, User $writer)
    {
        $rates = implode(",", request('rates'));
        $writer->update(['per_page_rate' => $rates, 'commission_amt' => request('commission_amt')]);
        flash('Writer has rate added successfully!')->success();
        return redirect('/admin/writers/'.$writer->id.'#pills-rate');
    }

    public function destroy(User $writer)
    {
        Project::where('writer_id', $writer->id)->update(['writer_id' => 0]);
        $writer->delete();
        flash('Writer deleted successfully!');
        return back();
    }
}
