<?php

namespace App\Http\Controllers;
use App\User;
use App\Models\Project;
use Illuminate\Http\Request;

class FilesController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'verified', 'writerTest', 'WriterDoc']);
    }

    public function index(){
        if(auth()->user()->be_writer){
            $projects = Project::where('writer_id', auth()->id())->get();
        }else{
            $projects = auth()->user()->projects;
        }
        return view('files.index', compact('projects'));
    }
}
