<?php

namespace App\Http\Controllers;
use App\User;
use App\Models\Project;
use App\Model\Assignment;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(auth()->user()->be_writer)
        {
            $projects = Project::where('writer_id', auth()->id())->paginate(9);
        } else {
            $projects = auth()->user()->projects()->paginate(9);
        }

        if(auth()->user()->be_writer && auth()->user()->is_test && auth()->user()->writerDocument && auth()->user()->is_verify != 1){
            return view('writer.notverify');
        }
        elseif(auth()->user()->be_writer && auth()->user()->is_test && !auth()->user()->writerDocument){
            return view('writer.uploadDocument');
        }
        elseif(auth()->user()->be_writer && auth()->user()->is_test != 1){

            return view('writer.test.index');
        }
        else{
            return view('home', compact('projects'));
        }
    }
}
