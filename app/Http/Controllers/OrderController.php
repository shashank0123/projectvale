<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class OrderController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'verified', 'writerTest', 'WriterDoc']);
    }
    public function index(){
    	return view('order.index');
    }
    public function show(){
    	return view('order.show');
    }
}
