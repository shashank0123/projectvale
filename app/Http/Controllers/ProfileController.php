<?php

namespace App\Http\Controllers;
use File;
use App\User;
use Illuminate\Http\Request;

class ProfileController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'verified', 'writerTest', 'WriterDoc']);
    }

    public function index(){
        // dd(auth()->user());
    	return view('profile.index');
    }

    public function updateProfile(Request $request)
    {
        $this->validate($request, ['fname' => 'required', 'lname' => 'required', 'phone' => 'required']);

        auth()->user()->update([
            'fname' => request('fname'),
            'lname' => request('lname'),
            'phone' => request('phone'),
            'subjects' => array_key_exists("subjects", $request->all()) ? implode(',', request('subjects')) : ''
        ]);

         flash('Your profile information was updated successfully.')->success();

        return back();
    }

    public function updateAvatar(Request $request)
    {
        $data = $request->profile_photo;
        list($type, $data) = explode(';', $data);
        list(, $data)      = explode(',', $data);
        $data = base64_decode($data);
        $image_name= "avatar".time().'.png';
        if(!File::exists(public_path().'/'.'storage/profile_photo/')) {
            File::makeDirectory(public_path().'/'.'storage/profile_photo/',0777,true);
        }
        $path = public_path() . "/storage/profile_photo/" . $image_name;
        $image_path = 'profile_photo/'.$image_name;
        file_put_contents($path, $data);

        auth()->user()->update(['profile_photo' => $image_path]);

        flash('Your profile photo was updated successfully.')->success();

        return response()->json(['success'=>'done']);
    }

    public function updatePassword(Request $request)
    {
        $this->validate($request, [
            'old_password' => 'required',
            'password' => 'required|string|min:8|strong_password|confirmed'
        ]);

        $old_password = $request->get('old_password');
        $password = $request->get('password');

        if(!\Hash::check($old_password, auth()->user()->password))
        {
            flash('Your old password is incorrect!')->error();
            return back();
        }

        auth()->user()->password = bcrypt($password);
        auth()->user()->save();

        flash('Your password was successfully updated!')->success();

        return back();
    }

    public function feedback(){
        return view('profile.feedback');
    }

    public function availability(){
        $availability = auth()->user()->availability;
        return view('profile.availability', compact('availability'));
    }


    public function saveavailability(Request $request)
    {
        if (auth()->user()->availability) {
            auth()->user()->availability()->update(['available_dates' => request('available_dates')]);
            flash('Your availability status was successfully added!')->success();
        }else{
            auth()->user()->availability()->create(['available_dates' => request('available_dates')]);
            flash('Your availability status was successfully updated!')->success();
        }
        return back();
    }

    public function tours(User $user)
    {
        $user->update(['tour' => 1]);
        return response(['status' => 'success'], 200);
    }

    public function wallet(){
        return view('writer.wallet');
    }

    public function security(){
        return view('profile.security');
    }

    public function view(){
        return view('profile.view');
    }
}
