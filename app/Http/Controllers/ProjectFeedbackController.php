<?php

namespace App\Http\Controllers;

use App\User;
use App\Models\Project;
use Illuminate\Http\Request;
use App\Notifications\CompleteProject;
use App\Notifications\SendInvoice;
use Auth;
use PDF;
class ProjectFeedbackController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'verified', 'writerTest', 'WriterDoc']);
    }
    
    public function store(Project $project)
    {
        $project->completed = 1;
        $project->status = 4;
        $project->save();

        $data = request()->all();
        $data['writer_id'] = $project->writer_id;
        if(request('score')){
            $data['rating'] = request('score');
        }else{
            $data['rating'] = 3;
        }
        $project->feedback()->create($data);

        $writer = User::where('id', $project->writer_id)->first();
        $user = Auth::user();

        $userMessage = 'Congratulations! The project that you had posted on ProjectVala was successfully completed.';
        $response = sendSMS($project->user->phone, $userMessage);

        $writerMessage = 'Congratulations! A project you were working on ProjectVala is marked complete by the client.';
        $response = sendSMS($writer->phone, $writerMessage);

        $user->notify(new SendInvoice($project,$user));

        $writer->notify(new CompleteProject($project, $writer));
        flash('Your project was completed successfully!')->success();

        return back();
    }

    public function skip(Project $project)
    {
        $project->completed = 1;
        $project->status = 4;
        $project->save();

        $writer = User::where('id', $project->writer_id)->first();
        $user = Auth::user();

        $userMessage = 'Congratulations! The project that you had posted on ProjectVala was successfully completed.';
        $response = sendSMS($project->user->phone, $userMessage);

        $writerMessage = 'Congratulations! A project you were working on ProjectVala is marked complete by the client.';
        $response = sendSMS($writer->phone, $writerMessage);

        $user->notify(new SendInvoice($user));
        $writer->notify(new CompleteProject($project, $writer));

        flash('Your project was completed successfully!')->success();
        return back();
    }
}
