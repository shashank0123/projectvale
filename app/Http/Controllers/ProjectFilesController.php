<?php
namespace App\Http\Controllers;
use App\User;
use File;
use App\Models\Project;
use App\Models\ProjectFile;
use Illuminate\Http\Request;
use App\Notifications\UploadProjectFile;
use App\Notifications\UploadWriterProjectFile;
 class ProjectFilesController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'verified', 'writerTest', 'WriterDoc']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }
     /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }
     /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Project $project, User $user, Request $request)
    {
        $request->validate([
            'name' => 'required',
            'file' => 'required|file|max:10020390000',
        ]);
        $fileName = time().'.'.$request->file('file')->getClientOriginalExtension();

        $path = \Storage::putFileAs('project_file', $request->file('file'), $fileName, 'public');

        if(auth()->id() == $project->user->id){
            $project->projectfiles()->create(['name' => request('name'), 'description' => request('description'), 'user_id' => $user->id, 'file' => $path, 'is_verify' => 1]);
            $message = 'A new file was uploaded on ProjectVala. Visit your account to check the file.';
            $response = sendSMS($project->writer->phone, $message);
            $project->writer->notify(new UploadWriterProjectFile($project, $project->writer));
        }else{
            $project->projectfiles()->create(['name' => request('name'), 'description' => request('description'), 'user_id' => $user->id, 'file' => $path, 'is_verify' => 1]);
            /*$message = 'A new file was uploaded on ProjectVala. Visit your account to check the file.';
            $response = sendSMS($project->user->phone, $message);*/
            $project->user->notify(new UploadProjectFile($project, $project->user));
        }
        flash('Your file was uploaded to the project!')->success();
        return redirect('/projects/'.$project->id.'#pills-files');
    }
     /**
     * Display the specified resource.
     *
     * @param  \App\Models\ProjectFile  $projectFile
     * @return \Illuminate\Http\Response
     */
    public function show(ProjectFile $projectFile)
    {
        //
    }
     /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ProjectFile  $projectFile
     * @return \Illuminate\Http\Response
     */
    public function edit(ProjectFile $projectFile)
    {
        //
    }
     /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ProjectFile  $projectFile
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ProjectFile $projectFile)
    {
        //
    }
     /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ProjectFile  $projectFile
     * @return \Illuminate\Http\Response
     */
    public function destroy(ProjectFile $file, Project $project)
    {
        $file->delete();
        flash('The file was deleted successfully!')->success();

        return redirect('/projects/'.$project->id.'#pills-files');
    }
}