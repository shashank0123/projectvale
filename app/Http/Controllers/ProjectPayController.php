<?php

namespace App\Http\Controllers;

use App\User;
use App\Models\Project;
use Illuminate\Http\Request;
use App\Notifications\HireWriter;
use App\Notifications\PostProject;
use App\Notifications\ProjectPaymentDone;
class ProjectPayController extends Controller
{

    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Project $project, User $writer)
    {
        return view('projects.payment', compact('project', 'writer'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Project $project, User $writer)
    {
        $amount = request('amount')/100;

        $project->update(['writer_id' => $writer->id,'status' => 3, 'payment_status' => 1]);
        $writerMessage = 'You have received a new project on ProjectVala. Visit your account to start working on it.';
        $response = sendSMS($writer->phone, $writerMessage);

        $userMessage = 'Thank you for making the payment on ProjectVala. Please visit your account to stay updated.';
        $response = sendSMS($project->user->phone, $userMessage);

        $project->user->notify(new ProjectPaymentDone($project, $project->user, $amount));
        $writer->notify(new HireWriter($project, $writer));
        flash('You have hired a writer for project ' . $project->topic . '!')->success();
        return response(["success"], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function turniton(Project $project)
    {
        $project->update(['turniton' => 1]);
        return response(["success"], 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
