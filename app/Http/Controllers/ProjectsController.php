<?php

namespace App\Http\Controllers;
use App\User;
use App\Models\Project;
use App\Model\Assignment;
use Illuminate\Http\Request;
use App\Notifications\HireWriter;
use App\Notifications\PostProject;
use App\Notifications\ProjectChanges;
use App\Notifications\AcceptHireWriter;
use App\Notifications\UserRevokeWriter;
use App\Notifications\AcceptHireRequest;
use App\Notifications\DeclineHireWriter;
use App\Notifications\RequestHireWriter;
use App\Notifications\DeclineHireRequest;
use App\Notifications\WriterRevokeProject;
use App\Notifications\CancelRequestHireWriter;

class ProjectsController extends Controller
{

    public function __construct()
    {
        $this->middleware(['auth', 'verified', 'writerTest', 'WriterDoc']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('projects.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       // dd($request->all());
        $request->validate([
            'type' => 'required',
            'topic' => 'required',
            'subject' => 'required',
            'pages_count' => 'required',
            'deadline_date' => 'required',
            'deadline_time' => 'required',
            // 'type_of_service' => 'required',
            // 'writing_quality' => 'required',
            'citations_count' => 'required',
            'citation_format' => 'required',
            'instructions' => 'required'
        ]);

        $paths = [];

        $files = $request->file('file');

        if($request->hasFile('file'))
        {
            foreach ($files as $file) {
                $paths[] = $file->store('/assignments/files');
            }
        }

     //   dd($paths);

      /*  if(isset($request->file)){
            $fileName = "assignmentFile".time().'.'.request()->file->getClientOriginalExtension();
            $request->file->storeAs('file',$fileName);
            $path = \Storage::putFileAs('assignment_file', $request->file('file'), $fileName, 'public');
        }*/

        $newData = $request->all();
        $newData['files'] = $paths;
        $writer = getAvailableWriters($newData['subject']);
        $newData['writer_id'] = $writer->id;
        $newData['status'] = '2';
        $project = auth()->user()->projects()->create($newData);

        $message = 'You have posted a new project. Please visit your account to stay updated.';
        $response = sendSMS(auth()->user()->phone, $message);
        // auth()->user()->notify(new PostProject($project, auth()->user()));

        //flash()->overlay("Explore all our available writers and choose a perfect fit for your project.", 'Your project is posted!');
        
        return redirect('/projects/' . $project->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function show(Project $project)
    {
        // dd($project);
        if($project->user_id == auth()->id() || $project->writer_id == auth()->id())
        {
            // $writers = User::where('id', '!=', auth()->user()->id)->where('be_writer', 1)->where('is_verify', 1)->where('is_available', 1)->where('quality', $project->writing_quality)->get();
            $writers = User::where('id', '!=', auth()->user()->id)->where('be_writer', 1)->where('is_verify', 1)->get();

            $history = $project->user->writer_history;
            return view('projects.show', compact('project','writers','history'));
        } else {
            return redirect('/home');
        }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function edit(Project $project)
    {
        //
        return view('projects.edit', compact('project'));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Project $project)
    {
        if(!$project->status){
            $request->validate([
                'type' => 'required',
                'topic' => 'required', 
                'pages_count' => 'required',
                'deadline_date' => 'required',
                'deadline_time' => 'required',
                'citations_count' => 'required',
                'citation_format' => 'required',
                'instructions' => 'required'
            ]);
        }
        if(isset($request->file)){
            $fileName = "assignmentFile".time().'.'.request()->file->getClientOriginalExtension();
            $request->file->storeAs('file',$fileName);
            $path = \Storage::putFileAs('assignment_file', $request->file('file'), $fileName, 'public');
            $data = $request->all();
            $data['file'] = $path;
            $project->update($data);
            if($project->status == 3){
                $message = 'The user has updated instructions on ProjectVala. Visit your account to know the changes.';
                $response = sendSMS($project->writer->phone, $message);
                $project->writer->notify(new ProjectChanges($project, $project->writer));
            }
        }else{
            $project->update($request->all());
            if($project->status == 3){
                $message = 'The user has updated instructions on ProjectVala. Visit your account to know the changes.';
                $response = sendSMS($project->writer->phone, $message);
                $project->writer->notify(new ProjectChanges($project, $project->writer));
            }
        }
        flash('Your project was updated successfully!')->success();
        return redirect('/projects/' . $project->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function destroy(Project $project)
    {
        $project->projectfiles()->delete();
        $project->chats()->delete();
        $project->feedback()->delete();

        $project->delete();
        flash('Your project was deleted!')->success();
        return redirect('/home');
    }

    public function hire(Project $project, User $writer)
    {
        $project->update(['writer_id' => $writer->id,'status' => 1]);
        $message = 'You have received a new project request on ProjectVala. Visit your account to respond.';
        $response = sendSMS($writer->phone, $message);
        $writer->notify(new RequestHireWriter($project, $writer));
        flash('Your request to hire the writer has been sent successfully, Please wait for the response!')->success();
        return redirect('/projects/'.$project->id);

        /*$dates=[];
        if(isset($writer->availability)){
            $availability = str_replace(' ', '', $writer->availability->available_dates);
            $dates = explode(',' , $availability);

            if (in_array(date('Y-m-d'), $dates)) {
                $project->update(['writer_id' => $writer->id,'status' => 1]);
                $message = 'You have received a new project request on ProjectVala. Visit your account to respond.';
                $response = sendSMS($writer->phone, $message);
                $writer->notify(new RequestHireWriter($project, $writer));
                flash('Your hire writer request send successfully, Please wait for response!')->success();
                return redirect('/projects/'.$project->id);
            }else{
                $project->update(['status' => 2]);
                return redirect('/projects/'.$project->id.'/'.$writer->id.'/payment');
            }
        }else{
            $project->update(['status' => 2]);
            return redirect('/projects/'.$project->id.'/'.$writer->id.'/payment');
        }*/

    }

    public function accept(Project $project, User $writer)
    {
        $project->update(['status' => 2]);
        // $writer->notify(new HireWriter($project, $writer));
        $message = 'The writer has accepted your project. Please visit your account to make the payment.';
        $response = sendSMS($project->user->phone, $message);
        $project->user->notify(new AcceptHireWriter($project, $project->user));
        $writer->notify(new AcceptHireRequest($project, $writer));
        flash('You have accepted the request for project ' . $project->topic . '!')->success();
        return redirect('/projects/'.$project->id);
    }

    public function decline(Project $project, User $writer)
    {
        $message = 'We are sorry to inform you that the writer has rejected your project. Please visit your account and find another writer for your project.';
        $response = sendSMS($project->user->phone, $message);
        $project->user->notify(new DeclineHireWriter($project, $project->user));
        $writer->notify(new DeclineHireRequest($project, $writer));
        $project->update(['writer_id' => 0, 'status' => 0]);
        flash('You have declined the request for project successfully.')->warning();
        return redirect('/home');
    }


    public function cancelRequest(Project $project, User $writer)
    {
        $project->update(['writer_id' => 0, 'status' => 0]);
        // $writer->notify(new HireWriter($project, $writer));
        $writer->notify(new CancelRequestHireWriter($project, $writer));
        flash('You have cancelled the hire request for project ' . $project->topic . '!')->success();
        return redirect('/projects/'.$project->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function revoke(Project $project)
    {
        // dd($project);
        if(auth()->id() == $project->user_id){
            $writer = User::where('id', $project->writer_id)->first();
            $writer->notify(new UserRevokeWriter($project, $writer));
        }else{
            $user = User::where('id', $project->user_id)->first();
            $user->notify(new WriterRevokeProject($project, $user));
        }
        $project->update(['writer_id' => 0]);
        /*$project->projectfiles()->delete();
        $project->chats()->delete();*/
        flash('The project was revoked successfully')->warning();
        return redirect('/projects/'.$project->id);

    }

    public function demo(){
        return view('projects.demo');
    }

    public function maildemo(){
        return view('emails.demo');
    }

}
