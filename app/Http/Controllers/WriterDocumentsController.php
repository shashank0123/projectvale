<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\WriterDocument;
use App\Notifications\WriterSubmitDocuments;

class WriterDocumentsController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'verified', 'writerTest']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('writer.uploadDocument');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'aadhar_card' => 'file|max:20480',
            'pan_card' => 'file|max:20480',
            'tenth_marksheet' => 'file|max:20480',
            'twelfth_marksheet' => 'file|max:20480',
            'degree_marksheet' => 'file|max:20480',
        ]);

        $aadharCardPath = '';
        $panCardPath = '';
        $tenthMarksheetPath = '';
        $twelfthMarksheetPath = '';
        $degreeMarksheetPath = '';
        $collegeTranscriptPath = '';

        if(request('aadhar_card')){
            $aadharCard = 'aadhar_card_'.auth()->id().time().'.'.$request->file('aadhar_card')->getClientOriginalExtension();
            $aadharCardPath = \Storage::putFileAs('writerDocuments', $request->file('aadhar_card'), $aadharCard, 'public');
        }
        if(request('pan_card')){
            $panCard = 'pan_card_'.auth()->id().time().'.'.$request->file('pan_card')->getClientOriginalExtension();
            $panCardPath = \Storage::putFileAs('writerDocuments', $request->file('pan_card'), $panCard, 'public');
        }
        if(request('tenth_marksheet')){
            $tenthMarksheet = 'tenth_marksheet_'.auth()->id().time().'.'.$request->file('tenth_marksheet')->getClientOriginalExtension();
            $tenthMarksheetPath = \Storage::putFileAs('writerDocuments', $request->file('tenth_marksheet'), $tenthMarksheet, 'public');
        }
        if(request('twelfth_marksheet')){
            $twelfthMarksheet = 'twelfth_marksheet_'.auth()->id().time().'.'.$request->file('twelfth_marksheet')->getClientOriginalExtension();
            $twelfthMarksheetPath = \Storage::putFileAs('writerDocuments', $request->file('twelfth_marksheet'), $twelfthMarksheet, 'public');
        }
        if(request('degree_marksheet')){
            $degreeMarksheet = 'degree_marksheet_'.auth()->id().time().'.'.$request->file('degree_marksheet')->getClientOriginalExtension();
            $degreeMarksheetPath = \Storage::putFileAs('writerDocuments', $request->file('degree_marksheet'), $degreeMarksheet, 'public');
        }
        if(request('college_transcript')){
            $collegeTranscript = 'college_transcript_'.auth()->id().time().'.'.$request->file('college_transcript')->getClientOriginalExtension();
            $collegeTranscriptPath = \Storage::putFileAs('writerDocuments', $request->file('college_transcript'), $collegeTranscript, 'public');
        }
        auth()->user()->writerDocument()->create([
            'aadhar_card' => $aadharCardPath,
            'pan_card' => $panCardPath,
            'tenth_marksheet' => $tenthMarksheetPath,
            'twelfth_marksheet' => $twelfthMarksheetPath,
            'degree_marksheet' => $degreeMarksheetPath,
            'college_transcript' => $collegeTranscriptPath
        ]);
        auth()->user()->notify(new WriterSubmitDocuments(auth()->user()));

        flash('Your documents were uploaded successfully!')->success();
        return redirect('/home');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\WriterDocument  $writerDocument
     * @return \Illuminate\Http\Response
     */
    public function show(WriterDocument $writerDocument)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\WriterDocument  $writerDocument
     * @return \Illuminate\Http\Response
     */
    public function edit(WriterDocument $writerDocument)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\WriterDocument  $writerDocument
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, WriterDocument $writerDocument)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\WriterDocument  $writerDocument
     * @return \Illuminate\Http\Response
     */
    public function destroy(WriterDocument $writerDocument)
    {
        //
    }
}
