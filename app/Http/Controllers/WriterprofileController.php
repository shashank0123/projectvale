<?php

namespace App\Http\Controllers;
use App\Models\Project;
use Illuminate\Http\Request;
use App\Models\ProjectFeedback;

class WriterprofileController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'verified', 'writerTest', 'WriterDoc']);
    }
    public function index(){
    	return view('writer.profile.index');
    }

    public function security(){
    	return view('writer.profile.security');
    }

    public function feedback(){
        $feedback = ProjectFeedback::where('writer_id', auth()->id())->get();
    	return view('writer.profile.feedback', compact('feedback'));
    }

    public function wallet(){
        $projects = Project::where('writer_id', auth()->id())->paginate(10);
        return view('writer.wallet', compact('projects'));
    }
}
