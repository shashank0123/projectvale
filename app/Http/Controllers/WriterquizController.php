<?php

namespace App\Http\Controllers;
use App\User;
use Illuminate\Http\Request;
use App\Notifications\WriterRegistration;

class WriterquizController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
    }

    public function index(){
    	return view('writer.test.index');
    }

    public function store(Request $request){
        $score = 0;
        $quality = 'failed';
        $is_test = 0;
        if(request('q1')=='1'){
            $score = $score + 20;
        }
        if(request('q2')=='4'){
            $score = $score + 20;
        }
        if(request('q3')=='2'){
            $score = $score + 20;
        }
        if(request('q4')=='4'){
            $score = $score + 20;
        }
        if(request('q5')=='3'){
            $score = $score + 20;
        }

        if($score >= 40){
            // $quality = 'Standard';
            $is_test = 1;
        }
        // dd($quality);
        auth()->user()->update(['score' => $score, 'quality' => $quality, 'is_test' => $is_test]);

        if(auth()->user()->is_test){
            auth()->user()->notify(new WriterRegistration(auth()->user()));
            flash()->overlay("You are passed writer test, upload your documents for writer account verification.", 'Congratulations!');
            return redirect('/writer/document');
        }else{
            flash()->overlay("You did not score enough marks to be an writer.", 'Sorry! You are not verified.');
            return redirect('/writer/notverify');
        }
    }
}
