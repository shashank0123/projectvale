<?php

namespace App\Http\Controllers;

use App\User;
use App\Models\Project;
use Illuminate\Http\Request;
use App\Models\ProjectFeedback;

class WritersController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'verified', 'writerTest', 'WriterDoc'])->except('notverify');
    }
    public function index(){
        $users = User::where('be_writer', 0)->get();
    	return view('writer.index', compact('users'));
    }

    public function details(User $user){
    	return view('writer.details', compact('user'));
    }

    public function profile(Project $project, User $writer){
        $history = $writer->history;
        $feedback = ProjectFeedback::where('writer_id', $writer->id)->get();
    	return view('writer.profile', compact('project','writer','feedback','history'));
    }

    public function notverify(){
        return view('writer.notverify');
    }
}
