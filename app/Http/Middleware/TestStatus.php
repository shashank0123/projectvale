<?php

namespace App\Http\Middleware;

use Closure;

class TestStatus
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(auth()->user()->be_writer && auth()->user()->is_test != 1)
        {
            return redirect('/home');
        }else{
            return $next($request);
        }
    }
}
