<?php

namespace App\Http\Middleware;

use Closure;

class WriterDocuments
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(auth()->user()->be_writer && auth()->user()->is_test && auth()->user()->is_verify != 1){
            return redirect('/home');
        }else{
            return $next($request);
        }
    }
}
