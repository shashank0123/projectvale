<?php

namespace App\Mail;
use App\User;
use App\Models\Project;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class Cancel_Request_Hire_Writer extends Mailable
{
    use Queueable, SerializesModels;
    protected $project;
    protected $writer;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Project $project, User $writer)
    {
        $this->project = $project;
        $this->writer = $writer;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject("Sorry! Project owner cancelled request for project")
            ->view('emails.cancel_request_hire_writer', ['project' => $this->project, 'writer' => $this->writer]);
    }
}
