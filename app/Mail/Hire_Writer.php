<?php

namespace App\Mail;
use App\User;
use App\Models\Project;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class Hire_Writer extends Mailable
{
    use Queueable, SerializesModels;
    protected $project;
    protected $writer;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Project $project, User $writer)
    {
        $this->project = $project;
        $this->writer = $writer;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject("You have a new request for a project!")
            ->view('emails.hire_writer', ['project' => $this->project, 'writer' => $this->writer]);
    }
}
