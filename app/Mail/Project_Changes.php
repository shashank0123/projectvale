<?php

namespace App\Mail;
use App\User;
use App\Models\Project;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class Project_Changes extends Mailable
{
    use Queueable, SerializesModels;
    protected $project;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Project $project)
    {
        $this->project = $project;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject("Project instruction updated!")
            ->view('emails.project_changes', ['project' => $this->project]);
    }
}
