<?php

namespace App\Mail;
use App\User;
use App\Models\Project;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class Project_Payment_Done extends Mailable
{
    use Queueable, SerializesModels;
    protected $project;
    protected $user;
    protected $amount;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Project $project, User $user, $amount)
    {
        $this->project = $project;
        $this->user = $user;
        $this->amount = $amount;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject("Your project has been posted successfully!")
            ->view('emails.project_payment_done', ['project' => $this->project, 'user' => $this->user, 'amount' => $this->amount]);
    }
}
