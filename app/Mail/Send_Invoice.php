<?php

namespace App\Mail;
use App\User;
use App\Models\Project;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use PDF;

class Send_Invoice extends Mailable
{
    use Queueable, SerializesModels;
    protected $project;
    protected $user;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Project $project,User $user)
    {
        $this->project = $project;
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $project = $this->project;
        $pdf = PDF::loadView('emails.inv',compact('project'));
        return $this->subject("Invoice of your project.")
            ->view('emails.send_invoice', ['user' => $this->user])
            ->attachData($pdf->output(), "invoice.pdf");
    }
}
