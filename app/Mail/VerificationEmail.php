<?php

namespace App\Mail;

use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class VerificationEmail extends Mailable
{
    use Queueable, SerializesModels;
    protected $url;
    protected $user;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($url, User $user)
    {
        $this->url = $url;
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject("Verify Email Address.")
            ->view('emails.verification', ['url' => $this->url, 'user' => $this->user]);
    }
}
