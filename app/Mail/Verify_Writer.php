<?php

namespace App\Mail;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class Verify_Writer extends Mailable
{
    use Queueable, SerializesModels;
    protected $writer;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $writer)
    {
        $this->writer = $writer;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject("Your writer account is Verified.")
            ->view('emails.verify_writer', ['writer' => $this->writer]);
    }
}
