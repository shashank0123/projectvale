<?php

namespace App\Model;
use App\User;
use Illuminate\Database\Eloquent\Model;

class Assignment extends Model
{
    protected $fillable = ['type','topic','subject','pages_count','deadline_date','deadline_time','type_of_service','writing_quality','citations_count','citation_format','instructions','file'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
