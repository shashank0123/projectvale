<?php

namespace App\Model;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Wallet extends Model
{
    protected $fillable = ['balance'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
