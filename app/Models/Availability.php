<?php

namespace App\Models;
use App\User;
use Illuminate\Database\Eloquent\Model;

class Availability extends Model
{
    protected $fillable = ['available_dates'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

}
