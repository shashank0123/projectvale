<?php

namespace App\Models\Backend;

use Illuminate\Database\Eloquent\Model;

class Citation extends Model
{
    protected $fillable = ['name'];
}
