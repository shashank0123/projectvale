<?php

namespace App\Models\Backend;

use Illuminate\Database\Eloquent\Model;

class Duration extends Model
{
    protected $fillable = [
        'duration_name',
        '20_days',
        '14_days',
        '10_days',
        '7_days',
        '5_days',
        '4_days',
        '3_days',
        '48_hours',
        '24_hours',
        '12_hours',
        '6_hours',
        '3_hours',
        '2_hours',
        '1_hours'
    ];
}
