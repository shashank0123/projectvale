<?php

namespace App\Models\Backend;

use Illuminate\Database\Eloquent\Model;

class PaperType extends Model
{
    protected $fillable = ['name'];
}
