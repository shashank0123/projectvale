<?php

namespace App\Models\Backend;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    protected $fillable = ['plagiarism_amt'];

}
