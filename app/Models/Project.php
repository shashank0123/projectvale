<?php

namespace App\Models;
use App\User;
use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    protected $fillable = ['writer_id','type','topic','subject','pages_count','deadline_date','deadline_time','type_of_service','writing_quality','citations_count','citation_format','instructions','additional_instructions','files', 'completed', 'status', 'payment_status', 'admin_payment_status', 'truniton'];

    protected $appends = ['file_url', 'ongoing_file_url', 'feedback'];

    protected $casts = [
        'files' => 'array',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
    public function writer()
    {
        return $this->belongsTo(User::class, 'writer_id');
    }
    public function getIsOwnedAttribute()
    {
        return $this->user_id == auth()->id() ? 1 : 0;
    }

    public function getTimestampAttribute()
    {
        return \Carbon\Carbon::createFromFormat('Y-m-d h:i a', $this->deadline_date . ' ' . $this->deadline_time);
    }


    public function getProjectStatusAttribute()
    {
        if($this->writer_id == 0)
        {
            return 'Posted';
        }
        elseif($this->status == 4) {
            return 'Completed';
        }
        elseif($this->status == 2) {
            return 'Pay';
        }
        elseif($this->status == 3) {
            return 'Ongoing';
        }
        elseif($this->user_id == auth()->id() && $this->status == 1) {
            return 'Requested';
        }
        elseif($this->writer_id == auth()->id() && $this->status == 1) {
            return 'New Request';
        }
    }

    public function projectfiles()
    {
        if(auth()->id() == $this->writer_id){
            return $this->hasMany(ProjectFile::class)->latest();
        }else{
            return $this->hasMany(ProjectFile::class)->where('is_verify', 1)->latest();
        }
    }

    public function projectAdminFiles()
    {
        return $this->hasMany(ProjectFile::class)->where('user_id', $this->writer_id)->latest();
    }

    public function projectWriterFiles()
    {
        if(auth()->id() == $this->writer_id){
            return $this->hasMany(ProjectFile::class)->where('user_id', $this->writer_id)->latest();
        }else{
            return $this->hasMany(ProjectFile::class)->where('user_id', $this->writer_id)->where('is_verify', 1)->latest();
        }
    }

    public function projectUserFiles()
    {
        return $this->hasMany(ProjectFile::class)->where('user_id', $this->user_id)->where('is_verify', 1)->latest();
    }

    /*public function getOwnerUserNameAttribute()
    {
        return $this->user->username;
    }

    public function getWriterUserNameAttribute()
    {
        return $this->writer->username;
    }*/

    public function getFileUrlAttribute()
    {
        $files =[];
        if(isset($this->files)){
            foreach ($this->files as $key => $file) {
                $files[$key] = \Storage::url($file);
            }
        }
        return $files;
    }
    public function getOngoingFileUrlAttribute()
    {
        $files = $this->projectfiles;
    }

    public function chats()
    {
        return $this->hasMany(ProjectChat::class);
    }

    public function feedback()
    {
        return $this->hasOne(ProjectFeedback::class);
    }

    public function getFeedbackAttribute()
    {
        return $this->hasOne(ProjectFeedback::class)->first();
    }

}
