<?php
 namespace App\Models;
 use App\User;
 use App\Models\Project;
 use Illuminate\Database\Eloquent\Model;

 class ProjectChat extends Model
{
    protected $fillable = ['message','sender','receiver'];
    public function project()
    {
        return $this->belongsTo(Project::class);
    }
}