<?php

namespace App\Models;
use App\Models\Project;
use App\User;
use Illuminate\Database\Eloquent\Model;

class ProjectFeedback extends Model
{
    protected $fillable = ['onTime', 'fullyComplete', 'rating', 'review', 'writer_id'];

    public function project()
    {
        return $this->belongsTo(Project::class);
    }
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}

