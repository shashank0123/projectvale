<?php
 namespace App\Models;
use App\User;
use App\Models\Project;
use Illuminate\Database\Eloquent\Model;
 class ProjectFile extends Model
{
    protected $fillable = ['name','description','user_id','file','is_verify','plagiarism_report', 'unverify_reason'];

    protected $appends = ['file_url'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
    public function project()
    {
        return $this->belongsTo(Project::class);
    }

    public function getFileUrlAttribute()
    {
        return \Storage::url($this->file);
    }
}