<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class WriterDocument extends Model
{
    protected $fillable = ['aadhar_card','pan_card','tenth_marksheet','twelfth_marksheet','degree_marksheet','college_transcript'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
