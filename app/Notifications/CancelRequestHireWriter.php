<?php

namespace App\Notifications;
use App\User;
use App\Models\Project;
use Illuminate\Bus\Queueable;
use App\Mail\Cancel_Request_Hire_Writer;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class CancelRequestHireWriter extends Notification implements ShouldQueue
{
    use Queueable;
    protected $project;
    protected $writer;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Project $project, User $writer)
    {
        $this->project = $project;
        $this->writer = $writer;
    }


    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail', 'database', 'broadcast'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new Cancel_Request_Hire_Writer($this->project, $this->writer))->to($this->writer->email);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'id' => $this->id,
            'read_at' => null,
            'data' => [
                'user_id'  => $this->writer->id,
                'project_id' => $this->project->id,
                'message' => 'Sorry! Project owner cancelled request for project <b>'. $this->project->topic .'</b>.',
                'icon' => '../img/project-icon.png',
                'type' => 'project'
            ]
        ];
    }

    public function toDatabase($notifiable)
    {
        return [
            'id' => $this->id,
            'read_at' => null,
            'user_id'  => $this->writer->id,
            'project_id' => $this->project->id,
            'message' => 'Sorry! Project owner cancelled request for project <b>'. $this->project->topic .'</b>.',
            'icon' => '../img/project-icon.png',
            'type' => 'project'
        ];
    }
}