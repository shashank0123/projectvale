<?php

namespace App\Notifications;
use App\User;
use App\Models\Project;
use App\Mail\Decline_Hire_Writer;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class DeclineHireWriter extends Notification implements ShouldQueue
{
    use Queueable;
    protected $project;
    protected $user;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Project $project, User $user)
    {
        $this->project = $project;
        $this->user = $user;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail', 'database', 'broadcast'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new Decline_Hire_Writer($this->project, $this->user))->to($this->user->email);
        /*return (new MailMessage)
                    ->line('The introduction to the notification.')
                    ->action('Notification Action', url('/'))
                    ->line('Thank you for using our application!');*/
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'id' => $this->id,
            'read_at' => null,
            'data' => [
                'user_id'  => $this->user->id,
                'project_id' => $this->project->id,
                'message' => 'Writer declined request for project <b>'. $this->project->topic .'</b>.',
                'icon' => '../img/project-icon.png',
                'link' => '/projects/'.$this->project->id,
                'type' => 'project'
            ]
        ];
    }

    public function toDatabase($notifiable)
    {
        return [
            'id' => $this->id,
            'read_at' => null,
            'user_id'  => $this->user->id,
            'project_id' => $this->project->id,
            'message' => 'Writer declined request for project <b>'. $this->project->topic .'</b>.',
            'icon' => '../img/project-icon.png',
            'link' => '/projects/'.$this->project->id,
            'type' => 'project'
        ];
    }
}
