<?php

namespace App\Notifications;

use App\Models\ProjectChat;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class NewMessage extends Notification implements ShouldQueue
{
    use Queueable;
    protected $message;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(ProjectChat $message)
    {
        $this->message = $message;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['broadcast', 'database'];
    }



    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'id' => $this->id,
            'read_at' => null,
            'data' => [
                'message' => $this->message,
                'pid' => $this->message->project_id
            ]
        ];
    }

    public function toDatabase($notifiable)
    {
        return [
                'message' => $this->message,
                'pid' => $this->message->project_id

        ];
    }
}
