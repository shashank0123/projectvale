<?php

namespace App\Notifications;
use App\User;
use App\Models\Project;
use App\Mail\Post_Project;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class PostProject extends Notification implements ShouldQueue
{
    use Queueable;
    protected $project;
    protected $user;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Project $project, User $user)
    {
        $this->project = $project;
        $this->user = $user;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail', 'database', 'broadcast'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new Post_Project($this->project, $this->user))->to($this->user->email);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'id' => $this->id,
            'read_at' => null,
            'data' => [
                'user_id'  => $this->user->id,
                'project_id' => $this->project->id,
                'message' => '<b>'. $this->project->topic .'</b> project has been posted.',
                'icon' => '../img/project-icon.png',
                'link' => '/projects/'.$this->project->id,
                'type' => 'project'
            ]
        ];
    }

    public function toDatabase($notifiable)
    {
        return [
            'id' => $this->id,
            'read_at' => null,
            'user_id'  => $this->user->id,
            'project_id' => $this->project->id,
            'message' => '<b>'. $this->project->topic .'</b> project has been posted.',
            'icon' => '../img/project-icon.png',
            'link' => '/projects/'.$this->project->id,
            'type' => 'project'
        ];
    }
}
