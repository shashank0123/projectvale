<?php

namespace App\Notifications;
use App\User;
use App\Models\Project;
use App\Mail\Project_Changes;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class ProjectChanges extends Notification implements ShouldQueue
{
    use Queueable;
    protected $project;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Project $project)
    {
        $this->project = $project;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail', 'database', 'broadcast'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new Project_Changes($this->project))->to($this->project->writer->email);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'id' => $this->id,
            'read_at' => null,
            'data' => [
                'user_id'  => $this->project->writer->id,
                'project_id' => $this->project->id,
                'message' => 'Project owner made changes in <b>'. $this->project->topic .'</b>.',
                'icon' => '../img/project-icon.png',
                'link' => '/projects/'.$this->project->id,
                'type' => 'project'
            ]
        ];
    }

    public function toDatabase($notifiable)
    {
        return [
            'id' => $this->id,
            'read_at' => null,
            'user_id'  => $this->project->writer->id,
            'project_id' => $this->project->id,
            'message' => 'Project owner made changes in <b>'. $this->project->topic .'</b>.',
            'icon' => '../img/project-icon.png',
            'link' => '/projects/'.$this->project->id,
            'type' => 'project'
        ];
    }
}