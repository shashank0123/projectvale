<?php

namespace App\Notifications;
use App\User;
use App\Models\Project;
use Illuminate\Bus\Queueable;
use App\Mail\Project_Payment_Done;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class ProjectPaymentDone extends Notification implements ShouldQueue
{
    use Queueable;
    protected $project;
    protected $user;
    protected $amount;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Project $project, User $user, $amount)
    {
        $this->project = $project;
        $this->user = $user;
        $this->amount = $amount;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail', 'database', 'broadcast'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new Project_Payment_Done($this->project, $this->user, $this->amount))->to($this->user->email);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'id' => $this->id,
            'read_at' => null,
            'data' => [
                'user_id'  => $this->user->id,
                'project_id' => $this->project->id,
                'message' => 'Payment done for project <b>'. $this->project->topic .'</b>.',
                'icon' => '../img/project-icon.png',
                'link' => '/projects/'.$this->project->id,
                'type' => 'project'
            ]
        ];
    }

    public function toDatabase($notifiable)
    {
        return [
            'id' => $this->id,
            'read_at' => null,
            'user_id'  => $this->user->id,
            'project_id' => $this->project->id,
            'message' => 'Payment done for project <b>'. $this->project->topic .'</b>.',
            'icon' => '../img/project-icon.png',
            'link' => '/projects/'.$this->project->id,
            'type' => 'project'
        ];
    }
}