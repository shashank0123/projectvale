<?php

namespace App\Notifications;
use App\User;
use App\Models\Project;
use App\Mail\Request_Hire_Writer;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class RequestHireWriter extends Notification implements ShouldQueue
{
    use Queueable;
    protected $project;
    protected $writer;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Project $project, User $writer)
    {
        $this->project = $project;
        $this->writer = $writer;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail', 'database', 'broadcast'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new Request_Hire_Writer($this->project, $this->writer))->to($this->writer->email);
        /*return (new MailMessage)
                    ->line('The introduction to the notification.')
                    ->action('Notification Action', url('/'))
                    ->line('Thank you for using our application!');*/
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'id' => $this->id,
            'read_at' => null,
            'data' => [
                'user_id'  => $this->writer->id,
                'project_id' => $this->project->id,
                'message' => 'You have new request for project <b>'. $this->project->topic .'</b>.',
                'icon' => '../img/project-icon.png',
                'link' => '/projects/'.$this->project->id,
                'type' => 'project'
            ]
        ];
    }

    public function toDatabase($notifiable)
    {
        return [
            'id' => $this->id,
            'read_at' => null,
            'user_id'  => $this->writer->id,
            'project_id' => $this->project->id,
            'message' => 'You have new request for project <b>'. $this->project->topic .'</b>.',
            'icon' => '../img/project-icon.png',
            'link' => '/projects/'.$this->project->id,
            'type' => 'project'
        ];
    }
}
