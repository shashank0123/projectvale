<?php

namespace App\Notifications;
use App\User;
use App\Mail\Verify_Writer;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class VerifyWriter extends Notification
{
    use Queueable;
    protected $writer;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(User $writer)
    {
        $this->writer = $writer;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail', 'database', 'broadcast'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new Verify_Writer($this->writer))->to($this->writer->email);
        /*return (new MailMessage)
                    ->line('The introduction to the notification.')
                    ->action('Notification Action', url('/'))
                    ->line('Thank you for using our application!');*/
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'id' => $this->id,
            'read_at' => null,
            'data' => [
                'user_id'  => $this->writer->id,
                'message' => 'Admin verify your writer account.',
                'icon' => '../img/users.png',
                'link' => '/home',
                'type' => 'user'
            ]
        ];
    }

    public function toDatabase($notifiable)
    {
        return [
            'id' => $this->id,
            'read_at' => null,
            'user_id'  => $this->writer->id,
            'message' => 'Admin verify your writer account.',
            'icon' => '../img/users.png',
            'link' => '/home',
            'type' => 'user'
        ];
    }
}
