<?php

namespace App\Notifications;
use App\User;
use Illuminate\Bus\Queueable;
use App\Mail\Writer_Submit_Documents;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class WriterSubmitDocuments extends Notification implements ShouldQueue
{
    use Queueable;
    protected $writer;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(User $writer)
    {
        $this->writer = $writer;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail', 'database', 'broadcast'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new Writer_Submit_Documents($this->writer))->to($this->writer->email);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'id' => $this->id,
            'read_at' => null,
            'data' => [
                'expert_id'  => $this->writer->id,
                'message' => 'You have successfully submitted your writer profile for verification.',
                'icon' => '../img/user.png',
                'link' => '/home',
                'type' => 'writer'
            ]
        ];
    }
    public function toDatabase($notifiable)
    {
        return [
            'id' => $this->id,
            'read_at' => null,
            'expert_id'  => $this->writer->id,
            'message' => 'You have successfully submitted your writer profile for verification.',
            'icon' => '../img/user.png',
            'link' => '/home',
            'type' => 'writer'
        ];
    }
}
