<?php

namespace App;

use App\Model\Wallet;
use App\Models\Project;
use App\Model\Assignment;
use App\Models\Availability;
use App\Models\WriterDocument;
use App\Models\Backend\Setting;
use App\Models\ProjectFeedback;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Password;
use App\Notifications\VerifyEmailNotification;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
// use Illuminate\Auth\Notifications\VerifyEmailNotification;

class User extends Authenticatable implements MustVerifyEmail
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'fname', 'lname', 'username', 'email', 'phone', 'password', 'be_writer', 'per_page_rate', 'subjects','score', 'quality', 'is_test', 'is_verify', 'profile_photo', 'is_available', 'api_token', 'commission_amt', 'tour'
    ];

    protected $appends = ['profile_photo_url', 'rating', 'email_verified'];

    protected $casts = ['per_page_rate' => 'array'];


    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function assignments()
    {
        return $this->hasMany(Assignment::class)->latest();
    }

    public function projects()
    {
        return $this->hasMany(Project::class)->latest();
    }

    public function writerProjects()
    {
        return $this->hasMany(Project::class, 'writer_id')->latest();
    }

    public function userProjects()
    {
        return $this->hasMany(Project::class, 'user_id')->latest();
    }

    /*public function getWriterRateAttribute()
    {
        return ($this->per_page_rate) + (getGlobalSettings() && isset(getGlobalSettings()->commission_amt) ? getGlobalSettings()->commission_amt : 0);
        // return number_format(($this->per_page_rate) + (getGlobalSettings() && isset(getGlobalSettings()->commission_amt) ? (getGlobalSettings()->commission_amt * $this->per_page_rate) /100 : 0), 2);

    }*/

    public function getCompletedProjectsCountAttribute()
    {
        if ($this->be_writer) {
            return $this->writerProjects()->where('completed', 1)->count();
        }else{
            return $this->userProjects()->where('completed', 1)->count();
        }
    }

    public function getOngoingProjectsCountAttribute()
    {
        if ($this->be_writer){
            return $this->writerProjects()->where('writer_id', '!=', 0)->where('completed', '!=', 1)->count();
        }else{
            return $this->userProjects()->where('writer_id', '!=', 0)->where('completed', '!=', 1)->count();
        }
    }

    public function project_rate(Project $project)
    {
        $diff = $project->timestamp->diffInHours(\Carbon\Carbon::now());
        $rateArray = explode(',', $this->per_page_rate);
        $rate = $rateArray[rateIndex($diff)];
        return $rate + ($rate * ((isset($this->commission_amt) ? $this->commission_amt : 0)/100));
    }

    public function project_rate_for_writer(Project $project)
    {
        $diff = $project->timestamp->diffInHours(\Carbon\Carbon::now());
        $rateArray = explode(',', $this->per_page_rate);
        $rate = $rateArray[rateIndex($diff)];
        return $rate;
    }

    public function wallet()
    {
        return $this->hasOne(Wallet::class);
    }

    public function feedback()
    {
        return $this->hasMany(ProjectFeedback::class, 'writer_id');
    }

    public function writerDocument()
    {
        return $this->hasOne(WriterDocument::class);
    }

    public function availability()
    {
        return $this->hasOne(Availability::class);
    }

    public function initWallet()
    {
        return isset($this->wallet) ? $this->wallet : $this->wallet()->create(['balance' => 0]);
    }

    public function getRatingAttribute()
    {

        return count($this->feedback) ? $this->feedback()->sum('rating') / count($this->feedback) : '3.0';
    }


     public function isWriterAvailable(Project $project)
    {
         if(isset($this->availability)){

            $availability = str_replace(' ', '', $this->availability->available_dates);
            $dates = explode(',' , $availability);
            if (in_array($project->deadline_date, $dates)) {
                return true;
            } else {
                return false;
            }
        }

        return false;
    }

    public function getWriterIsAvailableAttribute()
    {
        if(isset($this->availability)){

            $availability = str_replace(' ', '', $this->availability->available_dates);
            $dates = explode(',' , $availability);
            if (in_array(date('Y-m-d'), $dates)) {
                return true;
            } else {
                return false;
            }
        }

        return false;
    }

    public function getHistoryAttribute()
    {
        return count($this->writerProjects) ? $this->writerProjects()->where('user_id', auth()->id())->get() : [];
    }

    public function getWriterHistoryAttribute()
    {
        return count($this->userProjects) ? $this->userProjects()->where('writer_id', auth()->id())->get() : [];
    }

    public function getApiHistoryAttribute()
    {
        return count($this->writerProjects) ? $this->writerProjects()->where('user_id', auth()->id())->select('id','topic', 'created_at', 'deadline_date', 'deadline_time', 'status', 'completed')->get() : [];
    }

    public function getApiWriterHistoryAttribute()
    {
        return count($this->userProjects) ? $this->userProjects()->where('writer_id', auth()->id())->select('id','topic', 'created_at', 'deadline_date', 'deadline_time', 'status', 'completed')->get() : [];
    }

    public function getFullnameAttribute()
    {
        return $this->fname . ' ' . $this->lname;
    }

    public function getEmailVerifiedAttribute()
    {
        return isset($this->email_verified_at) ? 1 : 0;
    }

    public function getProfilePhotoUrlAttribute()
    {
        return $this->profile_photo ? \Storage::url($this->profile_photo) : null;
    }

    public function generateToken()
    {
        if($this->api_token == null) {

          $token = substr(Password::getRepository()->createNewToken(), 0, 58);
          if (User::where('api_token', '=', $token)->exists()) {
                //Model Found -- call self.
                self::generate($length, $modelClass, $fieldName);
            } else {
                $this->api_token = $token;
                $this->save();
            }

       }

    }


    /**
     * Send the email verification notification.
     *
     * @return void
     */
    public function sendEmailVerificationNotification()
    {
        $this->notify(new VerifyEmailNotification($this));
    }
}
