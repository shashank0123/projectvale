<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAssignmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('assignments', function (Blueprint $table) {
            $table->increments('id');
            $table->string('type');
            $table->string('topic');
            $table->string('subject');
            $table->integer('pages_count')->default(1);
            $table->string('deadline_date');
            $table->string('deadline_time');
            $table->string('type_of_service');
            $table->string('writing_quality');
            $table->string('citations_count');
            $table->string('citation_format');
            $table->text('instructions')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('assignments');
    }
}
