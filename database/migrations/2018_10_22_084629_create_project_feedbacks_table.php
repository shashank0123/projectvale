<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectFeedbacksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('project_feedbacks', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('project_id');
            $table->integer('writer_id');
            $table->integer('fullyComplete')->default(0);
            $table->integer('onTime')->default(0);
            $table->float('rating')->default(2);
            $table->text('review')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('project_feedbacks');
    }
}
