<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDurationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('durations', function (Blueprint $table) {
            $table->increments('id');
            $table->string('duration_name');
            $table->string('20_days');
            $table->string('14_days');
            $table->string('10_days');
            $table->string('7_days');
            $table->string('5_days');
            $table->string('4_days');
            $table->string('3_days');
            $table->string('48_hours');
            $table->string('24_hours');
            $table->string('12_hours');
            $table->string('6_hours');
            $table->string('3_hours');
            $table->string('2_hours');
            $table->string('1_hours');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('durations');
    }
}
