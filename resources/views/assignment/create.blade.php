@section('navbar')
    <div class="" style="box-shadow: 0 2px 4px rgba(0, 0, 0, 0.04);">

    </div>
@endsection
@extends('layouts.app')

@section('content')
<div class="container">
	<div class="row justify-content-center">
		<div class="col-6">
			<form class="demo-form">
				<div class="form-section">
					<div class="form-group">
						<label>Type of paper<sup class="error">*</sup> </label>
						<select class="form-control{{ $errors->has('type') ? ' is-invalid' : '' }}" value="{{ old('type') }}" name="type" required>
							<option>Essay (Any Type)</option>
							<option>Article (Any Type)</option>
							<option>Assignment</option>
							<option>Content (Any Type)</option>
							<option>Admission Essay</option>
						</select>
						@if ($errors->has('type'))
						<span class="invalid-feedback" role="alert">
							<strong>{{ $errors->first('type') }}</strong>
						</span>
						@endif
					</div>
					<div class="form-group">
						<label>Topic<sup class="error">*</sup></label>
						<input type="text" name="topic" class="form-control{{ $errors->has('topic') ? ' is-invalid' : '' }}" value="{{ old('topic') }}" placeholder="Enter your topic" required>
						@if ($errors->has('topic'))
						<span class="invalid-feedback" role="alert">
							<strong>{{ $errors->first('topic') }}</strong>
						</span>
						@endif
					</div>

					<div class="form-group">
						<label>Select your subject<sup class="error">*</sup></label>
						<select class="form-control{{ $errors->has('subject') ? ' is-invalid' : '' }}" value="{{ old('subject') }}" name="subject" required>
							<option>English</option>
							<option>Business and Entrepreneurship</option>
							<option>Nursing</option>
							<option>History</option>
							<option>African-American Studies</option>
						</select>
						@if ($errors->has('subject'))
						<span class="invalid-feedback" role="alert">
							<strong>{{ $errors->first('subject') }}</strong>
						</span>
						@endif
					</div>
					<div class="form-group">
						<label>Number of pages<sup class="error">*</sup></label>
						<input type="number" name="pages_count" min="0" class="form-control{{ $errors->has('pages_count') ? ' is-invalid' : '' }}" value="{{ old('pages_count') }}" placeholder="Number of pages" required>
						@if ($errors->has('pages_count'))
						<span class="invalid-feedback" role="alert">
							<strong>{{ $errors->first('pages_count') }}</strong>
						</span>
						@endif
					</div>
					<div class="form-group row">
						<div class="col">
							<label>Deadline<sup class="error">*</sup></label>
							<input type="text" name="deadline_date" class="form-control datedropper{{ $errors->has('deadline_date') ? ' is-invalid' : '' }}" value="{{ old('deadline_date') }}" required data-format="Y-m-d" data-modal="true" data-large-default="true" data-init-set="false" >
							@if ($errors->has('deadline_date'))
							<span class="invalid-feedback" role="alert">
								<strong>{{ $errors->first('deadline_date') }}</strong>
							</span>
							@endif
						</div>
						<div class="col">
							<label>&nbsp;</label>
							<input type="text" name="deadline_time" class="form-control timedropper{{ $errors->has('deadline_time') ? ' is-invalid' : '' }}" value="{{ old('deadline_time') }}" required>
							@if ($errors->has('deadline_time'))
							<span class="invalid-feedback" role="alert">
								<strong>{{ $errors->first('deadline_time') }}</strong>
							</span>
							@endif
						</div>
					</div>
				</div>
				<div class="form-section">
					<label for="email">Email:</label>
					<input type="email" class="form-control" name="email" required="">
				</div>

				<div class="form-section">
					<label for="color">Favorite color:</label>
					<input type="text" class="form-control" name="color" required="">
				</div>

				<div class="form-navigation">
					<button type="button" class="previous btn btn-info pull-left">&lt; Previous</button>
					<button type="button" class="next btn btn-info pull-right">Next &gt;</button>
					<input type="submit" class="btn btn-default pull-right">
					<span class="clearfix"></span>
				</div>

			</form>
		</div>
	</div>
</div>
@endsection

@section('scripts')
<script type="text/javascript">
$(function () {
  var $sections = $('.form-section');
  console.log('here');
  function navigateTo(index) {
    // Mark the current section with the class 'current'
    $sections
      .removeClass('current')
      .eq(index)
        .addClass('current');
    // Show only the navigation buttons that make sense for the current section:
    $('.form-navigation .previous').toggle(index > 0);
    var atTheEnd = index >= $sections.length - 1;
    $('.form-navigation .next').toggle(!atTheEnd);
    $('.form-navigation [type=submit]').toggle(atTheEnd);
  }

  function curIndex() {
    // Return the current index by looking at which section has the class 'current'
    return $sections.index($sections.filter('.current'));
  }

  // Previous button is easy, just go back
  $('.form-navigation .previous').click(function() {
    navigateTo(curIndex() - 1);
  });

  // Next button goes forward iff current block validates
  $('.form-navigation .next').click(function() {
    $('.demo-form').parsley().whenValidate({
      group: 'block-' + curIndex()
    }).done(function() {
      navigateTo(curIndex() + 1);
    });
  });

  // Prepare sections by setting the `data-parsley-group` attribute to 'block-0', 'block-1', etc.
  $sections.each(function(index, section) {
    $(section).find(':input').attr('data-parsley-group', 'block-' + index);
  });
  navigateTo(0); // Start at the beginning
});
</script>
@endsection