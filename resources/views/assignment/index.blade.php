@extends('layouts.app')

@section('content')

	<section>
		<div class="container">
			<div class="my-5">
				<h2 class="font-weight-bold">Post New Assignment</h2>
				<div class=" mt-4">
					<div class="">
						<form>
						  	<div class="form-group row">
						  		<div class="col">
							    	<label for="formGroupExampleInput">Type of paper</label>
							    	<select class="form-control">
							    		<option>Essay (Any Type)</option>
							    		<option>Article (Any Type)</option>
							    		<option>Assignment</option>
							    		<option>Content (Any Type)</option>
							    		<option>Admission Essay</option>
							    	</select>
						  		</div>
						  		<div class="col">
							    	<label for="formGroupExampleInput2">Topic</label>
							    	<input type="text" class="form-control" id="formGroupExampleInput2" placeholder="Enter your topic">
						  		</div>
						  </div>
						  	
  							<div class="form-group row">
						  		<div class="col">
						    	<label for="formGroupExampleInput">Type of paper</label>
							    	<select class="form-control">
							    		<option>English</option>
							    		<option>Business and Entrepreneurship</option>
							    		<option>Nursing</option>
							    		<option>History</option>
							    		<option>African-American Studies</option>
							    	</select>
						  		</div>
						  		<div class="col">
							    	<label for="formGroupExampleInput2">Number of pages</label>
							    	<input type="text" class="form-control" id="formGroupExampleInput2" placeholder="Number of pages">
						  		</div>
						  	</div>
  							<div class="form-group row">
    							<div class="col">
						    		<label for="formGroupExampleInput2">Number of cited resources</label>
      								<input type="text" class="form-control" placeholder="cited resources">
    							</div>
    							<div class="col">
      								<label for="formGroupExampleInput2">Format of citation</label>
      								<select class="form-control">
							    		<option>Other</option>
							    		<option>APA</option>
							    		<option>Chicago/Turabian</option>
							    		<option>Harvard</option>
							    		<option>Vancouver</option>
							    		<option>Not Applicable</option>
							    		<option>MLA</option>
						    		</select>
    							</div>
  							</div>
  							<div class="row">
  								<div class="col">
      								<label for="formGroupExampleInput2">Deadline</label>
      								<select class="form-control">
							    		<option>10-14-2018</option>
							    		<option>10-14-2018</option>
							    		<option>10-14-2018</option>
							    		<option>10-14-2018</option>
							    		<option>10-14-2018</option>
						    		</select>
    							</div>
    							<div class="col">
						    		<label for="formGroupExampleInput2">Time</label>
      								<select class="form-control">
							    		<option>10am</option>
							    		<option>11am</option>
							    		<option>13am</option>
							    		<option>14am</option>
							    		<option>15am</option>
						    		</select>
    							</div>
  							</div>

  							<div class="form-group row mt-4">
    							<div class="col">
						    		<label for="formGroupExampleInput2">Type of service</label>
      								<div class="form-check">
										<input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios1" value="option1" checked>
											<label class="form-check-label" for="exampleRadios1">
										    Writing from scratch
										</label>
									</div>
									<div class="form-check">
										<input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios2" value="option2">
										<label class="form-check-label" for="exampleRadios2">
										    Rewriting
										</label>
									</div>
									<div class="form-check">
										<input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios3" value="option3" disabled>
										<label class="form-check-label" for="exampleRadios3">
										    Editing
										</label>
									</div>
    							</div>
    							<div class="col">
      								<div class="col">
							    		<label for="formGroupExampleInput2">Writer quality</label>
	      								<div class="form-check">
											<input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios1" value="option1" checked>
												<label class="form-check-label" for="exampleRadios1">
											    Standard
											</label>
										</div>
										<div class="form-check">
											<input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios2" value="option2">
											<label class="form-check-label" for="exampleRadios2">
											    Premium
											</label>
										</div>
										<div class="form-check">
											<input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios3" value="option3" disabled>
											<label class="form-check-label" for="exampleRadios3">
											    Platinum
											</label>
										</div>
    								</div>
    							</div>
  							</div>
						</form>
					</div>
				</div>
				<button class="btn btn-secondary btn-lg mt-4">Post Assignment</button>
			</div>
		</div>
	</section>

@endsection