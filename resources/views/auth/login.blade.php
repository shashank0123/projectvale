@extends('layouts.app')

@section('styles')

    <style type="text/css">
        .navbar-laravel {
            display: none;
        }
    </style>

@endsection

@section('content')
<div class="" style=" /*   background: linear-gradient(to bottom, #4a9bc8 0%, #9bc533 100%);*/height: 91vh;">
<div class="container login-card-section-custom">
    <div class="row justify-content-center pt-md-5">

        <div class="col-md-6 pl-md-0 mt-md-5 mb-3">
            <div class="card shadow-boder-none">
                <!-- <div class="card-header">{{ __('Login') }}</div> -->

                <div class="card-body user-login-section" style="">
                    <header><a href="/"><p class="projectvala-logo projectvala-logo-long"></p></a></header>

                    <form method="POST" action="{{ route('login') }}">
                        @csrf

                        <div class="form-group">

                            <input id="email" type="email" placeholder="E-mail Address" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>
                            @if ($errors->has('email'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif

                    </div>

                        <div class="form-group">


                                <input id="password" placeholder="Password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>
                                @if ($errors->has('password'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                                @endif

                        </div>
                        <div class="form-group ">
                            <div class="">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                                    <label class="form-check-label" for="remember">
                                        {{ __('Remember Me') }}
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group  mb-0">
                            <div class="">
                                <button type="submit" class="btn btn-block mb-3 btn-primary">
                                    {{ __('Login') }}
                                </button>

                                <a class="btn btn-link d-block text-center" href="{{ route('password.request') }}">
                                    {{ __('Forgot Your Password?') }}
                                </a>
                            </div>
                        </div>
                    </form>

                    <div class="loginSignUpSeparator"><span class="textInSeparator">or</span></div>

                     <a class="btn btn-block btn-secondary" href="{{ route('register') }}">
                        {{ __('Sign Up') }}
                    </a>
                </div>
            </div>
        </div>

    </div>
</div>
</div>
@endsection
