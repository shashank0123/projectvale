@extends('layouts.app')

@section('styles')

<style type="text/css">
.navbar-laravel {
    display: none;
}
#wizard .steps a{
    pointer-events: none;
}
</style>

@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center pt-md-4">
        <div class="col-md-6 pl-md-0 mt-md-2">
            <div class="card shadow-boder-none">
                <div class="card-body registration-user-section" style="">
                  <header><a href="/"><p class="projectvala-logo projectvala-logo-long"></p></a></header>
                  <form method="POST" class="frmRegistration" action="{{ route('register') }}">
                    @csrf
                    <section class="form-section">
                        <h4 class="mb-3">Create your account on ProjectVala</h4>
                        <div class="">
                            <div class="row">
                                <div class="col-md-6 form-group">
                                    <input id="fname" placeholder="First Name" type="text" class="form-control{{ $errors->has('fname') ? ' is-invalid' : '' }}" name="fname" value="{{ old('fname') }}" required autofocus>
                                    @if ($errors->has('fname'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('fname') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="col-md-6 form-group">
                                    <input id="lname" placeholder="Last Name" type="text" class="form-control{{ $errors->has('lname') ? ' is-invalid' : '' }}" name="lname" value="{{ old('lname') }}" required autofocus>
                                    @if ($errors->has('lname'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('lname') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="form-group ">
                            <input id="email" placeholder="E-mail Address" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ request('email') ? request('email') : old('email') }}" required>
                            @if ($errors->has('email'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                            @endif
                        </div>
                        <div class="row">
                            <div class="col-md-6 form-group">
                                <input id="username" placeholder="Username"  autocomplete="off" type="text" class="form-control{{ $errors->has('username') ? ' is-invalid' : '' }} username-input" name="username" value="{{ old('username') }}" required >
                                @if ($errors->has('username'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('username') }}</strong>
                                </span>
                                @endif
                            </div>
                            <div class="col-md-6 form-group">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="basic-addon1">+91</span>
                                    </div>
                                    <input id="phone" type="text" pattern="\d*" autocomplete="off" class="form-control{{ $errors->has('phone') ? ' is-invalid' : '' }}" minlength="10" maxlength="10" name="phone" value="{{ request('phone') ? request('phone') : old('phone') }}" placeholder="Phone number" required>
                                </div>
                                @if ($errors->has('phone'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('phone') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group ">
                            <div class="row">
                                <div class="col-md-6 form-group">
                                    <input id="password" placeholder="Password" type="password" pattern="^(?=.*?[A-Z])(?=.*?[0-9]).{8,}$" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>
                                    @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="col-md-6 form-group">
                                    <input placeholder="Confirm Password" id="password-confirm" data-parsley-equalto="#password" type="password" class="form-control" name="password_confirmation" required>
                                </div>
                            </div>
                            <p class="mb-3 text-muted"><small>The password must contain at least 8 characters with one uppercase and one number.</small></p>
                            <div class="form-group">
                                <label for="chk-terms">
                                    <input type="checkbox" id="chk-terms" name="terms" class="mr-2" required>
                                    <small>I agree with the website <a href="/terms-of-conditions" target="_blank"> user agreement</a> and the <a href="/privacy-policy" target="_blank">privacy policy</a>.</small>
                                </label>
                            </div>
                        </div>
                        <div class="form-group mt-3">
                            <button type="submit" class="btn btn-block btn-primary mb-4 btn-lg btn-signup" disabled="true">
                                {{ __('Sign Up') }}
                            </button>
                            <div class="loginSignUpSeparator"><span class="textInSeparator">or</span></div>
                            <a class="btn btn-block btn-secondary" href="{{ route('login') }}">
                                {{ __('Login') }}
                            </a>
                        </div>
                    </section>

                </form>

            </div>
        </div>
    </div>
</div>
</div>
@endsection
@section('scripts')
<script src="{{ asset('js/jquery.alphanum.js') }}"></script>
<script>
    $('.frmRegistration').parsley();
    $('#chk-terms').on('click', function(){
        if ($(this).is(':checked')) {
            $('.btn-signup').attr('disabled', false);
        }else{
            $('.btn-signup').attr('disabled', true);
        }
    });
    $(document).ready(function () {
        $('#username').alphanum();
        $("#username").on({
          keydown: function(e) {
            if (e.which === 32)
              return false;
          },
          change: function() {
            this.value = this.value.replace(/\s/g, "");
          }
        });
    });
</script>
@endsection