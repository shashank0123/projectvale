@extends('layouts.backend')

@section('content')
<div class="my-md-5 my-sm-0">
    <div class="headline d-flex justify-content-between align-items-cente hidden-on-mobile">
        <h3 class="d-inline-block">Citations</h3>
        <a href="#addCitaionModel" data-toggle="modal" class="btn btn-secondary"><i class="fa fa-plus-circle"></i> Add Citation</a>
    </div>

    <div class="mt-4">
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th>Sr.No.</th>
                    <th>Citation</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach($citations as $key => $citation)
                <tr>
                    <td>{{$key + 1 }}</td>
                    <td>{{$citation->name}}</td>
                    <td>
                        <a href="#editCitaionModel{{$key}}" data-toggle="modal" class="btn btn-secondary btn-sm"><i class="fa fa-edit"></i> Edit</a>
                        <a href="/admin/citations/{{$citation->id}}/delete"  onclick="return confirm('Are you sure, You want to delete this citation?');" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i> Delete</a>

                        <div class="modal fade in editProjectTypeModel{{$key}}" id="editCitaionModel{{$key}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Edit Citation</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <form method="post" action="/admin/citations/{{$citation->id}}/update">
                                        @csrf
                                        <div class="modal-body">
                                            <div class="form-group">
                                                <label>Citation</label>
                                                <input type="text" name="name" class="form-control" placeholder="Citation" value="{{$citation->name}}">
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default bg-dark text-white" data-toggle="modal" data-dismiss="modal">Cancel</button>
                                            <button type="submit" class="btn btn-secondary">Update Citation</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        {{ $citations->links() }}
    </div>
</div>


<div class="modal fade in addCitaionModel" id="addCitaionModel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Add Citation</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="post" action="/admin/citations">
                @csrf
                <div class="modal-body">
                    <div class="form-group">
                        <label>Citation</label>
                        <input type="text" name="name" class="form-control" placeholder="Citation">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default bg-dark text-white" data-toggle="modal" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-secondary">Add Citation</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
