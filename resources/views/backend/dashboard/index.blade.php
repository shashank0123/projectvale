@extends('layouts.backend')

@section('content')
<div class="my-md-5 my-sm-0">
    <div class="headline d-flex justify-content-between align-items-cente hidden-on-mobile">
        <h3 class="d-inline-block">Dashboard</h3>
    </div>

    <div class="mt-4">
<div class="row">
    <div class="col-xl-4 col-sm-6 mb-4">
        <div class="card text-white bg-primary o-hidden h-100">
            <div class="card-body">
                <div class="card-body-icon">
                    <i class="fa fa-fw fa-user-circle"></i>
                </div>
                <h3>Users ({{$users}})</h3>
            </div>
            <a class="card-footer text-white clearfix small z-1" href="/admin/users">
                <span class="float-left">View Details</span>
                <span class="float-right">
                    <i class="fa fa-angle-right"></i>
                </span>
            </a>
        </div>
    </div>
    <div class="col-xl-4 col-sm-6 mb-4">
        <div class="card text-white bg-dark o-hidden h-100">
            <div class="card-body">
                <div class="card-body-icon">
                    <i class="fa fa-fw fa-edit"></i>
                </div>
                <h3>Writers ({{$writers}})</h3>
            </div>
            <a class="card-footer text-white clearfix small z-1" href="/admin/writers">
                <span class="float-left">View Details</span>
                <span class="float-right">
                    <i class="fa fa-angle-right"></i>
                </span>
            </a>
        </div>
    </div>
    <div class="col-xl-4 col-sm-6 mb-4">
        <div class="card text-white bg-success o-hidden h-100">
            <div class="card-body">
                <div class="card-body-icon">
                    <i class="fa fa-fw fa-circle-o"></i>
                </div>
                <h3>Pending Verification ({{$unverify}})</h3>
            </div>
            <a class="card-footer text-white clearfix small z-1" href="/admin/verification/pending">
                <span class="float-left">View Details</span>
                <span class="float-right">
                    <i class="fa fa-angle-right"></i>
                </span>
            </a>
        </div>
    </div>
    <div class="col-xl-4 col-sm-6 mb-4">
        <div class="card text-white bg-primary o-hidden h-100">
            <div class="card-body">
                <div class="card-body-icon">
                    <i class="fa fa-fw fa-th-list"></i>
                </div>
                <h3>Projects ({{$projects}})</h3>
            </div>
            <a class="card-footer text-white clearfix small z-1" href="/admin/projects">
                <span class="float-left">View Details</span>
                <span class="float-right">
                    <i class="fa fa-angle-right"></i>
                </span>
            </a>
        </div>
    </div>
    <div class="col-xl-4 col-sm-6 mb-4">
        <div class="card text-white bg-dark o-hidden h-100">
            <div class="card-body">
                <div class="card-body-icon">
                    <i class="fa fa-fw fa-circle-o"></i>
                </div>
                <h3>Ongoing Projects ({{$ongoing}})</h3>
            </div>
            <a class="card-footer text-white clearfix small z-1" href="/admin/projects">
                <span class="float-left">View Details</span>
                <span class="float-right">
                    <i class="fa fa-angle-right"></i>
                </span>
            </a>
        </div>
    </div>
    <div class="col-xl-4 col-sm-6 mb-4">
        <div class="card text-white bg-success o-hidden h-100">
            <div class="card-body">
                <div class="card-body-icon">
                    <i class="fa fa-fw fa-check-circle-o"></i>
                </div>
                <h3>Completed Projects ({{$completed}})</h3>
            </div>
            <a class="card-footer text-white clearfix small z-1" href="/admin/projects">
                <span class="float-left">View Details</span>
                <span class="float-right">
                    <i class="fa fa-angle-right"></i>
                </span>
            </a>
        </div>
    </div>
</div>
</div>
</div>

@endsection