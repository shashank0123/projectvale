@extends('layouts.backend')

@section('content')
<div class="my-md-5 my-sm-0">
    <div class="headline d-flex justify-content-between align-items-center">
        <h3 class="d-inline-block">Social Links</h3>
    </div>

    <div class="mt-4">
        <div class="card">
            <div class="card-body">
                <form method="post" action="/admin/durations">
                    @csrf
                    <div class="row form-group">
                        <div class="col-sm-6">
                            <label>Duration Name</label>
                            <input type="text" name="duration_name" value="{{ old('duration_name') }}" class="form-control" placeholder="" required>
                        </div> 
                        <div class="col-sm-6">
                            <label>20 days</label>
                            <input type="text" name="20_days" value="{{ old('20_days') }}" class="form-control" placeholder="" required>
                        </div>
                    </div> 

                   <div class="row form-group">
                    <div class="col-sm-6">
                        <label>14 days</label>
                        <input type="text" name="14_days" value="{{ old('14_days') }}" class="form-control" placeholder="" required>
                    </div>
                    <div class="col-sm-6">
                        <label>10 days</label>
                        <input type="text" name="10_days" value="{{ old('10_days') }}" class="form-control" placeholder="" required>
                    </div>
                   </div> 

                    <div class="row form-group">
                        <div class="col-sm-6">
                            <label>7 days</label>
                            <input type="text" name="7_days" value="{{ old('7_days') }}" class="form-control" placeholder="" required>
                        </div>
                        <div class="col-sm-6">
                            <label>5 days</label>
                            <input type="text" name="5_days" value="{{ old('5_days') }}" class="form-control" placeholder="" required>
                        </div>
                    </div> 

                    <div class="row form-group">
                        <div class="col-sm-6">
                            <label>4 days</label>
                            <input type="text" name="4_days" value="{{ old('4_days') }}" class="form-control" placeholder="" required>
                        </div>
                        <div class="col-sm-6">
                            <label>3 days</label>
                            <input type="text" name="3_days" value="{{ old('3_days') }}" class="form-control" placeholder="" required>
                        </div>
                    </div> 

                    <div class="row form-group">
                        <div class="col-sm-6">
                            <label>48 hours</label>
                            <input type="text" name="48_hours" value="{{ old('48_hours') }}" class="form-control" placeholder="" required>
                        </div>
                        <div class="col-sm-6">
                            <label>24 hours</label>
                            <input type="text" name="24_hours" value="{{ old('24_hours') }}" class="form-control" placeholder="" required>
                        </div>
                    </div> 

                    <div class="row form-group">
                        <div class="col-sm-6">
                            <label>12 hours</label>
                            <input type="text" name="12_hours" value="{{ old('12_hours') }}" class="form-control" placeholder="" required>
                        </div>
                        <div class="col-sm-6">
                            <label>6 hours</label>
                            <input type="text" name="6_hours" value="{{ old('6_hours') }}" class="form-control" placeholder="" required>
                        </div>
                    </div> 

                    <div class="row form-group">
                        <div class="col-sm-6">
                            <label>3 hours</label>
                            <input type="text" name="3_hours" value="{{ old('3_hours') }}" class="form-control" placeholder="" required>
                        </div>
                        <div class="col-sm-6">
                            <label>2 hours</label>
                            <input type="text" name="2_hours" value="{{ old('2_hours') }}" class="form-control" placeholder="" required>
                        </div>
                    </div> 

                    <div class="row form-group">
                        <div class="col-sm-6">
                            <label>1 hours</label>
                            <input type="text" name="1_hours" value="{{ old('1_hours') }}" class="form-control" placeholder="" required>
                        </div>
                    </div>
            

            <div class="card-footer">
                <button type="submit" class="btn btn-secondary">Save</button>
            </div>
            </form>
        </div>
    </div>
</div>
@endsection