@extends('layouts.backend')

@section('content')

<section>
    <div class="container">
        <div class="row mt-5">
            <div class="col-md-6">
                <div class="order-heading">
                    <h2><b>My Duration</b></h2>
                </div>
            </div>
            <div class="col-md-6">
                <div class="order-now-button">
                    <button class="btn btn-secondary float-right btn-lg">
                        <a href="/admin/durations/create">Add</a>
                    </button>
                </div>
            </div>
        </div>
        <div class="row my-4">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <table class="table mt-3 custom-table-order">
                            <thead class="">
                                <tr>
                                    <th scope="col">Duration name</th>
                                    <th scope="col">20 days</th>
                                    <th scope="col">14 days</th>
                                    <th scope="col">10 days</th>
                                    <th scope="col">7 days</th>
                                    <th scope="col">5 days</th>
                                    <th scope="col">4 days</th>
                                    <th scope="col">3 days</th>
                                    <th scope="col">48 hours</th>
                                    <th scope="col">24 hours</th>
                                    <th scope="col">12 hours</th>
                                    <th scope="col">6 hours</th>
                                    <th scope="col">3 hours</th>
                                    <th scope="col">2 hours</th>
                                    <th scope="col">1 hours</th> 
                                </tr>
                            </thead>
                            <tbody>
                                @if(count($results) > 0)
                                @foreach ($results as $key => $value)
                                    <tr>
                                        <td>{{ $value['duration_name'] }}</td>
                                        <td>{{ $value['20_days'] }}</td>
                                        <td>{{ $value['14_days'] }}</td>
                                        <td>{{ $value['10_days'] }}</td>
                                        <td>{{ $value['7_days'] }}</td>
                                        <td>{{ $value['5_days'] }}</td>
                                        <td>{{ $value['4_days'] }}</td>
                                        <td>{{ $value['3_days'] }}</td>
                                        <td>{{ $value['48_hours'] }}</td>
                                        <td>{{ $value['24_hours'] }}</td>
                                        <td>{{ $value['12_hours'] }}</td>
                                        <td>{{ $value['6_hours'] }}</td>
                                        <td>{{ $value['3_hours'] }}</td>
                                        <td>{{ $value['2_hours'] }}</td>
                                        <td>{{ $value['1_hours'] }}</td>
                                    </tr>
                                @endforeach 
                                @else
                                <tr>
                                    <td colspan="15"><center>No records found</center></td>
                                </tr>
                                @endif 
                                   
                            </tbody>
                            
                            
                        </table>
                        {{ $results->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection