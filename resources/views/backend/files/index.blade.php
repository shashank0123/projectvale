@extends('layouts.backend')

@section('content')
<div class="my-5">
    <div class="headline headline-details d-flex">
        <h3 class="d-inline-block">Files</h3>
    </div>
    @if(count($projects))
    @foreach($projects as $project)
    @if(count($project->projectAdminFiles))
    <div class=" my-3 project-file-topic">
        <div class="">
            <div class="border-bottom">
                <h4 class="d-inline-block pb-2 mb-2">{{$project->topic}}  <small>( Plagiarism Report : {{$project->turniton ? 'Yes' : 'No'}} ) </small></h4>
                <a class="btn btn-primary btn-sm pull-right" href="#projectInfo{{$project->id}}"data-toggle="modal">Project Information</a>
                <div class="clearfix"></div>
            </div>
            <div class="row left-right-spacing">
                @foreach($project->projectAdminFiles as $file)
                <div class="col-md-3 mt-4">
                    <div class="custom-file-upload">
                        <div class="attachments-container">
                            <div class="attachment-box ripple-effect">
                                <a target="_blank" href="{{Storage::url($file->file)}}" class="attachment-title">{{str_limit($file->name, 18)}}</a>
                                @if($file->plagiarism_report)
                                <a target="_blank" title="Plagiarism Report" class="btn btn-sm btn-outline-primary" href="{{Storage::url($file->plagiarism_report)}}"><span class="fa fa-file"></span></a>
                                @endif
                                @if($file->unverify_reason)
                                <a href="#unverifyReasonModal{{$file->id}}" data-toggle="modal" title="Unverify Reason" class="btn btn-sm btn-outline-dark"><span class="fa fa-file"></span></a>
                                <div class="modal fade in unverifyReasonModal{{$file->id}}" id="unverifyReasonModal{{$file->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title">Unverify Reason</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <p class="text-justify">{{$file->unverify_reason}}</p>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default bg-secondary text-white" data-dismiss="modal">Close</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @endif
                                <a target="_blank" title="View" class="btn btn-sm btn-outline-success" href="{{Storage::url($file->file)}}"><span class="fa fa-eye"></span></a>
                                @if($file->is_verify == 1)
                                <a href="javascript:;" title="Verify" class="btn btn-sm btn-secondary disabled"><span class="fa fa-check"></span></a>
                                <a href="#unverifyModal{{$file->id}}" data-toggle="modal" title="Unverify" class="btn btn-sm btn-dark"><span class="fa fa-times"></span></a>
                                @elseif($file->is_verify == -1)
                                <a href="#verifyModal{{$file->id}}" data-toggle="modal" title="Verify" class="btn btn-sm btn-secondary"><span class="fa fa-check"></span></a>
                                <a href="javascript:;" title="Unverify" class="btn btn-sm btn-dark disabled"><span class="fa fa-times"></span></a>
                                @else
                                <a href="#verifyModal{{$file->id}}" data-toggle="modal" title="Verify" class="btn btn-sm btn-secondary"><span class="fa fa-check"></span></a>
                                <a href="#unverifyModal{{$file->id}}" data-toggle="modal" title="Unverify" class="btn btn-sm btn-dark"><span class="fa fa-times"></span></a>
                                @endif

                                <div class="modal fade in verifyModal{{$file->id}}" id="verifyModal{{$file->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <form method="POST" action="/admin/files/{{$file->id}}/verify"  enctype="multipart/form-data">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title">Verify File</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                @csrf
                                                <div class="modal-body">
                                                    <div class="custom-file-upload">
                                                        <div class="attachments-container">
                                                            <div class="attachment-box ripple-effect">
                                                                <a target="_blank" href="{{Storage::url($file->file)}}" class="attachment-title">{{str_limit($file->name, 35)}}</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="font-weight-bold">File Description : </label>
                                                        <p>{{$file->description}}</p>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Add Plagiarism Report</label>
                                                        <input type="file" name="plagiarism_report" class="form-control">
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default bg-secondary text-white" data-dismiss="modal">Cancel</button>
                                                    <button type="submit" class="btn btn-primary">Verify</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>

                                <div class="modal fade in unverifyModal{{$file->id}}" id="unverifyModal{{$file->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <form method="POST" action="/admin/files/{{$file->id}}/unverify"  enctype="multipart/form-data">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title">Unverify File</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                @csrf
                                                <div class="modal-body">
                                                    <div class="custom-file-upload">
                                                        <div class="attachments-container">
                                                            <div class="attachment-box ripple-effect">
                                                                <a target="_blank" href="{{Storage::url($file->file)}}" class="attachment-title">{{str_limit($file->name, 35)}}</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="font-weight-bold">File Description : </label>
                                                        <p>{{$file->description}}</p>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Reason<sup class="error">*</sup></label>
                                                        <textarea type="text" name="unverify_reason" class="form-control" required></textarea>
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default bg-secondary text-white" data-dismiss="modal">Cancel</button>
                                                    <button type="submit" class="btn btn-primary">Unverify</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>

    <div class="modal fade in" id="projectInfo{{$project->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg"  role="document">
            <div class="modal-content" style="width: 1000px;">
                <div class="modal-header">
                    <h5 class="modal-title">Project Information</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <ul class="nav-pill-tabs nav nav-pills" id="pills-tab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="pills-detail-tab{{$project->id}}" href="#pills-detail{{$project->id}}">Details</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="pills-file-tab{{$project->id}}" href="#pills-files{{$project->id}}">Files</a>
                        </li>
                    </ul>
                    <div class="tab-content" id="pills-tabContent">
                        <div class="tab-pane fade in show active" id="pills-detail{{$project->id}}" role="tabpanel" aria-labelledby="pills-detail-tab{{$project->id}}">
                            <div class="pb-4">
                                <div class="my-md-3 responsive-writer-profile-description">
                                    <div class="row">
                                        <div class="col-sm-8 project-card">
                                            <div class="card">
                                                <div class="card-body">
                                                    <div class="d-flex pl-3">
                                                        <div class="">
                                                            @if(isset($project->user->profile_photo) && $project->user->profile_photo)
                                                            <img src="{{Storage::url($project->user->profile_photo)}}" class="img-responsive rounded-circle mb-2" style="width:100px; height: 100px; vertical-align: initial;border: 2px solid rgb(117, 117, 117);">
                                                            @else
                                                            <img src="{{asset('img/user.png')}}" class="mb-2 img-thumbnail rounded-circle"  style="width:100px; height: 100px;">
                                                            @endif
                                                        </div>
                                                        <div class="pl-4" style="flex: 1">
                                                            <p class="mb-2"><b>{{$project->user->username}}</b></p>
                                                            <p class="mb-2"><i class="fa fa-envelope"></i> {{$project->user->email}}</p>
                                                            <p class="mb-2"><i class="fa fa-phone"></i> {{$project->user->phone}}</p>
                                                        </div>
                                                        <div>
                                                            <span class="user-badge available">Project Owner</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="card rounded-0 border-top-green">
                                                <div class="card-body">
                                                    <div class="apply-job-detail d-inline-block">
                                                        <p class="mb-0 pl-1">{!!$project->instructions!!}</p>
                                                    </div>
                                                    <div class="apply-job-detail pt-3">
                                                        <ul class="skills" style="margin-top: -15px;margin-bottom: 0px;">
                                                            <li class="">{{$project->subject}}</li>
                                                            <!-- <li class="">{{$project->type}}</li> -->
                                                        </ul>
                                                    </div>
                                                    <div class="apply-job-header">
                                                        <span class="cl-success"><i class="fa fa-calendar-check-o" aria-hidden="true"></i>{{date('F d, Y', strtotime($project->deadline_date))}}</span>
                                                        <span class="ml-2"><i class="fa fa-clock-o" aria-hidden="true"></i>{{$project->deadline_time}}</span>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="apply-job-detail">
                                                                <h5>Specifications</h5>
                                                                <ul class="job-requirements">
                                                                    <li><span>Type :</span> {{$project->type}}</li>
                                                                    <!-- <li><span>Type of service :</span> <span class="text-capitalize"> {{$project->type_of_service}}</span></li> -->
                                                                    <!-- <li><span>Writting Quality :</span> {{$project->writing_quality}}</li> -->
                                                                    <li><span>No. of Citation :</span> {{$project->citations_count}}</li>
                                                                    <li><span>Citation Format :</span> {{$project->citation_format}}</li>
                                                                    <li><span>Plagiarism Report :</span> {{$project->turniton ? 'Yes' : 'No'}}</li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    @if(isset($project->files))
                                                    <div class="row mb-3">
                                                        <div class="col-sm-12">
                                                            <h5>Files</h5>
                                                        </div>
                                                        @foreach($project->files as $key => $file)
                                                        <div class="col-sm-4">
                                                            <div class="custom-file-upload mt-3">
                                                                <div class="attachments-container">
                                                                    <div class="attachment-box ripple-effect">
                                                                        <a  target="_blank" href="{{Storage::url($file)}}" class="attachment-title">Project File {{$key + 1}}</a>
                                                                        <a target="_blank" class="btn btn-sm btn-secondary text-white mr-2" href="{{Storage::url($file)}}"><span class="fa fa-eye"></span> View</a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        @endforeach
                                                    </div>
                                                    @endif
                                                    @if(isset($project->additional_instructions))
                                                    <div class="apply-job-detail d-inline-block">
                                                        <h5>Additional Instructions</h5>
                                                        <p class="mb-0 pl-1 text-justify">{!!$project->additional_instructions!!}</p>
                                                    </div>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-4 project-card attachments-container-responsive user-profile-details">
                                            @include('backend.projects.projectwriter')
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane fade in" id="pills-files{{$project->id}}" role="tabpanel" aria-labelledby="pills-files-tab{{$project->id}}">
                            <div class="card">
                                <div class="card-body">
                                    <div class="row">
                                        @if(count($project->projectfiles))
                                        @foreach($project->projectfiles as $file)
                                        <div class="col-sm-4">
                                            <div class="custom-file-upload mb-3">
                                                <div class="attachments-container">
                                                    <div class="attachment-box ripple-effect">
                                                        <a  target="_blank" href="{{Storage::url($file->file)}}" class="attachment-title">{{str_limit($file->name, 18)}}</a>
                                                        <a target="_blank" class="btn btn-sm btn-secondary text-white mr-2" href="{{Storage::url($file->file)}}"><span class="fa fa-eye"></span> View</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        @endforeach
                                        @else
                                        @if(isset($project) && isset($project->projectfiles) && !count($project->projectfiles))
                                        <div class="col-sm-12 project-card text-center no-posted-project">
                                            <h4 >No any project files yet!</h4>
                                            <div class="my-md-5 my-sm-3 no-project-logo">
                                                <img src="/img/file.png" alt="files">
                                            </div>
                                        </div>
                                        @endif
                                        @endif
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn bg-secondary text-white" data-toggle="modal" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    @else
    @if(!count($projects) && !count($project->projectAdminFiles))
    <div class="empty-box row">
        <div class="col-md-12 text-center">
           <h4 >You don't have any project files yet!</h4>
           <div class="my-md-5 my-sm-3 no-project-logo">
            <img src="/img/file.png" alt="files" height="120px">
        </div>
    </div>
</div>
@endif
@endif
@endforeach
@else
<div class="empty-box row">
    <div class="col-md-12 text-center">
       <h4 >You don't have any project files yet!</h4>
       <div class="my-md-5 my-sm-3 no-project-logo">
        <img src="/img/file.png" alt="files">
    </div>
</div>
</div>
@endif
</div>

@endsection
@section('scripts')
<script>
    $(document).ready(function(){
        var hash = window.location.hash;
        console.log(hash);
        $('ul.nav-pill-tabs a[href="' + hash + '"]').tab('show');

        $('.nav-pill-tabs .nav-link').on('click', function(){
            var url = $(this).attr('href');
            $('ul.nav-pill-tabs a[href="' + url + '"]').tab('show');
        });
    });
</script>
@endsection