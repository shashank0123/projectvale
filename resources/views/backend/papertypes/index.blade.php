@extends('layouts.backend')

@section('content')
<div class="my-md-5 my-sm-0">
    <div class="headline d-flex justify-content-between align-items-cente hidden-on-mobile">
        <h3 class="d-inline-block">Paper Types</h3>
        <a href="#addPaperTypeModel" data-toggle="modal" class="btn btn-secondary"><i class="fa fa-plus-circle"></i> Add Paper Type</a>
    </div>

    <div class="mt-4">
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th>Sr.No.</th>
                    <th>Paper Types</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach($papertypes as $key => $papertype)
                <tr>
                    <td>{{$key + 1 }}</td>
                    <td>{{$papertype->name}}</td>
                    <td>
                        <a href="#editPaperTypeModel{{$key}}" data-toggle="modal" class="btn btn-secondary btn-sm"><i class="fa fa-edit"></i> Edit</a>
                        <a href="/admin/papertypes/{{$papertype->id}}/delete"  onclick="return confirm('Are you sure, You want to delete this paper type?');" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i> Delete</a>

                        <div class="modal fade in editProjectTypeModel{{$key}}" id="editPaperTypeModel{{$key}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Edit Paper Type</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <form method="post" action="/admin/papertypes/{{$papertype->id}}/update">
                                        @csrf
                                        <div class="modal-body">
                                            <div class="form-group">
                                                <label>Paper Type</label>
                                                <input type="text" name="name" class="form-control" placeholder="Paper Type" value="{{$papertype->name}}">
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default bg-dark text-white" data-toggle="modal" data-dismiss="modal">Cancel</button>
                                            <button type="submit" class="btn btn-secondary">Update Paper Type</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        {{ $papertypes->links() }}
    </div>
</div>


<div class="modal fade in addPaperTypeModel" id="addPaperTypeModel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Add Paper Type</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="post" action="/admin/papertypes">
                @csrf
                <div class="modal-body">
                    <div class="form-group">
                        <label>Paper Type</label>
                        <input type="text" name="name" class="form-control" placeholder="Paper Type">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default bg-dark text-white" data-toggle="modal" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-secondary">Add Paper Type</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
