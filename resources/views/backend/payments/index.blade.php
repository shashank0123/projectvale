@extends('layouts.backend')

@section('content')
<div class="my-md-5 my-sm-0">
    <div class="headline d-flex justify-content-between align-items-cente hidden-on-mobile">
        <h3 class="d-inline-block">Payments</h3>
    </div>

    <div class="mt-4">
        <ul class="nav-pill-tabs nav nav-pills nav-pills-projectvala mt-3" id="pills-tab" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" id="pills-unpaid-tab" href="#tabUnpaid">Unpaid</a>
            </li>
            <li class="nav-item">
                <a class="nav-link " id="pills-paid-tab" href="#tabPaid">Paid</a>
            </li>
        </ul>
        <div class="tab-content pt-3" id="pills-tabContent">
            <div class="tab-pane fade in show active" id="tabUnpaid" role="tabpanel" aria-labelledby="pills-unpaid-tab">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>Sr.No.</th>
                            <th>Topic</th>
                            <th>Subject</th>
                            <th>Posted By</th>
                            <th>Writer</th>
                            <th>Amount</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($unpaidprojects as $key => $unpaidproject)
                        <tr>
                            <td>{{$key + 1}}</td>
                            <td>{{$unpaidproject->topic}}</td>
                            <td>{{$unpaidproject->subject}}</td>
                            <td>{{$unpaidproject->user->username}}</td>
                            <td>{{isset($unpaidproject->writer) ? $unpaidproject->writer->username : '-'}}</td>
                            <td>&#8377; {{isset($unpaidproject->writer) ? ($unpaidproject->pages_count * $unpaidproject->writer->project_rate_for_writer($unpaidproject)) : '-'}}</td>
                            <td>
                                <a href="/admin/projects/{{$unpaidproject->id}}/pay" class="btn btn-secondary btn-sm">Pay</a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                {{ $unpaidprojects->links() }}
            </div>

            <div class="tab-pane fade in" id="tabPaid" role="tabpanel" aria-labelledby="pills-paid-tab">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>Sr.No.</th>
                            <th>Topic</th>
                            <th>Subject</th>
                            <th>Posted By</th>
                            <th>Writer</th>
                            <th>Amount</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($paidprojects as $key => $paidproject)
                        <tr>
                            <td>{{$key + 1}}</td>
                            <td>{{$paidproject->topic}}</td>
                            <td>{{$paidproject->subject}}</td>
                            <td>{{isset($paidproject->writer) ? $paidproject->user->username : '-'}}</td>
                            <td>{{isset($paidproject->writer) ? $paidproject->writer->username : '-'}}</td>
                            <td>{{isset($paidproject->writer) ? ($paidproject->pages_count * $paidproject->writer->project_rate_for_writer($paidproject)) : '-'}}</td>
                            <td><span class="badge badge-success py-1 px-3">Paid</span></td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                {{ $paidprojects->links() }}
            </div>
        </div>
    </div>
</div>

@endsection
@section('scripts')
<script>
        $(document).ready(function(){
            var hash = window.location.hash;
            console.log(hash);
            $('ul.nav-pills-projectvala a[href="' + hash + '"]').tab('show');

            $('.nav-pills-projectvala .nav-link').on('click', function(){
                var url = $(this).attr('href');
                $('ul.nav-pills-projectvala a[href="' + url + '"]').tab('show');
            });
        });
</script>
@endsection