@extends('layouts.backend')

@section('content')
<div class="my-md-5 my-sm-0">
    <div class="headline d-flex justify-content-between align-items-cente hidden-on-mobile">
        <h3 class="d-inline-block">Projects</h3>
    </div>

    <div class="mt-4">
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th>Sr.No.</th>
                    <th>Project Name</th>
                    <th>Posted By</th>
                    <th>Writer</th>
                    <th>Status</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach($projects as $key => $project)
                <tr>
                    <td>{{$key + 1}}</td>
                    <td>{{$project->topic}}</td>
                    <td>{{$project->user->username}}</td>
                    <td>{{isset($project->writer) ? $project->writer->username : '-'}}</td>
                    <td><span class="full-time {{ $project->project_status == 'New Request' ? 'newrequest' : $project->project_status }} text-capitalize">{{ $project->project_status }}</span></td>
                    <td>
                        <a href="/admin/projects/{{$project->id}}" class="btn btn-secondary btn-sm">View</a>
                        <a href="/admin/projects/{{$project->id}}/delete" onclick="return confirm('Are you sure, You want to delete this project?');" class="btn btn-danger btn-sm">Delete</a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        {{ $projects->links() }}
    </div>
</div>
@endsection