@extends('layouts.backend')

@section('content')
<div class="py-4 py-r-0">
    <div class="page-heading custom-project-gatway">
        <h2 class="d-inline">{!! $project->completed ? '<i class="fa fa-check-circle text-success"></i>' : '' !!} {{$project->topic}}</h2>
        <a href="/admin/projects/{{$project->id}}" class="btn  btn-default bg-secondary text-white float-right">Back</a>
    </div>
    <div class="clearfix"></div>
    <ul class="nav-pill-tabs nav nav-pills mt-3" id="pills-tab" role="tablist">
        <li class="nav-item">
            <a class="nav-link active" id="pills-detail-tab" href="#pills-detail">Details</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="pills-file-tab" href="#pills-files">Files</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="pills-chat-tab" href="#pills-chat">Chats</a>
        </li>
    </ul>
    <div class="tab-content" id="pills-tabContent">
        <div class="tab-pane fade in show active" id="pills-detail" role="tabpanel" aria-labelledby="pills-detail-tab">
            <div class="pb-4">
                <div class="my-md-3 responsive-writer-profile-description">
                    <div class="row">
                        <div class="col-sm-8 project-card">
                            <div class="card">
                                <div class="card-body">
                                    <div class="d-flex pl-3">
                                        <div class="">
                                            @if(isset($project->user->profile_photo) && $project->user->profile_photo)
                                            <img src="{{Storage::url($project->user->profile_photo)}}" class="img-responsive rounded-circle mb-2" style="width:100px; height: 100px; vertical-align: initial;border: 2px solid rgb(117, 117, 117);">
                                            @else
                                            <img src="{{asset('img/user.png')}}" class="mb-2 img-thumbnail rounded-circle"  style="width:100px; height: 100px;">
                                            @endif
                                        </div>
                                        <div class="pl-4" style="flex: 1">
                                            <p class="mb-2"><b>{{$project->user->username}}</b></p>
                                            <p class="mb-2"><i class="fa fa-envelope"></i> {{$project->user->email}}</p>
                                            <p class="mb-2"><i class="fa fa-phone"></i> {{$project->user->phone}}</p>
                                        </div>
                                        <div>
                                            <span class="user-badge available">Project Owner</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card rounded-0 border-top-green">
                                <div class="card-body">
                                    <div class="apply-job-detail d-inline-block">
                                        <p class="mb-0 pl-1">{!!$project->instructions!!}</p>
                                    </div>
                                    <div class="apply-job-detail pt-3">
                                        <ul class="skills" style="margin-top: -15px;margin-bottom: 0px;">
                                            <li class="">{{$project->subject}}</li>
                                            <!-- <li class="">{{$project->type}}</li> -->
                                        </ul>
                                    </div>
                                    <div class="apply-job-header">
                                        <span class="cl-success"><i class="fa fa-calendar-check-o" aria-hidden="true"></i>{{date('F d, Y', strtotime($project->deadline_date))}}</span>
                                        <span class="ml-2"><i class="fa fa-clock-o" aria-hidden="true"></i>{{$project->deadline_time}}</span>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="apply-job-detail">
                                                <h5>Specifications</h5>
                                                <ul class="job-requirements">
                                                    <li><span>Type :</span> {{$project->type}}</li>
                                                    <!-- <li><span>Type of service :</span> <span class="text-capitalize"> {{$project->type_of_service}}</span></li> -->
                                                    <!-- <li><span>Writting Quality :</span> {{$project->writing_quality}}</li> -->
                                                    <li><span>No. of Citation :</span> {{$project->citations_count}}</li>
                                                    <li><span>Citation Format :</span> {{$project->citation_format}}</li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    @if(isset($project->files))
                                        <div class="row mb-3">
                                            <div class="col-sm-12">
                                                <h5>Files</h5>
                                            </div>
                                            @foreach($project->files as $key => $file)
                                            <div class="col-sm-4">
                                                <div class="custom-file-upload mt-3">
                                                    <div class="attachments-container">
                                                        <div class="attachment-box ripple-effect">
                                                            <a  target="_blank" href="{{Storage::url($file)}}" class="attachment-title">Project File {{$key + 1}}</a>
                                                            <a target="_blank" class="btn btn-sm btn-secondary text-white mr-2" href="{{Storage::url($file)}}"><span class="fa fa-eye"></span> View</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            @endforeach
                                        </div>
                                        @endif
                                    @if(isset($project->additional_instructions))
                                    <div class="apply-job-detail d-inline-block">
                                        <h5>Additional Instructions</h5>
                                        <p class="mb-0 pl-1 text-justify">{!!$project->additional_instructions!!}</p>
                                    </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4 project-card attachments-container-responsive user-profile-details">
                            @include('backend.projects.projectwriter')
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="tab-pane fade in" id="pills-files" role="tabpanel" aria-labelledby="pills-files-tab">
            <div class="row my-md-3 project-file-upload">
                <div class="col-md-8 project-card desktop-project-card">
                    <div class="card">
                        <div class="card-body">
                            <div class="d-flex pl-3">
                                <div class="">
                                    @if(isset($project->user->profile_photo) && $project->user->profile_photo)
                                    <img src="{{Storage::url($project->user->profile_photo)}}" class="img-responsive rounded-circle mb-2" style="width:100px; height: 100px; vertical-align: initial;border: 2px solid rgb(117, 117, 117);">
                                    @else
                                    <img src="{{asset('img/user.png')}}" class="mb-2 img-thumbnail rounded-circle"  style="width:100px; height: 100px;">
                                    @endif
                                </div>
                                <div class="pl-4" style="flex: 1">
                                    <p class="mb-2"><b>{{$project->user->username}}</b></p>
                                    <p class="mb-2"><i class="fa fa-envelope"></i> {{$project->user->email}}</p>
                                    <p class="mb-2"><i class="fa fa-phone"></i> {{$project->user->phone}}</p>
                                </div>
                                <div>
                                    <span class="user-badge available">Project Owner</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                @if(count($project->projectfiles))
                                @foreach($project->projectfiles as $file)
                                <div class="col-sm-4">
                                    <div class="custom-file-upload mb-3">
                                        <div class="attachments-container">
                                            <div class="attachment-box ripple-effect">
                                                <a  target="_blank" href="{{Storage::url($file->file)}}" class="attachment-title">{{str_limit($file->name, 18)}}</a>
                                                <a target="_blank" class="btn btn-sm btn-secondary text-white mr-2" href="{{Storage::url($file->file)}}"><span class="fa fa-eye"></span> View</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                                @else
                                @if(isset($project) && isset($project->projectfiles) && !count($project->projectfiles))
                                <div class="col-sm-12 project-card text-center no-posted-project">
                                    <h4 >No any project files yet!</h4>
                                    <div class="my-md-5 my-sm-3 no-project-logo">
                                        <img src="/img/file.png" alt="files">
                                    </div>
                                </div>
                                @endif
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 project-card attachments-container-responsive user-profile-details">
                    @include('backend.projects.projectwriter')
                </div>
            </div>
        </div>


        <div class="tab-pane fade in" id="pills-chat" role="tabpanel" aria-labelledby="pills-chat-tab">
            <div class="row my-md-3 chat-section-custom">
                <div class="col-sm-8 project-card">
                    <div class="card">
                        <div class="card-body">
                            <div class="d-flex pl-3">
                                <div class="">
                                    @if(isset($project->user->profile_photo) && $project->user->profile_photo)
                                    <img src="{{Storage::url($project->user->profile_photo)}}" class="img-responsive rounded-circle mb-2" style="width:100px; height: 100px; vertical-align: initial;border: 2px solid rgb(117, 117, 117);">
                                    @else
                                    <img src="{{asset('img/user.png')}}" class="mb-2 img-thumbnail rounded-circle"  style="width:100px; height: 100px;">
                                    @endif
                                </div>
                                <div class="pl-4" style="flex: 1">
                                    <p class="mb-2"><b>{{$project->user->username}}</b></p>
                                    <p class="mb-2"><i class="fa fa-envelope"></i> {{$project->user->email}}</p>
                                    <p class="mb-2"><i class="fa fa-phone"></i> {{$project->user->phone}}</p>
                                </div>
                                <div>
                                    <span class="user-badge available">Project Owner</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    @if($project->status != 1)
                    <div class="writer-list-chat">
                        <div class="card border-top-green">
                            <div class="card-body">
                                <ul class="chat">
                                    @foreach($project->chats as $message)
                                    @if($message->sender != $project->writer_id)
                                    <li class="left clearfix" >
                                        <div class="chat-body clearfix">
                                            <p>
                                                {!!$message->message!!}
                                            </p>
                                        </div>
                                        <div class="clearfix"></div>
                                        <span class="msg-time"><i class="fa fa-clock-o"></i> {{$message->created_at->diffForHumans()}}</span>
                                    </li>
                                    @else
                                    <li class="right clearfix" >
                                        <div class="chat-body clearfix">
                                            <p>
                                                {!!$message->message!!}
                                            </p>
                                        </div>
                                        <div class="clearfix"></div>
                                        <span class="msg-time"><i class="fa fa-clock-o"></i> {{$message->created_at->diffForHumans()}}</span>
                                    </li>
                                    @endif
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                    @endif
                </div>
                <div class="col-sm-4 project-card attachments-container-responsive user-profile-details">
                    @include('backend.projects.projectwriter')
                </div>
            </div>
        </div>
    </div>
</div>


<!-- View Feedback Modal -->
<div class="modal fade in" id="viewFeedbackModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Review</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="mb-3">
                    @if(isset($project->feedback) && $project->feedback->fullyComplete)
                    <span class="badge badge-success py-2 px-3"><i class="fa fa-check"></i> Fully Complete</span>
                    @else
                    <span class="badge badge-danger py-2 px-3"><i class="fa fa-times"></i> Fully Complete</span>
                    @endif
                    @if(isset($project->feedback) && $project->feedback->onTime)
                    <span class="badge badge-success py-2 px-3"><i class="fa fa-check"></i> On Time</span>
                    @else
                    <span class="badge badge-danger py-2 px-3"><i class="fa fa-times"></i> On Time</span>
                    @endif
                </div>
                <div class="projectRating"></div>
                <hr>
                <p>{{isset($project->feedback) && $project->feedback->review ? $project->feedback->review : ''}}</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary btn-lg btn-block" data-toggle="modal" data-dismiss="modal">Ok</button>
            </div>
        </div>
    </div>
</div>

<!-- warning for review -->
<div class="modal fade in" id="writerHistoryModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">History</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="mb-3" style="max-height: 350px; overflow: auto;">
                    <div class="mt-3 responsive-writer-profile">
                        @if(isset($history) && count($history))
                        <div class="list-group">
                            @foreach($history as $key => $item)
                            <div class="list-group-item list-group-item-action flex-column align-items-start">
                                <div class="d-flex w-100 justify-content-between">
                                    <h5 class="mb-1">{{$item->topic}}</h5>
                                    <div class="brows-job-type">
                                        <span class="full-time {{ $item->project_status }} text-capitalize">{{ $item->project_status }}</span>
                                    </div>
                                </div>
                                <small>{{date('F d, Y', strtotime($item->created_at))}} - {{date('F d, Y', strtotime($item->deadline_date))}}</small>
                            </div>
                            @endforeach
                        </div>
                        @else
                        <h5>You have no work history yet.</h5>
                        @endif
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary btn-lg btn-block" data-toggle="modal" data-dismiss="modal">Ok</button>
            </div>
        </div>
    </div>
</div>


@endsection
@section('scripts')
<script>
    $('div.rating').raty({ starType: 'i' });
    $('div.projectRating').raty({ starType: 'i',readOnly:true, score:'{{isset($project->feedback) && $project->feedback->rating ? $project->feedback->rating :'' }}' });

    $('div.projectWriterRating').raty({ starType: 'i',readOnly:true, score:'{{isset($project->writer) && $project->writer->rating ? $project->writer->rating :'3.0' }}' });

    $(document).ready(function(){
        var hash = window.location.hash;
        console.log(hash);
        $('ul.nav-pill-tabs a[href="' + hash + '"]').tab('show');

        $('.nav-pill-tabs .nav-link').on('click', function(){
            var url = $(this).attr('href');
            $('ul.nav-pill-tabs a[href="' + url + '"]').tab('show');
        });
    });
</script>
@endsection