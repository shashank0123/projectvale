@extends('layouts.backend')

@section('content')
<div class="my-md-5 my-sm-0">
    <div class="headline d-flex justify-content-between align-items-center">
        <h3 class="d-inline-block">Settings</h3>
    </div>

    <div class="mt-4 row">
        <div class="col-sm-6">
            <div class="card">
                <form method="post" action="/admin/settings">
                    <div class="card-body">
                        @csrf
                        <div class="row form-group">
                            <div class="col-sm-12">
                                <label>Plagiarism Amount</label>
                                <input type="text" name="plagiarism_amt"  value="{{isset($settings) ? $settings->plagiarism_amt : ''}}" class="form-control" placeholder="Plagiarism Amount" required>
                            </div>
                        <!-- <div class="col-sm-6">
                            <label>Commission Amount</label>
                            <input type="text" name="commission_amt"  value="{{isset($settings) ? $settings->commission_amt : ''}}" class="form-control" placeholder="Commission Amount">
                        </div> -->
                    </div>
                </div>
                <div class="card-footer">
                    <button type="submit" class="btn btn-secondary">Save</button>
                </div>
            </form>
        </div>
    </div>

    <div class="col-sm-6">
        <div class="card">
            <div class="card-header">
                Database
            </div>
            <div class="card-body">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>Sr.No.</th>
                            <th>Tables</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>1</td>
                            <td>User / Writer / Project </td>
                            <td>
                                <form method="post" action="/admin/database/clear">
                                    @csrf
                                    <button type="submit" class="btn btn-sm btn-secondary">Clear</button>
                                </form>
                            </td>
                        </tr>
                        <tr>
                            <td>2</td>
                            <td>Subjects </td>
                            <td>
                                <form method="post" action="/admin/database/subjects/clear">
                                    @csrf
                                    <button type="submit" class="btn btn-sm btn-secondary">Clear</button>
                                </form>
                            </td>
                        </tr>
                        <tr>
                            <td>3</td>
                            <td>Paper Type </td>
                            <td>
                                <form method="post" action="/admin/database/papertype/clear">
                                    @csrf
                                    <button type="submit" class="btn btn-sm btn-secondary">Clear</button>
                                </form>
                            </td>
                        </tr>
                        <tr>
                            <td>4</td>
                            <td>Citation </td>
                            <td>
                                <form method="post" action="/admin/database/citation/clear">
                                    @csrf
                                    <button type="submit" class="btn btn-sm btn-secondary">Clear</button>
                                </form>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection