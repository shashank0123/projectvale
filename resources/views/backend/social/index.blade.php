@extends('layouts.backend')

@section('content')
<div class="my-md-5 my-sm-0">
    <div class="headline d-flex justify-content-between align-items-center">
        <h3 class="d-inline-block">Social Links</h3>
    </div>

    <div class="mt-4">
        <div class="card">
            <div class="card-body">
                <form method="post" action="/admin/sociallinks">
                    @csrf
                    <div class="row form-group">
                        <div class="col-sm-6">
                            <label>Facebook</label>
                            <input type="url" name="facebook" value="{{isset($sociallinks) ? $sociallinks->facebook : ''}}" class="form-control" placeholder="Enter facebook page url" required>
                        </div>
                        <div class="col-sm-6">
                            <label>Instagram</label>
                            <input type="url" name="instagram" value="{{isset($sociallinks) ? $sociallinks->instagram : ''}}" class="form-control" placeholder="Enter instagram page url">
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-sm-6">
                            <label>Twitter</label>
                            <input type="url" name="twitter" value="{{isset($sociallinks) ? $sociallinks->twitter : ''}}" class="form-control" placeholder="Enter twitter page url">
                        </div>
                        <div class="col-sm-6">
                            <label>LinkedIn</label>
                            <input type="url" name="linkedin" value="{{isset($sociallinks) ? $sociallinks->linkedin : ''}}" class="form-control" placeholder="Enter linkedin page url">
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-sm-6">
                            <label>Google Plus</label>
                            <input type="url" name="googlepluse" value="{{isset($sociallinks) ? $sociallinks->googlepluse : ''}}" class="form-control" placeholder="Enter googleplus page url">
                        </div>
                        <div class="col-sm-6">
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    <button type="submit" class="btn btn-secondary">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection