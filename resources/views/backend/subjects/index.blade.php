@extends('layouts.backend')

@section('content')
<div class="my-md-5 my-sm-0">
    <div class="headline d-flex justify-content-between align-items-cente hidden-on-mobile">
        <h3 class="d-inline-block">Subjects</h3>
        <a href="#addSubjectModel" data-toggle="modal" class="btn btn-secondary"><i class="fa fa-plus-circle"></i> Add Subject</a>
    </div>

    <div class="mt-4">
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th>Sr.No.</th>
                    <th>Subject</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach($subjects as $key => $subject)
                <tr>
                    <td>{{$key + 1 }}</td>
                    <td>{{$subject->name}}</td>
                    <td>
                        <a href="#editSubjectsModel{{$key}}" data-toggle="modal" class="btn btn-secondary btn-sm"><i class="fa fa-edit"></i> Edit</a>
                        <a href="/admin/subjects/{{$subject->id}}/delete"  onclick="return confirm('Are you sure, You want to delete this subject?');" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i> Delete</a>

                        <div class="modal fade in editSubjectsModel{{$key}}" id="editSubjectsModel{{$key}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Edit Subject</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <form method="post" action="/admin/subjects/{{$subject->id}}/update">
                                        @csrf
                                        <div class="modal-body">
                                            <div class="form-group">
                                                <label>Subject</label>
                                                <input type="text" name="name" class="form-control" placeholder="Subject Name" value="{{$subject->name}}">
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default bg-dark text-white" data-toggle="modal" data-dismiss="modal">Cancel</button>
                                            <button type="submit" class="btn btn-secondary">Update Subject</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        {{ $subjects->links() }}
    </div>
</div>


<div class="modal fade in addSubjectModel" id="addSubjectModel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Add Subject</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="post" action="/admin/subjects">
                @csrf
                <div class="modal-body">
                    <div class="form-group">
                        <label>Subject</label>
                        <input type="text" name="name" class="form-control" placeholder="Subject Name">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default bg-dark text-white" data-toggle="modal" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-secondary">Add Subject</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
