<div class="card rounded-0 mb-md-3 border-top-green">
    <div class="card-body">
        <div class="writer-status">
            <span class="freelance-status available">Writer</span>
            <h4 class="flc-rate">&#8377; {{ $project->writer->project_rate($project)}} / word</h4>
        </div>
        <div class="text-center mt-5">
            @if(isset($project->writer) && $project->writer->profile_photo)
            <img src="{{Storage::url($project->writer->profile_photo)}}" style="width:100px;height:100px !important;" class="mb-3 img-thumbnail rounded-circle">
            @else
            <img src="{{asset('img/user.png')}}" class="mb-3 img-thumbnail rounded-circle"  style="width:100px; height: 100px;">
            @endif
            <a href="#" style="color: #677484;text-decoration: none;">
                <h4 class="mt-0 mb-2 sidebar-box-detail">
                    {{isset($project->writer) ? $project->writer->username : ''}}
                </h4>
            </a>
            <span class="mt-0 mb-2 d-block desination">
                {{isset($project->writer) ? $project->writer->email : ''}}
            </span>
            <span class="badge badge-success d-inline text-white">{{isset($project->writer) ? $project->writer->quality : ''}}</span>
            <div class="mt-1 projectWriterRating"></div>
        </div>
        <div class="freelance-box-extra text-center">
            <ul>
                <?php $subjects = explode(',', $project->writer->subjects); ?>
                @foreach($subjects as $subject)
                <li>{{ $subject}}</li>
                @endforeach
            </ul>
        </div>
        @if($project->status == 4)
        @if(isset($project->feedback))
        <a href="#viewFeedbackModal" class="btn btn-secondary btn-freelance bt-1 border-0 mt-3" data-toggle="modal"  style="color: #fff;">View Feedback</a>
        @else
        <a href="javascript:;" class="btn btn-secondary btn-freelance bt-1 border-0 mt-3"  style="color: #fff;">No Feedback</a>
        @endif
        @endif
    </div>
</div>