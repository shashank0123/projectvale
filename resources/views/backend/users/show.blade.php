@extends('layouts.backend')

@section('content')
<div class="my-md-5 my-sm-0">
    <div class="headline d-flex justify-content-between align-items-cente hidden-on-mobile">
        <h3 class="d-inline-block">User</h3>
        <a href="/admin/users" class="btn  btn-default bg-secondary text-white float-right">Back</a>
    </div>

    <section class="profile-section profile-wallet-section my-5">
        <div class="card mb-3">
            <div class="card-body">
                <div class="row pl-3">
                    <div class="flex">
                        @if(isset($user->profile_photo) && $user->profile_photo)
                        <img src="{{Storage::url($user->profile_photo)}}" class="img-responsive rounded-circle mb-2" style="width:100px; height: 100px; vertical-align: initial;border: 2px solid rgb(117, 117, 117);">
                        @else
                        <img src="{{asset('img/user.png')}}" class="mb-2 img-thumbnail rounded-circle"  style="width:100px; height: 100px;">
                        @endif
                    </div>
                    <div class="flex pl-4">
                        <p class="mb-2"><b>{{$user->fullname}}</b></p>
                        <p class="mb-2"><i class="fa fa-envelope"></i> {{$user->email}}</p>
                        <p class="mb-2"><i class="fa fa-phone"></i> {{$user->phone}}</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="card">
            <div class="card-body">
                <h5 class="d-inline">Projects</h5>
                <hr>
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>Sr.No.</th>
                            <th>Project Name</th>
                            <th>Writer</th> 
                            <th>Subject</th>
                            <th>Words</th>
                            {{-- <th>Amount</th> --}}
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($projects as $key => $project)
                        <tr>
                            <td>{{$key + 1}}</td>
                            <td>{{$project->topic}}</td>
                            <td>{{isset($project->writer) ? $project->writer->fullname : 'No Writer'}}</td>
                            <td>{{$project->subject}}</td>
                            <td>{{$project->pages_count}}</td>
                            {{-- <td>{{ isset($project->writer_id) ? $project->writer->project_rate: 0 }}</td> --}}
                            <td>{{ ($project->payment_status == '1') ? 'Paid' : 'Unpaid' }}</td>
                            <td>
                                <a href="/admin/users/{{$user->id}}/project/{{$project->id}}" class="btn btn-secondary btn-sm">View</a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                {{ $projects->links() }}
            </div>
        </div>
    </section>
</div>

@endsection