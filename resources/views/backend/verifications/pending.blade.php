@extends('layouts.backend')

@section('content')
<div class="my-md-5 my-sm-0">
    <div class="headline d-flex justify-content-between align-items-cente hidden-on-mobile">
        <h3 class="d-inline-block">Writers Pending Verifications</h3>
    </div>
    <div class="clearfix"></div>
    <div class="mt-4">
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th>Sr.No.</th>
                    <th>Writer Name</th>
                    <th>Email</th>
                    <th>Phone</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach($writers as $key => $writer)
                <tr>
                    <td>{{$key + 1}}</td>
                    <td>{{$writer->fullname}}</td>
                    <td>{{$writer->email}}</td>
                    <td>{{$writer->phone}}</td>
                    <td>
                        <a href="/admin/verification/writer/{{$writer->id}}" class="btn btn-secondary btn-sm">View</a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        {{ $writers->links() }}
    </div>
</div>
@endsection