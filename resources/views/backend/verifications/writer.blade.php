@extends('layouts.backend')

@section('content')
<div class="my-md-5 my-sm-0">
    <div class="headline d-flex justify-content-between align-items-cente hidden-on-mobile">
        <h3 class="d-inline-block">Writer Profile</h3>
        <a href="/admin/verification/pending" class="btn  btn-default bg-secondary text-white float-right">Back</a>
    </div>

    <section class="profile-section profile-wallet-section my-5">
        <div class="d-flex border rounded justify-content-between align-center p-3 mb-3">
            <div class="">
                @if(isset($writer->profile_photo) && $writer->profile_photo)
                <img src="{{Storage::url($writer->profile_photo)}}" class="img-responsive rounded-circle mb-2" style="width:100px; height: 100px; vertical-align: initial;border: 2px solid rgb(117, 117, 117);">
                @else
                <img src="{{asset('img/user.png')}}" class="mb-2 img-thumbnail rounded-circle"  style="width:100px; height: 100px;">
                @endif
            </div>
            <div class="pl-4" style="flex: 1">
                <p class="mb-2"><b>{{$writer->fullname}}</b></p>
                <p class="mb-2"><i class="fa fa-envelope"></i> {{$writer->email}}</p>
                <p class="mb-2"><i class="fa fa-phone"></i> {{$writer->phone}}</p>
                <!-- <p class="mb-2 badge badge-success py-2 px-4">Test Score : {{$writer->score}}</p> -->
                <p class="mb-2 badge badge-success py-2 px-4">{{$writer->quality}}</p>
            </div>
            <div class="">
                <div class="">
                    @if($writer->is_verify == 1)
                    <a href="javascript:;" class="btn btn-success disabled"><i class="fa fa-check"></i> Verified</a>
                    <a href="/admin/writers/{{$writer->id}}/unverify" class="btn btn-danger">Unverify</a>
                    @elseif($writer->is_verify == 2)
                    <a href="#verifyModal" data-toggle="modal" class="btn btn-success">Verify</a>
                    <a href="javascript:;" class="btn btn-danger disabled"><i class="fa fa-times"></i> Unverified</a>
                    @else
                    <a href="#verifyModal" data-toggle="modal" class="btn btn-success">Verify</a>
                    <a href="/admin/writers/{{$writer->id}}/unverify" class="btn btn-danger">Unverify</a>
                    @endif
                </div>
            </div>
        </div>
        <div class="card">
            <div class="card-body">
                <div class="row">
                    @if(isset($writer->writerDocument) && isset($writer->writerDocument->aadhar_card) && $writer->writerDocument->aadhar_card)
                    <div class="col-sm-3">
                        <div class="custom-file-upload">
                            <div class="attachments-container">
                                <div class="attachment-box ripple-effect">
                                    <a  target="_blank" href="{{Storage::url($writer->writerDocument->aadhar_card)}}" class="attachment-title">Aadhar Card</a>
                                    <a target="_blank" class="btn btn-sm btn-secondary text-white mr-2" href="{{Storage::url($writer->writerDocument->aadhar_card)}}"><span class="fa fa-eye"></span> View</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endif
                    @if(isset($writer->writerDocument) && isset($writer->writerDocument->pan_card) && $writer->writerDocument->pan_card)
                    <div class="col-sm-3">
                        <div class="custom-file-upload">
                            <div class="attachments-container">
                                <div class="attachment-box ripple-effect">
                                    <a  target="_blank" href="{{Storage::url($writer->writerDocument->pan_card)}}" class="attachment-title">Pan Card</a>
                                    <a target="_blank" class="btn btn-sm btn-secondary text-white mr-2" href="{{Storage::url($writer->writerDocument->pan_card)}}"><span class="fa fa-eye"></span> View</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endif
                    @if(isset($writer->writerDocument) && isset($writer->writerDocument->tenth_marksheet) && $writer->writerDocument->tenth_marksheet)
                    <div class="col-sm-3">
                        <div class="custom-file-upload">
                            <div class="attachments-container">
                                <div class="attachment-box ripple-effect">
                                    <a  target="_blank" href="{{Storage::url($writer->writerDocument->tenth_marksheet)}}" class="attachment-title">10<sup>th</sup> Marksheet</a>
                                    <a target="_blank" class="btn btn-sm btn-secondary text-white mr-2" href="{{Storage::url($writer->writerDocument->tenth_marksheet)}}"><span class="fa fa-eye"></span> View</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endif
                    @if(isset($writer->writerDocument) && isset($writer->writerDocument->twelfth_marksheet) && $writer->writerDocument->twelfth_marksheet)
                    <div class="col-sm-3">
                        <div class="custom-file-upload">
                            <div class="attachments-container">
                                <div class="attachment-box ripple-effect">
                                    <a  target="_blank" href="{{Storage::url($writer->writerDocument->twelfth_marksheet)}}" class="attachment-title">12<sup>th</sup> Marksheet</a>
                                    <a target="_blank" class="btn btn-sm btn-secondary text-white mr-2" href="{{Storage::url($writer->writerDocument->twelfth_marksheet)}}"><span class="fa fa-eye"></span> View</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endif
                    @if(isset($writer->writerDocument) && isset($writer->writerDocument->degree_marksheet) && $writer->writerDocument->degree_marksheet)
                    <div class="col-sm-3">
                        <div class="custom-file-upload mt-3">
                            <div class="attachments-container">
                                <div class="attachment-box ripple-effect">
                                    <a  target="_blank" href="{{Storage::url($writer->writerDocument->degree_marksheet)}}" class="attachment-title">Degree Marksheet</a>
                                    <a target="_blank" class="btn btn-sm btn-secondary text-white mr-2" href="{{Storage::url($writer->writerDocument->degree_marksheet)}}"><span class="fa fa-eye"></span> View</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endif
                    @if(isset($writer->writerDocument) && isset($writer->writerDocument->college_transcript) && $writer->writerDocument->college_transcript)
                    <div class="col-sm-3">
                        <div class="custom-file-upload mt-3">
                            <div class="attachments-container">
                                <div class="attachment-box ripple-effect">
                                    <a  target="_blank" href="{{Storage::url($writer->writerDocument->college_transcript)}}" class="attachment-title">College Transcript</a>
                                    <a target="_blank" class="btn btn-sm btn-secondary text-white mr-2" href="{{Storage::url($writer->writerDocument->college_transcript)}}"><span class="fa fa-eye"></span> View</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </section>
</div>


<div class="modal fade in verifyModal" id="verifyModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm" role="document">
        <form method="POST" action="/admin/writers/{{$writer->id}}/verify"  enctype="multipart/form-data">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Verify Writer</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                @csrf
                <div class="modal-body">
                    <div class="row form-group">
                        <div class="col-xl-12 col-md-12">
                            <label>Select Writer Quality <span style="color: red;">*</span></label>
                            <select class="form-control" name="quality" required>
                                <option value="">Select Quality</option>
                                <option>Standard</option>
                                <option>Premium</option>
                                <option>Platinum</option>
                            </select>
                        </div>
                    </div>
                    <!-- <div class="row form-group">
                        <div class="col-xl-12 col-md-12">
                            <label>Rate<span style="color: red;">*</span></label>
                            <input type="text" name="per_page_rate" pattern="^[0-9]\d*(\.\d+)?$" class="form-control" placeholder="Enter writer rate" required>
                        </div>
                    </div> -->
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default bg-secondary text-white" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary">Verify</button>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection

@section('scripts')

@endsection