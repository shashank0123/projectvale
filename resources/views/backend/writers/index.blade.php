@extends('layouts.backend')

@section('content')
<div class="my-md-5 my-sm-0">
    <div class="headline d-flex justify-content-between align-items-cente hidden-on-mobile">
        <h3 class="d-inline-block">Writers</h3>
    </div>
    <div class="clearfix"></div>
    <ul class="nav-pill-tabs nav nav-pills mt-3" role="tablist">
        <li class="nav-item">
            <a class="nav-link active" href="/admin/writers">All</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="/admin/writers/pending">Pending</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="/admin/writers/verified">Verified</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="/admin/writers/unverified">Unverified</a>
        </li>
    </ul>
    <div class="mt-4">
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th>Sr.No.</th>
                    <th>Writer Name</th>
                    <th>Email</th>
                    <th>Phone</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach($writers as $key => $writer)
                <tr>
                    <td>{{$key + 1}}</td>
                    <td>{{$writer->fullname}}</td>
                    <td>{{$writer->email}}</td>
                    <td>{{$writer->phone}}</td>
                    <td>
                        <a href="/admin/writers/{{$writer->id}}" class="btn btn-secondary btn-sm">View</a>
                        <a href="/admin/writers/{{$writer->id}}/delete" onclick="return confirm('Are you sure, You want to delete this writer?');" class="btn btn-danger btn-sm">Delete</a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        {{ $writers->links() }}
    </div>
</div>
@endsection

