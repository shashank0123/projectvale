<div class="card rounded-0 mb-md-3 border-top-green">
    <div class="card-body">
        <div class="writer-status">
            <span class="freelance-status available">User</span>
        </div>
        <div class="text-center mt-5">
            @if(isset($project->user) && $project->user->profile_photo)
            <img src="{{Storage::url($project->user->profile_photo)}}" style="width:100px;height:100px !important;" class="mb-3 img-thumbnail rounded-circle">
            @else
            <img src="{{asset('img/user.png')}}" class="mb-3 img-thumbnail rounded-circle"  style="width:100px; height: 100px;">
            @endif
            <a href="#" style="color: #677484;text-decoration: none;">
                <h4 class="mt-0 mb-2 sidebar-box-detail">
                    {{isset($project->user) ? $project->user->username : ''}}
                </h4>
            </a>
            <span class="mt-0 mb-2 desination">
                {{isset($project->user) ? $project->user->email : ''}}
            </span>
        </div>

        @if($project->status == 4)
        @if(isset($project->feedback))
        <a href="#viewFeedbackModal" class="btn btn-secondary btn-freelance bt-1 border-0 mt-3" data-toggle="modal"  style="color: #fff;">View Feedback</a>
        @else
        <a href="javascript:;" class="btn btn-secondary btn-freelance bt-1 border-0 mt-3"  style="color: #fff;">No Feedback</a>
        @endif
        @endif
    </div>
</div>