@extends('layouts.backend')

@section('content')
<div class="my-md-5 my-sm-0">
    <div class="headline d-flex justify-content-between align-items-cente hidden-on-mobile">
        <h3 class="d-inline-block">Writers</h3>
        <a href="/admin/writers" class="btn  btn-default bg-secondary text-white float-right">Back</a>
    </div>

    <section class="profile-section profile-wallet-section my-5">
        <div class="d-flex border rounded justify-content-between align-center p-3 mb-3">
            <div class="">
                @if(isset($writer->profile_photo) && $writer->profile_photo)
                <img src="{{Storage::url($writer->profile_photo)}}" class="img-responsive rounded-circle mb-2" style="width:100px; height: 100px; vertical-align: initial;border: 2px solid rgb(117, 117, 117);">
                @else
                <img src="{{asset('img/user.png')}}" class="mb-2 img-thumbnail rounded-circle"  style="width:100px; height: 100px;">
                @endif
            </div>
            <div class="pl-4" style="flex: 1">
                <p class="mb-2"><b>{{$writer->fullname}}</b></p>
                <p class="mb-2"><i class="fa fa-envelope"></i> {{$writer->email}}</p>
                <p class="mb-2"><i class="fa fa-phone"></i> {{$writer->phone}}</p>
                <!-- <p class="mb-2 badge badge-success py-2 px-4">Test Score : {{$writer->score}}</p> -->
                <p class="mb-2 badge badge-success py-2 px-4">{{$writer->quality}}</p>
            </div>
            <div class="">
                <div class="">
                    @if($writer->is_verify == 1)
                    <a href="javascript:;" class="btn btn-success disabled"><i class="fa fa-check"></i> Verified</a>
                    <a href="/admin/writers/{{$writer->id}}/unverify" class="btn btn-danger">Unverify</a>
                    @elseif($writer->is_verify == 2)
                    <a href="#verifyModal" data-toggle="modal" class="btn btn-success">Verify</a>
                    <a href="javascript:;" class="btn btn-danger disabled"><i class="fa fa-times"></i> Unverified</a>
                    @else
                    <a href="#verifyModal" data-toggle="modal" class="btn btn-success">Verify</a>
                    <a href="/admin/writers/{{$writer->id}}/unverify" class="btn btn-danger">Unverify</a>
                    @endif
                </div>
            </div>
        </div>
        <ul class="nav-pill-tabs nav nav-pills" id="pills-tab" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" id="pills-documents-tab" href="#pills-documents">Documents</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="pills-projects-tab" href="#pills-projects">Projects</a>
            </li>
            @if($writer->is_verify == 1)
            <li class="nav-item">
                <a class="nav-link" id="pills-rate-tab" href="#pills-rate">Rate</a>
            </li>
            @endif
        </ul>
        <div class="tab-content" id="pills-tabContent">
            @include('backend.writers.tabs.documents')
            @include('backend.writers.tabs.projects')
            @include('backend.writers.tabs.rates')
        </div>
    </section>
</div>


<div class="modal fade in verifyModal" id="verifyModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm" role="document">
        <form method="POST" action="/admin/writers/{{$writer->id}}/verify"  enctype="multipart/form-data">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Verify Writer</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                @csrf
                <div class="modal-body">
                    <div class="row form-group">
                        <div class="col-xl-12 col-md-12">
                            <label>Select Writer Quality <span style="color: red;">*</span></label>
                            <select class="form-control" name="quality" required>
                                <option value="">Select Quality</option>
                                <option>Standard</option>
                                <option>Premium</option>
                                <option>Platinum</option>
                            </select>
                        </div>
                    </div>
                    <!-- <div class="row form-group">
                        <div class="col-xl-12 col-md-12">
                            <label>Commission<span style="color: red;">*</span></label>
                            <input type="text" name="commission_amt" pattern="^[0-9]\d*(\.\d+)?$" class="form-control" placeholder="Commission for writer" required>
                        </div>
                    </div> -->
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default bg-secondary text-white" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary">Verify</button>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection

@section('scripts')
<script>
    $(document).ready(function(){
        var hash = window.location.hash;
        console.log(hash);
        $('ul.nav-pill-tabs a[href="' + hash + '"]').tab('show');

        $('.nav-pill-tabs .nav-link').on('click', function(){
            var url = $(this).attr('href');
            $('ul.nav-pill-tabs a[href="' + url + '"]').tab('show');
        });
    });
</script>
@endsection