<div class="tab-pane fade in show active" id="pills-documents" role="tabpanel" aria-labelledby="pills-documents-tab">
    <div class="card">
        <div class="card-body">
            <div class="row">
                @if(isset($writer->writerDocument) && isset($writer->writerDocument->aadhar_card) && $writer->writerDocument->aadhar_card)
                <div class="col-sm-3">
                    <div class="custom-file-upload">
                        <div class="attachments-container">
                            <div class="attachment-box ripple-effect">
                                <a  target="_blank" href="{{Storage::url($writer->writerDocument->aadhar_card)}}" class="attachment-title">Aadhar Card</a>
                                <a target="_blank" class="btn btn-sm btn-secondary text-white mr-2" href="{{Storage::url($writer->writerDocument->aadhar_card)}}"><span class="fa fa-eye"></span> View</a>
                            </div>
                        </div>
                    </div>
                </div>
                @endif
                @if(isset($writer->writerDocument) && isset($writer->writerDocument->pan_card) && $writer->writerDocument->pan_card)
                <div class="col-sm-3">
                    <div class="custom-file-upload">
                        <div class="attachments-container">
                            <div class="attachment-box ripple-effect">
                                <a  target="_blank" href="{{Storage::url($writer->writerDocument->pan_card)}}" class="attachment-title">Pan Card</a>
                                <a target="_blank" class="btn btn-sm btn-secondary text-white mr-2" href="{{Storage::url($writer->writerDocument->pan_card)}}"><span class="fa fa-eye"></span> View</a>
                            </div>
                        </div>
                    </div>
                </div>
                @endif
                @if(isset($writer->writerDocument) && isset($writer->writerDocument->tenth_marksheet) && $writer->writerDocument->tenth_marksheet)
                <div class="col-sm-3">
                    <div class="custom-file-upload">
                        <div class="attachments-container">
                            <div class="attachment-box ripple-effect">
                                <a  target="_blank" href="{{Storage::url($writer->writerDocument->tenth_marksheet)}}" class="attachment-title">10<sup>th</sup> Marksheet</a>
                                <a target="_blank" class="btn btn-sm btn-secondary text-white mr-2" href="{{Storage::url($writer->writerDocument->tenth_marksheet)}}"><span class="fa fa-eye"></span> View</a>
                            </div>
                        </div>
                    </div>
                </div>
                @endif
                @if(isset($writer->writerDocument) && isset($writer->writerDocument->twelfth_marksheet) && $writer->writerDocument->twelfth_marksheet)
                <div class="col-sm-3">
                    <div class="custom-file-upload">
                        <div class="attachments-container">
                            <div class="attachment-box ripple-effect">
                                <a  target="_blank" href="{{Storage::url($writer->writerDocument->twelfth_marksheet)}}" class="attachment-title">12<sup>th</sup> Marksheet</a>
                                <a target="_blank" class="btn btn-sm btn-secondary text-white mr-2" href="{{Storage::url($writer->writerDocument->twelfth_marksheet)}}"><span class="fa fa-eye"></span> View</a>
                            </div>
                        </div>
                    </div>
                </div>
                @endif
                @if(isset($writer->writerDocument) && isset($writer->writerDocument->degree_marksheet) && $writer->writerDocument->degree_marksheet)
                <div class="col-sm-3">
                    <div class="custom-file-upload mt-3">
                        <div class="attachments-container">
                            <div class="attachment-box ripple-effect">
                                <a  target="_blank" href="{{Storage::url($writer->writerDocument->degree_marksheet)}}" class="attachment-title">College Degree</a>
                                <a target="_blank" class="btn btn-sm btn-secondary text-white mr-2" href="{{Storage::url($writer->writerDocument->degree_marksheet)}}"><span class="fa fa-eye"></span> View</a>
                            </div>
                        </div>
                    </div>
                </div>
                @endif
                @if(isset($writer->writerDocument) && isset($writer->writerDocument->college_transcript) && $writer->writerDocument->college_transcript)
                <div class="col-sm-3">
                    <div class="custom-file-upload mt-3">
                        <div class="attachments-container">
                            <div class="attachment-box ripple-effect">
                                <a  target="_blank" href="{{Storage::url($writer->writerDocument->college_transcript)}}" class="attachment-title">College Transcript</a>
                                <a target="_blank" class="btn btn-sm btn-secondary text-white mr-2" href="{{Storage::url($writer->writerDocument->college_transcript)}}"><span class="fa fa-eye"></span> View</a>
                            </div>
                        </div>
                    </div>
                </div>
                @endif
            </div>
        </div>
    </div>
</div>