<div class="tab-pane fade in" id="pills-projects" role="tabpanel" aria-labelledby="pills-projects-tab">
    <div class="card">
        <div class="card-body">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>Sr.No.</th>
                        <th>Project Name</th>
                        <th>Project Owner</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($projects as $key => $project)
                    <tr>
                        <td>{{$key + 1}}</td>
                        <td>{{$project->topic}}</td>
                        <td>{{isset($project->user) ? $project->user->fullname : 'No User'}}</td>
                        <td><span class="full-time {{ $project->project_status == 'New Request' ? 'newrequest' : $project->project_status }} text-capitalize">{{ $project->project_status }}</span></td>
                        <td>
                            <a href="/admin/writers/{{$writer->id}}/project/{{$project->id}}" class="btn btn-secondary btn-sm">View</a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            {{ $projects->links() }}
        </div>
    </div>
</div>