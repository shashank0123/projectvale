<div class="tab-pane fade in" id="pills-rate" role="tabpanel" aria-labelledby="pills-rate-tab">
    <div class="card">
        <div class="card-body">
            <form method="post" action="/admin/writers/{{$writer->id}}/rate">
                @csrf
                <?php $rate = explode(',', $writer->per_page_rate); ?>
                <div class="row form-group">
                    <div class="col-sm-3">
                        <label>4 to 8 hrs<span style="color: red;">*</span></label>
                        <input type="text" name="rates[]" pattern="^[0-9]\d*(\.\d+)?$" value="{{ count($rate) && isset($rate[0]) ? $rate[0] : ''}}" class="form-control" required>
                    </div>
                    <div class="col-sm-3">
                        <label>8 to 12 hrs<span style="color: red;">*</span></label>
                        <input type="text" name="rates[]" pattern="^[0-9]\d*(\.\d+)?$" value="{{ count($rate) && isset($rate[1]) ? $rate[1] : ''}}" class="form-control" required>
                    </div>
                    <div class="col-sm-3">
                        <label>12 to 24 hrs<span style="color: red;">*</span></label>
                        <input type="text" name="rates[]" pattern="^[0-9]\d*(\.\d+)?$" value="{{ count($rate) && isset($rate[2]) ? $rate[2] : ''}}" class="form-control" required>
                    </div>
                    <div class="col-sm-3">
                        <label>24 to 48 hrs<span style="color: red;">*</span></label>
                        <input type="text" name="rates[]" pattern="^[0-9]\d*(\.\d+)?$" value="{{ count($rate) && isset($rate[3]) ? $rate[3] : ''}}" class="form-control" required>
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col-sm-3">
                        <label>48 to 72 hrs<span style="color: red;">*</span></label>
                        <input type="text" name="rates[]" pattern="^[0-9]\d*(\.\d+)?$" value="{{ count($rate) && isset($rate[4]) ? $rate[4] : ''}}" class="form-control" required>
                    </div>
                    <div class="col-sm-3">
                        <label>72 to 96 hrs<span style="color: red;">*</span></label>
                        <input type="text" name="rates[]" pattern="^[0-9]\d*(\.\d+)?$" value="{{ count($rate) && isset($rate[5]) ? $rate[5] : ''}}" class="form-control" required>
                    </div>
                    <div class="col-sm-3">
                        <label>96 to 120 hrs<span style="color: red;">*</span></label>
                        <input type="text" name="rates[]" pattern="^[0-9]\d*(\.\d+)?$" value="{{ count($rate) && isset($rate[6]) ? $rate[6] : ''}}" class="form-control" required>
                    </div>
                    <div class="col-sm-3">
                        <label>120 to 144 hrs<span style="color: red;">*</span></label>
                        <input type="text" name="rates[]" pattern="^[0-9]\d*(\.\d+)?$" value="{{ count($rate) && isset($rate[7]) ? $rate[7] : ''}}" class="form-control" required>
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col-sm-3">
                        <label>144 to 168 hrs<span style="color: red;">*</span></label>
                        <input type="text" name="rates[]" pattern="^[0-9]\d*(\.\d+)?$" value="{{ count($rate) && isset($rate[8]) ? $rate[8] : ''}}" class="form-control" required>
                    </div>
                    <div class="col-sm-3">
                        <label>168hrs or more<span style="color: red;">*</span></label>
                        <input type="text" name="rates[]" pattern="^[0-9]\d*(\.\d+)?$" value="{{ count($rate) && isset($rate[9]) ? $rate[9] : ''}}" class="form-control" required>
                    </div>
                    <div class="col-md-3">
                        <label>Commission in percent(%)<span style="color: red;">*</span></label>
                        <input type="text" name="commission_amt" pattern="^[0-9]\d*(\.\d+)?$" value="{{isset($writer->commission_amt) ? $writer->commission_amt : ''}}" class="form-control" placeholder="Commission for writer" required>
                    </div>
                </div>
                <button type="submit" class="btn btn-primary">Save</button>
            </form>
        </div>
    </div>
</div>