@include('emails.header')
<table border="0" cellpadding="0" cellspacing="0" style="font-family:'Open+Sans', 'Open Sans', Helvetica, Arial, sans-serif; font-size:14px; line-height:24px; color:#525C65; text-align:left; width:100%;">
    <tbody>
        <tr>
            <td>
                <p style="margin:0; font-size:18px; line-height:23px; color:#102231; font-weight:bold;">
                    <strong>Hello {{$writer->fullname}},</strong><br><br>
                </p>
            </td>
        </tr>
        <tr>
            <td>
                <p style="margin:0; font-size:15px; line-height:23px; color:#102231; font-weight:bold;">
                    <strong>Sorry..!!</strong><br><br>
                </p>
            </td>
        </tr>
        <tr>
            <td>
                {{$project->user->username}} have cancelled request to hiring on a project - <b>{{$project->topic}}</b>. <br> <br> <br>
            </td>
        </tr>

@include('emails.footer')