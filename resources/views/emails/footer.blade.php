       <tr>
            <td style="font:14px/16px Arial, Helvetica, sans-serif; color:#363636; padding:0 0 14px;">
                Thank you,
            </td>
        </tr>
        <tr>
            <td style="font:bold 14px/16px Arial, Helvetica, sans-serif; color:#363636; padding:0 0 7px;">
                {{ config('app.name') }}
            </td>
        </tr>
    </tbody>
</table>
</div>
<!--[if gte mso 9]>
</td>
</tr>
</table>
<![endif]-->
</td>
</tr>
<tr>
    <td bgcolor="#e0e2e5" style="height:1px; width:100%; line-height:1px; font-size:0;">&nbsp;</td>
</tr>
<tr>
    <td bgcolor="#e0e2e4" style="height:1px; width:100%; line-height:1px; font-size:0;">&nbsp;</td>
</tr>
<tr>
    <td bgcolor="#e8ebed" style="height:1px; width:100%; line-height:1px; font-size:0;">&nbsp;</td>
</tr>
<tr>
    <td bgcolor="#f1f3f6" style="height:1px; width:100%; line-height:1px; font-size:0;">&nbsp;</td>
</tr>
</tbody>
</table>
</td>
<td bgcolor="#edeef1" style="width:1px; font-size:1px;">&nbsp;</td>
<td bgcolor="#f0f2f3" style="width:1px; font-size:1px;">&nbsp;</td>
<td bgcolor="#f5f6f7" style="width:1px; font-size:1px;">&nbsp;</td>
<td bgcolor="#fafbfc" style="width:7px; font-size:1px;">&nbsp;</td>
</tr>
</tbody>
</table>
</td>
</tr>
 
</tbody>
</table>
</div>

<!--[if gte mso 9]>
</td>
</tr>
</table>
<![endif]-->
</td>
</tr>
<tr>
<td align="center" style="padding:30px 0 20px 0;">
<table border="0" cellpadding="0" cellspacing="0" style="width:220px;">
<tbody>
    <tr>
@if(isset(getSocialLinks()->twitter))
<td align="center">
<a href="{{ getSocialLinks()->twitter }}" target="_blank"><img alt="Twitter" border="0" height="26" src="{{ asset('img/twitter.jpg') }}" style="display:block; width:26px!important; height:26px!important;" width="26"></a>
</td>
@endif
@if(isset(getSocialLinks()->facebook))
<td align="center">
<a href="{{ getSocialLinks()->facebook }}" target="_blank"><img alt="Facebook" border="0" height="26" src="{{ asset('img/facebook.jpg') }}" style="display:block; width:26px!important; height:26px!important;" width="26"></a>
</td>
@endif
@if(isset(getSocialLinks()->googlepluse))
<td align="center">
<a href="{{ getSocialLinks()->googlepluse }}" target="_blank"><img alt="Google" border="0" height="26" src="{{ asset('img/google.jpg') }}" style="display:block; width:28px!important; height:26px!important;" width="28"></a>
</td>
@endif
@if(isset(getSocialLinks()->linkedin))
<td align="center">
<a href="{{ getSocialLinks()->linkedin }}" target="_blank"><img alt="Linkedin" border="0" height="26" src="{{ asset('img/linkedin.jpg') }}" style="display:block; width:26px!important; height:26px!important;" width="26"></a>
</td>
@endif
</tr>
</tbody>
</table>
</td>
</tr>
<!-- <tr>
<td align="center" style="padding-bottom:40px;">
<table border="0" cellpadding="0" cellspacing="0" style="font-family:'Open+Sans', 'Open Sans', Helvetica, Arial, sans-serif; font-size:12px; line-height:18px;  text-align:center; width:auto;">
<tbody>
    <tr>
<td style="color:#b7bdc1;">
<p style="Margin:0;"><span class="appleLinksGrey">2465 Latham St.</span> &nbsp;•&nbsp; <span class="appleLinksGrey">Mountain View, CA 94040</span></p>
</td>
</tr>
</tbody>
</table>
</td>
</tr> -->
</tbody></table>
<!--[if gte mso 9]>
</td>
</tr>
</table>
<![endif]-->
</div>

<!-- <div style=" width:100%;">
<table cellpadding="0" cellspacing="0" border="0" style="width:100%;">
<tbody>
    <tr>
<td align="center" bgcolor="#7d97ad" style="padding:10px 0;">
<table border="0" cellpadding="0" cellspacing="0" style="font-family:'Open+Sans', 'Open Sans', Helvetica, Arial, sans-serif; font-size:14px; line-height:19px;  text-align:center; width:auto;">
<tbody>
    <tr>
<td style="color:#ffffff;">
Be in demand
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</div> -->
</center>
</td>
</tr>
</tbody>
</table>
</body>