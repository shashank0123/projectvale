<!DOCTYPE html>
<html>
<head>
<!--[if gte mso 9]><xml>
<o:OfficeDocumentSettings>
<o:AllowPNG/>
<o:PixelsPerInch>96</o:PixelsPerInch>
</o:OfficeDocumentSettings>
</xml><![endif]-->
<title>{{ config('app.name') }} Email</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta content="width=device-width, initial-scale=1.0" name="viewport">
<!--[if !mso]><!-- --><meta content="IE=edge" http-equiv="X-UA-Compatible"><!--<![endif]-->
<!--[if !mso]><!-- --><link href="https://fonts.googleapis.com/css?family=Open+Sans:600,400,300" rel="stylesheet" type="text/css"><!--<![endif]-->
<style type="text/css">html, body { background-color:#fafbfc; }
img { display:block; }
.ReadMsgBody {width: 100%; }
.ExternalClass {width: 100%; }
* { -webkit-text-size-adjust: none; }
.whiteLinks a:link, .whiteLinks a:visited { color:#ffffff!important;}
.appleLinksGrey a { color:#b7bdc1!important; text-decoration:none!important; }
table {border-collapse:collapse;}
.preheader{ font-size: 1px; line-height:1px; display: none!important; mso-hide:all; }

/* AOL Mail td overrides */
#maincontent td{color:#525C65;}
</style>
<!--[if mso]>
<style type="text/css">
body, table, td, a {font-family: Arial, Helvetica, sans-serif !important;}
</style>
<![endif]-->

</head>
<body bgcolor="#fafbfc" style="Margin:0; padding:0;" yahoo="fix">
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tbody>
    <tr>
<td style="background-color:#fafbfc">
<center bgcolor="#fafbfc" style="width:100%;background-color:#fafbfc;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;">
<div id="maincontent" style="max-width:620px; font-size:0;margin:0 auto;">
<div class="preheader" style="font-size: 1px; line-height:1px; display: none!important; mso-hide:all;">
One more step to get started
</div>
<!--[if gte mso 9]>
<table border="0" cellpadding="0" cellspacing="0" style="width:620px;">
<tr>
<td valign="top">
<![endif]-->

<table border="0" cellpadding="0" cellspacing="0" style="width:100%;">
<tbody>
    <tr>
<td>
<table border="0" cellpadding="0" cellspacing="0" style="width:100%;">
<tbody>
    <tr>
<td align="center" style="padding-bottom:20px;">
<table border="0" cellpadding="0" cellspacing="0" style="font-family:'Open+Sans', 'Open Sans', Helvetica, Arial, sans-serif; font-size:13px; line-height:18px; color:#00C0EA; text-align:center; width:152px;">

<tbody>
    <tr>
<td style="padding:20px 0 10px 0;">
<a href="#tba" style="text-decoration:none;" target="_blank">
    <img alt="{{ config('app.name') }}" border="0" height="27" src="{{ asset('img/logo-navbar.png') }}" style="display:block; width:300px !important; font-family:'Open+Sans', 'Open Sans', Helvetica, Arial, sans-serif; font-size:22px; line-height:26px; color:#000000; text-transform:uppercase; text-align:center; letter-spacing:1px;" width="152"></a>
</td>
</tr>

</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</td>
</tr>

<tr>
<td>
<table border="0" cellpadding="0" cellspacing="0" style="width:100%;">
<tbody><tr>
<td bgcolor="#fafbfc" style="width:7px; font-size:1px;">&nbsp;</td>
<td bgcolor="#f5f6f7" style="width:1px; font-size:1px;">&nbsp;</td>
<td bgcolor="#f0f2f3" style="width:1px; font-size:1px;">&nbsp;</td>
<td bgcolor="#edeef1" style="width:1px; font-size:1px;">&nbsp;</td>
<td bgcolor="#ffffff">
<table border="0" cellpadding="0" cellspacing="0" style="width:100%;">
<tbody>
    <tr>
<td style="text-align:center; padding:40px 40px 40px 40px; border-top:3px solid #95c229;">
<!--[if gte mso 9]>
<table border="0" cellpadding="0" cellspacing="0" style="width:520px;">
<tr>
<td valign="top">
<![endif]-->
<div style="display:inline-block; width:100%; max-width:520px;">
@yield('content')