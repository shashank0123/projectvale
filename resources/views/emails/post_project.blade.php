@include('emails.header')
<table border="0" cellpadding="0" cellspacing="0" style="font-family:'Open+Sans', 'Open Sans', Helvetica, Arial, sans-serif; font-size:14px; line-height:24px; color:#525C65; text-align:left; width:100%;">
    <tbody>
        <tr>
            <td>
                <p style="margin:0; font-size:18px; line-height:23px; color:#102231; font-weight:bold;">
                    <strong>Hello {{$user->fullname}},</strong><br><br>
                </p>
            </td>
        </tr>
        <tr>
            <td>
                <p style="margin:0; font-size:15px; line-height:23px; color:#102231; font-weight:bold;">
                    <strong>Congratulations..!!</strong><br><br>
                </p>
            </td>
        </tr>
        <tr>
            <td>
                You have successfully posted your project ! Here is what to expect next.<br>
            </td>
        </tr>
        <tr>
            <td>
                <ul>
                    <li>Now you can search for writers and choose a perfect fit for your project.</li>
                    <li>When you find the perfect writer for your project, reach out and let them know you’d like for him/her to work on your project!</li>
                </ul>
            </td>
        </tr>
        <tr>
            <td align="center" style="padding:15px 0 40px 0; border-bottom:1px solid #f3f6f9; ">
                <table border="0" cellpadding="0" cellspacing="0" style="border-collapse:separate !important; border-radius:15px; width:210px;">
                    <tbody>
                        <tr>
                            <td align="center" valign="top">
                                <!--[if gte mso 9]>
                                <table border="0" cellspacing="0" cellpadding="0" style="width:210px">
                                <tr>
                                <td bgcolor="#95c229" style="padding:0px 10px; text-align:center;" valign="top">
                                <![endif]-->
                                <a href="{{ url('/projects/'.$project->id) }}" target="_blank" style="background-color:#95c229; border-collapse:separate !important; border-top:10px solid #95c229; border-bottom:10px solid #95c229; border-right:45px solid #95c229; border-left:45px solid #95c229; border-radius:4px; color:#ffffff; display:inline-block; font-family:'Open+Sans','Open Sans',Helvetica, Arial, sans-serif; font-size:13px; font-weight:bold; text-align:center; text-decoration:none; letter-spacing:2px;">
                                    View Project
                                </a>
                                <!--[if gte mso 9]>
                                </td>
                                </tr>
                                </table>
                                <![endif]-->
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        <tr>
            <td style="padding-top:30px;">
                <p style="margin:20px 0 20px 0;">Or copy this link and paste in your web browser</p>
                <p style="margin:20px 0; font-size:12px; line-height:17px; word-wrap:break-word; word-break:break-all;">
                    <a href="{{ url('/projects/'.$project->id) }}" style="color:#5885ff; text-decoration:underline;" target="_blank">
                        {{ url('/projects/'.$project->id) }}
                    </a>
                </p>
            </td>
        </tr>

@include('emails.footer')