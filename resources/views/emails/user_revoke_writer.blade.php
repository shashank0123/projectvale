@component('mail::message')
<div class="container">
    <div class="content">
        <p>Hello {{$writer->fullname}},</p>

        <p>We're sorry to inform you that your client, <b>{{$project->user->username}}</b>, has requested to take you off the project, <b>{{$project->topic}}</b>. This may be for any number of reasons and we want to ensure that this does not discourage you from helping other clients in their work (mention the fields this writer is good at).</p>

        <p>Thank you for understanding and cooperating.</p>

        <p>We hope to see you working on a new project very soon.</p>
    </div>
</div>
Thanks,<br>
{{ config('app.name') }}
@endcomponent

@include('emails.header')
<table border="0" cellpadding="0" cellspacing="0" style="font-family:'Open+Sans', 'Open Sans', Helvetica, Arial, sans-serif; font-size:14px; line-height:24px; color:#525C65; text-align:left; width:100%;">
    <tbody>
        <tr>
            <td>
                <p style="margin:0; font-size:18px; line-height:23px; color:#102231; font-weight:bold;">
                    <strong>Hi {{$writer->fullname}},</strong><br><br>
                </p>
            </td>
        </tr>
        <tr>
            <td>
                We're sorry to inform you that your client, <b>{{$project->user->username}}</b>, has requested to take you off the project, <b>{{$project->topic}}</b>. This may be for any number of reasons and we want to ensure that this does not discourage you from helping other clients in their work (mention the fields this writer is good at).<br>
            </td>
        </tr>
        <tr>
            <td>
                Thank you for understanding and cooperating.
            </td>
        </tr>
        <tr>
            <td>
                We hope to see you working on a new project very soon.
            </td>
        </tr>

@include('emails.footer')


