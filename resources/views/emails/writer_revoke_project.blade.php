@include('emails.header')
<table border="0" cellpadding="0" cellspacing="0" style="font-family:'Open+Sans', 'Open Sans', Helvetica, Arial, sans-serif; font-size:14px; line-height:24px; color:#525C65; text-align:left; width:100%;">
    <tbody>
        <tr>
            <td>
                <p style="margin:0; font-size:18px; line-height:23px; color:#102231; font-weight:bold;">
                    <strong>Hello {{$user->fullname}},</strong><br><br>
                </p>
            </td>
        </tr>
        <tr>
            <td>
                We're sorry to inform you that your writer, <b>{{$project->writer->username}}</b>, has requested to take you off the project, <b>{{$project->topic}}</b>. This may be for any number of reasons.<br>
            </td>
        </tr>
        <tr>
            <td>
                Thank you for understanding and cooperating.
            </td>
        </tr>

@include('emails.footer')