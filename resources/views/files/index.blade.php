@extends('layouts.app')

@section('content')
<div class="my-5">
    <div class="headline headline-details d-flex">
        <h3 class="d-inline-block">Files</h3>
    </div>
    @section('heading', 'Files')
    @if(count($projects))
    @foreach($projects as $project)
    @if(count($project->projectfiles))
    <div class=" my-3 project-file-topic">
        <div class="">
            <h4 class="pb-2 mb-3 border-bottom">{{$project->topic}}</h4>
            <div class="row left-right-spacing">
                @foreach($project->projectfiles as $file)
                <div class="col-md-3 mt-4">
                    <div class="custom-file-upload">
                        <div class="attachments-container">
                            <div class="attachment-box ripple-effect">
                                @if($file->user_id == $project->writer->id)
                                @if($file->is_verify == 1)
                                <span class="fileStatus badge-primary py-0 px-1">Verified</span>
                                @elseif($file->is_verify == -1)
                                <span class="fileStatus badge-danger py-0 px-1">Unverified</span>
                                @else
                                <span class="fileStatus badge-dark py-0 px-1">Pending Verification</span>
                                @endif
                                @endif
                                <a target="_blank" href="{{Storage::url($file->file)}}" class="attachment-title">{{str_limit($file->name, 18)}}</a>
                                @if($file->plagiarism_report)
                                <a target="_blank" title="Plagiarism Report" class="btn btn-sm btn-outline-primary" href="{{Storage::url($file->plagiarism_report)}}"><span class="fa fa-file"></span> Plagiarism</a>
                                @endif
                                @if($file->unverify_reason)
                                <a href="#unverifyReasonModal{{$file->id}}" data-toggle="modal" title="Unverify Reason" class="btn btn-sm btn-outline-danger">Reason</a>
                                <div class="modal fade in unverifyReasonModal{{$file->id}}" id="unverifyReasonModal{{$file->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title">Unverify Reason</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <p class="text-justify">{{$file->unverify_reason}}</p>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default bg-secondary text-white" data-dismiss="modal">Close</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @endif
                                <a target="_blank" title="View" class="btn btn-sm btn-outline-success mr-2" href="{{Storage::url($file->file)}}"><span class="fa fa-eye"></span> View</a>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>

    <div class="col-sm-12 file-listing pt-2">
            <h4 class="pt-2" style="font-size: 18px;">{{$project->topic}}</h4>
            <div class="clearfix"></div>
            <div class="d-flex flex-sm-column mt-3 ">
                <img src="/img/pdf-page.png" alt="pdf" style="width: 40px;height: 40px;margin-left: 15px;margin-right: 10px;">
                <div>
                    <a target="_blank" href="{{Storage::url($file->file)}}">
                        <h5 class="align-self-start mb-0" style="font-size: 15px;">{{$file->name}}</h5>
                        <p class="" style="margin-top: 2px;font-size: 13px;">{{date('F d, Y', strtotime($file->created_at))}}</p>
                    </a>
                </div>
                <div class="float-right" style="position: absolute; right: 30px;">
                    <a href="/files/{{$file->id}}/delete"  onclick="return confirm('Are you sure, You want to delete this file?');" class="btn btn-sm btn-light text-muted"><i class="fa fa-trash"></i></a>
                </div>
            </div>
            <hr class="my-0">
        </div>



        @else
        @if(!count($projects) && !count($project->projectfiles))
        <div class="empty-box row">
            <div class="col-md-12 text-center">
                 <h4 >You don't have any project files yet!</h4>
                 <div class="my-md-5 my-sm-3 no-project-logo">
                    <img src="/img/file.png" alt="files" height="120px">
                 </div>
            </div>
        </div>
        @endif
        @endif
    @endforeach
    @else
        <div class="empty-box row">
            <div class="col-md-12 text-center">
                 <h4 >You don't have any project files yet!</h4>
                 <div class="my-md-5 my-sm-3 no-project-logo">
                    <img src="/img/file.png" alt="files">
                 </div>
            </div>
        </div>
        @endif




</div>

@auth
 <nav class="bottom-nav">
            <a class="bottom-nav__action {{ request()->is('home') ? 'bottom-nav__action--active' : '' }}" href="/home">
                <!-- <svg class="bottom-nav__icon" viewBox="0 0 24 24">
                     <path d="M11,7V12.11L15.71,14.9L16.5,13.62L12.5,11.25V7M12.5,2C8.97,2 5.91,3.92 4.27,6.77L2,4.5V11H8.5L5.75,8.25C6.96,5.73 9.5,4 12.5,4A7.5,7.5 0 0,1 20,11.5A7.5,7.5 0 0,1 12.5,19C9.23,19 6.47,16.91 5.44,14H3.34C4.44,18.03 8.11,21 12.5,21C17.74,21 22,16.75 22,11.5A9.5,9.5 0 0,0 12.5,2Z"></path>
                </svg> -->
                <i class="fa fa-home"></i>
                <span class="bottom-nav__label">Home</span>
             </a>

            <a class="bottom-nav__action {{ request()->is('files') ? 'bottom-nav__action--active' : '' }}" href="/files">
                <!-- <svg class="bottom-nav__icon" viewBox="0 0 24 24">
                    <path d="M12,21.35L10.55,20.03C5.4,15.36 2,12.27 2,8.5C2,5.41 4.42,3 7.5,3C9.24,3 10.91,3.81 12,5.08C13.09,3.81 14.76,3 16.5,3C19.58,3 22,5.41 22,8.5C22,12.27 18.6,15.36 13.45,20.03L12,21.35Z"></path>
                </svg> -->
                <i class="fa fa-files-o"></i>
                <span class="bottom-nav__label">Files</span>
            </a>

            <a class="bottom-nav__action {{ request()->is('profile') ? 'bottom-nav__action--active' : '' }}" href="/profile">
                <!-- <svg class="bottom-nav__icon" viewBox="0 0 24 24">
                   <path d="M12,20A8,8 0 0,1 4,12A8,8 0 0,1 12,4A8,8 0 0,1 20,12A8,8 0 0,1 12,20M12,2A10,10 0 0,0 2,12A10,10 0 0,0 12,22A10,10 0 0,0 22,12A10,10 0 0,0 12,2M12,12.5A1.5,1.5 0 0,1 10.5,11A1.5,1.5 0 0,1 12,9.5A1.5,1.5 0 0,1 13.5,11A1.5,1.5 0 0,1 12,12.5M12,7.2C9.9,7.2 8.2,8.9 8.2,11C8.2,14 12,17.5 12,17.5C12,17.5 15.8,14 15.8,11C15.8,8.9 14.1,7.2 12,7.2Z"></path>
                </svg> -->
                <i class="fa fa-cog"></i>
                <span class="bottom-nav__label">Settings</span>
            </a>
        </nav>
        @endauth
@endsection