@extends('layouts.app')
@section('styles')
<style type="text/css">
#wizard .steps a{
    pointer-events: none;
}
</style>
@endsection

@section('content')

    <div class="my-md-5 my-sm-0">

            <div class="headline d-flex justify-content-between align-items-cente hidden-on-mobile">
                <h3 class="d-inline-block">Projects</h3>
                @if(!auth()->user()->be_writer)
                 <a href="#postProjectModal" data-toggle="modal" id="tour-step-1" class="btn btn-secondary">Post a project</a>
                @endif
            </div>
    @section('heading', 'Projects')

        @if(count($projects))
        <div class="row user-project-details project-list">
            @foreach($projects as $project)
            <div class="col-sm-4 project-card">
                <div class="card my-3">
                    <div class="card-body">
                        <div class="brows-job-type">
                            <span class="full-time {{ $project->project_status == 'New Request' ? 'newrequest' : $project->project_status }} text-capitalize">{{ $project->project_status }}</span>
                        </div>
                        {{-- <span class="tg-themetag tg-featuretag">{{$project->writing_quality}}</span> --}}
                        <!-- <div class="brows-job-company-img mt-3">
                            <img src="/img/com-2.jpg" class="img-responsive" alt="">
                        </div> -->
                        <div class="project-list-detalis">
                            <h4>{{str_limit($project->topic,15)}} </h4>

                            <p class="mt-2">{{str_limit($project->instructions, 75)}}</p>
                            <div class="d-flex justify-content-between align-items-center pt-2">
                            <a href="/projects/{{$project->id}}" class="btn btn-sm btn-outline-success">Manage</a>
                            @if(!auth()->user()->be_writer)
                            <div>
                                @if(!$project->completed)
                                <a href="/projects/{{$project->id}}/edit" class="btn btn-sm btn-light text-muted"><i class="fa fa-pencil"></i></a>
                                @endif
                                @if($project->status == 0 || $project->status == 1 || $project->status == 2)
                                <a href="/projects/{{$project->id}}/delete" onclick="return confirm('Are you sure, You want to delete this project?');" class="btn btn-sm btn-light text-muted"><i class="fa fa-trash"></i></a>
                                @endif
                            </div>
                            @endif
                            </div>
                        </div>
                    </div>
                    <ul class="grid-view-caption">
                        <li>
                            <div class="brows-job-location">
                                <p class="p-0" style="font-size: 12px;"><span><i class="fa fa-calendar-check-o" aria-hidden="true"></i>{{date('F d, Y', strtotime($project->deadline_date))}}</span></p>
                            </div>
                        </li>
                        <li>
                            <p style="font-size: 12px;"><span class="brows-job-location text-uppercase"><i class="fa fa-clock-o" aria-hidden="true"></i>{{$project->deadline_time}}</span></p>
                        </li>
                    </ul>
                </div>
            </div>
            @endforeach
        </div>
        @else
        @if(!auth()->user()->be_writer)
            <div class="empty-box row">
                <div class="col-md-12 text-center">
                     <h4 >You haven't posted any projects yet!</h4>
                     <p>Post a new project and choose an ideal writer on ProjectVala.</p>
                     <div class="my-md-5 my-sm-3 no-project-logo">
                        <img src="/img/typing.png" alt="project">
                     </div>
                     <div class="post-project-btn">
                        <a href="#postProjectModal" data-toggle="modal" class="btn btn-primary btn-lg mt-4">Let's Get Started</a>
                     </div>
                </div>
            </div>
        @else
        <div class="empty-box row">
                <div class="col-md-12 text-center">
                     <div class="no-project-logo">
                        <img src="/img/user/package.png" alt="project">
                     </div>
                     <h4 class="mt-3">You have no projects yet.</h4>
                     <div class="post-project-btn">
                        <!-- <a href="#" class="btn btn-secondary btn-lg mt-4">Post Project</a> -->
                     </div>
                </div>
            </div>
        @endif
        @endif


        {!! $projects->links() !!}
    </div>
@auth
 <nav class="bottom-nav">
            <a class="bottom-nav__action {{ request()->is('home') ? 'bottom-nav__action--active' : '' }}" href="/home">
                <!-- <svg class="bottom-nav__icon" viewBox="0 0 24 24">
                     <path d="M11,7V12.11L15.71,14.9L16.5,13.62L12.5,11.25V7M12.5,2C8.97,2 5.91,3.92 4.27,6.77L2,4.5V11H8.5L5.75,8.25C6.96,5.73 9.5,4 12.5,4A7.5,7.5 0 0,1 20,11.5A7.5,7.5 0 0,1 12.5,19C9.23,19 6.47,16.91 5.44,14H3.34C4.44,18.03 8.11,21 12.5,21C17.74,21 22,16.75 22,11.5A9.5,9.5 0 0,0 12.5,2Z"></path>
                </svg> -->
                <i class="fa fa-home"></i>
                <span class="bottom-nav__label">Home</span>
             </a>

            <a class="bottom-nav__action {{ request()->is('files') ? 'bottom-nav__action--active' : '' }}" href="/files">
                <!-- <svg class="bottom-nav__icon" viewBox="0 0 24 24">
                    <path d="M12,21.35L10.55,20.03C5.4,15.36 2,12.27 2,8.5C2,5.41 4.42,3 7.5,3C9.24,3 10.91,3.81 12,5.08C13.09,3.81 14.76,3 16.5,3C19.58,3 22,5.41 22,8.5C22,12.27 18.6,15.36 13.45,20.03L12,21.35Z"></path>
                </svg> -->
                <i class="fa fa-files-o"></i>
                <span class="bottom-nav__label">Files</span>
            </a>

            <a class="bottom-nav__action {{ request()->is('profile') ? 'bottom-nav__action--active' : '' }}" href="/profile">
                <!-- <svg class="bottom-nav__icon" viewBox="0 0 24 24">
                   <path d="M12,20A8,8 0 0,1 4,12A8,8 0 0,1 12,4A8,8 0 0,1 20,12A8,8 0 0,1 12,20M12,2A10,10 0 0,0 2,12A10,10 0 0,0 12,22A10,10 0 0,0 22,12A10,10 0 0,0 12,2M12,12.5A1.5,1.5 0 0,1 10.5,11A1.5,1.5 0 0,1 12,9.5A1.5,1.5 0 0,1 13.5,11A1.5,1.5 0 0,1 12,12.5M12,7.2C9.9,7.2 8.2,8.9 8.2,11C8.2,14 12,17.5 12,17.5C12,17.5 15.8,14 15.8,11C15.8,8.9 14.1,7.2 12,7.2Z"></path>
                </svg> -->
                <i class="fa fa-cog"></i>
                <span class="bottom-nav__label">Settings</span>
            </a>
        </nav>
        @endauth

        @if(!auth()->user()->be_writer)
        <a href="#postProjectModal" data-toggle="modal" style="display: none;"  class="fab"> + </a>
        @endif
    @include('projects.create')
@endsection


@section('scripts')

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-timepicker/1.10.0/jquery.timepicker.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-timepicker/1.10.0/jquery.timepicker.js"></script>


<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<script>
  $(document).ready(function(){
 
    $('#deadline_date').datepicker({ 
    minDate: 0+2, 
    });


    $('#deadline_time').timepicker({
    'minTime': '00:00am',
    'maxTime': '06:00pm' 
    });

    
    
  });

</script>

    <script type="text/javascript">
        var tour = new Tour({
          steps: [
          {
            element: "#tour-step-1",
            title: "Post Project",
            content: "Click here to add new projects.",
            placement: "left",
            smartPlacement: false,
          },
          {
            element: "#tour-step-2",
            title: "Your Dashboard",
            content: "Click here for details of your projects.",
            placement: "right",
            smartPlacement: false,
          },
          {
            element: "#tour-step-3",
            title: "Files",
            content: "Click here to access files from writers.",
            placement: "right",
            smartPlacement: false,
          },
          {
            element: "#tour-step-3w",
            title: "Files",
            content: "Click here to access files from users.",
            placement: "right",
            smartPlacement: false,
          },
          {
            element: "#tour-step-4",
            title: "Settings",
            content: "Manage your profile and account settings.",
            placement: "right",
            smartPlacement: false,
          },
        ],
        backdrop: true,
        onEnd: function (tour) {
            axios.get('/user/{{auth()->id()}}/tours')
                .then(function (response) {
                })
                .catch(function (error) {
                });
        },
        });

        // Initialize the tour
        tour.init();

        // Start the tour
        @if(!auth()->user()->tour)
            localStorage.removeItem("tour_end");
            localStorage.removeItem("tour_current_step");
            tour.start();
        @endif

$(function () {
  var $sections = $('.form-section');

  function navigateTo(index) {
    // Mark the current section with the class 'current'
    $sections
      .removeClass('current')
      .eq(index)
        .addClass('current');
    // Show only the navigation buttons that make sense for the current section:
    /*$('.wizard-control[href="#previous"]').toggle(index > 0);
    */var atTheEnd = index >= $sections.length - 1;

    /*$('.wizard-control[href="#next"]').toggle(!atTheEnd);
    $('.wizard-control[href="#finish"]').toggle(atTheEnd);*/
  }

  function curIndex() {
    // Return the current index by looking at which section has the class 'current'
    return $sections.index($sections.filter('.current'));
  }

  // Previous button is easy, just go back
  $('.wizard-control [href="#previous"]').click(function() {
    navigateTo(curIndex() - 1);
  });

  // Next button goes forward iff current block validates
  $('.wizard-control[href="#next"]').click(function() {
    $('.frmPostAssignment').parsley().whenValidate({
      group: 'block-' + curIndex()
    }).done(function() {
        // console.log('clicked')
      $("#wizard").steps('next');
      //navigateTo(curIndex() + 1);
    });
  });

  // Prepare sections by setting the `data-parsley-group` attribute to 'block-0', 'block-1', etc.
  $sections.each(function(index, section) {
    $(section).find(':input').attr('data-parsley-group', 'block-' + index);
  });
  navigateTo(0); // Start at the beginning
});

$('#wizard').on('click', '.btn-postProject', function(){
    $(this).addClass('disabled');
    $(this).html('Creating project...')
});

$(function () {
var inputs = document.querySelectorAll( '#postProjectModal .uploadButton-input ' );
  Array.prototype.forEach.call( inputs, function( input )
  {
    var label = input.nextElementSibling.nextElementSibling,
    labelVal = label.innerHTML;

    input.addEventListener( 'change', function( e )
    {
      var fileName = '';

      fileName = e.target.value.split( '\\' ).pop();

      if( fileName )
        label.innerHTML = 'Selected File : <span class="filename">' + fileName + '</span>';
      else
        label.innerHTML = labelVal;
    });
  });
  });


    </script>
    


@endsection