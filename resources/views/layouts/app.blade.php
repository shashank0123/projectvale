<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->

    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
            'userId' => auth()->check() ? auth()->id() : ''
        ]); ?>
    </script>
    <!-- Fonts -->
    <link rel="shortcut icon" href="/img/logo.png">
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/wizard.css') }}">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="fonts/material-design-iconic-font/css/material-design-iconic-font.css">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/material-design-iconic-font.min.css') }}">
    <!-- Styles -->

    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/chat.css') }}">
    <link rel="stylesheet" href="{{ asset('css/jquery.raty.css') }}">
    <link href="{{ asset('css/datedropper.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/datedroppertheme.css') }}" rel="stylesheet">

    <link href="{{ asset('css/timedropper.css') }}" rel="stylesheet">
     <link href="{{ asset('css/responsive.css') }}" rel="stylesheet">

    <link href="{{ asset('css/tour.css') }}" rel="stylesheet">
    <link href="{{ asset('css/croppie.css') }}" rel="stylesheet">
    <!-- <link href="{{ asset('css/dropzone.css') }}" rel="stylesheet"> -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    @yield('styles')
</head>
<body>

    <div id="app">

        @auth
        <header class="app-bar promote-layer paper-toolbar paper-shadow">
            <div class="app-bar-container paper-toolbar">


                <a href="/home"><h1 class="logo"> @yield('heading', 'Projectvala')</h1></a>

                <a href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                    <i class="fa fa-power-off" aria-hidden="true" style="color: #fff;color: #fff;position: absolute;right: 5px;top: 11px;padding: 10px;font-size: 15px;"></i>
                </a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>

            </div>
        </header>
        @endauth


        @auth
         <aside class="app-sidebar custom-sidebar">
             <div class="my-5">
                 <div class="app-logo">
                    <a href="/home">
                     <img src="{{ asset('img/logo.png') }}" alt="logo" style="width: 40px;">
                 </a>
                   <div class="login_user">{{auth()->user()->be_writer ? 'Writer' : 'User'}}</div>
                 </div>
                 <div class="links">
                     <ul class="nav flex-md-column">
                      <li class="nav-item" id="tour-step-2">
                        <a class="nav-link {{ request()->is('home') ? 'active' : ''}}" href="/home">Home</a>
                      </li>
                      <li class="nav-item" id="{{auth()->user()->be_writer ? 'tour-step-3w' : 'tour-step-3'}}">
                        <a class="nav-link {{ request()->is('files') ? 'active' : ''}}" href="/files">Files</a>
                      </li>
                      @if(auth()->user()->be_writer)
                      <li class="nav-item" id="tour-step-5">
                        <a class="nav-link {{ request()->is('wallet') ? 'active' : ''}}" href="/wallet">Wallet</a>
                      </li>
                      @endif
                      <li class="nav-item" id="tour-step-4">
                        <a class="nav-link {{ request()->is('profile') ? 'active' : ''}}" href="/profile/edit">Settings</a>
                      </li>
                       <li class="nav-item">
                        <a class="nav-link"  href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">Logout</a>
                      </li>
                      <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                    </ul>
                 </div>
             </div>
         </aside>


        @endauth

        <main class="app-content {{ auth()->check() ? 'sidebar-margins' : ''}}">
            @include('flash::message')
            @yield('content')
        </main>
    </div>

    <script src="{{ asset('js/app.js') }}"></script>
    <script src="{{ asset('js/datedropper.min.js') }}"></script>
    <script src="{{ asset('js/timedropper.js') }}"></script>
    <script src="{{ asset('js/parsley.min.js') }}"></script>
    <script src="{{ asset('js/jquery.steps.js') }}"></script>
    <script src="{{ asset('js/chart.min.js') }}"></script>
    <script src="{{ asset('js/main.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <script src="{{ asset('js/jquery.raty.js') }}"></script>
    <script src="{{ asset('js/tour.js') }}"></script>
    <script src="{{ asset('js/croppie.js') }}"></script>
    <!-- <script src="{{ asset('js/dropzone.js') }}"></script> -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    @yield('scripts')
    @stack('js')
    <script>
        $('#flash-overlay-modal').modal();
        $('div.alert').not('.alert-important').delay(3000).fadeOut(350);
        $(document).ready(function(){
            $('[data-toggle="popover"]').popover();
            $('.datedropper').dateDropper();
            $( ".timedropper" ).timeDropper({
                meridians : true,
                setCurrentTime : true,
                mousewheel:true,
                autoswitch:true,
                init_animation:'dropdown'
            });
        });

    </script>

</body>

</html>
