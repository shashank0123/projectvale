<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>

    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>
    <!-- Fonts -->
    <link rel="shortcut icon" href="/img/logo.png">
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/wizard.css') }}">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="fonts/material-design-iconic-font/css/material-design-iconic-font.css">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/material-design-iconic-font.min.css') }}">
    <!-- Styles -->

    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/admin.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/jquery.raty.css') }}">
     <link href="{{ asset('css/responsive.css') }}" rel="stylesheet">

    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    @yield('styles')
</head>
<body>

    <div id="app">
         <aside class="app-sidebar custom-sidebar">
             <div class="">
                 <div class="app-logo pl-3 py-3">
                    <a href="/admin">
                        <img src="{{ asset('img/logo.png') }}" alt="logo" style="width: 30px;">
                        Administration
                    </a>
                 </div>
                 <div class="links">
                     <ul class="nav flex-md-column">
                      <li class="nav-item">
                        <a class="nav-link {{ request()->is('admin') ? 'active' : '' }}" href="/admin"><i class="fa fa-dashboard"></i> Dashboard</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link {{ request()->is('admin/users*') ? 'active' : '' }}" href="/admin/users"><i class="fa fa-user-circle"></i> All Users</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link {{ request()->is('admin/writers*') ? 'active' : '' }}" href="/admin/writers"><i class="fa fa-edit"></i> Writers</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link {{ request()->is('admin/verification*') ? 'active' : '' }}" href="/admin/verification/pending"><i class="fa fa-circle-o"></i> Pending Verifications</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link {{ request()->is('admin/projects*') ? 'active' : '' }}" href="/admin/projects"><i class="fa fa-th-list"></i> Projects</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link {{ request()->is('admin/files*') ? 'active' : '' }}" href="/admin/files"><i class="fa fa-file"></i> Files</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link {{ request()->is('admin/payments*') ? 'active' : '' }}" href="/admin/payments"><i class="fa fa-money"></i> Payments</a>
                      </li>
                      <li class="nav-item">
                        <a href="#pageSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle nav-link"><i class="fa fa-globe"></i> Website Content</a>
                        <ul class="collapse list-unstyled" id="pageSubmenu">
                            <li>
                                <a class="nav-link {{ request()->is('admin/subjects*') ? 'active' : '' }}" href="/admin/subjects"><i class="fa fa-book"></i> Subjects</a>
                            </li>
                            <li>
                                <a class="nav-link {{ request()->is('admin/papertypes*') ? 'active' : '' }}" href="/admin/papertypes"><i class="fa fa-th"></i> Paper Types</a>
                            </li>
                            <li>
                                <a class="nav-link {{ request()->is('admin/citations*') ? 'active' : '' }}" href="/admin/citations"><i class="fa fa-link"></i> Citations</a>
                            </li>
                            <li>
                              <a class="nav-link {{ request()->is('admin/sociallinks*') ? 'active' : '' }}" href="/admin/sociallinks"><i class="fa fa-link"></i> Social Links</a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link {{ request()->is('admin/settings*') ? 'active' : '' }}" href="/admin/settings"><i class="fa fa-cog"></i> Settings</a>
                    </li>

                    <li class="nav-item">
                      <a class="nav-link {{ request()->is('admin/durations*') ? 'active' : '' }}" href="/admin/durations"><i class="fa fa-cog"></i> All Durations</a>
                    </li>

                      <li class="nav-item">
                        <a class="nav-link" href="/admin/logout"><i class="fa fa-power-off"></i> Logout</a>
                      </li>
                    </ul>
                 </div>
             </div>
         </aside>
         <main class="app-content sidebar-margins ">
            @include('flash::message')
            @yield('content')
        </main>
    </div>
    <script src="{{ asset('js/parsley.min.js') }}"></script>
    <script src="{{ asset('js/jquery.raty.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    @yield('scripts')
    <script>
        $('div.alert').not('.alert-important').delay(3000).fadeOut(350);
    </script>

</body>

</html>
