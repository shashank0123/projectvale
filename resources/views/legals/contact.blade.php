@extends('layouts.app')
@section('content')
<section>
    <div class="container">
        <div class="row py-5">
            <div class="col-md-12">
                <div class="">
                    <div class="d-flex justify-space-between align-items-center">
                        <a href="/" style="flex: 1">
                            <img class="banner-logo" style="width: 300px;" src="/img/logo-navbar.png" alt="">
                        </a>
                        <div class="d-flex" >

                           <a href="/" style="padding-right: 24px;color: #898989; font-weight: bold;">Home</a>

                           <a href="/terms-of-conditions" style="padding-right: 24px;color: #898989; font-weight: bold;">Terms & Conditions</a>

                           <a href="/privacy-policy" style="padding-right: 24px;color: #898989; font-weight: bold;">Privacy Policy</a>

                           <a href="/contact-us" style="color: #898989; font-weight: bold;">Contact Us</a>

                       </div>
                   </div>


                   <div class="row">
                    <div class="col-sm-5 text-center mt-5 mb-4">
                        <div class=" mb-5">
                            <h4><i class="fa fa-envelope-o text-primary fa-2x mb-2"></i> <br>OUR EMAIL</h4>
                            <p><a href="mailto:info@projectvala.com" class="text-dark">info@projectvala.com</a></p>
                        </div>
                    </div>
                    <div class="col-sm-7">
                        <h1 class="heading-section mt-5 mb-4">
                            GET IN TOUCH
                        </h1>
                        <p class="text-muted mb-4">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                        tempor incididunt ut labore et dolore magna aliqua.</p>
                        <form action="/contact" method="post">
                            <div class="form-group">
                                <input type="text" class="form-control" name="name" required placeholder="Full Name">
                            </div>
                            <div class="form-group">
                                <input type="email" class="form-control" name="email" required placeholder="Email ID">
                            </div>
                            <div class="form-group">
                                <textarea type="text" class="form-control" name="message" required placeholder="Message..."></textarea>
                            </div>
                            <button type="submit" class="btn btn-secondary btn-lg">Send</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
</section>
@endsection
