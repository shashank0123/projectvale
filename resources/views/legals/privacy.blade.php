@extends('layouts.app')
@section('content')
<section>
    <div class="container">
        <div class="row py-5">
            <div class="col-md-12">
                <div class="">
                    <div class="d-flex justify-space-between align-items-center">
                        <a href="/" style="flex: 1">
                            <img class="banner-logo" style="width: 300px;" src="/img/logo-navbar.png" alt="">
                        </a>
                         <div class="d-flex" >

                             <a href="/" style="padding-right: 24px;color: #898989; font-weight: bold;">Home</a>

                            <a href="/terms-of-conditions" style="padding-right: 24px;color: #898989; font-weight: bold;">Terms & Conditions</a>


                            <a href="/privacy-policy" style="padding-right: 24px;color: #898989; font-weight: bold;">Privacy Policy</a>


                            <a href="/contact-us" style="color: #898989; font-weight: bold;">Contact Us</a>

                        </div>
                    </div>
                    <h1 class="heading-section mt-5">
                        Privacy Policy
                    </h1>
                    <hr>
                    <div class="description text-justify" style="font-size: 18px">
                        This Policy determines how we obtain, collect and use information on our website <a href="/">www.projectvala.com</a> (We, the Website or Our Website) or other products and services provided on the website <a href="/">www.projectvala.com</a> (all of the above together referred to as the Services).
                        <br><br>
                        1) By visiting this Website or using the Services, you accept rules and conditions described in this Policy, which could be changed over time. Every time, when you visit the Website or use the Services you express your direct consent to us collecting, using and disclosing information provided by you in accordance with the procedure described in this Policy.

                        <br><br>
                        <h5>2. We collect, register or process any of the following general categories of information:</h5>

                        <ol style="list-style: lower-alpha;">
                            <li>Information about your visits to our website, your use of the website</li>
                            <li>Geographical location</li>
                            <li>Operating system</li>
                            <li>Browser type</li>
                            <li>Information source</li>
                            <li>Visit duration</li>
                            <li>Number of pages viewed</li>
                            <li>Pages requested by your browser from our server (via cookies). </li>
                            <li>Personal information when you are registering and using the Website with using your Facebook ID/ Google/ any other such platform (email, date of the people, sex, place of birth, location (current place of residence).</li>
                        </ol>

                        <br><br>
                        <h5>3. USE OF COOKIES</h5>
                            <a href="/">www.projectvala.com</a> website may gather information about your general Internet use by using cookies. Where used, these cookies are downloaded to your computer automatically. A cookie is a file that is placed on your hard disk by a web page server. Cookies cannot be used to run programs or deliver viruses to your computer. Cookies are uniquely assigned to you and can only be read by a web server in the domain that issued the cookie to you. <a href="/">www.projectvala.com</a> website uses "cookies" to help you personalize your online experience.
                        <br><br>
                        <a href="/">www.projectvala.com</a> website may also gather information such as IP address, internal session ID, user's domain, type of Internet browser, domain of Website. Such information will not identify you personally: it is statistical data about our visitors and their use of our site. This statistical data does not identify any personal details whatsoever.
                        <br><br>
                        One of the primary purposes of cookies is to provide a convenience feature to save your time. The purpose of a cookie is to tell the web server that you have returned to a specific page. For example, if you personalize <a href="/">www.projectvala.com</a> pages, or register with <a href="/">www.projectvala.com</a> site or services, a cookie helps <a href="/">www.projectvala.com</a> to recall your specific information on subsequent visits. This simplifies the process of recording your personal information. When you return to the same <a href="/">www.projectvala.com</a> website, the information you previously provided can be retrieved, so you can easily use the <a href="/">www.projectvala.com</a> features that you customized.
                        <br><br>
                        You have the ability to accept or decline cookies. Most web browsers automatically accept cookies, but you can usually modify your browser setting to decline cookies if you prefer. If you choose to decline cookies, you may not be able to fully experience the interactive features of the <a href="/">www.projectvala.com</a> services or websites you visit.
                        <br><br>
                        Third party links provided on our website may also use cookies, over which <a href="/">www.projectvala.com</a> has no control. Such cookies (if used) would be downloaded once you click on their advertisement banners on our website or visit their sites.
                        <br><br>
                        <h5>4. Our server processes the following specialized information while you are using the Website:</h5>
                        Your IP address is processed for the purpose of demonstration the language version of the Website corresponding to the language of your country. In case if at the moment of using the Website, the language version corresponding to the country of your IP-address is not available, you will see the version of the Website in English.
                        <br><br>
                        Data about your IP address is processed by our server exclusively during the time of your visit of our website. We do not register, do not process for any purpose, other than the above, or do not store any data about your IP-address.
                        <br><br>
                        <h5>5. Payment Information</h5>
                        We will accept available precautions to protect your personal information from unauthorized access, use or disclosure. We will use encrypted connections customers with security protocols (SSL), to protect your credit card data and other data that require a reliable security.
                        <br><br>
                        Payment information, including name, address, data on settlement account and credit card is required for the provision of certain services of Service. This information is used solely for billing purposes, receiving and control of payment. We do not store information about your credit card and automatically not write off bankroll.
                        <br><br>
                        <h5>6. We register and process the following general information provided by you:</h5>
                        - while communicating with us via the feedback form on our Website with the purpose of responding to your requests:
                        <ol style="list-style: lower-alpha;">
                            <li>Name</li>
                            <li>E-mail</li>
                            <li>The contents of your message addressed to us</li>
                        </ol>
                        <br>
                        <h5>7. Distribution and disclosure of information</h5>
                        <a href="/">www.projectvala.com</a> may transfer your personal information and submitted content that <a href="/">www.projectvala.com</a> collects from you to our main server for processing and storing. By submitting your personal data, you agree to this transfer, storing or processing. <a href="/">www.projectvala.com</a> will take all reasonable steps to make sure that your data is treated securely without any loss, misuse or alteration of information and in agreement with this Privacy Policy. <a href="/">www.projectvala.com</a> secures the information you provide on computer servers in a controlled, secure environment, protected from unauthorized access, use or disclosure.
                        <br><br>
                        We do not rent, sell or share your personal information and we will not disclose any of your personally identifiable information to third parties, unless:

                        <ul>
                            <li>We have your permission</li>
                            <li>To provide products or services you've requested</li>
                            <li>To help investigate, prevent or take action regarding unlawful and illegal activities, suspected fraud, potential threat to the safety or security of any person, violations of Acolyte Ventures LLP's terms of use or to defend against legal claims</li>
                            <li>To bill you for goods and services</li>
                            <li>As required by law, such as to comply with a subpoena, or similar legal process</li>
                            <li>When we believe in good faith that disclosure is necessary to protect our rights, protect your safety or the safety of others, investigate fraud, or respond to a government request</li>
                            <li>If we are involved in a merger, acquisition, or sale of all or a portion of our assets, you will be notified via email and/or prominent notice on our Website of any change in ownership or uses of your personal information, as well as any choice you may have regarding your personal information.</li>
                        </ul>
                        <br>
                        We share your information with advertisers on an aggregate basis only
                        <br><br>
                        To protect your privacy and security, we will also take reasonable steps to verify your identity, such as requiring a password and user ID, before granting access to your data.
                        <br><br>
                        While you can terminate your account at any time, your information may remain stored in archive on our servers even after the deletion or termination of your account. However, we will not sell, transfer, or use the information relating to the terminated account of an individual in any way.
                        <br><br>
                        By writing a testimonial you agree that we are allowed to use your testimonials linked to your account or not for marketing purposes. We are also allowed to place your testimonials on pages of <a href="/">www.projectvala.com</a>
                        <br><br>
                        You can always contact support team and ask to block / remove your testimonials if you feel that it may harm your educational profile.
                        <br><br>
                        The transmission of information via the internet is not completely secure and therefore <a href="/">www.projectvala.com</a> cannot guarantee the security of data sent to us electronically and transmission of such data is therefore entirely at your own risk.
                        <br><br>
                        We reserve the right to disclose personal User information subject to lawful demands or if we are bound by a court decision.
                        <br><br>
                        <h5>8. We protect your information in the following way:</h5>
                        We take all possible steps to ensure the safety of your information, which is required for functioning of the Services, on our secure servers;
                        <br><br>
                        You bear responsibility for observing the privacy of your passwords and safety of your keys stored on your device.
                        <br><br>
                        <h5>9. Third party websites:</h5>
                        Our website may, from time to time, contain links to and from the websites. If you follow any links to a third-party website, keep in mind that these sites have their own privacy policies and that we do not assume any responsibility or obligations under such policies. Please examine these policies prior to transferring any personal data to other websites.
                        <br><br>
                        <h5>10. Access to information:</h5>
                        Our website does not possess the capability to access your personal information on your personal computer.
                        <br><br>
                        <h5>11. Our relations with advertisers:</h5>
                        We do not disclose your data to our advertisers, however, we are entitled to provide summarized information about our users for the purpose of demonstrating to advertisers the characteristics of the target audience.
                        <br><br>
                        We are also entitled to use summarized information to provide assistance to advertisers in attracting the target audience.
                        <br><br>
                        If you have agreed to receive advertisements by email or via SMS from us or our partners, you can unsubscribe at any time by sending us an email via the Website of the Company or software developed by the Company.
                        <br><br>
                        <h5>12. Changes to our Privacy Policy:</h5>
                        We may revise this Privacy Policy from time to time. We reserve the right to update or change this Privacy Policy or any other policies or procedures at any time with or without prior notice. However, we will not use your Personal information in any way that would substantially differ from the one described in this Policy without providing you with a possibility to exclude such use. We will publish revised Policy on the Website to provide users with the possibility to see what kinds of information we collect, how it is used, and under which circumstances it can be disclosed. You agree to re-read the policy from time to time to stay updated. Your further use of Website shall be viewed as your consent to any changes and conditions of our Policy. If you do not agree to these conditions, you should not use the Website or any other Service
                        <br><br>
                        <div class="font-weight-bold">This privacy policy was last updated on 26th November 2018.</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection