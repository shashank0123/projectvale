@extends('layouts.app')
@section('content')
<section>
    <div class="container">
        <div class="row py-5">
            <div class="col-md-12">
                <div class="">
                    <div class="d-flex justify-space-between align-items-center">
                        <a href="/" style="flex: 1">
                            <img class="banner-logo" style="width: 300px;" src="/img/logo-navbar.png" alt="">
                        </a>
                        <div class="d-flex" >

                           <a href="/" style="padding-right: 24px;color: #898989; font-weight: bold;">Home</a>

                           <a href="/terms-of-conditions" style="padding-right: 24px;color: #898989; font-weight: bold;">Terms & Conditions</a>


                           <a href="/privacy-policy" style="padding-right: 24px;color: #898989; font-weight: bold;">Privacy Policy</a>


                           <a href="/contact-us" style="color: #898989; font-weight: bold;">Contact Us</a>

                       </div>
                   </div>
                   <h1 class="heading-section mt-5">
                    Terms & Conditions
                </h1>
                <hr>
                <div class="description text-justify" style="font-size: 18px">
                    This Website User Agreement and the Privacy Policy (hereinafter referred to as the “Agreement”) lays out the terms and conditions and rules, as maybe amended and supplemented, from time to time (“Terms”) which shall be applicable to the access and use of the website of Acolyte Ventures LLP (hereinafter referred to as the Company) i.e. htpps://projectvala.com (“Website”) and various other URLs channelized through the Website by you, the visitor/ user (“User”) of the Website.
                    <br><br>
                    <h5>1. ACCEPTANCE OF TERMS AND MODIFICATION THEREOF</h5>
                    <div class="pl-4">
                        <br>
                        1.1.  This Agreement constitutes a binding contract between User and the Company, with regard to the access and use of the Website.
                        <br><br>
                        1.2. The Company reserves the right to change the terms, conditions and notices pursuant to which the Website is accessed by the User or any services/information (hereinafter referred to as “Services”) that are offered through the Website, without any notice or intimation of such change. The User shall be responsible for regularly reviewing the terms and conditions of this Agreement. Any Changes to this Agreement will be effective when posted and the User agrees to review this Agreement periodically to become aware of any changes.
                        <br><br>
                        1.3. The User agrees that this Agreement forms a valid contract between the User and the Company, and that the Company may, at its sole discretion, amend any of the use or Services being provided by it vide the Website and/ or this Agreement either wholly or partially, at any time and without the requirement of any prior notice or consent.
                        <br><br>
                        1.4. The User expressly agrees that he will be deemed to have consented to the disclosure of any data, information of the User to, and same is used by the Company, a subsequent owner or operator of the Website, of any information about the User contained in the applicable Company database, to the extent the Company assigns its rights and obligations regarding such information in connection with a merger, acquisition, or sale of all or substantially all of the Company's assets, or in connection with a merger, acquisition or sale of all or substantially all of the assets related to this particular Website to a subsequent owner or operator. In the event of such a merger, acquisition, or sale, the User’s continued use of the Website signifies the User’s agreement to be bound by the terms of use, Website User Agreement, Privacy Policy and Disclaimer or otherwise of the Website's subsequent owner or operator.
                        <br><br>
                        1.5. The Company reserves the right, in its sole discretion, to terminate the access to the Website and/or any of the Services or any portion thereof at any time, without any prior notice.
                        <br><br>
                        1.6. The headings and subheadings herein are included for convenience and identification only and are not intended to describe, interpret, define or limit the scope, extent or intent of this Agreement or the right to use the Website by the User as contained herein or any other section or pages of the Website in any manner whatsoever.
                        <br><br>
                        1.7. The terms and conditions herein shall apply equally to both the singular and plural form of the terms defined. Whenever the context may require, any pronoun shall include the corresponding masculine and feminine. The words "include", "includes" and "including" shall be deemed to be followed by the phrase "without limitation". Unless the context otherwise requires, the terms "herein", "hereof", "hereto", "hereunder" and words of similar import refer to this Agreement as a whole.
                    </div>
                    <br><br>
                    <h5>2. LIMITED USER</h5>
                    <div class="pl-4">
                        <br>
                        2.1. The User agrees that given the nature of the Internet, even though the Website is targeted to Indian Residents, Non-Resident Indian (NRI), and Person of Indian Origin (PIO) only, it may be accessed in other parts of the world. The material/information on the Website is not intended for use by persons located in, or residents in countries that restrict the distribution of such material/information or by any person in any jurisdiction where distribution or use of such material/information or usage or access of Website will be contrary to law or any regulation. It shall be the responsibility of every User to be aware of and fully observe the applicable laws and regulations of the jurisdiction which User is subject of. If the User is not an Indian resident, NRI or PIO and yet uses the Website, he acknowledges, understands and agrees that he is doing so on his own initiative and at his own risk and the Company shall not be liable for violation/breach of any of the laws applicable to usage of the Website. The Website is not to be and should not be construed as purporting to offer or inviting to offer any information or services to residents of countries where the Company is not licensed or authorized to perform its business activities. If the User is not an Indian resident, by using this Website and/or submitting his personally identifiable information or any other information on the Website, he expressly consents to the transfer of such data to India, and to the processing of such data on the Company’s Indian servers, where his data will be governed by Indian laws that may provide a level of data protection different than his country.
                        <br><br>
                        2.2 The User further agrees and undertakes not to reverse engineer, modify, copy, distribute, transmit, display, perform, reproduce, publish, license, create derivative works from, transfer, or sell any information, software, products, services or intellectual property obtained from the Website in any manner whatsoever. Reproduction, copying of the content for commercial or non‐commercial purposes and unwarranted modification of data and information within the content of the Website is strictly not permitted without prior written consent from the Company and/or third-party owners. However, some of the content of our services or other files may be made available for download from the website which is permitted to be copied and/or used only for personal purposes of the User. The User and/or any third party is prohibited from running or displaying this Website and /or information displayed on this Website on any other Website or frames, without prior written consent from the Company.
                    </div>
                    <br><br>
                    <h5>3. Disclaimer of Warranties</h5>
                    <div class="pl-4">
                        <br>
                        3.1. The Company has endeavored to ensure that all the information provided by it on this Website is correct, but the Company neither warrants nor makes any representations regarding the quality, accuracy or completeness of any data or information displayed on this Website and the Company shall not be, in any manner liable for inaccuracy/error if any. The Company makes no warranty, express or implied, concerning the Website and/or its contents and disclaims all warranties of fitness for a particular purpose and warranties of merchantability in respect of information displayed and communicated through or on the Website, including any liability, responsibility or any other claim, whatsoever, in respect of any loss, whether direct or consequential, to any User or any other person, arising out of or from the use of any such information as is displayed or communicated through or on the Website or the provision of the Services.
                        <br><br>
                        3.2. Although the Company makes commercial efforts, reasonable or otherwise to ensure that the description and content of the information displayed or communicated to the User on each page of the Website is correct, it does not, however, take responsibility for changes that may have occurred due to human or data entry errors or for any loss or damages suffered by any User due to any such changed information.
                        <br><br>
                        3.3. The Company does not endorse any advertiser on its Website in any manner. The Users are requested to verify the accuracy of all information on their own before undertaking any reliance on such information.
                        <br><br>
                        3.4. Neither shall the Company be responsible for the delay or inability to use the Website or the Services, the provision of or failure to provide the Services, or for any information, software, products, services and related graphics obtained from the Company through the Website, whether based on contract, tort, negligence, strict liability or otherwise. Further, the Company shall not be held responsible for non-availability of the Website during periodic maintenance operations or any unplanned suspension of access to the Website and/ or the Services that may occur due to technical reasons or for any other reason whatsoever. The User understands and agrees that any material and/or data downloaded or otherwise obtained from the Company through the Website is done entirely at his discretion and risk and he will be solely responsible for any damage to his computer systems or any other loss that results from such material and/or data.
                        <br><br>
                        3.5. These limitations, disclaimer of warranties and exclusions apply without regard to whether the damages arise from (a) breach of contract, (b) breach of warranty, (c) negligence, or (d) any other cause of action, to the extent such exclusion and limitations are not prohibited by applicable law.
                    </div>
                    <br><br>
                    <h5>4. Links to Third-Party Sites</h5>
                    <div class="pl-4">
                        <br>
                        4.1. The Website may contain links to other websites or may contain features of any nature of other websites on the Website ("Linked Sites"). The Linked Sites are not under the control of the Company or the Website and the Company is not responsible for the contents of any Linked Site, including without limitation any link or advertisement contained in a Linked Site, or any changes or updates to a Linked Site. The Company is not responsible for any form of transmission, whatsoever, received by the User from any Linked Site. The inclusion of any link does not imply endorsement of any nature by the Company or the Website of the Linked Sites or any association with its operators or owners.
                        <br><br>
                        4.2. The Company is not responsible for any errors, inclusions, omissions or representations on any Linked Site, or on any link contained in a Linked Site. The User is requested to verify the accuracy of all information on his own before undertaking any reliance on such information of such products/ services.
                    </div>
                    <br><br>
                    <h5>5. Use of Communication Services</h5>
                    <div class="pl-4">
                        <br>
                        5.1. The Company does not control or endorse the content, messages or information found in any Communication Service and, therefore, the Company specifically disclaims any liability or responsibility whatsoever with regard to the Communication Services and any actions resulting from the User's participation in any Communication Service.
                        <br><br>
                        5.2. When User registers with the Website, User consents that the Company or any of its partners/ affiliate/ group companies or third party service providers may contact User from time to time to provide the offers/ information of such products/ services that they believe may benefit User.
                        <br><br>
                        5.3. The Company reserves the right at all times to disclose any information as is necessary to satisfy or comply with any applicable law, regulation, legal process or governmental request, or to edit, refuse to post or to remove any information or materials, in whole or in part, in the Company’s sole discretion.
                    </div>
                    <br><br>
                    <h5>6. User’s Obligations</h5>
                    <div class="pl-4">
                        <br>
                        6.1. As a condition of access and use of the Website, the User warrants that he will not use the Website for any purpose that is unlawful or illegal under any law for the time being in force within or outside India or prohibited by this Agreement. In addition, the Website shall not be used in any manner, which could damage, disable, overburden or impair it or interfere with any other party's use and/or enjoyment of the Website or infringe any intellectual property rights of the Company or any third party.
                        <br><br>
                        6.2. The User represents and confirms that the User is of legal age to enter into a binding contract and is not a person barred from accessing and using the Website and availing the Services under the laws of India or other applicable law.
                        <br><br>
                        6.3. The User acknowledges and agrees that:
                        <br>
                        <br>
                        <ol>
                            <li>As between User on one hand, and the Company and its Affiliates on the other, the Company or the Affiliates own all right, title and interest in the Company and the Company’s Brand Features.</li>
                            <li>Nothing in this Agreement shall confer in the User any license or right of ownership in the Company Brand Features and/or the Website or its contents.</li>
                            <li>The data provided by the User shall be retained by the Company till the time it may be required for providing the Company’s services or any other additional products/services of the Company and its affiliates.</li>
                        </ol>
                        <br><br>
                        6.4. To avail a Service through the Website, the User has and must continue to maintain at his sole cost:
                        <div class="pl-4">
                            <br>
                            6.4.1. All the necessary equipments including a computer and modem etc. to access the Website and/or avail the Services;
                            <br><br>
                            6.4.2. Own access to the World Wide Web. The User shall be responsible for accessing the Website and/or availing the Services which may involve third party fees including, airtime charges or internet service provider’s charges which are to be exclusively borne by the User
                            <br><br>
                            6.4.3. The User also understands that the Services may include certain communications from the Company as service announcements and administrative messages. The User understands and agrees that the Services are provided on an "as-is-where-is" basis and that the Company does not assume any responsibility for deletions, mis-delivery or failure to store any User communications or personalized settings.
                        </div>
                    </div>
                    <br><br>
                    <h5>7. Procedure Manual for the User</h5>
                    <div class="pl-4">
                        <br>
                        7.1. Registration of the User on the Website is mandatory. Upon completion of the registration process, the User shall receive a User id and password. The User agrees and undertakes at all times to be responsible for maintaining the confidentiality of the password and User id, and shall be fully responsible for all activities that occur by use of such password or User id. Further, the User agrees not to use any other party's under id and password for any purpose whatsoever without proper authorization from such party. The User is responsible for the security of his password and for all transactions undertaken using his password. The password entered by the User is transmitted in one- way encrypted form to the Company’s database and stored as such. The Company will not be responsible for any financial loss or any other loss, inconvenience or mental agony resulting from misuse of your ID / password / account details.
                        <br><br>
                        7.2. The User also agrees and undertakes to immediately notify the Company of any unauthorized use of the User's password or User id and to ensure that the User logs off at the end of each session at the Website. The Company shall not be responsible for any, direct or indirect, loss or damage arising out of the User's failure to comply with this requirement.
                        <br><br>
                        7.3. The User also agrees to: (a) provide true, accurate and complete information about himself and his beneficiaries as prompted by the registration form ("Registration Data") on the Website; and (b) Upon registration the Company will permit the User to review the information/data provided by User and it shall be User’s obligation to maintain and promptly update the Registration Data to keep it true, accurate, current and complete. If the User provides any information that is untrue, inaccurate, not current or incomplete or the Company has reasonable grounds to suspect that the Registration Data or any part thereof is untrue, inaccurate, not current or incomplete and the Company shall not be responsible for authenticity of data provided by User. The Company has the right to suspend or terminate the User's registration and refuse any and all current or future use of the Website and/or any of the Company Services where is found that User had wilfully provided any inaccurate information/data.
                        <br><br>
                        7.4. A Writer specializing in a field chosen by the User, contacts the User right after the job was assigned. The Writer is responsible for obtaining the detailed list of instructions for the job to be successfully completed. Once a Writer's bid is accepted, the User deposits the agreed amount of money through our payment portal.
                        <br><br>
                        7.5. The User is not allowed to disclose and share any personal information (including e-mail address or phone number) with the Writers, on the messaging system of the Website, for whatsoever purposes. Any Message containing fraudulent words and word-phrases will be automatically blocked by our system and will not be visible to the Writer. In case the Website understands that personal information is being exchanged, the order will be automatically proceed to “Finished” with no further investigation or compensation option. We consider such cases as an attempt to scam our service.
                        <br><br>
                        7.4. Furthermore, the User grants the Company the right to disclose to third parties the Registration Data to the extent necessary for the purpose of providing services by the Company, its affiliates and third party service providers.
                    </div>
                        <br><br>
                        <h5>8. Confidentiality Clause</h5>
                    <div class="pl-4">
                        <br>
                        8.1. The Parties hereby agree to keep the data, information and terms of the contract (“Confidential Information”) confidential and shall not disclose the same without the consent of the other Party.
                        <br><br>
                        8.2. The Parties shall not be liable for breach of the above clause when the Confidential Information is disclosed in pursuant to an order of any court or government authority and/or, in performance of this contract by the Company and/or when the parties independently obtain from third party.
                    </div>
                        <br><br>
                        <h5>9. Payment</h5>
                        <div class="pl-4">
                            <br>9.1. The User must make the payment to the Company as soon as the order for the product is placed.
                        <br><br>
                        9.2. After the final document is uploaded on the Website, the User has 3 days to send the Company any complaints or revision requests. If there are no complaints sent by the User using any of the available methods of contact, the money will be released to the writer herewith automatically.
                        <br><br>
                        9.3. Any challenge to a payment that a User files with the debit/credit card issuer or an financial institution, and any subsequent reversal instruction, is made by the payment product issuer or third parties (such as payment processors) and not by the Company, the Company is bound to follow such instructions.
                        <br><br>
                        9.4. The User acknowledges and agree that the Company shall be entitled to recover any chargebacks and reversals that may be imposed on the Company by a payment product issuer or third parties (such as payment processors) on funds paid to the Writers by the User through the Website, as well as any processing or any other fees whatsoever incurred by the Company on those chargebacks and reversals.
                        <br><br>
                        9.5. The User is responsible for paying any taxes, including any services or value added taxes, which may be applicable depending on the jurisdiction of the services provided.
                        <br><br>
                        9.6. In cases wherein the User is not satisfied with any of the products, the User can submit a refund request within the Refund Period (within 3days of the delivery of the product) and no refunds will be processed once the refund period elapses.
                        </div>

                        <br><br>
                        <h5>10. Breach</h5>
                        <div class="pl-4">
                            <br>
                            10.1. Without prejudice to the other remedies available to the Company under this Agreement or under applicable law, the Company may limit the User's activity, warn other Users of the User's actions, immediately temporarily / indefinitely suspend or terminate the User's registration, and/or refuse to provide the User with access to the Website if:
                            <br><br>
                            <ol>
                                <li>The User is in breach of this Agreement and/or the documents it incorporates by reference;</li>
                                <li>The Company is unable to verify or authenticate any information provided by the User; or</li>
                                <li>The Company believes that the User's actions may infringe on any third party rights or breach any applicable law or otherwise result in any liability for the User, other Users of the Website and/or the Company.</li>
                            </ol>
                            <br>
                            10.2. The Company may at any time in its sole discretion reinstate suspended Users. Once the User has been indefinitely suspended the User may not register or attempt to register with the Company or use the Website in any manner whatsoever until such time that the User is reinstated by the Company. Notwithstanding the foregoing, if the User breaches this Agreement or the documents it incorporates by reference, the Company reserves the right to recover any amounts due and owing by the User to the Company and to take strict legal action as the Company deems necessary.
                        </div>
                        <br><br>
                        <h5>11. License and Proprietary Rights</h5>
                        <div class="pl-4">
                            <br>
                            11.1. The content of the Website and all copyrights, patents, trademarks, service marks, trade names and all other intellectual property rights therein are owned by the Company and/or its licensors and are protected by applicable Indian and international copyright and other intellectual property law. The User acknowledges, understands and agrees that he shall not have, nor be entitled to claim, any rights in and to the Website content and/or any portion thereof.
                            <br><br>
                            11.2. The Company may provide the User with content including information, sound, photographs, graphics, video or other material through the Website. This material may be protected by copyrights, trademarks or other intellectual property rights and laws. The User may use this material only as expressly authorized by the Company and shall not copy, transmit or create derivative works of such material without express authorization from the Company.
                            <br><br>
                            11.3. The User agrees not to copy, reproduce, duplicate, stream, capture, access through technology or means other than those provided on the Website, perform, transfer, sell, resell, download, upload, archive, license to others, edit, modify, manipulate, create derivative works from or based upon (including, without limitation, mash-ups, montages, wallpapers, ringtones, greeting cards, or other merchandise), publish, republish, post, transmit, publicly display, frame, link from or to, distribute, share, embed, translate, decompile, reverse engineer, translate, incorporate into any hardware or software application, use for commercial purposes, or otherwise use or exploit the Website or any component part thereof. Any of the unauthorized uses referred to above would constitute an infringement of the copyrights and other proprietary rights of the Company and/or its licensors (including, without limitation, other Website Users who have submitted their own information) and a violation of this Agreement and may subject User to civil and/or criminal liability under applicable laws.
                            <br><br>
                            11.4. The products delivered by the Writer engaged by the User shall be as per the specifications explained to the Writer. The User shall own the full copyright pertaining to products and other materials delivered. If the User finds the Writer attempting to pass on plagiarized content as original, the same might be rejected and payment may be withheld. Further, the User has an option to request revision within 12 business days from the delivery of the product.
                            <br><br>
                            11.5. Neither the Company nor any of its affiliates and / or partners shall be liable for any inappropriate, illegal, unethical, or otherwise wrongful use of the products and / or other written material received from Writers. This includes plagiarism, expulsion, academic probation, loss of scholarships / titles / awards / prizes / grants / positions, lawsuits, poor grading, failure, suspension, or any other disciplinary or legal actions. The User can seek revisions of the product if it does not meet the specifications provided therein.
                            <br><br>
                            11.6. In the event that the sources required for the order are rare, obscure, not open sourced, or readily available to the general public, it is the responsibility of the User who hereby agrees to provide those materials at the time of placing the order. Failure to do so will result in a violation of these Terms and Conditions and the Company may in its sole discretion to initiate appropriate action, including, but not limited to, refusal to complete your order at all or within the agreed time frame, refusal to fully comply with agreed page requirements of your order, and/or refusal to offer free revisions and/or any refund.
                        </div>
                        <br><br>
                        <h5>12. Limitation of Liability and Injunctive Relief</h5>
                        <div class="pl-4">
                            <br>
                            12.1. THE USER UNDERSTANDS AND EXPRESSLY AGREES THAT THE COMPANY PROVIDES SERVICES ON AN “AS IS” BASIS AND TO THE EXTENT PERMITTED UNDER APPLICABLE LAWS, IN NO EVENT WILL THE COMPANY OR ANY OF ITS AFFILIATES OR GROUP COMPANIES OR ANY OF THEIR RESPECTIVE OFFICERS, EMPLOYEES, DIRECTORS, SHAREHOLDERS, AGENTS, OR LICENSORS BE LIABLE TO USER OR ANYONE ELSE UNDER ANY THEORY OF LIABILITY (WHETHER IN CONTRACT, TORT, STATUTORY, OR OTHERWISE) FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, CONSEQUENTIAL OR EXEMPLARY DAMAGES, INCLUDING BUT NOT LIMITED TO, DAMAGES FOR LOSS OF REVENUES, PROFITS, GOODWILL, USE, DATA OR OTHER INTANGIBLE LOSSES (EVEN IF SUCH PARTIES WERE ADVISED OF, KNEW OF OR SHOULD HAVE KNOWN OF THE POSSIBILITY OF SUCH DAMAGES), RESULTING FROM THE USER’S USE (OR THE USE OF ANYONE USING AN ACCOUNT REGISTERED TO THE USER) ON THE WEBSITE OR ANY PARTS THEREOF.
                            <br><br>
                            12.2. NOTWITHSTANDING ANYTHING TO THE CONTRARY HEREIN, THE USER HEREBY IRREVOCABLY WAIVES ANY RIGHT OR REMEDY TO SEEK AND/OR OBTAIN INJUNCTIVE OR OTHER EQUITABLE RELIEF OR ANY ORDER WITH RESPECT TO, AND/OR TO ENJOIN OR RESTRAIN OR OTHERWISE IMPAIR IN ANY MANNER, THE PRODUCTION, DISTRIBUTION, EXHIBITION OR OTHER EXPLOITATION OF ANY THE COMPANY OR ANY OF ITS AFFILIATE OR GROUP COMPANY RELATED SERVICES, OR THE USE, PUBLICATION OR DISSEMINATION OF ANY ADVERTISING IN CONNECTION WITH SUCH SERVICES.
                        </div>
                        <br><br>
                        <h5>13. Indemnification</h5>
                        <div class="pl-4">
                            <br>
                            13.1. The User agrees to indemnify, defend and hold harmless the Company, its affiliates, group companies and their directors, officers, employees, agents, third party service providers, and any other third party providing any service to the Company in relation to the Services whether directly or indirectly, from and against any and all losses, liabilities, claims, damages, costs and expenses (including legal fees and disbursements in connection therewith and interest chargeable thereon) asserted against or incurred by the Company that arise out of, result from, or may be payable by virtue of, any breach or non-performance of any terms of this Agreement including any representation, warranty, covenant or agreement made or obligation to be performed by the User pursuant to this Agreement.
                        </div>
                        <br><br>
                        <h5>14. Severability</h5>
                        <div class="pl-4">
                            <br>
                            14.1. If any provision of this Agreement is determined to be invalid or unenforceable in whole or in part, such invalidity or unenforceability shall attach only to such provision or part of such provision and the remaining part of such provision and all other provisions of this Agreement shall continue to be in full force and effect.
                        </div>
                        <br><br>
                        <h5>15. Restriction of Usage and Access to Website</h5>
                        <div class="pl-4">
                            <br>
                            15.1. The User agrees that the Company may under certain circumstances and without prior notice, immediately restrict the User's usage of and/or limit access to the Website/ Company Services. Causes for restriction and /or limitation may include, but shall not be limited to, breach by the User of this Agreement, requests by enforcement or government agencies, requests by the User.
                            <br><br>
                            15.2. Should the User object to any terms and conditions of this Agreement or become dissatisfied with any of the Company’s Service in any way or wishes to withdraw any consent previously given, the User's should immediately:
                            <br><br>
                            <ol>
                                <li>discontinue use of the Website/ Company Service; and notify the Company of such discontinuance by written communication or by sending e-mail from registered e-mail address of the User</li>
                                <li>Upon such communication by the User, the Company shall have a right to put a restriction or limitation on use the Website/ Services and software by the User. The User shall have no right and the Company shall have no obligation thereafter to execute any of the User's uncompleted tasks or forward any unread or unsent messages to the User or any third party. Once the User's usage of and /or access to website has been restricted and/or limited any data that the User had stored on the Website may not be available for the User’s disposal.</li>
                            </ol>
                        </div>
                        <br><br>
                        <h5>16. Complaints</h5>
                        <div class="pl-4">
                            <br>16.1. In the event User has any grievance in relation to any information uploaded on the Website or with respect to processing of information or use of Website , User may contact our Customer Care provided at the Website ; or write at the address provided at the Website. The Customer Care shall endeavour to expeditiously redress the grievances, but within reasonable time as may be provided in applicable laws.
                        </div>
                        <br><br>
                        <h5>17. Force Majeure</h5>
                        <div class="pl-4">
                            <br>17.1 The Company shall not be liable for any failure to perform any of its obligations under this Agreement or provide the Services or any part thereof if the performance is prevented, hindered or delayed by a Force Majeure Event (defined below) and in such case its obligations shall be suspended for so long as the Force Majeure Event continues.
                        <br><br>
                        17.2. “Force Majeure Event” means any event due to any cause beyond the reasonable control of any Party, including, without limitation, unavailability of any communication system, breach or virus in the systems, fire, flood, explosion, acts of God, civil commotion, riots, insurrection, war, acts of government.
                        </div>
                        <br><br>
                        <h5>18. Governing Law</h5>
                        <div class="pl-4">
                            <br>
                            18.1. This Agreement shall be governed by and constructed in accordance with the laws of India without reference to conflict of laws principles. In the event any dispute in relation hereto is brought by the User, it shall be subject to the exclusive jurisdiction of the courts of Jaipur, India.
                        </div>
                        <br><br>
                    <div class="font-weight-bold">This user agreement was last updated on 26th November 2018.</div>
                </div>
            </div>
        </div>
    </div>
</div>
</section>
@endsection