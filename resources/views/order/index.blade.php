@extends('layouts.app')

@section('content')

<section>
	<div class="container">
		<div class="row mt-5">
			<div class="col-md-6">
				<div class="order-heading">
					<h2><b>My Orders</b></h2>
				</div>
			</div>
			<div class="col-md-6">
				<div class="order-now-button">
					<button class="btn btn-secondary float-right btn-lg">Place Order</button>
				</div>
			</div>
		</div>
		<div class="row my-4">
			<div class="col-md-12">
				<div class="card">
					<div class="card-body">
						<table class="table mt-3 custom-table-order">
							<thead class="">
								<tr>
									<th scope="col">Topic & Order ID</th>
									<th scope="col">Due Date</th>
									<th scope="col">Price</th>
									<th scope="col">Status</th>
									<th scope="col">Assigned writer</th>
									<th scope="col">Action</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<th scope="row">
										<h6>Sample Assignment</h6>
										<p>Order ID: 1220279</p>
									</th>
									<td>
										<h6>22 Oct</h6>
										<p>2018</p>
									</td>
									<td>
										<h6>$22</h6>
									</td>
									<td>
										<h6>Cancelled</h6>
										<p>by system</p>
									</td>
									<td>
										<h6>Dr. Louisa (PhD)</h6>
									</td>
									<td>
										<a class="btn btn-primary" href="/order/show">View</a>
									</td>
								</tr>
							</tbody>
							<tbody>
								<tr>
									<th scope="row">
										<h6>Sample Assignment</h6>
										<p>Order ID: 1220279</p>
									</th>
									<td>
										<h6>22 Oct</h6>
										<p>2018</p>
									</td>
									<td>
										<h6>$22</h6>
									</td>
									<td>
										<h6>Cancelled</h6>
										<p>by system</p>
									</td>
									<td>
										<h6>Dr. Louisa (PhD)</h6>
									</td>
									<td>
										<a class="btn btn-primary" href="/order/show">View</a>
									</td>
								</tr>
							</tbody>
							<tbody>
								<tr>
									<th scope="row">
										<h6>Sample Assignment</h6>
										<p>Order ID: 1220279</p>
									</th>
									<td>
										<h6>22 Oct</h6>
										<p>2018</p>
									</td>
									<td>
										<h6>$22</h6>
									</td>
									<td>
										<h6>Cancelled</h6>
										<p>by system</p>
									</td>
									<td>
										<h6>Dr. Louisa (PhD)</h6>
									</td>
									<td>
										<a class="btn btn-primary" href="/order/show">View</a>
									</td>
								</tr>
							</tbody>
							<tbody>
								<tr>
									<th scope="row">
										<h6>Sample Assignment</h6>
										<p>Order ID: 1220279</p>
									</th>
									<td>
										<h6>22 Oct</h6>
										<p>2018</p>
									</td>
									<td>
										<h6>$22</h6>
									</td>
									<td>
										<h6>Cancelled</h6>
										<p>by system</p>
									</td>
									<td>
										<h6>Dr. Louisa (PhD)</h6>
									</td>
									<td>
										<a class="btn btn-primary" href="/order/show">View</a>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

@endsection