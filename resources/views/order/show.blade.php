@extends('layouts.app')

@section('content')

<section>
	<div class="container">
		<div class="view-order-section my-5">
			<div class="row">
				<div class="col-md-6">
					<div class="order-heading">
						<h2><b>Order ID: 1220279</b></h2>
					</div>
				</div>
				<div class="col-md-6">
					<div class="order-now-button">
						<a class="btn btn-secondary float-right btn-lg">Place Order</a>
					</div>
				</div>
			</div>
			<div class="card mt-3">
				<div class="card-body">
					<div class="row">
						<div class="col-md-6">
							<h6 class="pt-3">Sample Assignment</h6>
						</div>
						<div class="col-md-6">
							<div class="drag-drop-upload">
								<form action="/file-upload" class="dropzone">
	  								<div class="fallback">
	  									<span><i class="fa fa-upload" aria-hidden="true"></i>
	    							</span>
 										<input name="file" type="file" multiple />
	  								</div>
								</form>
							</div>
						</div>
					</div>
					<div class="row py-4">
						<div class="col-md-3">
							<div class="sample-assignemt">
								<span>Subject</span>
								<h6>Business and Entrepreneurship</h6>
							</div>
							<br>
							<div class="sample-assignemt">
								<span>Number of cited resources</span>
								<h6>1</h6>
							</div>
							<br>
							<div class="sample-assignemt">
								<span>Format of citation</span>
								<h6>Other</h6>
							</div>
							<br>
						</div>
						<div class="col-md-3">
							<div class="sample-assignemt">
								<span>Type of paper</span>
								<h6>Assignment</h6>
							</div>
							<br>
							<div class="sample-assignemt">
								<span>Number of pages</span>
								<h6>2 pages / 550 words</h6>
							</div>
							<br>
							<div class="sample-assignemt">
								<span>Writer quality</span>
								<h6>Standard</h6>
							</div>
							<br>
						</div>
						<div class="col-md-6">
							<div class="sample-assignemt">
								<span>Paper instructions</span>
								<h6>Instructions will be uploaded later.</h6>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="card mt-3">
				<div class="card-body">
					<h4>Order status</h4>
					<div class="row pt-4 pb-2">
						<div class="col-md-3">
							<div class="cancelled-system">
								<a href="#" class="btn">Cancelled by System</a>
							</div>
						</div>
						<div class="col-md-3">
							<div class="sample-assignemt">
								<span>Due Date</span>
								<h6 style="font-size: 15px;">Oct 22 2018 • 5PM</h6>
							</div>
						</div>
						<div class="col-md-6">
							<div class="reserved-money float-right">
								<p>Reserved money $0.00</p>
								<div class="compensation_form_imitation" id="js-tour-step-3">
    								<input class="b-release-money__input" type="text" value="0%" disabled="" readonly="">
    								<button class="b-release-money__button btn" disabled="">Release money    </button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="card mt-3">
				<div class="card-body">
					<div class="row">
						<div class="col-md-9">
							<div class="uploaded-writter-file">
								<h4>List of uploaded files by writer</h4>
								<p>Once the writer partially completes your order, the file will be uploaded here.</p>
							</div>
						</div>
						<div class="col-md-3">
							<div class="float-right">
								<a href="#" style="color: #807b7b !important;"><p>More Option...</p></a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

@endsection