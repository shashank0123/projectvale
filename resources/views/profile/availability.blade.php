@extends('layouts.app')

@section('styles')
<link href="{{ asset('css/jquery-ui.css') }}" rel="stylesheet">
<link href="{{ asset('css/jquery-ui.multidatespicker.css') }}" rel="stylesheet">
@endsection
@section('content')

<section class="profile-section profile-wallet-section my-5">
    <div class="">
        <div class="row">
            <div class="col-md-4">
                @include('profile.sidebar')
            </div>
            <div class="col-md-8">

                <div class="card border-top-green">
                    <div class="card-body">
                        <form action="/profile/availability" method="post">
                            @csrf
                            <div class="form-group">
                                <div class="btn-group btn-group-toggle" data-toggle="buttons">
                                    <label class="btn btn-secondary btnStatus available {{ isset($availability) ? '' : 'active' }}">
                                        <input type="radio" name="is_available" autocomplete="off" value="1" {{ isset($availability) ? '' : 'checked' }} > Available
                                    </label>
                                    <label class="btn btn-secondary btnStatus unavailable {{ isset($availability) ? 'active' : '' }}">
                                        <input type="radio" name="is_available" autocomplete="off" value="0" {{ isset($availability) ? 'checked' : '' }}> Unavailable
                                    </label>
                                </div>
                            </div>
                            <div class="divUnavilable" style="display:{{ isset($availability) ? 'block' : 'none' }};">
                                <hr>
                                <div class="form-group">
                                    <label>Choose unavailable dates</label>
                                    <input type="text" name="available_dates" autocomplete="off" value="{{isset($availability) ? $availability->available_dates : ''}}" required class="multiDatesPicker form-control input-lg"  placeholder="Click to choose">
                                </div>
                                <button class="btn btn-primary" type="submit">Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection
@section('scripts')
<script src="{{ asset('js/jquery-ui.min.js') }}"></script>
<script src="{{ asset('js/jquery-ui.multidatespicker.js') }}"></script>
<script>
    $('div.writerProfileRating').raty({ starType: 'i',readOnly:true, score:'{{isset($writer) && $writer->rating ? $writer->rating :'3.0' }}' });
    $('.multiDatesPicker').multiDatesPicker({
        dateFormat: "yy-mm-dd"
    });

    $('.btnStatus.available').on('click', function(){
        $('.divUnavilable').hide();
    });
    $('.btnStatus.unavailable').on('click', function(){
        $('.divUnavilable').show();
    });

    $('[name="is_available"]').click(function(){
        console.log('here');
        if ((this).is(':checked')) {
            $('.divUnavilable').show();
        }else{
            $('.divUnavilable').hide();
        }
    });

    var resize = $('#upload-demo').croppie({
        enableExif: true,
        enableOrientation: true,
            viewport: { // Default { width: 100, height: 100, type: 'square' }
            width: 200,
            height: 200,
            type: 'circle' //square
        },
        boundary: {
            width: 300,
            height: 300
        }
    });

    $('#upload').on('change', function () {
        var reader = new FileReader();
        reader.onload = function (e) {
            resize.croppie('bind',{
                url: e.target.result
            }).then(function(){
                console.log('jQuery bind complete');
            });
        }
        reader.readAsDataURL(this.files[0]);
    });

    $('.upload-result').on('click', function (ev) {
        resize.croppie('result', {
            type: 'canvas',
            size: 'viewport'
        }).then(function (img) {
            axios.post('/profile/avatar', {
                profile_photo:img
            })
            .then(function (response) {
                location.reload(true);
            })
            .catch(function (error) {
                console.log(error);
            });
        });
    });
</script>
@endsection