@extends('layouts.app')

@section('content')

<section class="profile-section profile-wallet-section my-5">
	<div class="">
		<div class="row">
			<div class="col-md-4">
				@include('profile.sidebar')
			</div>
			<div class="col-md-8">

				<div class="card border-top-green">
					<div class="card-body">
						<div class="row">
							<div class="col-md-8">
								<h6 style="font-size: 16px;" class="pb-2">Presentation Preparation</h6>
								<p>Article Review, Other, 1 page</p>
							</div>
							<div class="col-md-4">
								<div class="float-right">
									<p class="mb-0">Finished </p>
									<span style="color: #fac917;"><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star-half-o" aria-hidden="true"></i><i class="fa fa-star-o" aria-hidden="true"></i></span>
								</div>
							</div>
						</div>
						<hr class="mt-0 mb-3">
						<div class="row">
							<div class="col-md-8">
								<h6>customer-270073<span style="color: #a49d9d;">(100 orders)</span></h6>
								<p>Great work as usual</p>
							</div>
							<div class="col-md-4">
								<div class="float-right">
									<p style="color: #a49d9d;">Sep 27, 2018</p>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="card mt-3 border-top-green">
					<div class="card-body">
						<div class="row">
							<div class="col-md-8">
								<h6 style="font-size: 16px;" class="pb-2">Presentation Preparation</h6>
								<p>Article Review, Other, 1 page</p>
							</div>
							<div class="col-md-4">
								<div class="float-right">
									<p class="mb-0">Finished </p>
									<span style="color: #fac917;"><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star-half-o" aria-hidden="true"></i><i class="fa fa-star-o" aria-hidden="true"></i></span>
								</div>
							</div>
						</div>
						<hr class="mt-0 mb-3">
						<div class="row">
							<div class="col-md-8">
								<h6>customer-270073<span style="color: #a49d9d;">(100 orders)</span></h6>
								<p>Great work as usual</p>
							</div>
							<div class="col-md-4">
								<div class="float-right">
									<p style="color: #a49d9d;">Sep 27, 2018</p>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="card mt-3 border-top-green">
					<div class="card-body">
						<div class="row">
							<div class="col-md-8">
								<h6 style="font-size: 16px;" class="pb-2">Presentation Preparation</h6>
								<p>Article Review, Other, 1 page</p>
							</div>
							<div class="col-md-4">
								<div class="float-right">
									<p class="mb-0">Finished </p>
									<span style="color: #fac917;"><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star-half-o" aria-hidden="true"></i><i class="fa fa-star-o" aria-hidden="true"></i></span>
								</div>
							</div>
						</div>
						<hr class="mt-0 mb-3">
						<div class="row">
							<div class="col-md-8">
								<h6>customer-270073<span style="color: #a49d9d;">(100 orders)</span></h6>
								<p>Great work as usual</p>
							</div>
							<div class="col-md-4">
								<div class="float-right">
									<p style="color: #a49d9d;">Sep 27, 2018</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

@endsection
@section('scripts')
<script>
	$('div.writerProfileRating').raty({ starType: 'i',readOnly:true, score:'{{isset($writer) && $writer->rating ? $writer->rating :'3.0' }}' });

	var resize = $('#upload-demo').croppie({
		enableExif: true,
		enableOrientation: true,
            viewport: { // Default { width: 100, height: 100, type: 'square' }
            width: 200,
            height: 200,
            type: 'circle' //square
        },
        boundary: {
        	width: 300,
        	height: 300
        }
    });

	$('#upload').on('change', function () {
		var reader = new FileReader();
		reader.onload = function (e) {
			resize.croppie('bind',{
				url: e.target.result
			}).then(function(){
				console.log('jQuery bind complete');
			});
		}
		reader.readAsDataURL(this.files[0]);
	});

	$('.upload-result').on('click', function (ev) {
		resize.croppie('result', {
			type: 'canvas',
			size: 'viewport'
		}).then(function (img) {
			axios.post('/profile/avatar', {
				profile_photo:img
			})
			.then(function (response) {
				location.reload(true);
			})
			.catch(function (error) {
				console.log(error);
			});
		});
	});
</script>
@endsection