@extends('layouts.app')

@section('content')

<section class="profile-section profile-wallet-section my-md-5">
	<div class="profilr-user-update-section">
		<div class="row">
			<div class="col-md-4 user-security-view">
				@include('profile.sidebar')
			</div>
			<div class="col-md-8">
				<div class="card border-top-green user-password-change">
					<div class="card-body">
						<div class="row">
							<div class="col-md-12">
								<div class="user-details">
									<form method="POST" action="/user/{{Auth()->id()}}/changepassword">
										@csrf
									  		<div class="form-group">
									    		<label>Old Password<sup class="error">*</sup> </label>
									    		<input type="password" class="form-control" name="old_password" required placeholder="Enter old password">
									  		</div>
										  	<div class="form-group">
										    	<label>New Password<sup class="error">*</sup></label>
										    	<input id="password" type="password" pattern="^(?=.*?[A-Z])(?=.*?[0-9]).{8,}$" class="form-control" name="password" required placeholder="Enter new password">
										  		<p class="m-0 text-muted"><small>The password must contain at least 8 characters with one uppercase and one number.</small></p>
										  	</div>
										  	<div class="form-group">
										    	<label>Confirm Password<sup class="error">*</sup></label>
										    	<input type="password" class="form-control" pattern="^(?=.*?[A-Z])(?=.*?[0-9]).{8,}$" data-parsley-equalto="#password" name="password_confirmation" required placeholder="Enter confirm password">
										  	</div>
									  	<button type="submit" class="btn btn-primary user-security-toggles">Change Password</button>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>


@endsection
@section('scripts')
<script>
	$('div.writerProfileRating').raty({ starType: 'i',readOnly:true, score:'{{isset($writer) && $writer->rating ? $writer->rating :'3.0' }}' });
	$('form').parsley();
	var resize = $('#upload-demo').croppie({
		enableExif: true,
		enableOrientation: true,
            viewport: { // Default { width: 100, height: 100, type: 'square' }
            width: 200,
            height: 200,
            type: 'circle' //square
        },
        boundary: {
        	width: 300,
        	height: 300
        }
    });

	$('#upload').on('change', function () {
		var reader = new FileReader();
		reader.onload = function (e) {
			resize.croppie('bind',{
				url: e.target.result
			}).then(function(){
				console.log('jQuery bind complete');
			});
		}
		reader.readAsDataURL(this.files[0]);
	});

	$('.upload-result').on('click', function (ev) {
		resize.croppie('result', {
			type: 'canvas',
			size: 'viewport'
		}).then(function (img) {
			axios.post('/profile/avatar', {
				profile_photo:img
			})
			.then(function (response) {
				location.reload(true);
			})
			.catch(function (error) {
				console.log(error);
			});
		});
	});
</script>
@endsection