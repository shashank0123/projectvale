@section('heading', 'Profile')

<div class="sidebar-profile">
    <div class="card user-card-view border-top-green shadow-boder-none">
        <div class="card-body d-flex d-md-block">
            @if(auth()->user()->be_writer)
            <div class="writer-status freelance-writer-status">
                <span class="freelance-status {{ auth()->user()->writer_is_available ? 'unavailable' : 'available'}} ">{{ auth()->user()->writer_is_available ? 'Unavailable' : 'Available'}}</span>
                <!-- <h4 class="flc-rate">&#8377; {{ auth()->user()->writer_rate}} / word</h4> -->
            </div>
            @endif
            <div class="text-md-center mt-5">
                <div class="img-profile user-profile-photo">
                    <input type="file" id="upload" name="profile_photo" href="#addProfilePhoto" data-toggle="modal" style="opacity: 0;position: absolute; height: 100px !important; width: 100px;z-index: 999999; cursor: pointer;">
                    @if(isset(auth()->user()->profile_photo) && auth()->user()->profile_photo)
                    <img src="{{Storage::url(auth()->user()->profile_photo)}}" class="img-responsive rounded-circle mb-2" style="">
                    @else
                    <img src="{{asset('img/user.png')}}" class="mb-3 img-thumbnail rounded-circle"  style="">
                    @endif
                    <a href="#addProfilePhoto" data-toggle="modal" class="btn bg-dark text-white rounded-circle btn-change-photo"><i class="fa fa-pencil"></i></a>
                </div>
            </div>
            <div class="text-md-center mt-md-3 ml-md-2 users-profile-details">
                <p class="mb-0 font-weight-bold">{{auth()->user()->fullname}}</p>
                <p class="mb-2">{{auth()->user()->email}}</p>
                @if(auth()->user()->be_writer)
                <span class="p-1 badge-success">{{auth()->user()->quality}}</span>
                <div class="mt-2 writerProfileRating"></div>
                @endif
            </div>
            <div class="d-flex text-md-center justify-content-between mt-2">
                <div class="col status-profile">
                    <h5>{{auth()->user()->completed_projects_count}}</h5>
                    <p>Completed</p>
                </div>
                <div class="col status-profile">
                    <h5>{{auth()->user()->ongoing_projects_count}}</h5>
                    <p>In progress</p>
                </div>
            </div>
        </div>
        <hr class="my-0">
        <div class="p-3 user-profile-tab-section">
            <a href="/profile/edit"><p><i class="fa fa-user-circle-o" aria-hidden="true"></i> Profile</p></a>
            <hr>
            <a href="/profile/security"><p><i class="fa fa-key" aria-hidden="true"></i> Security</p></a>
            @if(auth()->user()->be_writer)
            <hr>
            <a href="/profile/feedback"><p><i class="fa fa-commenting" aria-hidden="true"></i> Feedback</p></a>
            <!-- <hr>
            <a href="/wallet"><p><i class="fa fa-google-wallet" aria-hidden="true"></i> Wallet</p></a> -->
            <hr>
            <a href="/profile/availability"><p><i class="fa fa-calendar" aria-hidden="true"></i> Availability</p></a>

                @endif
                <hr>
                <a href="{{ route('logout') }}" onclick="event.preventDefault();
                document.getElementById('logout-form').submit();">
                <p><i class="fa fa-sign-out" aria-hidden="true"></i> {{ __('Logout') }}</p>
            </a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
            </form>
        </div>
    </div>
</div>

<div class="modal fade in addProfilePhoto" id="addProfilePhoto" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <form method="POST" action=""  enctype="multipart/form-data">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title font-weight-bold">Add Profile Photo</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                @csrf
                <div class="modal-body">
                    <div class="row form-group" style="margin-bottom: 0;">
                        <div class="col-xl-12 col-md-12">
                            <div id="upload-demo-i" style="cursor: pointer" onclick="editImage()"></div>
                            <!-- <input type="file"  class="form-control" name="profile_photo" required> -->
                            <div id="upload-demo" style="width:350px margin:auto;"></div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-success upload-result" data-dismiss="modal">Upload Photo</button>
                </div>
            </div>
        </form>
    </div>
</div>

@auth
 <nav class="bottom-nav">
            <a class="bottom-nav__action {{ request()->is('home') ? 'bottom-nav__action--active' : '' }}" href="/home">
                <!-- <svg class="bottom-nav__icon" viewBox="0 0 24 24">
                     <path d="M11,7V12.11L15.71,14.9L16.5,13.62L12.5,11.25V7M12.5,2C8.97,2 5.91,3.92 4.27,6.77L2,4.5V11H8.5L5.75,8.25C6.96,5.73 9.5,4 12.5,4A7.5,7.5 0 0,1 20,11.5A7.5,7.5 0 0,1 12.5,19C9.23,19 6.47,16.91 5.44,14H3.34C4.44,18.03 8.11,21 12.5,21C17.74,21 22,16.75 22,11.5A9.5,9.5 0 0,0 12.5,2Z"></path>
                </svg> -->
                <i class="fa fa-home"></i>
                <span class="bottom-nav__label">Home</span>
             </a>

            <a class="bottom-nav__action {{ request()->is('files') ? 'bottom-nav__action--active' : '' }}" href="/files">
                <!-- <svg class="bottom-nav__icon" viewBox="0 0 24 24">
                    <path d="M12,21.35L10.55,20.03C5.4,15.36 2,12.27 2,8.5C2,5.41 4.42,3 7.5,3C9.24,3 10.91,3.81 12,5.08C13.09,3.81 14.76,3 16.5,3C19.58,3 22,5.41 22,8.5C22,12.27 18.6,15.36 13.45,20.03L12,21.35Z"></path>
                </svg> -->
                <i class="fa fa-files-o"></i>
                <span class="bottom-nav__label">Files</span>
            </a>

            <a class="bottom-nav__action {{ request()->is('profile') ? 'bottom-nav__action--active' : '' }}" href="/profile">
                <!-- <svg class="bottom-nav__icon" viewBox="0 0 24 24">
                   <path d="M12,20A8,8 0 0,1 4,12A8,8 0 0,1 12,4A8,8 0 0,1 20,12A8,8 0 0,1 12,20M12,2A10,10 0 0,0 2,12A10,10 0 0,0 12,22A10,10 0 0,0 22,12A10,10 0 0,0 12,2M12,12.5A1.5,1.5 0 0,1 10.5,11A1.5,1.5 0 0,1 12,9.5A1.5,1.5 0 0,1 13.5,11A1.5,1.5 0 0,1 12,12.5M12,7.2C9.9,7.2 8.2,8.9 8.2,11C8.2,14 12,17.5 12,17.5C12,17.5 15.8,14 15.8,11C15.8,8.9 14.1,7.2 12,7.2Z"></path>
                </svg> -->
                <i class="fa fa-cog"></i>
                <span class="bottom-nav__label">Settings</span>
            </a>
        </nav>
        @endauth