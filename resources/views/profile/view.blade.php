@extends('layouts.app')

@section('content')

<section class="profile-section profile-wallet-section my-md-5">
	<div class="profilr-user-update-section">
		<div class="row">
			<div class="col-md-4 user-profile-sidebar">
				@include('profile.sidebar')
			</div>
			<div class="col-md-8 profile-user-card-details">
				<div class="card border-top-green">
					<div class="card-body">

						<div class="user-details">
							<form method="POST" action="/user/{{Auth()->id()}}/updateprofile">
								@csrf
								<div class="form-group row">
									<div class="col-md-6">
										<label>First Name</label>
										<input type="text" class="form-control" name="fname" value="{{ Auth::user()->fname }}" placeholder="Enter first name">
									</div>
									<div class="col-md-6">
										<label>Last Name</label>
										<input type="text" class="form-control" name="lname" value="{{ Auth::user()->lname }}" placeholder="Enter last name">
									</div>
								</div>
								<div class="form-group row">
									<div class="col-md-6">
										<label for="" class="user-custom-label">Username</label>
										<input type="text" class="form-control" readonly value="{{ Auth::user()->username }}">
									</div>
									<div class="col-md-6">
										<label for="" class="user-custom-label">Email</label>
										<input type="email" class="form-control" readonly value="{{ Auth::user()->email }}">
									</div>
								</div>
								<div class="form-group row">
									<div class="col-md-6">
										<label for="" class="user-custom-label">Phone</label>
										<div class="input-group">
											<div class="input-group-prepend">
												<span class="input-group-text" id="basic-addon1">+91</span>
											</div>
											<input type="text" pattern="\d*" autocomplete="off" class="form-control" minlength="10" maxlength="10" name="phone" value="{{ Auth::user()->phone }}" placeholder="Phone number">
										</div>
									</div>
									@if(auth()->user()->be_writer)
										<div class="col-md-6">
											<label>Subject</label>
										<select multiple="multiple" class="form-control select2-multiple" value="" name="subjects[]">
											@foreach(getSubjects() as $subject)
											<option <?= array_search($subject->name, explode(',', auth()->user()->subjects)) !== false ? 'selected' : '' ?>>{{$subject->name}}</option>
											@endforeach
											<!-- <option <?= array_search('English', explode(',', auth()->user()->subjects)) !== false ? 'selected' : '' ?>>English</option>
											<option <?= array_search('Business and Entrepreneurship', explode(',', auth()->user()->subjects)) !== false ? 'selected' : '' ?>>Business and Entrepreneurship</option>
											<option <?= array_search('History', explode(',', auth()->user()->subjects)) !== false ? 'selected' : '' ?>>History</option>
											<option <?= array_search('Economics', explode(',', auth()->user()->subjects)) !== false ? 'selected' : '' ?>>Economics</option>
											<option <?= array_search('Finance', explode(',', auth()->user()->subjects)) !== false ? 'selected' : '' ?>>Finance</option>
											<option <?= array_search('Law', explode(',', auth()->user()->subjects)) !== false ? 'selected' : '' ?>>Law</option>
											<option <?= array_search('Management', explode(',', auth()->user()->subjects)) !== false ? 'selected' : '' ?>>Management</option>
											<option <?= array_search('Marketing', explode(',', auth()->user()->subjects)) !== false ? 'selected' : '' ?>>Marketing</option>
											<option <?= array_search('Political Science', explode(',', auth()->user()->subjects)) !== false ? 'selected' : '' ?>>Political Science</option>
											<option <?= array_search('Others', explode(',', auth()->user()->subjects)) !== false ? 'selected' : '' ?>>Others</option> -->
										</select>
										</div>
									@endif
								</div>
							 
								<div class="d-none d-sm-block">
									<button type="submit" class="btn btn-primary">Save Changes</button>
									<a href="/profile" class="btn btn-default text-white bg-secondary">Cancel</a>
								</div>
								<div class="px-3 d-sm-none">
									<button type="submit" class="btn btn-primary btn-block">Save Changes</button>
									<a href="/profile" class="btn btn-default btn-block text-white bg-secondary">Cancel</a>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>



@endsection

@section('scripts')
<script>
	$(document).ready(function(){
		$( ".select2-single, .select2-multiple" ).select2({
			placeholder: 'Select subjects',
			tags: true,
		});
	});
	$('div.writerProfileRating').raty({ starType: 'i',readOnly:true, score:'{{isset($writer) && $writer->rating ? $writer->rating :'3.0' }}' });

	var resize = $('#upload-demo').croppie({
		enableExif: true,
		enableOrientation: true,
            viewport: { // Default { width: 100, height: 100, type: 'square' }
            width: 200,
            height: 200,
            type: 'circle' //square
        },
        boundary: {
        	width: 300,
        	height: 300
        }
    });

	$('#upload').on('change', function () {
		var reader = new FileReader();
		reader.onload = function (e) {
			resize.croppie('bind',{
				url: e.target.result
			}).then(function(){
				console.log('jQuery bind complete');
			});
		}
		reader.readAsDataURL(this.files[0]);
	});

	$('.upload-result').on('click', function (ev) {
		resize.croppie('result', {
			type: 'canvas',
			size: 'viewport'
		}).then(function (img) {
			axios.post('/profile/avatar', {
				profile_photo:img
			})
			.then(function (response) {
				location.reload(true);
			})
			.catch(function (error) {
				console.log(error);
			});
		});
	});
</script>
@endsection