@extends('layouts.app')

@section('content')

<section class="profile-wallet-section my-5">
	<div class="">
		<div class="row">
			<div class="col-md-4">
          @include('profile.sidebar')
			</div>
			<div class="col-md-8">
				<div class="row">
					<div class="col-md-12">
						<div class="add-bank-detail">
							<span>Payments Wallet</span>
							<a href="#" class="btn btn-secondary btn-lg float-right"  data-toggle="modal" data-target="#exampleModal" data-whatever="@mdo"><i class="fa fa-plus-circle" aria-hidden="true"></i> Add Bank</a>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-4 text-center">
						<a href="#addMoneyModal1" class="text-dark" data-toggle="modal" data-target="#exampleModal1" data-whatever="@mdo">
							<div class="card my-3">
								<div class="card-body">
									<div class="fun-fact-text">
										<i class="fa fa-inr fa-4x pb-2" aria-hidden="true"></i>
										<p>Add Money</p>
									</div>
								</div>
							</div>
						</a>
					</div>
					<div class="col-sm-4 text-center">
						<a href="#sendToBankModal2" class="text-dark" data-toggle="modal" data-target="#exampleModal2" data-whatever="@mdo">
							<div class="card my-3">
								<div class="card-body">
									<div class="fun-fact-text">
										<i class="fa fa-university fa-4x pb-2"></i>
										<p>Transfer to Bank</p>
									</div>
								</div>
							</div>
						</a>
					</div>
					<div class="col-sm-4 text-center">
						<a href="#sendToUserModal3" class="text-dark" data-toggle="modal" data-target="#exampleModal3" data-whatever="@mdo">
							<div class="card my-3"><div class="card-body fun-fact">
								<div class="fun-fact-text">
									<i class="fa fa-users fa-4x pb-2"></i>
									<p>Send to User</p>
								</div>
							</div>
						</div>
					</a>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-4">
					<div class="card my-3">
						<div class="card-body">
							<div class="d-flex">
								<div class="">
									<div class="fun-fact-text">
										<p class="mb-2">Wallet Ballance</p>
										<h4>₹ 50330</h4>
									</div>
								</div>
								<div class="">
									<div class="fun-fact-icon">
										<img src="/img/wallet.png" alt="wallet" style="width: 40px;margin-left: 30px;">
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-sm-4">
					<div class="card my-3">
						<div class="card-body">
							<div class="d-flex">
								<div class="">
									<div class="fun-fact-text">
										<p class="mb-2" style="width: 150px;">Payments Received</p>
										<h4>₹ 0</h4>
									</div>
								</div>
								<div class="">
									<div class="fun-fact-icon">
										<img src="/img/payment.png" alt="wallet" style="width: 40px;margin-left: 1px;">

									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-sm-4">
					<div class="card my-3">
						<div class="card-body">
							<div class="d-flex">
								<div class="">
									<div class="fun-fact-text">
										<p class="mb-2">Payments Made</p>
										<h4>₹ 0</h4>
									</div>
								</div>
								<div class="">
									<div class="fun-fact-icon">
										<img src="/img/cash.png" alt="wallet" style="width: 40px;margin-left: 25px;">
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="card border-0 my-3">
					    <div class="card-body dashboard-box">
					        <div class="headline">
					            <h3><i class="icon-feather-bar-chart-2 text-success"></i> Your Monthly Revenue</h3>
					        </div>
					        <div class="content">
					            <canvas id="chart" width="725" height="326" class="chartjs-render-monitor" style="display: block; width: 725px; height: 326px;"></canvas>
					        </div>
					    </div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</section>

<div class="modal fade" id="exampleModal3" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Send To User</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form>
          <div class="form-group">
            <label for="account-number" class="col-form-label">Amount</label>
            <input type="text" class="form-control" placeholder="Enter Amount">
          </div>
          <div class="form-group">
            <label for="account-number" class="col-form-label">User Email</label>
            <input type="text" class="form-control" placeholder="Enter Your Email">
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary btn-lg" data-dismiss="modal">Add</button>
        <button type="button" class="btn btn-primary btn-lg">Cancel</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Send To Bank</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form>
          <div class="form-group">
            <label for="account-number" class="col-form-label">Amount</label>
            <input type="text" class="form-control" placeholder="Enter Amount">
          </div>
          <div class="form-group">
            <label for="account-holder-name" class="col-form-label">Account Holder Name</label>
            <select class="form-control">
            	<option>Select Bank</option>
            	<option>State bank</option>
            </select>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary btn-lg" data-dismiss="modal">Add</button>
        <button type="button" class="btn btn-primary btn-lg">Cancel</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="exampleModal1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Add Money</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form>
          <div class="form-group">
            <label for="account-number" class="col-form-label">Amount</label>
            <input type="text" class="form-control" placeholder="Enter Amount">
          </div>
          <div class="form-group">
            <label for="account-holder-name" class="col-form-label">Account Holder Name</label>
            <select class="form-control">
            	<option>Select Bank</option>
            	<option>State bank</option>
            </select>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary btn-lg" data-dismiss="modal">Add</button>
        <button type="button" class="btn btn-primary btn-lg">Cancel</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Add Bank</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form>
          <div class="form-group">
            <label for="account-number" class="col-form-label">Account Number</label>
            <input type="text" class="form-control" placeholder="Enter Number">
          </div>
          <div class="form-group">
            <label for="account-holder-name" class="col-form-label">Account Holder Name</label>
            <input type="text" class="form-control" placeholder="Enter Account Holder Name">
          </div>
          <div class="form-group">
            <label for="bank-name" class="col-form-label">Bank Name</label>
            <input type="text" class="form-control" placeholder="Enter Bank Name">
          </div>
          <div class="form-group">
            <label for="branch-name" class="col-form-label">Branch Name</label>
            <input type="text" class="form-control" placeholder="Enter Branch Name">
          </div>
          <div class="form-group">
          	<label for="IFSC-code" class="col-form-label">IFSC Code</label>
          	<input type="text" class="form-control" placeholder="Enter Bank IFSC code">
          </div>
          <!-- <div class="form-group">
            <label for="message-text" class="col-form-label">Message:</label>
            <textarea class="form-control" id="message-text"></textarea>
          </div> -->
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary btn-lg" data-dismiss="modal">Add</button>
        <button type="button" class="btn btn-primary btn-lg">Cancel</button>
      </div>
    </div>
  </div>
</div>

@endsection


    @section('scripts')

    <script>
        Chart.defaults.global.defaultFontFamily = "Nunito";
        Chart.defaults.global.defaultFontColor = '#888';
        Chart.defaults.global.defaultFontSize = '14';

        var ctx = document.getElementById('chart').getContext('2d');

        var chart = new Chart(ctx, {
            type: 'line',
        // The data for our dataset
        data: {
            labels: ["January", "February", "March", "April", "May", "June"],
            // Information about the dataset
            datasets: [{
                label: "Views",
                backgroundColor: 'rgba(42,65,232,0.08)',
                borderColor: '#2a41e8',
                borderWidth: "3",
                data: [196,132,215,362,210,252],
                pointRadius: 5,
                pointHoverRadius:5,
                pointHitRadius: 10,
                pointBackgroundColor: "#fff",
                pointHoverBackgroundColor: "#fff",
                pointBorderWidth: "2",
            }]
        },

    // Configuration options
    options: {

        layout: {
            padding: 10,
        },

        legend: { display: false },
        title:  { display: false },

        scales: {
            yAxes: [{
                scaleLabel: {
                    display: false
                },
                gridLines: {
                    borderDash: [6, 10],
                    color: "#d8d8d8",
                    lineWidth: 1,
                },
            }],
            xAxes: [{
                scaleLabel: { display: false },
                gridLines:  { display: false },
            }],
        },

        tooltips: {
            backgroundColor: '#333',
            titleFontSize: 13,
            titleFontColor: '#fff',
            bodyFontColor: '#fff',
            bodyFontSize: 13,
            displayColors: false,
            xPadding: 10,
            yPadding: 10,
            intersect: false
        }
    },
});

</script>


@endsection