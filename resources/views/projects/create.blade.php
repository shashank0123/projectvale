<div class="modal fade in" id="postProjectModal" tabindex="-1" role="dialog" aria-labelledby="postProjectModal" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-body">
				<div class="">
					<h2 class="font-weight-bold text-center pb-3 mt-3">Post new project</h2>
				</div>
				<form id="wizard" method="POST" action="/projects" class="frmPostAssignment" enctype="multipart/form-data">
					@csrf
					<h2></h2>
					<section class="form-section">
						<div class="inner">
							<div class="container">
								<div class="d-flex justify-content-center mb-3">
									<div style="width: 100%">
										<div class="wrapper" id="block-1">
											<div class="form-group">
												<label>Type of paper <sup class="error">*</sup> </label>
												<select class="form-control{{ $errors->has('type') ? ' is-invalid' : '' }}" value="{{ old('type') }}" name="type" required>
													@foreach(getPaperTypes() as $papertype)
													<option>{{$papertype->name}}</option>
													@endforeach
												</select>
												@if ($errors->has('type'))
												<span class="invalid-feedback" role="alert">
													<strong>{{ $errors->first('type') }}</strong>
												</span>
												@endif
											</div>
											<div class="form-group">
												<label>Topic <sup class="error">*</sup></label>
												<input type="text" name="topic" class="form-control{{ $errors->has('topic') ? ' is-invalid' : '' }}" value="{{ old('topic') }}" placeholder="Enter your topic" required>
												@if ($errors->has('topic'))
												<span class="invalid-feedback" role="alert">
													<strong>{{ $errors->first('topic') }}</strong>
												</span>
												@endif
											</div>

											<div class="form-group">
												<label>Select your subject <sup class="error">*</sup></label>
												<select class="form-control{{ $errors->has('subject') ? ' is-invalid' : '' }}" value="{{ old('subject') }}" name="subject" required>
													@foreach(getSubjects() as $subject)
													<option>{{$subject->name}}</option>
													@endforeach
													<!-- <option>English</option>
													<option>Business and Entrepreneurship</option>
													<option>History</option>
													<option>Economics</option>
													<option>Finance</option>
													<option>Law</option>
													<option>Management</option>
													<option>Marketing</option>
													<option>Political Science</option>
													<option>Others</option> -->
												</select>
												@if ($errors->has('subject'))
												<span class="invalid-feedback" role="alert">
													<strong>{{ $errors->first('subject') }}</strong>
												</span>
												@endif
											</div>
											<div class="form-group mt-3">
												<a href="#next" class="btn btn-primary btn-lg btn-block wizard-control" data-parsley-group="block-1">
													Continue
												</a>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</section>
					<h2></h2>
					<section class="form-section">
						<div class="inner">
							<div class="container">
								<div class="d-flex justify-content-center mb-3">
									<div style="width: 100%">
										<div class="wrapper" id="block-2">
											<div class="form-group">
												<label>Number of words <sup class="error">*</sup></label>
												<input type="number" name="pages_count" min="0" class="form-control{{ $errors->has('pages_count') ? ' is-invalid' : '' }}" value="{{ old('pages_count') }}" placeholder="Number of words" required>
												@if ($errors->has('pages_count'))
												<span class="invalid-feedback" role="alert">
													<strong>{{ $errors->first('pages_count') }}</strong>
												</span>
												@endif
											</div>
											<div class="form-group">
												<label>Paper instructions <sup class="error">*</sup></label>
												<textarea type="text" class="form-control{{ $errors->has('instructions') ? ' is-invalid' : '' }}" name="instructions" required placeholder="List your instructions">{{ old('instructions') }}</textarea>
												@if ($errors->has('instructions'))
												<span class="invalid-feedback" role="alert">
													<strong>{{ $errors->first('instructions') }}</strong>
												</span>
												@endif
											</div>
											<div class="form-group row">
												<div class="col">
													<label>Deadline <sup class="error">*</sup></label>
													<input type="text" name="deadline_date" 
													id="deadline_date" readonly
													class="form-control" value="{{ old('deadline_date') }}" required  data-modal="true" data-large-default="true" data-init-set="false" data-large-mode="true" data-theme="projectvala" placeholder="Select deadline date">
													@if ($errors->has('deadline_date'))
													<span class="invalid-feedback" role="alert">
														<strong>{{ $errors->first('deadline_date') }}</strong>
													</span>
													@endif
												</div>
												<div class="col">
													<label>&nbsp;</label>
													<input type="text" name="deadline_time" 
													    id="deadline_time" class="form-control" placeholder="Deadline time" required>
													@if ($errors->has('deadline_time'))
													<span class="invalid-feedback" role="alert">
														<strong>{{ $errors->first('deadline_time') }}</strong>
													</span>
													@endif
												</div>
											</div>
											<div class="form-group mt-3">
												<a href="#next" class="btn btn-primary btn-lg btn-block wizard-control" data-parsley-group="block-2">
													Continue
												</a>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</section>
					<h2></h2>
					<section class="form-section">
						<div class="inner">
							<div class="container">
								<div class="d-flex justify-content-center mb-3">
									<div style="width: 100%">
										<div class="wrapper" id="block-3">

											<div class="form-group">
												<label>Number of cited resources <sup class="error">*</sup> <a href="#"  data-toggle="popover" data-trigger="hover" data-content="Approximate number of resources that you want the writer to cite in your project."><i class="fa fa-question-circle"></i></a></label>
												<input type="number" class="form-control{{ $errors->has('citations_count') ? ' is-invalid' : '' }}" value="2" name="citations_count" placeholder="Cited resources" required>
												@if ($errors->has('citations_count'))
												<span class="invalid-feedback" role="alert">
													<strong>{{ $errors->first('citations_count') }}</strong>
												</span>
												@endif
											</div>
											<div class="form-group">
												<label>Format of citation <sup class="error">*</sup></label>
												<select class="form-control{{ $errors->has('citation_format') ? ' is-invalid' : '' }}" value="{{ old('citation_format') }}" name="citation_format" required>

													@foreach(getCitations() as $citation)
													<option>{{$citation->name}}</option>
													@endforeach
													<!-- <option>Not Applicable</option>
													<option>APA</option>
													<option>Chicago/Turabian</option>
													<option>Harvard</option>
													<option>Vancouver</option>
													<option>MLA</option>
													<option>Other</option> -->
												</select>
												@if ($errors->has('citation_format'))
												<span class="invalid-feedback" role="alert">
													<strong>{{ $errors->first('citation_format') }}</strong>
												</span>
												@endif
											</div>
											<div class="form-group">
												<label>Drop files here or Click to upload</label>
												<div class="uploadButton margin-top-30">
													<input class="uploadButton-input form-control" type="file" id="uploadFile" name="file[]" multiple>
													<label class="uploadButton-button ripple-effect" for="uploadFile">Choose File</label>
													<span class="uploadButton-file-name">Click to upload</span>
												</div>
											</div>
											<div class="form-group mt-3">
												<button type="submit" class="btn btn-secondary btn-postProject btn-lg btn-block">
													Post Project
												</button>
											</div>
											<!-- <div class="form-group mt-3">
												<a href="#next" class="btn btn-primary btn-lg btn-block wizard-control" data-parsley-group="block-3">
													Continue
												</a>
											</div> -->
										</div>
									</div>
								</div>
							</div>
						</div>
					</section>
					<!-- <h2></h2>
					<section class="form-section">
						<div class="inner">
							<div class="container">
								<div class="d-flex justify-content-center mb-3">
									<div style="width: 100%">
										<div class="wrapper" id="block-4">
											<div class="form-group">
												<label for="formGroupExampleInput2">Type of service <sup class="error">*</sup></label>
												<div class="form-check">
													<label class="form-check-label">
														<input class="form-check-input" type="radio" name="type_of_service" value="scratch" checked>
														Writing from scratch
													</label>
												</div>
												<div class="form-check">
													<label class="form-check-label">
														<input class="form-check-input" type="radio" name="type_of_service" value="rewrite">
														Rewriting
													</label>
												</div>
												<div class="form-check">
													<label class="form-check-label">
														<input class="form-check-input" type="radio" name="type_of_service" value="editing">
														Editing
													</label>
												</div>
											</div>
											<div class="form-group">
												<label>Writer quality <sup class="error">*</sup></label>
												<div class="form-check">
													<label class="form-check-label">
														<input class="form-check-input" type="radio" name="writing_quality"value="Standard" checked>
														Standard
													</label>
												</div>
												<div class="form-check">
													<label class="form-check-label">
														<input class="form-check-input" type="radio" name="writing_quality" value="Premium">
														Premium
													</label>
												</div>
												<div class="form-check">
													<label class="form-check-label">
														<input class="form-check-input" type="radio" name="writing_quality" value="Platinum">
														Platinum
													</label>
												</div>
											</div>
											<div class="form-check">
												<input type="checkbox" class="form-check-input" id="exampleCheck1">
												<label class="form-check-label" for="exampleCheck1">$9.99</label>
												<span style="float: right;">We select the writer for you.</span>
											</div>
											<div class="form-group mt-3">
												<button type="submit" class="btn btn-secondary btn-lg btn-block">
													Post Project
												</button>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</section> -->
				</form>
				<a href="javascript:;" class="cancel-modal btn btn-outline-secondary mx-3" data-dismiss="modal">Cancel</a>
			</div>
		</div>
	</div>
</div>
