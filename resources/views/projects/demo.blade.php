@extends('layouts.app')

@section('content')
<div class="mt-5">
    <div class="row">
    	<div class="col-md-12 text-center"> 
    		 <div class="no-project-logo">
    		 	<img src="/img/user/project-management.png" alt="project">
    		 </div>
    		 <h4 class="mt-3">Sorry, You have no projects yet, lets start posting projects</h4>
    		 <div class="post-project-btn">
    		 	<a href="#" class="btn btn-secondary btn-lg mt-4">Post Project</a>
    		 </div>
    	</div>
    </div>
</div>
@endsection