@extends('layouts.app')
@section('content')

<div class="py-md-4">
    <div class="page-heading custom-project-edit-details">
        <h2 class="d-inline">{{$project->topic}}</h2>
        <nav aria-label="breadcrumb" class="float-md-right">
            <ol class="breadcrumb bg-dark ">
                <li class="breadcrumb-item"><a href="/home" class="text-white">Home</a></li>
                <li class="breadcrumb-item"><a href="/home" class="text-white">Projects</a></li>
                <li class="breadcrumb-item text-white active" aria-current="page">Edit Project</li>
            </ol>
        </nav>
    </div>
    <div class="clearfix"></div>
    <div class="card mt-md-3 mb-md-5 project-edit-details">
        <div class="card-header user-profile-edit">
            Edit Project
        </div>
        @section('heading', 'Edit Project')
        <form method="POST" action="/projects/{{$project->id}}/update" class="" enctype="multipart/form-data">
            <div class="card-body">
                @csrf
                <div class="row form-group">
                    <div class="col-md-4">
                        <label>Type of paper<sup class="error">*</sup> </label>
                        <select class="form-control{{ $errors->has('type') ? ' is-invalid' : '' }} " {{$project->status ? 'disabled' : 'required'}} value="{{ old('type') }}" name="type">
                            @foreach(getPaperTypes() as $papertype)
                            <option>{{$papertype->name}}</option>
                            @endforeach
                            <!-- <option>Essay</option>
                            <option>Article</option>
                            <option>Article Review</option>
                            <option>Book/Movie Review</option>
                            <option>Case Study</option>
                            <option>Dissertation</option>
                            <option>Research Paper</option>
                            <option>Research Proposal</option>
                            <option>Thesis</option>
                            <option>Other</option> -->
                        </select>
                        @if ($errors->has('type'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('type') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="col-md-4">
                        <label>Topic<sup class="error">*</sup></label>
                        <input type="text" name="topic" {{$project->status ? 'readonly' : 'required'}} class="form-control{{ $errors->has('topic') ? ' is-invalid' : '' }}" value="{{ $project->topic }}" placeholder="Enter your topic">
                        @if ($errors->has('topic'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('topic') }}</strong>
                        </span>
                        @endif
                    </div>
                    {{-- <div class="col-md-4">
                        <label>Select your subject<sup class="error">*</sup></label>
                        <select class="form-control{{ $errors->has('subject') ? ' is-invalid' : '' }}" value="{{ $project->subject }}" {{$project->status ? 'disabled' : 'required'}} name="subject">
                            @foreach(getSubjects() as $subject)
                            <option>{{$subject->name}}</option>
                            @endforeach 
                        </select>
                        @if ($errors->has('subject'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('subject') }}</strong>
                        </span>
                        @endif
                    </div> --}}

                    <div class="col-md-4">
                        <label>Paper instructions<sup class="error">*</sup></label>
                        <textarea type="text" name="instructions" class="form-control{{ $errors->has('instructions') ? ' is-invalid' : '' }}" {{$project->status ? 'disabled' : 'required'}} placeholder="List your instructions.">{{ $project->instructions }}</textarea>
                        @if ($errors->has('instructions'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('instructions') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-4">
                        <label>Number of words<sup class="error">*</sup></label>
                        <input type="number" name="pages_count" {{$project->status ? 'readonly' : 'required'}} min="0" class="form-control{{ $errors->has('pages_count') ? ' is-invalid' : '' }}" value="{{ $project->pages_count }}" placeholder="Number of words">
                        @if ($errors->has('pages_count'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('pages_count') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="col-md-4">
                        <label>Deadline Date<sup class="error">*</sup></label>
                        <input type="text" name="deadline_date" id="deadline_date" readonly {{$project->status ? 'disabled' : 'required'}} class="form-control {{ $errors->has('deadline_date') ? ' is-invalid' : '' }}" value="{{ $project->deadline_date }}"  data-format="Y-m-d" data-modal="true" data-large-default="true" data-init-set="false" data-large-mode="true" data-theme="projectvala" placeholder="Select deadline date">
                        @if ($errors->has('deadline_date'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('deadline_date') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="col-md-4">
                        <label>Deadline Time<sup class="error">*</sup></label>
                        <input type="text"  name="deadline_time" id="deadline_time" {{$project->status ? 'disabled' : 'required'}} class="form-control {{ $errors->has('deadline_time') ? ' is-invalid' : '' }}" value="{{ $project->deadline_time }}">
                        @if ($errors->has('deadline_time'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('deadline_time') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="row form-group">
                   <div class="col-md-4">
                    <label>Number of cited resources<sup class="error">*</sup></label>
                    <input type="number" class="form-control{{ $errors->has('citations_count') ? ' is-invalid' : '' }}" {{$project->status ? 'readonly' : 'required'}} value="{{ $project->citations_count }}" name="citations_count" placeholder="Cited resources">
                    @if ($errors->has('citations_count'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('citations_count') }}</strong>
                    </span>
                    @endif
                </div>
                <div class="col-md-4">
                    <label>Format of citation<sup class="error">*</sup></label>
                    <select class="form-control{{ $errors->has('citation_format') ? ' is-invalid' : '' }}" value="{{ $project->citation_format }}" {{$project->status ? 'disabled' : 'required'}} name="citation_format">
                        @foreach(getCitations() as $citation)
                        <option>{{$citation->name}}</option>
                        @endforeach
                        <!-- <option>Other</option>
                        <option>APA</option>
                        <option>Chicago/Turabian</option>
                        <option>Harvard</option>
                        <option>Vancouver</option>
                        <option>Not Applicable</option>
                        <option>MLA</option> -->
                    </select>
                    @if ($errors->has('citation_format'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('citation_format') }}</strong>
                    </span>
                    @endif
                </div>
                <div class="col-md-4">
                    <label>Additional instructions</label>
                    <textarea type="text" class="form-control{{ $errors->has('additional_instructions') ? ' is-invalid' : '' }}" name="additional_instructions" placeholder="List your additional instructions.">{{ $project->additional_instructions }}</textarea>
                    @if ($errors->has('additional_instructions'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('additional_instructions') }}</strong>
                    </span>
                    @endif
                </div>
               
            </div>
            <div class="row">
                <div class="col-md-4">
                    <label>Drop files here or Click to upload</label>
                    <div class="uploadButton margin-top-30">
                        <input class="uploadButton-input form-control" type="file" {{$project->status ? 'disabled' : ''}} name="file" id="uploadFile">
                        <label class="uploadButton-button ripple-effect" for="uploadFile">Upload File</label>
                        <!-- <span class="uploadButton-file-name">Drop files here or Click to upload</span> -->
                    </div>

                </div>
             
            </div>
            @if(isset($project->files))
            <div class="row">
                @foreach($project->files as $key => $file)
                <div class="col-md-3">
                    <div class="custom-file-upload mt-3">
                        <div class="attachments-container">
                            <div class="attachment-box ripple-effect">
                                <a  target="_blank" href="{{Storage::url($file)}}" class="attachment-title">Project File {{$key + 1}}</a>
                                <a target="_blank" class="btn btn-sm btn-secondary text-white mr-2" href="{{Storage::url($file)}}"><span class="fa fa-eye"></span> View</a>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
            @endif
                <!-- <div class="col-md-4">
                    <label for="formGroupExampleInput2">Type of service <sup class="error">*</sup></label>
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="type_of_service" value="scratch" {{isset($project->type_of_service) && $project->type_of_service == 'scratch' ? 'checked': '' }} {{$project->status ? 'disabled' : ''}}>
                            Writing from scratch
                        </label>
                    </div>
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="type_of_service" value="rewrite" {{isset($project->type_of_service) && $project->type_of_service == 'rewrite' ? 'checked': '' }} {{$project->status ? 'disabled' : ''}}>
                            Rewriting
                        </label>
                    </div>
                    <div class="form-check">
                        <label class="form-check-label">
                            <input class="form-check-input" type="radio" name="type_of_service" value="editing" {{isset($project->type_of_service) && $project->type_of_service == 'editing' ? 'checked': '' }} {{$project->status ? 'disabled' : ''}}>
                            Editing
                        </label>
                    </div>
                </div> -->
            <!-- <div class="col-md-4">
                <label>Writer quality <sup class="error">*</sup></label>
                <div class="form-check">
                    <label class="form-check-label">
                        <input class="form-check-input" type="radio" name="writing_quality" value="Standard" {{isset($project->writing_quality) && $project->writing_quality == 'Standard' ? 'checked': '' }} {{$project->status ? 'disabled' : ''}}>
                        Standard
                    </label>
                </div>
                <div class="form-check">
                    <label class="form-check-label">
                        <input class="form-check-input" type="radio" name="writing_quality" value="Premium" {{isset($project->writing_quality) && $project->writing_quality == 'Premium' ? 'checked': '' }} {{$project->status ? 'disabled' : ''}}>
                        Premium
                    </label>
                </div>
                <div class="form-check">
                    <label class="form-check-label">
                        <input class="form-check-input" type="radio" name="writing_quality" value="Platinum" {{isset($project->writing_quality) && $project->writing_quality == 'Platinum' ? 'checked': '' }} {{$project->status ? 'disabled' : ''}}>
                        Platinum
                    </label>
                </div>
            </div> -->
        </div>
                <!-- <div class="form-check">
                    <input type="checkbox" class="form-check-input" id="exampleCheck1">
                    <label class="form-check-label" for="exampleCheck1">$9.99</label>
                    <span style="float: right;">We select the writer for you.</span>
                </div> -->
            <div class="card-footer custom-card-footer">
                <button type="submit" class="btn btn-primary btn-lg">
                    Update
                </button>
            </div>
        </form>
    </div>
</div>
@endsection
@section('scripts')

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-timepicker/1.10.0/jquery.timepicker.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-timepicker/1.10.0/jquery.timepicker.js"></script>


<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<script>
    $(document).ready(function(){
        $('select[name="type"]').val('{{$project->type}}');
        $('select[name="subject"]').val('{{$project->subject}}');
        $('select[name="citation_format"]').val('{{$project->citation_format}}');
    });
 
 
    $(document).ready(function(){
 
    $('#deadline_date').datepicker({ 
    minDate: 0+2, 
    });


    $('#deadline_time').timepicker({
    'minTime': '00:00am',
    'maxTime': '06:00pm' 
    });

    
    
  });

</script>
@endsection