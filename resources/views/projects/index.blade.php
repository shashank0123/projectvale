@extends('layouts.app')

@section('content')
<div class="mt-5">
    <div class="card mb-4">
        <div class="headline card-header">
            <h3 class="d-inline-block">Projects</h3>
            <a href="/projects/create" class="btn btn-primary float-right">Post a Project</a>
        </div>
    </div>

    <div class="row">
        @foreach($projects as $project)
        <div class="col-sm-4">
            <div class="card my-3">
                <div class="card-body">
                    <h4>{{$project->topic}}</h4>
                    <p>{{str_limit($project->instructions, 110)}}</p>
                    <a href="projects/{{$project->id}}" class="btn btn-primary btn-sm">View</a>
                </div>
            </div>
        </div>
        @endforeach
    </div>
</div>
@endsection