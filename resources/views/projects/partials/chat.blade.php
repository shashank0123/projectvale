<div class="tab-pane fade in" id="pills-chat" role="tabpanel" aria-labelledby="pills-chat-tab">
    <div class="row chat-section-custom">
        <div class="col-sm-8 project-card">
            @if($project->status != 1)
            <div class="writer-list-chat mt-3">
                <div class="card border-top-green">
                    <div class="card-body">
                        <ul class="chat">
                            @foreach($project->chats as $message)
                            @if($message->sender != auth()->user()->id)
                            <li class="left clearfix" >
                                <div class="chat-body clearfix">
                                    <p>
                                        {!!$message->message!!}
                                    </p>
                                </div>
                                <div class="clearfix"></div>
                                <span class="msg-time"><i class="fa fa-clock-o"></i> {{$message->created_at->diffForHumans()}}</span>
                            </li>
                            @else
                            <li class="right clearfix" >
                                <div class="chat-body clearfix">
                                    <p>
                                        {!!$message->message!!}
                                    </p>
                                </div>
                                <div class="clearfix"></div>
                                <span class="msg-time"><i class="fa fa-clock-o"></i> {{$message->created_at->diffForHumans()}}</span>
                            </li>
                            @endif
                            @endforeach
                        </ul>
                        <hr>
                        @if(!$project->completed)
                        <form class="frmSendMessage">
                            @csrf
                            <div class="input-group">
                                <input type="text" autocomplete="off" name="message" class="form-control" placeholder="Type a message" aria-label="Type a message" aria-describedby="basic-addon2">
                                <div class="input-group-append">
                                    <button  class="btn btn-secondary sendBtn" type="submit">Send</button>
                                </div>
                            </div>
                        </form>
                        @endif
                    </div>
                </div>
            </div>
            @endif
        </div>
        <div class="col-sm-4 mt-3  project-card attachments-container-responsive user-profile-details">
            @include('projects.user.profile')
        </div>
    </div>



    <div class="responsive-chat-section pl-0 pr-0">
        <div class="d-flex align-items-center p-3">
            @if(isset($project->writer) && auth()->id() == $project->writer->id && $project->user->profile_photo)
            <img src="{{Storage::url($project->user->profile_photo)}}" style="height: 35px; width: 35px;" class="img-thumbnail rounded-circle">
            @elseif(isset($project->user) && auth()->id() ==$project->user->id && $project->writer->profile_photo)
            <img src="{{Storage::url($project->writer->profile_photo)}}" style="height: 35px; width: 35px;" class="img-thumbnail rounded-circle">
            @else
            <img src="{{asset('img/user.png')}}" style="height: 35px; width: 35px;" class="img-thumbnail rounded-circle">
            @endif
            <div>
                @if(isset($project->writer) && auth()->id() ==$project->writer->id)
                <p class="mb-0 ml-2">{{$project->user->username}}</p>
                @elseif(isset($project->user) && auth()->id() ==$project->user->id)
                <p class="mb-0 ml-2">{{$project->writer->username}}</p>
                @endif
            </div>
        </div>
        <div class="card msg_history" style="height: 60vh;">
            <div class="card-body" style="background-color: #f5f4f4;">
                <ul class="chat"  style="height: 58vh;">
                    @foreach($project->chats as $message)
                    @if($message->sender != auth()->user()->id)
                    <li class="left clearfix" >
                        <div class="chat-body clearfix">
                            <p>{!!$message->message!!}</p>
                        </div>
                        <div class="clearfix"></div>
                        <span class="msg-time"><i class="fa fa-clock-o"></i> {{$message->created_at->diffForHumans()}}</span>
                    </li>
                    @else
                    <li class="right clearfix" >
                        <div class="chat-body clearfix">
                            <p>{!!$message->message!!}</p>
                        </div>
                        <div class="clearfix"></div>
                        <span class="msg-time"><i class="fa fa-clock-o"></i> {{$message->created_at->diffForHumans()}}</span>
                    </li>
                    @endif
                    @endforeach
                </ul>
            </div>
        </div>

            @if(!$project->completed)
            <form class="frmMobSendMessage">
                @csrf
                <div class="input-group">
                    <input type="text" autocomplete="off" name="message" class="form-control" placeholder="Type a message" aria-label="Type a message" aria-describedby="basic-addon2">
                    <div class="input-group-append">
                        <button  class="btn btn-secondary sendBtn" type="submit">Send</button>
                    </div>
                </div>
            </form>
            @endif
            <!-- <div class="">
                <form action="/projects/{{$project->id}}/chat/{{$project->writer->id}}" class="message-reply" method="POST">
            <div class="">
                <form action="/projects/{{$project->id}}/chat/{{$project->writer->id}}" class="message-reply d-flex justify-content-between" method="POST">
                    @csrf
                    <input type="text" autocomplete="off" name="message" id="newMessage" class="form-control custom-responsive-chat-input" style="flex: 1;" autocomplete="off" placeholder="Type a message" />
                    <button class="msg_send_btn sendBtn" type="submit">Send</button>
                </form>
            </div> -->
        </div>
    </div>


    @push('js')
    <script>

        $('.frmSendMessage').on('submit', function(e){
            e.preventDefault();
            var msg = $('.frmSendMessage [name="message"]').val();
            $('.frmSendMessage .sendBtn').addClass('disabled');
            $('.frmSendMessage .sendBtn').html('Sending ...');
            axios.post('/projects/{{$project->id}}/chat/{{$project->writer->id}}',{
                message : msg
            })
            .then(function (response) {
                $('body ul.chat').animate({scrollTop:50000}, 3000);
                // window.location.href="/projects/{{$project->id}}#pills-chat";
                addMessage(response.data.message, 'right');
                $('.frmSendMessage [name="message"]').val('');
                $('.frmSendMessage .sendBtn').removeClass('disabled');
                $('.frmSendMessage .sendBtn').html('Send');
            })
            .catch(function (error) {
                alert('There was some problem sending your message. Please try again!')
                console.log(error);
            });
        });

        $('.frmMobSendMessage').on('submit', function(e){
            e.preventDefault();
            var msg = $('.frmMobSendMessage [name="message"]').val();
            $('.frmMobSendMessage .sendBtn').addClass('disabled');
            $('.frmMobSendMessage .sendBtn').html('Sending ...');
            axios.post('/projects/{{$project->id}}/chat/{{$project->writer->id}}',{
                message : msg
            })
            .then(function (response) {
                addMessage(response.data.message, 'right');
                $('body ul.chat').animate({scrollTop:50000}, 3000);
                $('.frmMobSendMessage [name="message"]').val('');
                $('.frmMobSendMessage .sendBtn').removeClass('disabled');
                $('.frmMobSendMessage .sendBtn').html('Send');
            })
            .catch(function (error) {
                alert('There was some problem sending your message. Please try again!')
                console.log(error);
            });
        });

            // window.Echo.private(`App.User.${Laravel.userId}`)
            window.Echo.private('App.User.' + window.Laravel.userId)
            .notification((notification) => {
                // console.log(notification);
                    if(notification.data.pid == {{$project->id}})
                    {
                        var message = notification.data.message;
                        addMessage(message, 'left');
                        $('body ul.chat').animate({scrollTop:50000}, 3000);
                    }
            });

        function addMessage(message, pos) {
            var msg = '<li class="'+pos+' clearfix">\
            <div class="chat-body clearfix">\
            <p>'+ message.message +'</p>\
            </div>\
            <div class="clearfix"></div>\
            <span class="msg-time"><i class="fa fa-clock-o"></i> '+ message.created_at + '</span>\
            </li>';

            $('ul.chat').append(msg);


        }

        $(document).ready(function(){
            var hash = window.location.hash;
            if (hash == '#pills-chat') {
                setTimeout(function() {
                    $('body ul.chat').scrollTop(50000);
                }, 5000);
                // $('body ul.chat').scrollTop(50000);
            }

            $('#pills-chat-tab').on('click', function(){
                $('body ul.chat').animate({scrollTop:50000}, 3000);
            });
        });
        /*setInterval(msgScrollBottom, 1000);
        function msgScrollBottom() {
            console.log('here');
            $('ul.chat').scrollTop($('ul.chat li'));
        }*/

    </script>
    @endpush