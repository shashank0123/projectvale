<div class="tab-pane fade in" id="pills-files" role="tabpanel" aria-labelledby="pills-files-tab">
    <div class="row project-file-upload">
        <div class="col-md-8 project-card desktop-project-card">
            @if($project->status != 1)
            @if(!$project->completed)
            <button type="button" class="btn mb-4 float-right btn-primary mt-3" data-toggle="modal" data-target="#addAdditionalFiles">Upload File</button>
            <div class="clearfix"></div>
            @endif
            <div class="row">
                @if(count($project->projectWriterFiles))
                <h5 class="col-sm-12 text-muted">Writer Uploaded</h5>
                @foreach($project->projectWriterFiles as $file)
                <div class="col-sm-4">
                    <div class="custom-file-upload mt-3">
                        <div class="attachments-container">
                            <div class="attachment-box ripple-effect">
                                @if($project->writer->id == auth()->id() && $file->user_id == $project->writer->id)
                                @if($file->is_verify == 1)
                                <span class="fileStatus badge-primary py-0 px-1">Verified</span>
                                @elseif($file->is_verify == -1)
                                <span class="fileStatus badge-danger py-0 px-1">Unverified</span>
                                @else
                                <span class="fileStatus badge-dark py-0 px-1">Pending Verification</span>
                                @endif
                                @endif
                                <a  target="_blank" href="{{Storage::url($file->file)}}" class="attachment-title">{{str_limit($file->name, 15)}}</a>
                                @if($file->plagiarism_report)
                                <div class="mb-2">
                                <a target="_blank" class="btn btn-sm" href="{{Storage::url($file->plagiarism_report)}}"><span class="fa fa-file"></span> Plagiarism Report</a>
                                </div>
                                @endif
                                @if($file->unverify_reason)
                                <div class="mb-2">
                                <a href="#unverifyReasonModal{{$file->id}}" data-toggle="modal" title="Unverify Reason" class="btn btn-sm"><span class="fa fa-file"></span> Unverify Reason</a>
                                </div>
                                <div class="modal fade in unverifyReasonModal{{$file->id}}" id="unverifyReasonModal{{$file->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title">Unverify Reason</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <p class="text-justify">{{$file->unverify_reason}}</p>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default bg-secondary text-white" data-dismiss="modal">Close</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @endif
                                <a target="_blank" class="btn btn-sm btn-secondary text-white mr-2" href="{{Storage::url($file->file)}}"><span class="fa fa-eye"></span> View</a>
                                @if(!$project->completed)
                                <a class="btn btn-sm btn-danger text-white" href="/files/{{$file->id}}/{{$project->id}}/delete" onclick="return confirm('Are you sure, You want to delete this file?');"><span class="fa fa-trash"></span> Delete</a>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
                @endif

                @if(count($project->projectUserFiles))
                <h5 class="col-sm-12 text-muted">User Uploaded</h5>
                @foreach($project->projectUserFiles as $file)
                <div class="col-sm-4">
                    <div class="custom-file-upload mt-3">
                        <div class="attachments-container">
                            <div class="attachment-box ripple-effect">
                                <a  target="_blank" href="{{Storage::url($file->file)}}" class="attachment-title">{{str_limit($file->name, 15)}}</a>
                                <a target="_blank" class="btn btn-sm btn-secondary text-white mr-2" href="{{Storage::url($file->file)}}"><span class="fa fa-eye"></span> View</a>
                                @if(!$project->completed)
                                <a class="btn btn-sm btn-danger text-white" href="/files/{{$file->id}}/{{$project->id}}/delete" onclick="return confirm('Are you sure, You want to delete this file?');"><span class="fa fa-trash"></span> Delete</a>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
                @endif
                @if(!count($project->projectUserFiles) && !count($project->projectWriterFiles))
                <div class="col-sm-12 project-card text-center no-posted-project">
                    <h4 >You dont have any project files yet!</h4>
                    <div class="my-md-5 my-sm-3 no-project-logo">
                        <img src="/img/file.png" alt="files">
                    </div>
                </div>
                @endif
            </div>


            @endif
        </div>
        <div class="col-sm-4 mt-3  project-card attachments-container-responsive user-profile-details">
            @include('projects.user.profile')
        </div>

        <div class="col-sm-12 file-listing pr-0">
            @if($project->status != 1)
            @if(!$project->completed)
            <!-- <button type="button" class="btn mb-4 float-right btn-primary mt-3" data-toggle="modal" data-target="#addAdditionalFiles">Upload File</button> -->
            <a href="#addAdditionalFiles" data-toggle="modal" style="display: none;" class="fab"> + </a>
            <div class="clearfix"></div>
            @endif
            @endif
            @if(count($project->projectfiles))
            @foreach($project->projectfiles as $file)
            <div class="d-flex flex-sm-column mt-3 project-file-tab">
                <img src="/img/pdf-page.png" alt="pdf" style="width: 40px;height: 40px;margin-left: 15px;margin-right: 10px;">
                <div>
                    <a target="_blank" href="{{Storage::url($file->file)}}">
                        <h5 class="align-self-start mb-0" style="font-size: 15px;">{{str_limit($file->name, 15)}}</h5>
                        <p class="" style="margin-top: 2px;font-size: 13px;">{{date('F d, Y', strtotime($file->created_at))}}</p>
                    </a>
                </div>
                <div class="float-right project-file-delete-btn" style="position: absolute; right: 30px;">
                    @if(!$project->completed)
                    <a href="/files/{{$file->id}}/{{$project->id}}/delete"  onclick="return confirm('Are you sure, You want to delete this file?');" class="btn btn-sm btn-light text-muted"><i class="fa fa-trash"></i></a>
                    @endif
                </div>
            </div>
            <hr class="my-0">
            @endforeach
            @endif
        </div>
    </div>
</div>
