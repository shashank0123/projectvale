<!-- Upload additional files
    ================================================== -->
  @if(isset($project))
    <div class="modal fade in addAdditionalFiles" id="addAdditionalFiles" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <form method="POST" action="/projects/{{$project->id}}/{{auth()->user()->id}}/files"  enctype="multipart/form-data">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Add Project Files</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    @csrf
                    <div class="modal-body">
                        <div class="row form-group">
                            <div class="col-xl-12 col-md-12">
                                <label>File Name <span style="color: red;">*</span></label>
                                <input type="text" class="form-control" name="name" required placeholder="Enter file name">
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-xl-12 col-md-12">
                                <div class="uploadButton mt-3">
                                    <input class="uploadButton-input form-control" type="file" id="uploadFile" name="file">
                                    <label class="uploadButton-button ripple-effect" for="uploadFile">Choose File</label>
                                    <span class="uploadButton-file-name">Click to upload</span>
                                </div>
                                <div class="projectFile"></div>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-xl-12 col-md-12">
                                <label>Description </label>
                                <textarea type="text" class="form-control" name="description" placeholder="Enter file description"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default bg-secondary text-white" data-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    @endif



    <!-- View Feedback Modal -->
    <div class="modal fade in" id="viewFeedbackModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Review</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="mb-3">
                        @if(isset($project->feedback) && $project->feedback->fullyComplete)
                        <span class="badge badge-success py-2 px-3"><i class="fa fa-check"></i> Fully Complete</span>
                        @else
                        <span class="badge badge-danger py-2 px-3"><i class="fa fa-times"></i> Fully Complete</span>
                        @endif
                        @if(isset($project->feedback) && $project->feedback->onTime)
                        <span class="badge badge-success py-2 px-3"><i class="fa fa-check"></i> On Time</span>
                        @else
                        <span class="badge badge-danger py-2 px-3"><i class="fa fa-times"></i> On Time</span>
                        @endif
                    </div>
                    <div class="projectRating"></div>
                    <hr>
                    <p>{{isset($project->feedback) && $project->feedback->review ? $project->feedback->review : ''}}</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary btn-lg btn-block" data-toggle="modal" data-dismiss="modal">Ok</button>
                </div>
            </div>
        </div>
    </div>

    
    @if(isset($project))
  
    <div class="modal fade in" id="FeedbackFormModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form method="POST" action="/projects/{{$project->id}}/complete">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Review</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        @csrf
                        <div class="form-group">
                            <label for="account-number" class="col-form-label">Is project fully complete?</label>
                            <br>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="fullyComplete" id="inlineRadio1" value="1" required>
                                <label class="form-check-label" for="inlineRadio1">Yes</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="fullyComplete" id="inlineRadio2" value="0">
                                <label class="form-check-label" for="inlineRadio2">No</label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="account-number" class="col-form-label">Was the project completed on time?</label>
                            <br>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="onTime" id="inlineRadio3" value="1" required>
                                <label class="form-check-label" for="inlineRadio3">Yes</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="onTime" id="inlineRadio4" value="0">
                                <label class="form-check-label" for="inlineRadio4">No</label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="bank-name" class="col-form-label">Your Rating</label>
                            <!-- <input type="text" class="form-control" placeholder="Enter Bank Name"> -->
                            <div class="rating"></div>
                        </div>
                        <div class="form-group">
                            <label for="exampleFormControlTextarea1">Your Feedback</label>
                            <textarea class="form-control" id="exampleFormControlTextarea1" name="review" rows="5"cols="4" required></textarea>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <a href="/projects/{{$project->id}}/skipcomplete" class="btn btn-default bg-secondary text-white btn-lg  mr-2" >Skip & Complete</a>
                        <button type="submit" class="btn btn-secondary btn-lg  mr-2" >Submit & Complete</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    @endif

    <!-- warning for review -->
    <div class="modal fade in" id="writerHistoryModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">History</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="mb-3" style="max-height: 350px; overflow: auto;">
                        <div class="mt-3 responsive-writer-profile">
                            @if(isset($history) && count($history))
                            <div class="list-group">
                                @foreach($history as $key => $item)
                                <div class="list-group-item list-group-item-action flex-column align-items-start">
                                    <div class="d-flex w-100 justify-content-between">
                                        <h5 class="mb-1">{{$item->topic}}</h5>
                                        <div class="brows-job-type">
                                            <span class="full-time {{ $item->project_status }} text-capitalize">{{ $item->project_status }}</span>
                                        </div>
                                    </div>
                                    <small>{{date('F d, Y', strtotime($item->created_at))}} - {{date('F d, Y', strtotime($item->deadline_date))}}</small>
                                </div>
                                @endforeach
                            </div>
                            @else
                            <h5>You have no work history yet.</h5>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary btn-lg btn-block" data-toggle="modal" data-dismiss="modal">Ok</button>
                </div>
            </div>
        </div>
    </div>

<!-- payment history -->
@if(isset($projects) && count($projects))
@foreach($projects as $key => $project)
 <div class="modal fade in" id="payment_history_{{$key}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Payment History</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="mb-6" style="max-height: 350px; overflow: auto;">
                        <div class="mt-6 responsive-writer-profile">
                            <div class="list-group">
                                 <div class="mt-4">
                                        <table class="table table-bordered">
                                            <thead>
                                                <tr>
                                                    <th>Topic</th>
                                                    <th>Date</th>
                                                    {{-- <th>Paid By</th> --}}
                                                    <th>Amount</th>
                                                    <th>Status</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>{{$project->topic}}</td>
                                                    <td>{{date('F d, Y', strtotime($project->created_at))}}</td>
                                                    {{-- <td>{{ucFirst($project->user->username)}}</td> --}}
                                                    <td>&#8377; {{($project->pages_count * $project->writer->project_rate_for_writer($project))}}</td>
                                                    <td><span class="badge badge-{{isset($project->admin_payment_status) && $project->admin_payment_status == 1 ? 'success' : 'danger'}} py-1 px-3">{{isset($project->admin_payment_status) && $project->admin_payment_status == 1 ? 'Paid' : 'Pending'}}</span></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary btn-lg btn-block" data-toggle="modal" data-dismiss="modal">Ok</button>
                </div>
            </div>
        </div>
    </div>
@endforeach
@endif