<div class="py-4 py-r-0">
    <div class="page-heading custom-project-gatway">
        <h2 class="d-inline">{!! $project->completed ? '<i class="fa fa-check-circle text-success"></i>' : '' !!} {{$project->topic}}</h2>
        <nav aria-label="breadcrumb" class="float-md-right">
            <ol class="breadcrumb bg-dark ">
                <li class="breadcrumb-item"><a href="/home" class="text-white">Home</a></li>
                <li class="breadcrumb-item"><a href="/home" class="text-white">Projects</a></li>
                <li class="breadcrumb-item text-white active" aria-current="page">{{str_limit($project->topic, 20)}}</li>
            </ol>
        </nav>
    </div>
    <div class="clearfix"></div>
    @section('heading', 'Project')
    <ul class="nav-pills-projectvala nav nav-pills" id="pills-tab" role="tablist">
        @if($project->payment_status)
        <li class="nav-item">
            <a class="nav-link active" id="pills-detail-tab" href="#pills-detail">Detail</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="pills-file-tab" href="#pills-files">Files</a>
        </li>
       <!--  <li class="nav-item">
            <a class="nav-link" id="pills-chat-tab" href="#pills-chat">Chat</a>
        </li> -->
        @endif
    </ul>
    <div class="tab-content" id="pills-tabContent">
        <div class="tab-pane fade in show active" id="pills-detail" role="tabpanel" aria-labelledby="pills-detail-tab">
            <div class="pb-4">
                <div class="">
                 @if($errors->any())
                 <div class="alert alert-danger">
                    <div class="alert-body">
                        @foreach($errors->all() as $error)
                        {{ $error }}
                        @endforeach
                    </div>
                </div>
                @endif
                <div class="my-md-3 responsive-writer-profile-description">
                    <div class="row">
                        <div class="col-sm-8 project-card">
                            <div class="card rounded-0 border-top-green">
                                <div class="card-body">
                                    <div class="apply-job-detail d-inline-block">
                                        <p class="mb-0 pl-1">{!!$project->instructions!!}</p>
                                    </div>
                                    @if($project->user_id == auth()->id() && !$project->completed)
                                    <div class="d-inline-block float-md-right headline pt-2">
                                        <div class="float-xl-right">
                                            <a href="/projects/{{$project->id}}/edit" class="btn btn-secondary btn-sm ml-1"><i class="fa fa-pencil"></i> Edit</a>
                                            @if($project->status == 0 || $project->status == 1 || $project->status == 2)
                                            <a href="/projects/{{$project->id}}/delete"  onclick="return confirm('Are you sure, You want to delete this project?');" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i> Delete</a>
                                            @endif
                                        </div>
                                    </div>
                                    @endif
                                    <!-- <h5>Specifications</h5> -->
                                    <div class="apply-job-detail pt-3">
                                        <!-- <h5>Subject</h5> -->
                                        <ul class="skills" style="margin-top: -15px;margin-bottom: 0px;">
                                            <li class="">{{$project->subject}}</li>
                                            <!-- <li class="">{{$project->type}}</li> -->
                                        </ul>
                                    </div>
                                    <div class="apply-job-header">
                                        <span class="cl-success"><i class="fa fa-calendar-check-o" aria-hidden="true"></i>{{date('F d, Y', strtotime($project->deadline_date))}}</span>
                                        <span class="ml-2"><i class="fa fa-clock-o" aria-hidden="true"></i>{{$project->deadline_time}}</span>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="apply-job-detail">
                                                <h5>Specifications</h5>
                                                <ul class="job-requirements">
                                                    <li><span>Type :</span> {{$project->type}}</li>
                                                    <!-- <li><span>Type of service :</span> <span class="text-capitalize"> {{$project->type_of_service}}</span></li> -->
                                                    <!-- <li><span>Writting Quality :</span> {{$project->writing_quality}}</li> -->
                                                    <li><span>No. of Citation :</span> {{$project->citations_count}}</li>
                                                    <li><span>Citation Format :</span> {{$project->citation_format}}</li>
                                                    <li><span>Order Number :</span> {{$project->id}}</li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                        @if(isset($project->files))
                                        <div class="row mb-3">
                                            <div class="col-sm-12">
                                                <h5>Files</h5>
                                            </div>
                                            @foreach($project->files as $key => $file)
                                            <div class="col-sm-4">
                                                <div class="custom-file-upload mt-3">
                                                    <div class="attachments-container">
                                                        <div class="attachment-box ripple-effect">
                                                            <a  target="_blank" href="{{Storage::url($file)}}" class="attachment-title">Project File {{$key + 1}}</a>
                                                            <a target="_blank" class="btn btn-sm btn-secondary text-white mr-2" href="{{Storage::url($file)}}"><span class="fa fa-eye"></span> View</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            @endforeach
                                        </div>
                                        @endif
                                    @if(isset($project->additional_instructions))
                                    <div class="apply-job-detail d-inline-block">
                                        <h5>Additional Instructions</h5>
                                        <p class="mb-0 pl-1 text-justify">{!!$project->additional_instructions!!}</p>
                                    </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4  project-card attachments-container-responsive">
                            @include('projects.user.profile')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    @include('projects.partials.files')
    @include('projects.partials.chat')
</div>
</div>
</div>
@include('projects.partials.modals')


