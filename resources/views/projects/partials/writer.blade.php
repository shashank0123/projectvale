<div class="my-md-5 my-sm-0">
    @section('heading', 'Hire Writer')
    <div class="headline d-flex justify-content-between align-items-center hire-writer-profile">
        <h3 class="d-inline-block">{{$project->topic}} <small>(Please hire writer)</small></h3>
        <a href="/home" class="btn btn-default bg-secondary text-white">Back</a>
    </div>

    <div class="clearfix"></div>
    <div class="row pb-4 pt-4 user-list-writer">
        @if(count($writers))
        @foreach($writers as $key => $writer)
        <div class="col-sm-4 col-md-4 col-lg-4">
            <div class="card text-center custom-writer-list mb-md-5">
                <div class="card-body">
                    {{-- <div class="writer-status">
                        <span class="freelance-status {{ $writer->isWriterAvailable($project) ? 'unavailable' : 'available'}}">{{ $writer->isWriterAvailable($project) ? 'Unavailable' : 'Available'}}</span>
                        <!-- <span class="freelance-status">Verified</span> -->
                        <h4 class="flc-rate"><i class="fa fa-inr"></i> {{$writer->project_rate($project)}} / word</h4>
                    </div> --}}
                    {{-- <div class="pt-5">
                        @if(isset($writer->profile_photo) && $writer->profile_photo)
                        <img src="{{Storage::url($writer->profile_photo)}}" style="width:100px;height:100px !important;" class="mb-3 img-thumbnail rounded-circle">
                        @else
                        <img src="{{asset('img/user.png')}}" class="mb-3 img-thumbnail rounded-circle"  style="width:100px; height: 100px;">
                        @endif
                    </div> --}}
                    {{-- <div class="classroom-writer-section ml-0">
                        <h2 class="post-classic-title pt-3 mb-2">
                            <a href="#">{{str_limit($writer->username, 19)}}</a>
                        </h2>
                        <!-- <span class="mb-2">{{str_limit($writer->email, 23)}}</span> -->
                        <span class="badge badge-success d-inline text-white">{{$writer->quality}}</span>
                    </div> --}}
                    {{-- <div class="mt-2 writerRating{{$key}}"></div>
                    <div class="freelance-box-extra">
                        <ul>
                            <?php $subjects = explode(',', $writer->subjects); ?>
                            @foreach($subjects as $subject)
                            <li>{{ $subject}}</li>
                            @endforeach
                            <!-- <li class="more-skill bg-primary">+3</li> -->
                            <!-- <p class="pt-2">Lorem ipsum dolor sit amet, consectetur adipisicing.</p> -->
                        </ul>
                    </div> --}}
                    {{-- <a  href="/projects/{{$project->id}}/writer/{{$writer->id}}/profile" class="btn btn-outline-primary btn-block mt-3">View Profile</a>
                    <a href="/projects/{{$project->id}}/writer/{{$writer->id}}" class="btn btn-primary btn-block mt-3" style="background-color: #01c73d;border-color: #01c73d;">Hire</a> --}}
                </div>
            </div>
        </div>
        @endforeach
        @else
        <div class="col-sm-12">
            <div class="empty-box row">
                <div class="col-md-12 text-center">
                     <h4 >No writers available!</h4>
                     <p>No writers available on ProjectVala for your project.</p>
                     <div class="my-md-5 my-sm-3 no-project-logo">
                        <img src="/img/user.png" class="rounded-circle" alt="project">
                     </div>
                </div>
            </div>
        </div>
        @endif
    </div>
</div>
