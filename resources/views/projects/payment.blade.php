@extends('layouts.app')

@section('content')

<div class="py-4 py-r-0">
    <div class="page-heading custom-project-gatway">
        <h2 class="d-inline">{!! $project->completed ? '<i class="fa fa-check-circle text-success"></i>' : '' !!} {{$project->topic}}</h2>
    </div>
    <div class="clearfix"></div>
    @section('heading', 'Project')

    <div class="pb-4">
        <div class="">
            @if($errors->any())
            <div class="alert alert-danger">
                <div class="alert-body">
                    @foreach($errors->all() as $error)
                    {{ $error }}
                    @endforeach
                </div>
            </div>
            @endif
            <div class="my-md-3 responsive-writer-profile-description">
                <div class="row">
                    <div class="col-sm-8 project-card">
                        <div class="card rounded-0 border-top-green">
                            <div class="card-body">
                                <div class="apply-job-detail d-inline-block">
                                    <p class="mb-0 pl-1">{!!$project->instructions!!}</p>
                                </div>

                                <div class="apply-job-detail pt-3">
                                    <ul class="skills" style="margin-top: -15px;margin-bottom: 0px;">
                                        <li class="">{{$project->subject}}</li>
                                        <!-- <li class="">{{$project->type}}</li> -->
                                    </ul>
                                </div>
                                <div class="apply-job-header">
                                    <span class="cl-success"><i class="fa fa-calendar-check-o" aria-hidden="true"></i>{{date('F d, Y', strtotime($project->deadline_date))}}</span>
                                    <span class="ml-2"><i class="fa fa-clock-o" aria-hidden="true"></i>{{$project->deadline_time}}</span>
                                </div>
                                <div class="row">
                                        <div class="col-md-12">
                                            <div class="apply-job-detail">
                                                <h5>Specifications</h5>
                                                <ul class="job-requirements">
                                                    <li><span>Type :</span> {{$project->type}}</li>
                                                    <!-- <li><span>Type of service :</span> <span class="text-capitalize"> {{$project->type_of_service}}</span></li> -->
                                                    <!-- <li><span>Writting Quality :</span> {{$project->writing_quality}}</li> -->
                                                    <li><span>No. of Citation :</span> {{$project->citations_count}}</li>
                                                    <li><span>Citation Format :</span> {{$project->citation_format}}</li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                        @if(isset($project->files))
                                        <div class="row mb-3">
                                            <div class="col-sm-12">
                                                <h5>Files</h5>
                                            </div>
                                            @foreach($project->files as $key => $file)
                                            <div class="col-sm-4">
                                                <div class="custom-file-upload mt-3">
                                                    <div class="attachments-container">
                                                        <div class="attachment-box ripple-effect">
                                                            <a  target="_blank" href="{{Storage::url($file)}}" class="attachment-title">Project File {{$key + 1}}</a>
                                                            <a target="_blank" class="btn btn-sm btn-secondary text-white mr-2" href="{{Storage::url($file)}}"><span class="fa fa-eye"></span> View</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            @endforeach
                                        </div>
                                        @endif
                                    @if(isset($project->additional_instructions))
                                    <div class="apply-job-detail d-inline-block">
                                        <h5>Additional Instructions</h5>
                                        <p class="mb-0 pl-1 text-justify">{!!$project->additional_instructions!!}</p>
                                    </div>
                                    @endif
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4  project-card attachments-container-responsive">
                        <div class="card rounded-0 mb-md-3 border-top-green">
                            <div class="card-body">
                                <h4>Payment Details</h4>
                                <div class="mt-3">
                                    <div class="mt-0 mb-2">
                                        <b>Writer : </b>{{$writer->username}}
                                    </div>

                                    <div class="row mt-3">
                                        <div class="col">Word of Count : </div>
                                        <div class="col text-right">{{$project->pages_count}}</div>
                                    </div>
                                    <div class="row mt-1">
                                        <div class="col"></div>
                                        <div class="col"></div>
                                        <div class="col"></div>
                                        <div class="col text-right">x</div>
                                        <div class="col"></div>
                                    </div>
                                    <div class="row mt-1">
                                        <div class="col">Price per word : </div>
                                        <div class="col text-right"><i class="fa fa-inr"></i> {{$writer->project_rate($project)}}</div>
                                    </div>
                                    <div class="row mt-3 turnitinPrice" style="display: none;">
                                        <div class="col">Turnitin Report : </div>
                                        <div class="col text-right"><i class="fa fa-inr"></i> {{isset(getGlobalSettings()->plagiarism_amt) ? getGlobalSettings()->plagiarism_amt : 0}}</div>
                                    </div>
                                    <hr>
                                    <div class="row mt-2 mb-2">
                                        <div class="col font-weight-bold">Total</div>
                                        <div class="col font-weight-bold text-right"><i class="fa fa-inr"></i> <span id="paymentTotal"> {{$project->pages_count * $writer->project_rate($project)}} </span></div>
                                    </div>
                                </div>
                                <!-- <a href="/projects/{{$project->id}}/{{$writer->id}}/pay" class="btn btn-secondary btn-freelance bt-1 border-0 mt-3 text-white">Pay</a> -->
                                <div class="my-3 p-3"  style="border: 1px dashed #ccc;">
                                    <div class="d-flex justify-space-between align-items-center">
                                        <div  style="flex: 1">
                                            <p><b>Add on</b></p>
                                            <p class="mb-1">Turnitin Report ( <i class="fa fa-inr"></i> {{isset(getGlobalSettings()->plagiarism_amt) ? getGlobalSettings()->plagiarism_amt : 0}} )</p>
                                            <a href="#plagiarismModal" data-toggle="modal" style="font-size: 12px;">View Details</a>
                                        </div>
                                        <div class="d-flex" >
                                            <input type="checkbox" name="plagiarism" id="chk-plagiarism" value="{{isset(getGlobalSettings()->plagiarism_amt) ? getGlobalSettings()->plagiarism_amt : 0}}">
                                        </div>
                                    </div>
                                </div>
                                <a href="javascript:;" id="rzp-button1" class="btn btn-secondary  btn-freelance bt-1 border-0 text-white">Pay</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



<div class="modal fade in plagiarismModal" id="plagiarismModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Plagiarism</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="text-center mb-4">
                    <img src="{{ asset('img/plagiarism/logo.png') }}">
                </div>
                <div class="row">
                    <div class="col-sm-5">
                        <img src="{{ asset('img/plagiarism/scene-a.png') }}" class="img-fluid">
                    </div>
                    <div class="col-sm-7">
                        <h4>Prevent Plagiarism</h4>
                        <p class="text-justify">Identify unoriginal content with the world’s most effective plagiarism detection solution. Manage potential academic misconduct by highlighting similarities to the world’s largest collection of internet, academic, and student paper content.</p>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary btn-select-plagiarism">Select</button>
            </div>
        </div>
    </div>
</div>


@endsection
@section('scripts')
<script src="https://checkout.razorpay.com/v1/checkout.js"></script>

<script>

    $('.btn-select-plagiarism').on('click', function(){
        $('.plagiarismModal').modal('hide');
        $('#chk-plagiarism').trigger('click');
        // $('#paymentTotal').html((parseInt({{($project->pages_count * $writer->project_rate($project)) * 100}}) + (plagiarismAmt * 100))/ 100);
    });

    var amount = parseInt({{($project->pages_count * $writer->project_rate($project)) * 100}});
    $('#chk-plagiarism').on('click', function(){
        var plagiarismAmt = $(this).val();
        if($(this).is(':checked')){
            amount = parseInt({{($project->pages_count * $writer->project_rate($project)) * 100}}) + (plagiarismAmt * 100);
            $('.turnitinPrice').show();
            $('#paymentTotal').html((parseInt({{($project->pages_count * $writer->project_rate($project)) * 100}}) + (plagiarismAmt * 100)) / 100 );
            var options = {
                "key": "rzp_test_CBPqELxRN9Vec5",
                "amount": amount, // 2000 paise = INR 20
                "name": "Projectvala",
                "description": "Payment for {{$project->topic}}",
                "image": "{{url('img/logo.png')}}",
                "handler": function (response){
                    console.log(response);
                    if (response.razorpay_payment_id) {
                        axios.post('/projects/{{$project->id}}/{{$writer->id}}/pay', {amount: amount})
                        .then(function (response) {
                            window.location.href="/projects/{{$project->id}}";
                        })
                        .catch(function (error) {
                            alert('There was some problem processing your payment. Please try again!')
                            console.log(error);
                        });
                    }else{

                    }
                },
                "modal": {
                    "ondismiss": function(){
                        $('#rzp-button1').removeClass('disabled');
                        $('#rzp-button1').html('Pay');
                    }
                },
                "prefill": {
                    "name": "{{auth()->user()->fullname}}",
                    "email": "{{auth()->user()->email}}",
                    "contact": "{{auth()->user()->phone}}"
                },
                "notes": {
                    "address": "Hello World"
                },
                "theme": {
                    "color": "#96c228"
                }
            };
            var rzp1 = new Razorpay(options);

            document.getElementById('rzp-button1').onclick = function(e){
                axios.get('/projects/{{$project->id}}/turniton')
                .then(function (response) {
                })
                .catch(function (error) {
                    console.log(error);
                });
                rzp1.open();
                e.preventDefault();
                $(this).addClass('disabled');
                $(this).html('Waiting...');
            }
        }else{
            $('.turnitinPrice').hide();
            $('#paymentTotal').html({{$project->pages_count * $writer->project_rate($project)}});
            amount = parseInt({{($project->pages_count * $writer->project_rate($project)) * 100}});
            var options = {
                "key": "rzp_test_CBPqELxRN9Vec5",
                "amount": amount, // 2000 paise = INR 20
                "name": "Projectvala",
                "description": "Payment for {{$project->topic}}",
                "image": "{{url('img/logo.png')}}",
                "handler": function (response){
                    console.log(response);
                    if (response.razorpay_payment_id) {
                        axios.post('/projects/{{$project->id}}/{{$writer->id}}/pay')
                        .then(function (response) {
                            window.location.href="/projects/{{$project->id}}";
                        })
                        .catch(function (error) {
                            alert('There was some problem processing your payment. Please try again!')
                            console.log(error);
                        });
                    }else{

                    }
                },
                "modal": {
                    "ondismiss": function(){
                        $('#rzp-button1').removeClass('disabled');
                        $('#rzp-button1').html('Pay');
                    }
                },
                "prefill": {
                    "name": "{{auth()->user()->fullname}}",
                    "email": "{{auth()->user()->email}}",
                    "contact": "{{auth()->user()->phone}}"
                },
                "notes": {
                    "address": "Hello World"
                },
                "theme": {
                    "color": "#96c228"
                }
            };
            var rzp1 = new Razorpay(options);

            document.getElementById('rzp-button1').onclick = function(e){
                rzp1.open();
                e.preventDefault();
                $(this).addClass('disabled');
                $(this).html('Waiting...');
            }
        }
    });
    var options = {
        "key": "rzp_test_CBPqELxRN9Vec5",
        "amount": amount, // 2000 paise = INR 20
        "name": "Projectvala",
        "description": "Payment for {{$project->topic}}",
        "image": "{{url('img/logo.png')}}",
        "handler": function (response){
            console.log(response);
            if (response.razorpay_payment_id) {
                axios.post('/projects/{{$project->id}}/{{$writer->id}}/pay')
                .then(function (response) {
                    window.location.href="/projects/{{$project->id}}";
                })
                .catch(function (error) {
                    alert('There was some problem processing your payment. Please try again!')
                    console.log(error);
                });
            }else{

            }
        },
        "modal": {
            "ondismiss": function(){
                $('#rzp-button1').removeClass('disabled');
                $('#rzp-button1').html('Pay');
            }
        },
        "prefill": {
            "name": "{{auth()->user()->fullname}}",
            "email": "{{auth()->user()->email}}",
            "contact": "{{auth()->user()->phone}}"
        },
        "notes": {
            "address": "Hello World"
        },
        "theme": {
            "color": "#96c228"
        }
    };
    var rzp1 = new Razorpay(options);

    document.getElementById('rzp-button1').onclick = function(e){
        rzp1.open();
        e.preventDefault();
        $(this).addClass('disabled');
        $(this).html('Waiting...');
    }
</script>
@endsection