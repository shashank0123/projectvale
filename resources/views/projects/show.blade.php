@extends('layouts.app')

@section('styles')
<style type="text/css">

</style>
@endsection

@section('content')

@if(isset($project->writer))
    @include('projects.partials.project')
@else
    @include('projects.partials.writer')
@endif
@endsection
@section('scripts')
<script>
    $('div.rating').raty({ starType: 'i' });
    $('div.projectRating').raty({ starType: 'i',readOnly:true, score:'{{isset($project->feedback) && $project->feedback->rating ? $project->feedback->rating :'3.0' }}' });

    $('div.projectWriterRating').raty({ starType: 'i',readOnly:true, score:'{{isset($project->writer) && $project->writer->rating ? $project->writer->rating :'3.0' }}' });

    @foreach ($writers as $key => $writer)
        $('div.writerRating{{$key}}').raty({ starType: 'i',readOnly:true, score:'{{isset($writer->rating) && $writer->rating ? $writer->rating : '3.0' }}' });
    @endforeach

        $(document).ready(function(){
            var hash = window.location.hash;
            console.log(hash);
            $('ul.nav-pills-projectvala a[href="' + hash + '"]').tab('show');

            $('.nav-pills-projectvala .nav-link').on('click', function(){
                var url = $(this).attr('href');
                $('ul.nav-pills-projectvala a[href="' + url + '"]').tab('show');
            });
        });

</script>
@endsection