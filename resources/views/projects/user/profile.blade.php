@if(auth()->id() != $project->writer_id)
<div class="card rounded-0 mb-md-3 border-top-green">
    <div class="card-body">
        <div class="writer-status">
            <span class="freelance-status {{ $project->writer->writer_is_available ? 'unavailable' : 'available'}}">{{ $project->writer->writer_is_available ? 'Unavailable' : 'Available'}}</span>
            <h4 class="flc-rate">&#8377; {{ $project->writer->project_rate($project)}} / word</h4>
        </div>
        <div class="text-center mt-5">
            @if(isset($project->writer) && $project->writer->profile_photo)
            <img src="{{Storage::url($project->writer->profile_photo)}}" style="width:100px;height:100px !important;" class="mb-3 img-thumbnail rounded-circle">
            @else
            <img src="{{asset('img/user.png')}}" class="mb-3 img-thumbnail rounded-circle"  style="width:100px; height: 100px;">
            @endif
            <a href="#" style="color: #677484;text-decoration: none;">
                <h4 class="mt-0 mb-2 sidebar-box-detail">
                    {{isset($project->writer) ? $project->writer->username : ''}}
                </h4>
            </a>
            <!-- <span class="mt-0 mb-2 desination d-block">
                {{isset($project->writer) ? $project->writer->email : ''}}
            </span> -->
            <span class="badge badge-success d-inline text-white">{{isset($project->writer) ? $project->writer->quality : ''}}</span>
            <div class="mt-1 projectWriterRating"></div>
        </div>
        <div class="freelance-box-extra text-center">
            <ul>
                <?php $subjects = explode(',', $project->writer->subjects); ?>
                @foreach($subjects as $subject)
                <li>{{ $subject}}</li>
                @endforeach
            </ul>
        </div>
        @if($project->status == 4)
        @if(isset($project->feedback))
        <a href="#viewFeedbackModal" class="btn btn-secondary btn-freelance bt-1 border-0 mt-3" data-toggle="modal"  style="color: #fff;">View Feedback</a>
        @else
        <a href="javascript:;" class="btn btn-secondary btn-freelance bt-1 border-0 mt-3"  style="color: #fff;">No Feedback</a>
        @endif
        @elseif($project->status == 1)
        <a href="/projects/{{$project->id}}/{{$project->writer->id}}/cancelrequest"class="btn btn-secondary mb-3 bt-1 border-0 btn-freelance text-white">Cancel Request</a>
        <div class="bg-dark p-3 text-center w-100  text-white">Waiting for Writers Confirmation</div>
        @elseif($project->status == 2)
        @if(isset($project->writer))
        <a href="/projects/{{$project->id}}/{{$project->writer->id}}/payment"class="btn btn-secondary bt-1 border-0 btn-freelance text-white">Pay</a>
        @endif
        @else
        <!-- <a href="/projects/{{$project->id}}/revoke" onclick="return confirm('Are you sure, You want to revoke this writer? When you revoke the writer, all your chats and files will be deleted.');" class="btn btn-danger btn-freelance bt-1 border-0" style="color: #fff;">Revoke Writer</a> -->
        <a href="#FeedbackFormModal" class="btn btn-secondary btn-freelance bt-1 border-0 mt-3" data-toggle="modal"  data-whatever="@mdo" style="color: #fff;">Complete Project</a>
        @endif
    </div>
</div>
@else
<div class="card rounded-0 mb-3 border-top-green">
    <div class="card-body">
        <div class="text-center mt-5">
            {{-- @if(isset($project->user) && $project->user->profile_photo)
            <img src="{{Storage::url($project->user->profile_photo)}}" style="width:100px;height:100px !important;" class="mb-3 img-thumbnail rounded-circle">
            @else
            <img src="{{asset('img/user.png')}}" class="mb-3 img-thumbnail rounded-circle"  style="width:100px; height: 100px;">
            @endif --}}
            {{-- <a href="#" style="color: #677484;text-decoration: none;">
                <h4 class="mt-0 mb-2 sidebar-box-detail">
                    {{isset($project->user) ? $project->user->username : ''}}
                </h4>
            </a> --}}
            <!-- <span class="mt-0 mb-2 desination">
                {{isset($project->user) ? $project->user->email : ''}}
            </span> -->
        </div>

        @if($project->status == 4)
        <a href="#viewFeedbackModal" class="mt-2 btn btn-secondary btn-freelance bt-1 border-0 mt-3" data-toggle="modal"  style="color: #fff;">View Feedback</a>
        <a href="#writerHistoryModal" data-toggle="modal" class="mt-2 btn btn-dark btn-freelance bt-1 border-0" style="color: #fff;">Projects History</a>
        @elseif($project->status == 1)
        <a href="/projects/{{$project->id}}/writer/{{auth()->id()}}/accept" class="mt-2 btn btn-secondary btn-freelance bt-1 border-0" style="color: #fff;">Accept Project</a>
        <a href="/projects/{{$project->id}}/writer/{{auth()->id()}}/decline" class="mt-2 btn btn-outline-primary btn-freelance bt-1">Decline Project</a>
        @elseif($project->status == 2)
        <a href="javascript:;" class="mt-2 btn btn-primary btn-freelance bt-1 border-0">Waiting for Payment</a>
        @else
        <!-- <a href="/projects/{{$project->id}}/revoke"  onclick="return confirm('Are you sure, You want to revoke this project? When you revoke the project, all your chats and files will be deleted.');" class="mt-2 btn btn-danger btn-freelance bt-1 border-0" style="color: #fff;">Revoke Project</a> -->
        <a href="#writerHistoryModal" data-toggle="modal" class="mt-2 btn btn-dark btn-freelance bt-1 border-0" style="color: #fff;">Projects History</a>
        @endif
    </div>
</div>
@endif