<!DOCTYPE html>
<html lang="en-US">
<head>

    <!-- Meta
    ============================================= -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, intial-scale=1, max-scale=1">

    <meta name="author" content="Trumpets">
    <!-- description -->
    <meta name="description" content="">
    <!-- keywords -->
    <meta name="keywords" content="">

    <!-- Stylesheets
    ============================================= -->
    <link href="/landing/css/css-assets.css" rel="stylesheet">
    <link href="/landing/css/style.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,400i,600,600i" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,400i,700i,700" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

    <!-- Favicon
    ============================================= -->
    <link rel="shortcut icon" href="/img/logo.png">

    <style type="text/css">
        .style-switcher.show {
     display: none !important;
}
    </style>

    <!-- Title
    ============================================= -->
    <title>ProjectVala</title>

</head>

<body>

    <div id="scroll-progress"><div class="scroll-progress"><span class="scroll-percent"></span></div></div>

    <!-- Document Full Container
    ============================================= -->
    <div id="full-container">

        <!-- Banner
        ============================================= -->
        <section id="banner">

            <div class="banner-parallax" data-banner-height="700">
                <img src="/landing/images/files/parallax-bg/img-2.jpg" alt="">
                <div class="overlay-colored color-bg-white opacity-90"></div><!-- .overlay-colored end -->
                <div class="slide-content">

                    <div class="">
                        <div class="row">
                            <div class="col-md-12">

                                <div class="banner-center-box text-center">
                                    <a href="/"><img class="banner-logo" style="width: 300px;" src="/img/logo-navbar.png" alt=""></a>
                                    <h1 class="heading-section">
                                        <span class="colored">Advisory</span> as good as your mom!
                                    </h1>
                                    <div class="description">
                                        Find someone reliable to guide you with your assignments.
                                    </div>

                                    <div class="d-flex login-register-btn">
                                        <a href="{{ route('login') }}">Sign In</a>

                                        <a href="{{ route('register') }}">Sign Up</a>
                                    </div>

                                    <div class="d-flex" style="margin-top: 45px;">

                                        <a href="/terms-of-conditions" style="padding-right: 24px;color: #898989; font-weight: bold;">Terms & Conditions</a>


                                        <a href="/privacy-policy" style="padding-right: 24px;color: #898989; font-weight: bold;">Privacy Policy</a>


                                        <a href="/contact-us" style="color: #898989; font-weight: bold;">Contact Us</a>

                                    </div>
                                </div><!-- .banner-center-box end -->

                            </div><!-- .col-md-12 end -->
                        </div><!-- .row end -->
                    </div><!-- .container end -->

                </div><!-- .slide-content end -->
            </div><!-- .banner-parallax end -->

        </section><!-- #banner end -->

        <!-- Content
        ============================================= -->
        <section id="content">

            <div id="content-wrap">
                <!-- === Intro Features =========== -->
                <div id="cta-video-lightbox" class="parallax-section text-white">

                    <img src="/landing/images/files/parallax-bg/img-2.jpg" alt="">
                    <div class="overlay-colored color-bg-gradient opacity-90"></div><!-- .overlay-colored end -->
                    <div class="section-content">

                        <div class="container">
                            <div class="row">
                                <div class="col-md-4 col-sm-4">
                                    <div class="box-info box-info-1 text-center">
                                        <div class="box-icon icon"><i class="fa fa-lightbulb"></i></div>
                                        <div class="box-content">
                                            <h5>Well Researched</h5>
                                            <p>You can choose instructor of your preference for good quality research.</p>
                                        </div><!-- .box-content end -->
                                    </div><!-- .box-info end -->
                                </div><!-- .col-md-3 end -->
                                <div class="col-md-4 col-sm-4">
                                    <div class="box-info box-info-1 text-center">
                                        <div class="box-icon icon"><i class="fa fa-eye"></i></div>
                                        <div class="box-content">
                                            <h5>Better Privacy</h5>
                                            <p>Your information is safe and confidential with us.</p>
                                        </div><!-- .box-content end -->
                                    </div><!-- .box-info end -->
                                </div>
                                <div class="col-md-4 col-sm-4">
                                    <div class="box-info box-info-1 text-center">
                                        <div class="box-icon icon"><i class="fa fa-random"></i></div>
                                        <div class="box-content">
                                            <h5>100% Unique</h5>
                                            <p>Delivery of plagiarism-free content.</p>
                                        </div><!-- .box-content end -->
                                    </div><!-- .box-info end -->
                                </div>
                            </div><!-- .row end -->
                        </div><!-- .container end -->
                    </div><!-- .section-content end -->
                </div><!-- .flat-section end -->
            </div>
        </section>


                <!-- === Clients Testimonials =========== -->
                {{-- <div id="clients-testimonials" class="flat-section">

                    <div class="section-content">

                        <div class="container">
                            <div class="row">
                                <div class="col-md-8 col-md-offset-2">

                                    <div class="section-title text-center">
                                        <h2>What Customers Say</h2>
                                        <p>
                                            This should be used to tell a story about your product or service. How can you benefit them?
                                        </p>
                                    </div><!-- .section-title end -->

                                </div><!-- .col-md-8 end -->
                                <div class="col-md-12">

                                    <div class="slider-testimonials">
                                        <ul class="owl-carousel">
                                            <li>
                                                <div class="slide">
                                                    <div class="testimonial-single-1">
                                                        <div class="ts-person">
                                                            <div class="ts-img">
                                                                <img src="/landing/images/files/sliders/clients-testimonials/img-1.jpg" alt="">
                                                            </div><!-- .ts-img end -->
                                                            <h5>Mark Smith</h5>
                                                            <span>Envato Inc.</span>
                                                        </div><!-- .ts-person end -->
                                                        <div class="ts-content">
                                                            This should be used to tell a story and let your users know about your product or service.
                                                            <div class="rating">
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                            </div><!-- .rating end -->
                                                        </div><!-- .ts-content end -->
                                                    </div><!-- .testimonial-single-1 -->
                                                </div><!-- .slide end -->
                                            </li>
                                            <li>
                                                <div class="slide">
                                                    <div class="testimonial-single-1">
                                                        <div class="ts-person">
                                                            <div class="ts-img">
                                                                <img src="/landing/images/files/sliders/clients-testimonials/img-2.jpg" alt="">
                                                            </div><!-- .ts-img end -->
                                                            <h5>Vera Duncan</h5>
                                                            <span>PayPal Inc.</span>
                                                        </div><!-- .ts-person end -->
                                                        <div class="ts-content">
                                                            This should be used to tell a story and let your users know about your product or service.
                                                            <div class="rating">
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                            </div><!-- .rating end -->
                                                        </div><!-- .ts-content end -->
                                                    </div><!-- .testimonial-single-1 -->
                                                </div><!-- .slide end -->
                                            </li>
                                            <li>
                                                <div class="slide">
                                                    <div class="testimonial-single-1">
                                                        <div class="ts-person">
                                                            <div class="ts-img">
                                                                <img src="/landing/images/files/sliders/clients-testimonials/img-3.jpg" alt="">
                                                            </div><!-- .ts-img end -->
                                                            <h5>Bryce Vaughn</h5>
                                                            <span>Unbounce Inc.</span>
                                                        </div><!-- .ts-person end -->
                                                        <div class="ts-content">
                                                            This should be used to tell a story and let your users know about your product or service.
                                                            <div class="rating">
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                            </div><!-- .rating end -->
                                                        </div><!-- .ts-content end -->
                                                    </div><!-- .testimonial-single-1 -->
                                                </div><!-- .slide end -->
                                            </li>
                                            <li>
                                                <div class="slide">
                                                    <div class="testimonial-single-1">
                                                        <div class="ts-person">
                                                            <div class="ts-img">
                                                                <img src="/landing/images/files/sliders/clients-testimonials/img-1.jpg" alt="">
                                                            </div><!-- .ts-img end -->
                                                            <h5>Mark Smith</h5>
                                                            <span>Envato Inc.</span>
                                                        </div><!-- .ts-person end -->
                                                        <div class="ts-content">
                                                            This should be used to tell a story and let your users know about your product or service.
                                                            <div class="rating">
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                            </div><!-- .rating end -->
                                                        </div><!-- .ts-content end -->
                                                    </div><!-- .testimonial-single-1 -->
                                                </div><!-- .slide end -->
                                            </li>
                                            <li>
                                                <div class="slide">
                                                    <div class="testimonial-single-1">
                                                        <div class="ts-person">
                                                            <div class="ts-img">
                                                                <img src="/landing/images/files/sliders/clients-testimonials/img-2.jpg" alt="">
                                                            </div><!-- .ts-img end -->
                                                            <h5>Vera Duncan</h5>
                                                            <span>PayPal Inc.</span>
                                                        </div><!-- .ts-person end -->
                                                        <div class="ts-content">
                                                            This should be used to tell a story and let your users know about your product or service.
                                                            <div class="rating">
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                                <i class="fa fa-star"></i>
                                                            </div><!-- .rating end -->
                                                        </div><!-- .ts-content end -->
                                                    </div><!-- .testimonial-single-1 -->
                                                </div><!-- .slide end -->
                                            </li>
                                        </ul>
                                    </div><!-- .slider-testimonials end -->

                                </div><!-- .col-md-12 end -->
                            </div><!-- .row end -->
                        </div><!-- .container end -->

                    </div><!-- .section-content end -->

                </div><!-- .flat-section end --> --}}

                <!-- === CTA Title 1 =========== -->
                <div id="cta-title-1" class="flat-section">

                    <div class="section-content">

                        <div class="">
                            <div class="row">
                                <div class="col-md-12 text-center">


                                    <h1>Want to be a <span class="colored">ProjectVala</span> writer?</h1>
                                    <p>Are you an academic writer and wish to list your profile on the ProjectVala platform. Click the button below and get started by creating your writer profile with simple steps.</p>
                                    <a class="btn  x-large colorful hover-colorful-darken mt-40" href="/writer/register">Be a Writer</a>

                                </div><!-- .col-md-12 end -->
                            </div><!-- .row end -->
                        </div><!-- .container end -->

                    </div><!-- .section-content end -->

                </div><!-- .flat-section end -->

            </div><!-- #content-wrap -->

        </section><!-- #content end -->

        <!-- Footer
        ============================================= -->
        <footer id="footer">

            <div id="footer-bar-1" class="footer-bar">

                <div class="footer-bar-wrap">

                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">

                                <div class="fb-row">
                                    <div class="copyrights-message">2018 © Designed by <a href="https://trumpets.co.in" target="_blank"><span class="colored">Trumpets</span></a>. All Rights Reserved by Acolyte Ventures LLP.</div>
                                    <ul class="social-icons animated x4 grey hover-colorful icon-only">
                                        <li><a class="si-facebook" href="https://facebook.com/theprojectvala" target="_blank"><i class="fa fa-facebook"></i><i class="fa fa-facebook"></i></a></li>
                                        <li><a class="si-instagramorange" href="https://www.instagram.com/theprojectvala/" target="_blank"><i class="fa fa-instagram"></i><i class="fa fa-instagram"></i></a></li>
                                    </ul><!-- .social-icons end -->
                                </div><!-- .fb-row end -->

                            </div><!-- .col-md-12 end -->
                        </div><!-- .row end -->
                    </div><!-- .container end -->

                </div><!-- .footer-bar-wrap -->

            </div><!-- #footer-bar-1 end -->

        </footer><!-- #footer end -->

    </div><!-- #full-container end -->

    <a class="scroll-top-icon scroll-top" href="#"><i class="fa fa-angle-up"></i></a>

    <!-- External JavaScripts
    ============================================= -->
    <script src="/landing/js/jquery.js"></script>
    <script src="/landing/js/jRespond.min.js"></script>
    <script src="/landing/js/jquery.easing.min.js"></script>
    <script src="/landing/js/jquery.fitvids.js"></script>
    <script src="/landing/js/jquery.stellar.js"></script>
    <script src="/landing/js/owl.carousel.min.js"></script>
    <script src="/landing/js/jquery.mb.YTPlayer.min.js"></script>
    <script src="/landing/js/jquery.magnific-popup.min.js"></script>
    <script src="/landing/js/jquery.validate.min.js"></script>
    <script src="/landing/js/jquery.ajaxchimp.min.js"></script>
    <script src="/landing/js/simple-scrollbar.min.js"></script>
    <script src='/landing/js/functions.js'></script>

</body>

</html>
