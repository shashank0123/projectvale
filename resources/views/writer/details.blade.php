@extends('layouts.app')

@section('content')

<section class="mt-5">
	<div class="">
		<div class="row">
			<div class="col-md-8">
				<div class="custom-project-deatls mt-4">
					<h4><b>Project Details</b></h4>
					<div class="card mt-3">
						<div class="card-body">
							<form>
								<div class="form-group row">
									<div class="col">
										<label for="exampleInputEmail1">Title</label>
										<input type="text" class="form-control" placeholder="Enter Title">
									</div>
									<div class="col">
										<label for="exampleInputPassword1">Writer</label>
										<select class="form-control">
											<option>writer1</option>
											<option>writer2</option>
											<option>writer3</option>
											<option>writer3</option>
										</select>
									</div>
								</div>
								<div class="form-group row">
									<div class="col">
										<label for="exampleInputEmail1">Duration</label>
										<input type="text" class="form-control" placeholder="Ex. 2 months">
									</div>
									<div class="col">
										<label for="exampleInputPassword1">What is your estimated budget?</label>
										<input type="text" class="form-control" placeholder="Enter budget">
									</div>
								</div>
								<div class="form-group row">
									<div class="col">
										<label for="exampleInputEmail1">Skills</label>
										<input type="text" class="form-control" placeholder="Enter Skills">
									</div>
									<div class="col">
										<label for="exampleInputPassword1">Demand</label>
										<input type="text" class="form-control" placeholder="Enter demand">
									</div>
								</div>
								<div class="form-group row">
									<div class="col">
										<label for="exampleInputEmail1">Start Date</label>
										<input type="text" class="form-control datedropper" required data-format="Y-m-d" data-modal="true" data-large-default="true" data-init-set="true" data-theme="projectvala">
									</div>
									<div class="col">
										<label for="exampleInputPassword1">End Date</label>
										<input type="text" class="form-control datedropper" required data-format="Y-m-d" data-modal="true" data-large-default="true" data-init-set="true"  data-theme="projectvala">
									</div>
								</div>
								<div class="form-group row">
									<div class="col">
										<label for="exampleInputEmail1">Description</label>
										<textarea class="form-control" rows="8" style="height: 100px!important;"></textarea>
									</div>
								</div>
								<button type="submit" class="btn btn-secondary">Post</button>
								<button type="submit" class="btn btn-default">Cancel</button>
							</form>
						</div>
					</div>
				</div>
				<div class="my-5">
					<h4 class="mt-5"><b>Chat</b></h4>
					<div class="writer-list-chat mt-3">
						<div class="card">
							<div class="card-body">
								<div class="messaging">
									<div class="inbox_msg">
										<div class="mesgs">
											<div class="msg_history">
												<div class="incoming_msg">
													<div class="incoming_msg_img">
														<img src="https://ptetutorials.com/images/user-profile.png" alt="sunil">
													</div>
													<div class="received_msg">
														<div class="received_withd_msg">
															<p>Test which is a new approach to have all
															solutions</p>
															<span class="time_date"> 11:01 AM    |    June 9</span></div>
														</div>
													</div>
													<div class="outgoing_msg">
														<div class="sent_msg">
															<p>Test which is a new approach to have all
															solutions</p>
															<span class="time_date"> 11:01 AM    |    June 9</span>
														</div>
													</div>
													<div class="incoming_msg">
														<div class="incoming_msg_img">
															<img src="https://ptetutorials.com/images/user-profile.png" alt="sunil">
														</div>
														<div class="received_msg">
															<div class="received_withd_msg">
																<p>Test, which is a new approach to have</p>
																<span class="time_date"> 11:01 AM    |    Yesterday</span>
															</div>
														</div>
													</div>
													<div class="outgoing_msg">
														<div class="sent_msg">
															<p>Apollo University, Delhi, India Test</p>
															<span class="time_date"> 11:01 AM    |    Today</span>
														</div>
													</div>
													<div class="incoming_msg">
														<div class="incoming_msg_img">
															<img src="https://ptetutorials.com/images/user-profile.png" alt="sunil">
														</div>
														<div class="received_msg">
															<div class="received_withd_msg">
																<p>We work directly with our designers and suppliers,
																	and sell direct to you, which means quality, exclusive
																products, at a price anyone can afford.</p>
																<span class="time_date"> 11:01 AM    |    Today</span>
															</div>
														</div>
													</div>
												</div>
												<div class="type_msg">
													<div class="input_msg_write">
														<input type="text" class="write_msg" placeholder="Type a message" />
														<button class="msg_send_btn" type="button"><i class="fa fa-paper-plane-o" aria-hidden="true"></i></button>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4">
					<div class="writer-details mt-4">
						<h4><b>Writer Details</b></h4>
						<div class="card mt-3">
							<div class="card-body">
								<form>
									<div class="form-group">
										<label for="exampleInputEmail1">Name</label>
										<input type="text" class="form-control" placeholder="Enter Name">
									</div>
									<div class="form-group">
										<label for="exampleInputEmail1">Email</label>
										<input type="text" class="form-control" placeholder="Enter Email">
									</div>
									<div class="form-group">
										<label for="exampleInputEmail1">Address</label>
										<input type="text" class="form-control" placeholder="Enter Address">
									</div>
									<div class="form-group">
										<label for="exampleInputEmail1">Skill</label>
											<input type="text" class="form-control" placeholder="Enter Title">
									</div>
									<div>
										<button class="btn btn-secondary" style="width: 100%;">Submit</button>
									</div>
								</form>
							</div>
						</div>
					</div>
					<div class="custom-file-upload mt-4">
						<div class="card">
							<div class="card-body text-justify">
								<div class="my-3">
									<div class="attachments-container">
										<a href="#" target="_blank">
											<div class="attachment-box ripple-effect">
												<span>Upload File</span> <i>PDF</i>
											</div>
										</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	@endsection