@extends('layouts.app')

@section('content')

<section class="mt-5">
	<div class="">
		<div class="row">
			<div class="col-md-12 mb-5">
		    <div class="input-group">
                <div class="input-group-btn search-panel">
                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" style="height: 46px;">
                    	<span id="search_concept">Filter by</span> <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu p-3" role="menu">
                      <li><a href="#contains">Contains</a></li>
                      <li><a href="#its_equal">It's equal</a></li>
                      <li><a href="#all">Anything</a></li>
                    </ul>
                </div>
                <input type="hidden" name="search_param" value="all" id="search_param">
                <input type="text" class="form-control" name="x" placeholder="Search term...">
                <span class="input-group-btn">
                    <button class="btn btn-default" type="button" style="height: 46px;"><span><i class="fa fa-search" aria-hidden="true"></i></span></button>
                </span>
            </div>
        </div>
		</div>
		<div class="row">
			@foreach($users as $user)
			<div class="col-md-4">
				<div class="card custom-writer-list mb-5">
					<div class="card-body text-center">
						<div class="writer-status">
							<span class="freelance-status">Available</span>
							<h4 class="flc-rate">$44</h4>
						</div>
						<div class="pt-5">
							<a class="post-classic-figure" href="/classroom/show">
								<img src="{{asset('/img/client-2.jpg')}}" alt="" width="94px" height="94px" class="rounded-circle">
							</a>
						</div>
						<div class="classroom-writer-section">
							<h2 class="post-classic-title pt-3 mb-0">
								<a href="/writer/{{$user->id}}" target="_blank">{{$user->username}}</a>
							</h2>
							<span>Writer</span>
							<p class="pl-0 pt-2">
								<span style="color: #fac917;margin-left: 10px;" class="ml-0"><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star-half-o" aria-hidden="true"></i><i class="fa fa-star-o" aria-hidden="true"></i></span>
							</p>
							<!-- <div class="writer-view-btn">
								<a href="/writer/{{$user->id}}" target="_blank" class="btn btn-primary">View</a>
								<a class="btn btn-secondary">Hire</a>
							</div> -->
						</div>
						<div class="freelance-box-extra">
							<ul>
								<li>English</li>
								<li>Nursing</li>
								<li>History</li>
								<li class="more-skill bg-primary">+3</li>
								<p class="pt-2">Lorem ipsum dolor sit amet, consectetur adipisicing.</p>
							</ul>
						</div>
					</div>
						<a href="/writer/{{$user->id}}" target="_blank" class="btn btn-freelance bt-1">View Detail</a>
					<!-- <div class="counter-writer-footer">
						<div class="row m-0 text-center">
							<div class="col p-0">
								<h5 class="mb-0 p-2" style="border-right: 1px solid #aeaaaa;">35</h5>
							</div>
							<div class="col p-0">
								<h5 class="mb-0 p-2" style="border-right: 1px solid #aeaaaa;">42</h5>
							</div>
							<div class="col p-0">
								<h5 class="mb-0 p-2">13</h5>
							</div>
						</div>
					</div> -->

				</div>
			</div>
			@endforeach
		</div>
	</div>
</section>

@endsection
@section('scripts')
<script>
	$(document).ready(function(e){
    $('.search-panel .dropdown-menu').find('a').click(function(e) {
		e.preventDefault();
		var param = $(this).attr("href").replace("#","");
		var concept = $(this).text();
		$('.search-panel span#search_concept').text(concept);
		$('.input-group #search_param').val(param);
	});
});
</script>
@endsection