@extends('layouts.app')

@section('content')
<div class="" style=" /*   background: linear-gradient(to bottom, #4a9bc8 0%, #9bc533 100%);*/height: 91vh;">
<div class="container login-card-section-custom">
    <div class="row justify-content-center pt-5">
        
        <div class="col-md-5 pl-0 mt-5">
            <div class="card">
                <!-- <div class="card-header">{{ __('Login') }}</div> -->

                <div class="card-body">
                    <form method="POST" action="{{ route('login') }}">
                        @csrf

                        <div class="form-group">
                            <label for="email" class="col-form-label text-md-right">{{ __('E-Mail Address') }}</label>
                            <div class="input-group">
                            <div class="input-group-prepend">
                                <div class="input-group-text is-invalid"><i class="fa fa-envelope-o" aria-hidden="true" style="padding: 8px;"></i></div>
                            </div>
                            <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>
                            @if ($errors->has('email'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>

                        <div class="form-group">
                            <label for="password" class="col-form-label text-md-right">{{ __('Password') }}</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <div class="input-group-text"><i class="fa fa-lock" aria-hidden="true" style="padding: 8px;"></i></div>
                                </div>
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>
                                @if ($errors->has('password'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group ">
                            <div class="">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                                    <label class="form-check-label" for="remember">
                                        {{ __('Remember Me') }}
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group  mb-0">
                            <div class="">
                                <button type="submit" class="btn btn-block mb-3 btn-primary">
                                    {{ __('Login') }}
                                </button>

                                <a class="btn btn-link" href="/writer/register">
                                    {{ __('Register') }}
                                </a>
                                <a class="btn btn-link float-right" href="#">
                                    {{ __('Forgot Your Password?') }}
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-4 pr-md-0 text-center mt-5">
                <div class="account-info">
                    <div class="chess-logo">
                        <a href="{{ url('/') }}"><img src="{{ asset('img/user/account.svg') }}" alt="logo" style="width: 200px;"></a>
                    </div>
                    <div class="pt-4">
                        <p><i class="fa fa-check-circle-o" aria-hidden="true"></i> Lorem ipsum dolor sit amet, consectetur adipisicing elit sed do eiusmod.</p>
                    </div>        
                </div>
            </div>
    </div>
</div>
</div>
@endsection
