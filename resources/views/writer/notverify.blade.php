@extends('layouts.app')

@section('content')

<section class="mt-5">
    <div class="container">
        <div class="row justify-content-center pt-4">
            <div class="col-md-5 pl-0">
                <div class="card shadow-boder-none">
                    <!-- <div class="card-header">Warning!</div> -->
                    <div class="card-body text-center p-5">
                        <h3>Notice!</h3>
                        @if(auth()->user()->be_writer && auth()->user()->is_test && auth()->user()->writerDocument)
                        <p style="font-size: 18px">{{auth()->user()->fullname}} your writer account verification is pending by admin. Please wait for the admin to verify your account.</p>
                        @else
                        <p style="font-size: 18px">Sorry! {{auth()->user()->fullname}} your fail in test so writer account is not verified.</p>
                        @endif
                        <a href="{{ route('logout') }}" class="btn btn-primary px-4" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">OK</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection
@section('scripts')

@endsection
