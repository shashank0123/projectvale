@extends('layouts.app')

@section('content')

<section>
    <div class="my-md-5 my-sm-0">
        <div class="headline d-flex justify-content-between align-items-center user-profile-heading">
            <h3 class="d-inline-block">Writer Profile</h3>
			<a href="/projects/{{$project->id}}" class="btn btn-default bg-secondary text-white">Back</a>
        </div>
         @section('heading', 'Writer Profile')
		<div class="row my-3 user-list-writer">
			<div class="col-md-4">
				<div class="card mt-3 border-top-green">
					<div class="card-body">
						<div class="writer-status">
							<span class="freelance-status {{ $writer->isWriterAvailable($project) ? 'unavailable' : 'available'}}">{{ $writer->isWriterAvailable($project) ? 'Unavailable' : 'Available'}}</span>
							<h4 class="flc-rate"><i class="fa fa-inr"></i> {{$writer->project_rate($project)}} / word</h4>
						</div>
						<div class="profile-details-user">
							<div class="text-center">
								<div class="mt-5">
									@if(isset($writer->profile_photo) && $writer->profile_photo)
			                        <img src="{{Storage::url($writer->profile_photo)}}" style="width:100px; height: 100px;" class="mb-3 img-thumbnail rounded-circle">
			                        @else
			                        <img src="{{asset('img/user.png')}}" class="mb-3 img-thumbnail rounded-circle">
			                        @endif
								</div>
								<div class="user-active-section">
									<h4 class="mt-0 mb-2 sidebar-box-detail">
										{{$writer->username}}
									</h4>
									<!-- <p class="mb-1">{{str_limit($writer->email, 23)}}</p> -->
									<span class="badge badge-success d-inline text-white">{{$writer->quality}}</span>
									<div class="writerProfileRating mt-1"></div>
								</div>
							</div>
							<!-- <div class="user-profile-rating mt-3">
								<div class="d-inline-block">
									<span style="color: #fac917;"><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star-half-o" aria-hidden="true"></i><i class="fa fa-star-o" aria-hidden="true"></i></span>
									<p>51 Reviews </p>
								</div>
								<div class="d-inline-block float-right pt-2">
									<h2>98%</h2>
								</div>
							</div> -->
							<div class="freelance-box-extra text-center">
								<ul>
									<?php $subjects = explode(',', $writer->subjects); ?>
									@foreach($subjects as $subject)
									<li>{{ $subject}}</li>
									@endforeach
								</ul>
                                <!-- <ul class="status-detail">
										<li class="br-1" style="border-right: 1px solid #ccc;"><strong>$44/hr</strong>Hourly Rate</li>
										<li class="br-1" style="border-right: 1px solid #ccc;"><strong>52 Jobs</strong>Done job</li>
										<li><strong>44</strong>Rehired</li>
									</ul> -->
								</div>
							<!-- <div class="user-profile-progress">
								<span>Finished <span class="float-right">77</span></span><br>
								<span>In progress <span class="float-right">2</span></span><br>
								<span>Not finished <span class="float-right">2</span></span>
							</div>
						-->							<div class="">
							<!-- <a href="/projects/{{$project->id}}/writer/{{$writer->id}}" class="btn btn-secondary btn-block btn-lg">Hire Writer</a> -->
						</div>
					</div>
				</div>
				<a href="/projects/{{$project->id}}/writer/{{$writer->id}}" class="btn btn-secondary btn-block btn-lg">Hire Writer</a>
			</div>
		</div>
		<div class="col-md-8">
			<div class="mt-3 responsive-writer-profile">
				@if(isset($history) && count($history))
				<h3>History</h3>
				<div class="list-group">
					@foreach($history as $key => $item)
					<div class="list-group-item list-group-item-action flex-column align-items-start">
						<div class="d-flex w-100 justify-content-between">
							<h5 class="mb-1">{{$item->topic}}</h5>
							<div class="brows-job-type">
								<span class="full-time {{ $item->project_status }} text-capitalize">{{ $item->project_status }}</span>
							</div>
						</div>
						<small>{{date('F d, Y', strtotime($item->created_at))}} - {{date('F d, Y', strtotime($item->deadline_date))}}</small>
					</div>
					@endforeach
				</div>
				@else
				<div class="px-5 py-4">
	                <div class="text-center">
	                     <h4>You have no work history yet!</h4>
	                     <p>You have no work history on ProjectVala with this writer.</p>
	                     <div class="my-sm-3">
	                        <img src="/img/history.png" alt="history">
	                     </div>
	                </div>
	            </div>
	            <hr>
                @endif
			</div>

			@if(isset($feedback) && count($feedback))
			<div class="mt-4">
				<h3>Feedback</h3>
				@foreach($feedback as $key => $item)
				<div class="card border-top-green">
					<div class="card-body">
						<div class="row">
							<div class="col-md-8">
								<h6 style="font-size: 16px;" class="pb-2">{{$item->project->topic}}</h6>
								@if($item->fullyComplete)
								<span class="badge badge-success py-2 px-3"><i class="fa fa-check"></i> Fully Complete</span>
								@else
								<span class="badge badge-danger py-2 px-3"><i class="fa fa-times"></i> Fully Complete</span>
								@endif
								@if($item->onTime)
								<span class="badge badge-success py-2 px-3"><i class="fa fa-check"></i> On Time</span>
								@else
								<span class="badge badge-danger py-2 px-3"><i class="fa fa-times"></i> On Time</span>
								@endif
							</div>
							<div class="col-md-4">
								<div class="float-right">
									<div class="writerFeedbackRating{{$key}}"></div>
								</div>
							</div>
						</div>
						<hr class="my-3">
						<div class="row">
							<div class="col-md-8">
								<h6>{{$item->project->user->username}}</h6>
								<p>{{$item->review}}</p>
							</div>
							<div class="col-md-4">
								<div class="float-right">
									<p style="color: #a49d9d;">{{date('F d, Y', strtotime($item->created_at))}}</p>
								</div>
							</div>
						</div>
					</div>
				</div>
				@endforeach
			</div>
			@else
			<div class="px-5 py-4">
                <div class="text-center">
                     <h4>This writer has no feedback.</h4>
                     <p>This writer has no feedback on ProjectVala.</p>
                     <div class="my-sm-3">
                        <img src="/img/feedback.png" alt="feedback">
                     </div>
                </div>
            </div>
			@endif
				<!-- <div class="card mt-3 container-detail-box">
					<div class="card-body">
						<div class="row">
								<div class="col-md-12">
									<h4>Review</h4>
								</div>
							</div>
							<div class="row mt-3">

								<div class="review-list">
									<div class="review-thumb">
										<img src="/img/user/client-1.jpg" style="width: 100px;" class="img-responsive rounded-circle" alt="">
									</div>
									<div class="review-detail pl-4">
										<h4>Daniel Luke<span>3 days ago</span></h4>
										<span class="re-designation">Web Developer</span>
										<p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga.</p>
									</div>
								</div>

								<div class="review-list">
									<div class="review-thumb">
										<img src="/img/user/client-1.jpg" style="width: 100px;" class="img-responsive rounded-circle" alt="">
									</div>
									<div class="review-detail pl-4">
										<h4>Daniel Luke<span>3 days ago</span></h4>
										<span class="re-designation">Web Developer</span>
										<p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga.</p>
									</div>
								</div>

								<div class="review-list">
									<div class="review-thumb">
										<img src="/img/user/client-1.jpg" style="width: 100px;" class="img-responsive rounded-circle" alt="">
									</div>
									<div class="review-detail pl-4">
										<h4>Daniel Luke<span>3 days ago</span></h4>
										<span class="re-designation">Web Developer</span>
										<p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga.</p>
									</div>
								</div>

								<div class="review-list">
									<div class="review-thumb">
										<img src="/img/user/client-1.jpg" style="width: 100px;" class="img-responsive rounded-circle" alt="">
									</div>
									<div class="review-detail pl-4">
										<h4>Daniel Luke<span>3 days ago</span></h4>
										<span class="re-designation">Web Developer</span>
										<p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga.</p>
									</div>
								</div>
							</div>
					</div>
				</div>
			</div> -->
		</div>
	</div>
</section>


@endsection
@section('scripts')
<script>
	$('div.writerProfileRating').raty({ starType: 'i',readOnly:true, score:'{{isset($writer) && $writer->rating ? $writer->rating :'3.0' }}' });

	@if(count($feedback))
	@foreach($feedback as $key => $item)
	$('div.writerFeedbackRating{{$key}}').raty({ starType: 'i',readOnly:true, score:'{{isset($item) && $item->rating ? $item->rating :'3.0' }}' });
	@endforeach
	@endif

</script>
@endsection