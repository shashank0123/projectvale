@extends('layouts.app')

@section('content')

<section class="profile-section profile-wallet-section my-5">
	<div class="writer-feedback-section">
		<div class="row">
			<div class="col-md-4 user-feedback-view">
				@include('profile.sidebar')
			</div>
			<div class="col-md-8 writer-feedback-display">
				@if(count($feedback))
				@foreach($feedback as $key => $item)
				<div class="card mb-3">
					<div class="card-body">
						<div class="row">
							<div class="col-md-8">
								<h6 style="font-size: 16px;" class="pb-2">{{$item->project->topic}}</h6>
								@if($item->fullyComplete)
		                        <span class="badge badge-success py-2 px-3"><i class="fa fa-check"></i> Fully Complete</span>
		                        @else
		                        <span class="badge badge-danger py-2 px-3"><i class="fa fa-times"></i> Fully Complete</span>
		                        @endif
		                        @if($item->onTime)
		                        <span class="badge badge-success py-2 px-3"><i class="fa fa-check"></i> On Time</span>
		                        @else
		                        <span class="badge badge-danger py-2 px-3"><i class="fa fa-times"></i> On Time</span>
		                        @endif
							</div>
							<div class="col-md-4">
								<div class="float-md-right star-rating-custom">
									<!-- <p class="mb-0">Finished </p> -->
									<div class="projectRating{{$key}}"></div>
								</div>
							</div>
						</div>
						<hr class="my-3">
						<div class="row">
							<div class="col-md-8">
								<h6>{{$item->project->user->username}}</h6>
								<p>{{$item->review}}</p>
							</div>
							<div class="col-md-4">
								<div class="float-right">
									<p style="color: #a49d9d;">{{date('F d, Y', strtotime($item->created_at))}}</p>
								</div>
							</div>
						</div>
						<hr class="feedback-bottom-border mt-1 mb-0">
					</div>
				</div>
				@endforeach
				@else
                <div class="empty-box">
                    <div class="text-center">
                         <h4>No feedback yet!</h4>
                         <p>You have no feedback on ProjectVala, when any user give feedback on project complete then show feedback to you.</p>
                         <div class="my-sm-3">
                            <img src="/img/feedback.png" alt="feedback">
                         </div>
                    </div>
                </div>
				@endif
			</div>
		</div>
	</div>
</section>

@auth
<nav class="bottom-nav">
            <a class="bottom-nav__action {{ request()->is('home') ? 'bottom-nav__action--active' : '' }}" href="/profile">
                <svg class="bottom-nav__icon" viewBox="0 0 24 24">
                     <path d="M11,7V12.11L15.71,14.9L16.5,13.62L12.5,11.25V7M12.5,2C8.97,2 5.91,3.92 4.27,6.77L2,4.5V11H8.5L5.75,8.25C6.96,5.73 9.5,4 12.5,4A7.5,7.5 0 0,1 20,11.5A7.5,7.5 0 0,1 12.5,19C9.23,19 6.47,16.91 5.44,14H3.34C4.44,18.03 8.11,21 12.5,21C17.74,21 22,16.75 22,11.5A9.5,9.5 0 0,0 12.5,2Z"></path>
                </svg>
                <span class="bottom-nav__label">Profile</span>
             </a>

            <a class="bottom-nav__action {{ request()->is('files') ? 'bottom-nav__action--active' : '' }}" href="/profile/security">
                <svg class="bottom-nav__icon" viewBox="0 0 24 24">
                    <path d="M12,21.35L10.55,20.03C5.4,15.36 2,12.27 2,8.5C2,5.41 4.42,3 7.5,3C9.24,3 10.91,3.81 12,5.08C13.09,3.81 14.76,3 16.5,3C19.58,3 22,5.41 22,8.5C22,12.27 18.6,15.36 13.45,20.03L12,21.35Z"></path>
                </svg>
                <span class="bottom-nav__label">Security</span>
            </a>

            <a class="bottom-nav__action {{ request()->is('profile') ? 'bottom-nav__action--active' : '' }}" href="/profile/feedback">
                <svg class="bottom-nav__icon" viewBox="0 0 24 24">
                   <path d="M12,20A8,8 0 0,1 4,12A8,8 0 0,1 12,4A8,8 0 0,1 20,12A8,8 0 0,1 12,20M12,2A10,10 0 0,0 2,12A10,10 0 0,0 12,22A10,10 0 0,0 22,12A10,10 0 0,0 12,2M12,12.5A1.5,1.5 0 0,1 10.5,11A1.5,1.5 0 0,1 12,9.5A1.5,1.5 0 0,1 13.5,11A1.5,1.5 0 0,1 12,12.5M12,7.2C9.9,7.2 8.2,8.9 8.2,11C8.2,14 12,17.5 12,17.5C12,17.5 15.8,14 15.8,11C15.8,8.9 14.1,7.2 12,7.2Z"></path>
                </svg>
                <span class="bottom-nav__label">Feedback</span>
            </a>
        </nav>

        @endauth

@endsection

@section('scripts')
<script>
	$('div.writerProfileRating').raty({ starType: 'i',readOnly:true, score:'{{isset($writer) && $writer->rating ? $writer->rating :'3.0' }}' });

	@if(count($feedback))
	@foreach($feedback as $key => $item)
	$('div.projectRating{{$key}}').raty({ starType: 'i',readOnly:true, score:'{{isset($item) && $item->rating ? $item->rating :'' }}' });
	@endforeach
	@endif
	var resize = $('#upload-demo').croppie({
		enableExif: true,
		enableOrientation: true,
            viewport: { // Default { width: 100, height: 100, type: 'square' }
            width: 200,
            height: 200,
            type: 'circle' //square
        },
        boundary: {
        	width: 300,
        	height: 300
        }
    });

	$('#upload').on('change', function () {
		var reader = new FileReader();
		reader.onload = function (e) {
			resize.croppie('bind',{
				url: e.target.result
			}).then(function(){
				console.log('jQuery bind complete');
			});
		}
		reader.readAsDataURL(this.files[0]);
	});

	$('.upload-result').on('click', function (ev) {
		resize.croppie('result', {
			type: 'canvas',
			size: 'viewport'
		}).then(function (img) {
			axios.post('/profile/avatar', {
				profile_photo:img
			})
			.then(function (response) {
				location.reload(true);
			})
			.catch(function (error) {
				console.log(error);
			});
		});
	});
</script>
@endsection