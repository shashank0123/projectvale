@extends('layouts.app')

@section('content')

<section class="profile-section profile-wallet-section my-5">
	<div class="">
		<div class="row">
			<div class="col-md-4">
				<div class="sidebar-profile">
					<div class="card border-top-green">
						<div class="card-body">
							<div class="text-center mt-3">
								<a class="" href="/classroom/show">
									<img src="/img/user.png" style="height:100px !important;" class="mb-3 img-thumbnail rounded-circle">
								</a>
							</div>
						<div class="ml-4 text-center mt-1">
							<p class="mb-0">{{auth()->user()->fullname}}</p>
							<p>
								<span style="color: #fac917;"><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star-half-o" aria-hidden="true"></i><i class="fa fa-star-o" aria-hidden="true"></i></span>
							</p>
						</div>
						<div class="d-flex justify-content-between mt-2">
							<div class="status-profile">
								<h5>0</h5>
								<p>Completed</p>
							</div>
							<div class="status-profile">
								<h5>0</h5>
								<p>Cancelled</p>
							</div>
							<div class="status-profile">
								<h5>0</h5>
								<p>In progress</p>
							</div>
						</div>
						</div>
						<hr class="my-0">
						<div class="p-3 user-profile-tab-section">
							<a href="/writer/profile"><p><i class="fa fa-user-circle-o" aria-hidden="true"></i> Profile</p></a>
							<hr>
							<a href="/writer/security"><p><i class="fa fa-key" aria-hidden="true"></i> Security</p></a>
							<hr>
							<a href="/writer/feedback"><p><i class="fa fa-commenting" aria-hidden="true"></i> Feedback</p></a>
							<hr>
							<a href="/writer/wallet"><p><i class="fa fa-google-wallet" aria-hidden="true"></i> Wallet</p></a>
							<hr>
							<a href="#"><p><i class="fa fa-sign-out" aria-hidden="true"></i> Logout</p></a>
						</div>
					</div>

				</div>
			</div>
			<div class="col-md-8">
				<div class="card border-top-green">
					<div class="card-body">
						<div class="row mt-4">
							<div class="col-md-12">
								<div class="user-details">
									<form method="" action="">
										@csrf
									  	<div class="form-group row">
									  		<div class="col">
									    		<label>Name</label>
									    		<input type="text" class="form-control" name="name" value="" placeholder="Enter first name">
									  		</div>
										</div>
										<div class="form-group row">
											<div class="col">
									    		<label for="">Email</label>
									    		<input type="email" class="form-control" name="email" value="" placeholder="Enter Email">
									  		</div>
									  		<div class="col">
									    		<label for="">Phone No</label>
									    		<input type="text" class="form-control" name="phone" value="" placeholder="Enter phone number">
									  		</div>
										</div>
										<div class="form-group row">
											<div class="col">
											<label>Subject</label>
												<select class="form-control" value="" name="subject" required="" data-parsley-group="block-0">
													<option>English</option>
													<option>Business and Entrepreneurship</option>
													<option>History</option>
													<option>Economics</option>
													<option>Finance</option>
													<option>Law</option>
													<option>Management</option>
													<option>Marketing</option>
													<option>Political Science</option>
													<option>Others</option>
												</select>
											</div>
										</div>
									  	<button type="submit" class="btn btn-primary">Save Changes</button>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

@endsection