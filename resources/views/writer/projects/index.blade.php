    @extends('layouts.app')

    @section('content')
    <div class=" my-5">
        <div class="row">
        	<div class="col-md-12">
        		<div class="page-heading">
        			<h2 class="d-inline">Project</h2>
			        <nav aria-label="breadcrumb" class="float-right">
			            <ol class="breadcrumb bg-dark ">
			                <li class="breadcrumb-item"><a href="/home" class="text-white">Gateway</a></li>
			                <li class="breadcrumb-item"><a href="/home" class="text-white">Ongoing</a></li>
			            </ol>
			        </nav>
    			</div>
        	</div>
        </div>
        <div class="row">
            <div class="col-sm-4">
                <div class="card my-3" style="height: 340px;">
                    <div class="card-body text-center">
                        <div class="brows-job-type">
                            <span class="full-time">Service</span>
                        </div>
                        <span class="tg-themetag tg-featuretag">Quality</span>
                        <!-- <div class="brows-job-company-img mt-3">
                            <img src="/img/com-2.jpg" class="img-responsive" alt="">
                        </div> -->
                        <div class="project-list-detalis mt-5">
                            <h4>Foodoor</h4>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                            quis nostrud exercitation ullamco laboris nisi.</p>
                            <a href="#" class="btn btn-primary">Manage</a>
                        </div>
                    </div>
                    <ul class="grid-view-caption">
                        <li>
                            <div class="brows-job-location">
                                <p class="p-0" style="font-size: 12px;"><span><i class="fa fa-calendar-check-o" aria-hidden="true"></i>25/4/2015</span></p>
                            </div>
                        </li>
                        <li>
                            <p style="font-size: 12px;"><span class="brows-job-sallery"><i class="fa fa-clock-o" aria-hidden="true"></i>05.45PM</span></p>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="card my-3" style="height: 340px;">
                    <div class="card-body text-center">
                        <div class="brows-job-type">
                            <span class="full-time">Service</span>
                        </div>
                        <span class="tg-themetag tg-featuretag">Quality</span>
                        <!-- <div class="brows-job-company-img mt-3">
                            <img src="/img/com-2.jpg" class="img-responsive" alt="">
                        </div> -->
                        <div class="project-list-detalis mt-5">
                            <h4>Foodoor</h4>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                            quis nostrud exercitation ullamco laboris nisi.</p>
                            <a href="#" class="btn btn-primary">Manage</a>
                        </div>
                    </div>
                    <ul class="grid-view-caption">
                        <li>
                            <div class="brows-job-location">
                                <p class="p-0" style="font-size: 12px;"><span><i class="fa fa-calendar-check-o" aria-hidden="true"></i>25/4/2015</span></p>
                            </div>
                        </li>
                        <li>
                            <p style="font-size: 12px;"><span class="brows-job-sallery"><i class="fa fa-clock-o" aria-hidden="true"></i>05.45PM</span></p>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="card my-3" style="height: 340px;">
                    <div class="card-body text-center">
                        <div class="brows-job-type">
                            <span class="full-time">Service</span>
                        </div>
                        <span class="tg-themetag tg-featuretag">Quality</span>
                        <!-- <div class="brows-job-company-img mt-3">
                            <img src="/img/com-2.jpg" class="img-responsive" alt="">
                        </div> -->
                        <div class="project-list-detalis mt-5">
                            <h4>Foodoor</h4>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                            quis nostrud exercitation ullamco laboris nisi.</p>
                            <a href="#" class="btn btn-primary">Manage</a>
                        </div>
                    </div>
                    <ul class="grid-view-caption">
                        <li>
                            <div class="brows-job-location">
                                <p class="p-0" style="font-size: 12px;"><span><i class="fa fa-calendar-check-o" aria-hidden="true"></i>25/4/2015</span></p>
                            </div>
                        </li>
                        <li>
                            <p style="font-size: 12px;"><span class="brows-job-sallery"><i class="fa fa-clock-o" aria-hidden="true"></i>05.45PM</span></p>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    @endsection