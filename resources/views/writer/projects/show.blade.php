@extends('layouts.app')

@section('content')

<div class=" py-4">
    <div class="page-heading custom-project-show-details">
        <h2 class="d-inline">Project Details</h2>
        <nav aria-label="breadcrumb" class="float-right">
            <ol class="breadcrumb bg-dark ">
                <li class="breadcrumb-item"><a href="/home" class="text-white">Home</a></li>
                <li class="breadcrumb-item"><a href="/home" class="text-white">Projects</a></li>
                <li class="breadcrumb-item text-white active" aria-current="page">Details</li>
            </ol>
        </nav>
    </div>
    <div class="clearfix"></div>
    <div class="row pb-4">
        <div class="col-sm-12">
            <div class="my-3">
                <div class="row">
                    <div class="col-sm-8">
                        <div class="card rounded-0 ">
                            <div class="card-body">
                                <div class="apply-job-detail d-inline-block">
                                    <p class="mb-0 pl-1">Lorem ipsum dolor sit amet</p>
                                </div>
                                 <div class="d-inline-block float-right headline">
                                    <div class="float-xl-right">
                                        <a href="#" class="btn btn-secondary btn-sm ml-1"><i class="fa fa-edit"></i> Edit</a>
                                        <a href="#"  onclick="return confirm('Are you sure, You want to delete this project?');" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i> Delete</a>
                                    </div>
                                </div>
                                <div class="apply-job-detail pt-3">
                                    <ul class="skills" style="margin-top: -15px;margin-bottom: 0px;">
                                        <li class="m-0">Foodoor</li>
                                        <li class="m-0">Lorem ipsum</li>
                                    </ul>
                                </div>
                                <div class="apply-job-header">
                                    <a href="#" class="cl-success"><span><i class="fa fa-calendar-check-o" aria-hidden="true"></i>25/42017</span></a>
                                    <span class="ml-2"><i class="fa fa-clock-o" aria-hidden="true"></i>06.45PM</span>
                                </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                    <div class="apply-job-detail">
                                        <h5>Specifications</h5>
                                        <ul class="job-requirements">
                                            <li><span>Type of service :</span> Service</li>
                                            <li><span>Writting Quality :</span> Quality</li>
                                            <li><span>No. of Citation :</span> Citation</li>
                                            <li><span>Citation Format :</span> Lorem ipsum</li>
                                        </ul>
                                    </div>
                                </div>
                                </div>
                            </div>
                        </div>
                        <div class="my-5">
                            <h4 class="mt-5"><b>Chat</b></h4>
                            <div class="writer-list-chat mt-3">
                                <div class="card">
                                    <div class="card-body">

                                        <div class="messaging">
                                            <div class="inbox_msg">
                                                <div class="mesgs">
                                                    <div class="msg_history">
                                                        <div class="incoming_msg">
                                                            <div class="incoming_msg_img">
                                                                <img src="https://ptetutorials.com/images/user-profile.png" alt="sunil">
                                                            </div>
                                                            <div class="received_msg">
                                                                <div class="received_withd_msg">
                                                                    <p>Test which is a new approach to have all
                                                                    solutions</p>
                                                                    <span class="time_date"> 11:01 AM    |    June 9</span></div>
                                                                </div>
                                                            </div>
                                                            <div class="outgoing_msg">
                                                                <div class="sent_msg">
                                                                    <p>Test which is a new approach to have all
                                                                    solutions</p>
                                                                    <span class="time_date"> 11:01 AM    |    June 9</span>
                                                                </div>
                                                            </div>
                                                            <div class="incoming_msg">
                                                                <div class="incoming_msg_img">
                                                                    <img src="https://ptetutorials.com/images/user-profile.png" alt="sunil">
                                                                </div>
                                                                <div class="received_msg">
                                                                    <div class="received_withd_msg">
                                                                        <p>Test, which is a new approach to have</p>
                                                                        <span class="time_date"> 11:01 AM    |    Yesterday</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="outgoing_msg">
                                                                <div class="sent_msg">
                                                                    <p>Apollo University, Delhi, India Test</p>
                                                                    <span class="time_date"> 11:01 AM    |    Today</span>
                                                                </div>
                                                            </div>
                                                            <div class="incoming_msg">
                                                                <div class="incoming_msg_img">
                                                                    <img src="https://ptetutorials.com/images/user-profile.png" alt="sunil">
                                                                </div>
                                                                <div class="received_msg">
                                                                    <div class="received_withd_msg">
                                                                        <p>We work directly with our designers and suppliers,
                                                                            and sell direct to you, which means quality, exclusive
                                                                        products, at a price anyone can afford.</p>
                                                                        <span class="time_date"> 11:01 AM    |    Today</span>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="type_msg">
                                                            <div class="input_msg_write">
                                                                <input type="text" class="write_msg" placeholder="Type a message" />
                                                                <button class="msg_send_btn" type="button"><i class="fa fa-paper-plane-o" aria-hidden="true"></i></button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    
                        <div class="col-sm-4">
                            <div class="card rounded-0 mb-3">
                                <div class="card-body">
                                    <div class="writer-status">
                                        <span class="freelance-status">Available</span>
                                        <h4 class="flc-rate">$44</h4>
                                    </div>
                                    <div class="text-center mt-5">
                                        <img src="{{asset('img/user.png')}}" style="height:100px !important;" class="mb-3 img-thumbnail rounded-circle">
                                        <h4 class="mt-0 mb-2 sidebar-box-detail">
                                            Lorem ipsum
                                        </h4>
                                        <span class="mt-0 mb-2 desination">
                                            Lorem
                                        </span> 
                                    </div>
                                    <div class="freelance-box-extra">
                                        <ul>
                                            <li>English</li>
                                            <li>Nursing</li>
                                            <li>History</li>
                                        </ul>
                                    </div>
                                <a href="#" class="btn btn-danger btn-freelance bt-1 border-0" style="color: #fff;">Revoke Writer</a>
                                <a href="#" class="btn btn-secondary btn-freelance bt-1 border-0 mt-3" data-toggle="modal" data-target="#exampleModal" data-whatever="@mdo" style="color: #fff;">Complete Project</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
    <div class="modal fade" id="modelAddTask" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form method="" action="">
                    @csrf
                    <div class="modal-header">
                        <h5 class="modal-title">Add Milestone</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label>Title <span style="color: red;">*</span></label>
                            <input type="text" name="title" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label>Date <span style="color: red;">*</span></label>
                            <input type="text" name="date" class="form-control datepicker" autocomplete="off" required>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Add</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

<!-- Upload additional files
    ================================================== -->

    <div class="modal fade addAdditionalFiles" id="addAdditionalFiles" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <form method="" action=""  enctype="">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Add Project Files</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    @csrf
                    <div class="modal-body">
                        <div class="row form-group">
                            <div class="col-xl-12 col-md-12">
                                <div class="margin-top-0 margin-bottom-0">
                                    <h5>File Name <span style="color: red;">*</span></h5>
                                </div>
                                <input type="text" class="form-control" name="name" required placeholder="Enter file name">
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-xl-12 col-md-12">
                                <div class="margin-top-0 margin-bottom-0">
                                    <h5>File <span style="color: red;">*</span></h5>
                                </div>
                                <input type="file" class="form-control" name="file" required>
                            </div>
                        </div>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input type="checkbox" name="islock" class="form-check-input" value="1"> &nbsp;&nbsp;&nbsp;File lock
                            </label>
                        </div>
                        <p class="alert alert-info"><small> <i class="fa fa-info-circle"></i> If file is locked then user should be able to download this file only after making payment.</small></p>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Save</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

<!-- warning for review -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header" style="background-color: #fac917;">
        <h5 class="modal-title" id="exampleModalLabel">Warning</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>The User Must pay remaining payment to the Writer and without that,they will not be able to complete the project or download the files form the files section.</p>
      </div>
      <div class="modal-footer">
        <a type="button" class="btn btn-secondary btn-lg btn-block" data-toggle="modal" data-target="#exampleModal1" data-whatever="@mdo" data-dismiss="modal" style="border-color: unset;">Ok</a>
        <!-- <button type="button" class="btn btn-primary btn-lg">Cancel</button> -->
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="exampleModal1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Review</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <h3>Leave a Review</h3>
        <form>
            <div class="form-group">
                <label for="account-number" class="col-form-label">Account Number</label>
                <br>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1">
                    <label class="form-check-label" for="inlineRadio1">Yes</label>
                </div>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2" value="option2">
                    <label class="form-check-label" for="inlineRadio2">No</label>
                </div>
            </div>
          <div class="form-group">
                <label for="account-number" class="col-form-label">Account Number</label>
                <br>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1">
                    <label class="form-check-label" for="inlineRadio1">Yes</label>
                </div>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2" value="option2">
                    <label class="form-check-label" for="inlineRadio2">No</label>
                </div>
            </div>
          <div class="form-group">
            <label for="bank-name" class="col-form-label">Your Rating</label>
            <!-- <input type="text" class="form-control" placeholder="Enter Bank Name"> -->
            <p>
                <span style="color: #fac917;font-size: 20px;">
                    <i class="fa fa-star" aria-hidden="true"></i>
                    <i class="fa fa-star" aria-hidden="true" style="color: #fac917;"></i>
                    <i class="fa fa-star" aria-hidden="true"></i>
                    <i class="fa fa-star-half-o" aria-hidden="true"></i>
                    <i class="fa fa-star-o" aria-hidden="true"></i>
                </span>
            </p>
          </div>
          <div class="form-group">
            <label for="exampleFormControlTextarea1">Description</label>
            <textarea class="form-control" id="exampleFormControlTextarea1" rows="5"cols="4"></textarea>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary btn-lg btn-block" data-dismiss="modal">Leave Review</button>
      </div>
    </div>
  </div>
</div>


    

@endsection