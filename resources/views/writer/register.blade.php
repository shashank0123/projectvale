@extends('layouts.app')
@section('styles')
<style type="text/css">
#wizard .steps a{
    pointer-events: none;
}
.select2-container{
    display: block;
}
</style>
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center pt-md-4">
        <div class="col-md-6 pl-md-0 mt-md-2">
            <div class="card shadow-boder-none">
                <div class="card-body writer-registration">
                    <header><a href="/"><p class="projectvala-logo projectvala-logo-long"></p></a></header>
                    <form id="wizard" method="POST" class="frmRegistration" action="{{ route('register') }}">
                        @csrf
                        <h2></h2>
                        <section class="form-section">
                            <h4 class="mb-3">Create a writer account on ProjectVala</h4>
                            <input type="hidden" name="be_writer" value="1">
                            <div class="row">
                                <div class="col-md-6 form-group">
                                    <input id="fname" placeholder="First Name" type="text" class="form-control{{ $errors->has('fname') ? ' is-invalid' : '' }}" name="fname" value="{{ old('fname') }}" required autofocus>
                                    @if ($errors->has('fname'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('fname') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="col-md-6 form-group">
                                    <input id="lname" placeholder="Last Name" type="text" class="form-control{{ $errors->has('lname') ? ' is-invalid' : '' }}" name="lname" value="{{ old('lname') }}" required autofocus>
                                    @if ($errors->has('lname'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('lname') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group ">
                                <input id="email" placeholder="E-mail Address" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ request('email') ? request('email') : old('email') }}" required>
                                @if ($errors->has('email'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                                @endif
                            </div>
                            <div class="row">
                                <div class="col-md-6 form-group">
                                    <input id="username" placeholder="Username"  autocomplete="off" type="text" class="form-control{{ $errors->has('username') ? ' is-invalid' : '' }} username-input" name="username" value="{{ old('username') }}" required >
                                    @if ($errors->has('username'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('username') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="col-md-6 form-group">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="basic-addon1">+91</span>
                                        </div>
                                        <input id="phone" type="text" pattern="\d*" autocomplete="off" class="form-control{{ $errors->has('phone') ? ' is-invalid' : '' }}" minlength="10" maxlength="10" name="phone" value="{{ request('phone') ? request('phone') : old('phone') }}" placeholder="Phone number" required>
                                    </div>
                                    @if ($errors->has('phone'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group mt-2">
                                <a href="#next" class="btn btn-primary mb-4 btn-lg btn-block wizard-control">
                                    Continue
                                </a>
                                <div class="loginSignUpSeparator"><span class="textInSeparator">or</span></div>
                                <a class="btn btn-block btn-secondary" href="{{ route('login') }}">
                                    {{ __('Login') }}
                                </a>
                            </div>
                        </section>
                        <h2></h2>
                        <section class="form-section">
                        <!-- <div class="form-group ">
                            <label class="col-form-label text-md-right">Per Page Rate</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <div class="input-group-text is-invalid"><i class="fa fa-money" aria-hidden="true" style="padding: 8px;"></i></div>
                                </div>
                                <input type="number" class="form-control" name="per_page_rate" required>
                            </div>
                        </div> -->
                        <div class="form-group ">
                            <label class="col-form-label text-md-right">Subjects</label>
                            <select multiple="multiple" id="select2-multiple" class="form-control select2-multiple" name="subjects[]">
                                <option value="">Select Subjects</option>
                                @foreach(getSubjects() as $subject)
                                <option>{{$subject->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group ">
                            <label for="password" class="col-form-label text-md-right">{{ __('Password') }}</label>
                            <input id="password" type="password" pattern="^(?=.*?[A-Z])(?=.*?[0-9]).{8,}$" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>
                            <p class="m-0 text-muted"><small>The password must contain at least 8 characters with one uppercase and one number.</small></p>
                            @if ($errors->has('password'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                            @endif
                        </div>
                        <div class="form-group ">
                            <label for="password-confirm" class="col-form-label text-md-right">{{ __('Confirm Password') }}</label>
                            <input id="password-confirm" data-parsley-equalto="#password" type="password" class="form-control" name="password_confirmation" required>
                        </div>
                        <div class="form-group">
                            <label for="chk-terms">
                                <input type="checkbox" id="chk-terms" name="terms" class="mr-2" required>
                                <small>I agree with the <a href="/privacy-policy" target="_blank">privacy policy</a>.</small>
                            </label>
                        </div>
                        <div class="mt-3 mb-0">
                            <a href="#previous" class="btn btn-secondary mb-4 btn-lg wizard-control">
                                Back
                            </a>
                            <button type="submit" class="btn btn-primary btn-signup mb-4 btn-lg float-right" disabled="true">
                                {{ __('Sign Up') }}
                            </button>
                        </div>
                    </section>

                </form>
            </div>
        </div>
    </div>
</div>
</div>
@endsection
@section('scripts')
<script src="{{ asset('js/jquery.alphanum.js') }}"></script>
<script>
    $( ".body #select2-multiple" ).select2({
        placeholder: 'Select subjects',
        tags: true
    });


    $(document).ready(function () {
        $('#username').alphanum();
        $("#username").on({
            keydown: function(e) {
                if (e.which === 32)
                    return false;
            },
            change: function() {
                this.value = this.value.replace(/\s/g, "");
            }
        });

        $('#wizard').on('click', '#chk-terms', function(){
            if ($(this).is(':checked')) {
                $('.btn-signup').attr('disabled', false);
            }else{
                $('.btn-signup').attr('disabled', true);
            }
        });
    });

    $(function () {
      var $sections = $('.form-section');

      function navigateTo(index) {
    // Mark the current section with the class 'current'
    $sections
    .removeClass('current')
    .eq(index)
    .addClass('current');
    // Show only the navigation buttons that make sense for the current section:
    /*$('.wizard-control[href="#previous"]').toggle(index > 0);
    */var atTheEnd = index >= $sections.length - 1;

    /*$('.wizard-control[href="#next"]').toggle(!atTheEnd);
    $('.wizard-control[href="#finish"]').toggle(atTheEnd);*/
}

function curIndex() {
    // Return the current index by looking at which section has the class 'current'
    return $sections.index($sections.filter('.current'));
}

  // Previous button is easy, just go back
  $('.wizard-control [href="#previous"]').click(function() {
    navigateTo(curIndex() - 1);
});

  // Next button goes forward iff current block validates
  $('.wizard-control[href="#next"]').click(function() {
    $("#select2-multiple" ).select2({
        placeholder: 'Select subjects',
        tags: true
    });
    $('.frmRegistration').parsley().whenValidate({
      group: 'block-' + curIndex()
  }).done(function() {
    console.log('clicked')
    $("#wizard").steps('next');
      //navigateTo(curIndex() + 1);
  });
});

  // Prepare sections by setting the `data-parsley-group` attribute to 'block-0', 'block-1', etc.
  $sections.each(function(index, section) {
    $(section).find(':input').attr('data-parsley-group', 'block-' + index);
});
  navigateTo(0); // Start at the beginning
});

</script>
@endsection
