@extends('layouts.app')

@section('content')

<section class="mt-5">
	<div class="">
		<div class="mb-md-4 pl-3 writer-header-custom">
			<h4>Solve Quiz</h4>
		</div>
		<div class="">
			<div class="col-sm-12">
			<form method="POST" action="/writer/test">
				@csrf
				<div class="card writer-quize-custom">
					<div class="card-body">
			            <div class="form-group">
			                <label for="account-number" class="col-form-label">1. What is the most significant task for a content writer..?</label>
			                <br>
			                <div class="form-check form-check-inline">
			                    <input class="form-check-input" type="radio" name="q1" id="q1o1" value="1" required>
			                    <label class="form-check-label" for="q1o1">None of these</label>
			                </div><br>
			                <div class="form-check form-check-inline">
			                    <input class="form-check-input" type="radio" name="q1" id="q1o2" value="2">
			                    <label class="form-check-label" for="q1o2">Research about a topic</label>
			                </div><br>
			                <div class="form-check form-check-inline">
			                    <input class="form-check-input" type="radio" name="q1" id="q1o3" value="3">
			                    <label class="form-check-label" for="q1o3">Applying techniques  of rewarding </label>
			                </div><br>
			                <div class="form-check form-check-inline">
			                    <input class="form-check-input" type="radio" name="q1" id="q1o4" value="4">
			                    <label class="form-check-label" for="q1o4">Altering information as per requirement</label>
			                </div>
			            </div>
            		</div>
           		</div>
         		<div class="card mt-2 writer-quize-custom">
					<div class="card-body">
			            <div class="form-group">
			                <label for="account-number" class="col-form-label">2. A content writer masters in Research______</label>
			                <br>
			                <div class="form-check form-check-inline">
			                    <input class="form-check-input" type="radio" name="q2" id="q2o1" value="1" required>
			                    <label class="form-check-label" for="q2o1">Grammer</label>
			                </div><br>
			                <div class="form-check form-check-inline">
			                    <input class="form-check-input" type="radio" name="q2" id="q2o2" value="2">
			                    <label class="form-check-label" for="q2o2">Application</label>
			                </div><br>
			                <div class="form-check form-check-inline">
			                    <input class="form-check-input" type="radio" name="q2" id="q2o3" value="3">
			                    <label class="form-check-label" for="q2o3">Learning</label>
			                </div><br>
			                <div class="form-check form-check-inline">
			                    <input class="form-check-input" type="radio" name="q2" id="q2o4" value="4">
			                    <label class="form-check-label" for="q2o4">Technique</label>
			                </div>
			            </div>
            		</div>
           		</div>
           		<div class="card mt-2 writer-quize-custom">
					<div class="card-body">
			            <div class="form-group">
			                <label for="account-number" class="col-form-label">3. Which of the following is not a very descriptive format of writing?</label>
			                <br>
			                <div class="form-check form-check-inline">
			                    <input class="form-check-input" type="radio" name="q3" id="q3o1" value="1" required>
			                    <label class="form-check-label" for="q3o1">Business Reports</label>
			                </div><br>
			                <div class="form-check form-check-inline">
			                    <input class="form-check-input" type="radio" name="q3" id="q3o2" value="2">
			                    <label class="form-check-label" for="q3o2">White Papers</label>
			                </div><br>
			                <div class="form-check form-check-inline">
			                    <input class="form-check-input" type="radio" name="q3" id="q3o3" value="3">
			                    <label class="form-check-label" for="q3o3">Academic Papers</label>
			                </div><br>
			                <div class="form-check form-check-inline">
			                    <input class="form-check-input" type="radio" name="q3" id="q3o4" value="4">
			                    <label class="form-check-label" for="q3o4">Blog</label>
			                </div>
			            </div>
            		</div>
           		</div>
           		<div class="card mt-2 writer-quize-custom">
					<div class="card-body">
			            <div class="form-group">
			                <label for="account-number" class="col-form-label">4. What is the work of Content Writer?</label>
			                <br>
			                <div class="form-check form-check-inline">
			                    <input class="form-check-input" type="radio" name="q4" id="q4o1" value="1" required>
			                    <label class="form-check-label" for="q4o1">Content writers helps in promoting product</label>
			                </div><br>
			                <div class="form-check form-check-inline">
			                    <input class="form-check-input" type="radio" name="q4" id="q4o2" value="2">
			                    <label class="form-check-label" for="q4o2">All of these</label>
			                </div><br>
			                <div class="form-check form-check-inline">
			                    <input class="form-check-input" type="radio" name="q4" id="q4o3" value="3">
			                    <label class="form-check-label" for="q4o3">Content writers nothing but people who collect information about given topic and compose</label>
			                </div><br>
			                <div class="form-check form-check-inline">
			                    <input class="form-check-input" type="radio" name="q4" id="q4o4" value="4">
			                    <label class="form-check-label" for="q4o4">The main purpose of content writer is to describe a given product , work, service or subject matter</label>
			                </div>
			            </div>
            		</div>
           		</div>
           		<div class="card mt-2 writer-quize-custom">
					<div class="card-body">
			            <div class="form-group">
			                <label for="account-number" class="col-form-label">5. Content writing can be defined as the creation of written text in variety of different forms such as :</label>
			                <br>
			                <div class="form-check form-check-inline">
			                    <input class="form-check-input" type="radio" name="q5" id="q5o1" value="1" required>
			                    <label class="form-check-label" for="q5o1">Forums</label>
			                </div><br>
			                <div class="form-check form-check-inline">
			                    <input class="form-check-input" type="radio" name="q5" id="q5o2" value="2">
			                    <label class="form-check-label" for="q5o2">All of these</label>
			                </div><br>
			                <div class="form-check form-check-inline">
			                    <input class="form-check-input" type="radio" name="q5" id="q5o3" value="3">
			                    <label class="form-check-label" for="q5o3">Reviews</label>
			                </div><br>
			                <div class="form-check form-check-inline">
			                    <input class="form-check-input" type="radio" name="q5" id="q5o4" value="4">
			                    <label class="form-check-label" for="q5o4">Articles</label>
			                </div>
			            </div>
            		</div>
           		</div>
           		<div class="solve-quize-button my-md-5">
           			<button type="submit" class="btn btn-secondary btn-lg">Submit</button>
           		</div>
        </form>
        </div>
		</div>
	</section>

	@endsection