@extends('layouts.app')

@section('content')

<section class="mt-5">
    <div class="container">
        @section('heading', 'Documents for Verification')
    <div class="mb-md-4 documents-for-verification">
        <h4>Documents for Verification</h4>
    </div>
    <div class="row">
        <div class="col-md-12">
            <form method="POST" action="/writer/document" enctype="multipart/form-data">
                @csrf
                <div class="card shadow-boder-none">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm-6">
                                <label>Aadhar Card<sup class="error">*</sup></label>
                                <div class="uploadButton mb-md-5">
                                    <input class="uploadButton-input form-control" type="file" id="uploadAdhar" name="aadhar_card" required>
                                    <label class="uploadButton-button ripple-effect" for="uploadAdhar">Choose File</label>
                                    <span class="uploadButton-file-name">Click to upload AADHAR Card</span>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <label>PAN Card<sup class="error">*</sup></label>
                                <div class="uploadButton mb-md-5">
                                    <input class="uploadButton-input form-control" type="file" id="uploadPan" name="pan_card" required>
                                    <label class="uploadButton-button ripple-effect" for="uploadPan">Choose File</label>
                                    <span class="uploadButton-file-name">Click to upload PAN Card</span>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <label>10<sup>th</sup> Marksheet<sup class="error">*</sup></label>
                                <div class="uploadButton mb-md-5">
                                    <input class="uploadButton-input form-control" id="uploadTenthMarksheet" type="file" name="tenth_marksheet" required>
                                    <label class="uploadButton-button ripple-effect" for="uploadTenthMarksheet">Choose File</label>
                                    <span class="uploadButton-file-name">Click to upload 10<sup>th</sup> Marksheet</span>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <label>12<sup>th</sup> Marksheet<sup class="error">*</sup></label>
                                <div class="uploadButton mb-md-5">
                                    <input class="uploadButton-input form-control" id="uploadTwelfthMarksheet" type="file" name="twelfth_marksheet" required>
                                    <label class="uploadButton-button ripple-effect" for="uploadTwelfthMarksheet">Choose File</label>
                                    <span class="uploadButton-file-name">Click to upload  12<sup>th</sup> Marksheet</span>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <label>College Degree<sup class="error">*</sup></label>
                                <div class="uploadButton mb-md-5">
                                    <input class="uploadButton-input form-control" type="file" name="degree_marksheet" id="uploadDegreeMarksheet" required>
                                    <label class="uploadButton-button ripple-effect" for="uploadDegreeMarksheet">Choose File</label>
                                    <span class="uploadButton-file-name">Click to upload Degree Marksheet</span>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <label>College Transcript</label>
                                <div class="uploadButton mb-md-5">
                                    <input class="uploadButton-input form-control" type="file" name="college_transcript" id="uploadTranscriptMarksheet">
                                    <label class="uploadButton-button ripple-effect" for="uploadTranscriptMarksheet">Choose File</label>
                                    <span class="uploadButton-file-name">Click to upload College Transcript</span>
                                </div>
                            </div>
                        </div>
                        <div class="border-top">
                            <button type="submit" class="btn mt-3 btn-secondary btn-lg">Submit</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    </div>
</section>

@endsection


@section('scripts')

    <script type="text/javascript">
        var inputs = document.querySelectorAll( '.uploadButton-input ' );
        Array.prototype.forEach.call( inputs, function( input )
        {
              var label = input.nextElementSibling.nextElementSibling,
              labelVal = label.innerHTML;

            input.addEventListener( 'change', function( e )
            {
                var fileName = '';

                fileName = e.target.value.split( '\\' ).pop();

                if( fileName )
                    label.innerHTML = 'Selected File : <span class="filename">' + fileName + '</span>';
                else
                    label.innerHTML = labelVal;
            });
        });
    </script>

@endsection