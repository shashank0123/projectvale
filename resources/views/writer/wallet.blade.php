@extends('layouts.app')

@section('content')
<div class="my-md-5 my-sm-0">
    <div class="headline d-flex justify-content-between align-items-cente hidden-on-mobile">
        <h3 class="d-inline-block">Wallet</h3>
    </div>

    <div class="mt-4">
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th>Sr.No.</th>
                    <th>Topic</th>
                    <th>Subject</th> 
                    <th>Amount</th>
                    <th>Payment History</th>
                    <th>Status</th>
                </tr>
            </thead>
            <tbody>
                @foreach($projects as $key => $project)
                <tr>
                    <td>{{$key + 1}}</td>
                    <td>{{$project->topic}}</td>
                    <td>{{$project->subject}}</td> 
                    <td>&#8377; {{($project->pages_count * $project->writer->project_rate_for_writer($project))}}</td>
                    <td><button href="#payment_history_{{$key}}" data-toggle="modal" class="btn btn-primary">Show</button></td>
                    <td><span class="badge badge-{{isset($project->admin_payment_status) && $project->admin_payment_status == 1 ? 'success' : 'danger'}} py-1 px-3">{{isset($project->admin_payment_status) && $project->admin_payment_status == 1 ? 'Paid' : 'Pending'}}</span></td>
                </tr>
                @endforeach
            </tbody>
        </table>
        {{ $projects->links() }}
    </div>
</div>
<!-- <section class="mt-5">
    <div class="row">
        <div class="col-sm-12">

        </div>
    </div>
</section> -->
@include('projects.partials.modals')
@endsection
@section('scripts')
<script>
    $(document).ready(function(e){

    });
</script>
@endsection