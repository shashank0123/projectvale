<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('login', 'Auth\LoginController@login');
Route::post('register', 'Auth\RegisterController@register');

Route::middleware('auth:api')->get('projects', 'Api\ProjectsController@index');
Route::middleware('auth:api')->get('projects/{project}', 'Api\ProjectsController@show');
Route::middleware('auth:api')->post('projects', 'Api\ProjectsController@store');
Route::middleware('auth:api')->post('projects/{project}/update', 'Api\ProjectsController@update');
Route::middleware('auth:api')->get('projects/{project}/delete', 'Api\ProjectsController@destroy');
Route::middleware('auth:api')->get('projects/{project}/chats', 'Api\ProjectsController@chats');
Route::middleware('auth:api')->get('projects/{project}/files', 'Api\ProjectsController@files');
Route::middleware('auth:api')->get('projects/{project}/writer', 'Api\WritersController@index');

Route::middleware('auth:api')->post('/projects/{project}/complete','Api\ProjectFeedbackController@store');
Route::middleware('auth:api')->get('/projects/{project}/skipcomplete','Api\ProjectFeedbackController@skip');
Route::middleware('auth:api')->get('/projects/{project}/writer/{writer}/profile','Api\WritersController@profile');
Route::middleware('auth:api')->get('/projects/{project}/writer/{writer}/hire','Api\ProjectsController@hire');

Route::middleware('auth:api')->get('/projects/{project}/getusernames','Api\ProjectsController@getProjectUsersname');

Route::middleware('auth:api')->get('/projects/{project}/writer/{writer}/accept','Api\ProjectsController@accept');
Route::middleware('auth:api')->get('/projects/{project}/writer/{writer}/decline','Api\ProjectsController@decline');
Route::middleware('auth:api')->get('/projects/{project}/writer/{writer}/cancelrequest','Api\ProjectsController@cancelRequest');


Route::middleware('auth:api')->get('/projects/{project}/writer/{writer}/pay', 'Api\ProjectPayController@store');
Route::middleware('auth:api')->post('/projects/{project}/chat', 'Api\ProjectChatsController@store');
Route::middleware('auth:api')->post('/projects/{project}/references', 'Api\ProjectReferencesController@store');
Route::middleware('auth:api')->post('/projects/{project}/{user}/files', 'Api\ProjectFilesController@store');
Route::middleware('auth:api')->get('/files/{file}/{project}/delete','Api\ProjectFilesController@destroy');
Route::middleware('auth:api')->get('/files','Api\FilesController@index');


Route::middleware('auth:api')->post('/user/{user}/updateprofile', 'Api\ProfileController@updateProfile');
Route::middleware('auth:api')->post('/user/{user}/changepassword', 'Api\ProfileController@updatePassword');
Route::middleware('auth:api')->get('/user/{user}', 'Api\ProfileController@getProfile');
Route::middleware('auth:api')->post('/profile/avatar', 'Api\ProfileController@updateAvatar');

Route::middleware('auth:api')->get('/profile/feedback', 'Api\WriterprofileController@feedback');
Route::middleware('auth:api')->get('/profile/availability', 'Api\ProfileController@availability');
Route::middleware('auth:api')->post('/profile/availability', 'Api\ProfileController@saveavailability');
Route::middleware('auth:api')->get('/profile/completedprojectscount', 'Api\ProfileController@CompletedProjectsCount');
Route::middleware('auth:api')->get('/profile/ongoingprojectscount', 'Api\ProfileController@OngoingProjectsCount');
Route::middleware('auth:api')->post('password/email', 'Api\ProfileController@sendResetLinkEmail');



Route::middleware('auth:api')->get('writers', 'Api\WritersController@index');
Route::middleware('auth:api')->get('writers/{writer}', 'Api\WritersController@show');
Route::middleware('auth:api')->get('writer/{writer}/profile/feedback', 'Api\WritersController@feedbacks');
Route::middleware('auth:api')->get('writer/{writer}/profile/history', 'Api\WritersController@history');
Route::middleware('auth:api')->get('/writer/{writer}/profile','Api\WritersController@detail');

Route::middleware('auth:api')->post('/broadcast/auth', 'Api\BroadcastAuthController@auth');


