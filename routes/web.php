<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'WelcomeController@index');
Route::get('/projects/create', 'ProjectsController@create')->name('create');
Route::post('/projects', 'ProjectsController@store');
Route::get('/projects/{project}','ProjectsController@show');
Route::get('/projects/{project}/delete','ProjectsController@destroy');
Route::get('/projects/{project}/revoke','ProjectsController@revoke');
Route::get('/projects/{project}/edit','ProjectsController@edit');
Route::post('/projects/{project}/complete','ProjectFeedbackController@store');
Route::get('/projects/{project}/skipcomplete','ProjectFeedbackController@skip');
Route::post('/projects/{project}/update','ProjectsController@update');
Route::get('/projects/{project}/writer/{writer}/profile','WritersController@profile');
Route::get('/projects/{project}/writer/{writer}','ProjectsController@hire');
Route::get('/demo', 'ProjectsController@demo');
Route::post('/projects/{project}/{user}/files', 'ProjectFilesController@store');
Route::post('/projects/{project}/chat/{user}', 'ProjectChatsController@store');
Route::get('/projects/{project}/{writer}/payment', 'ProjectPayController@index');
Route::post('/projects/{project}/{writer}/pay', 'ProjectPayController@store');
Route::get('/projects/{project}/turniton', 'ProjectPayController@turniton');

Route::get('/projects/{project}/writer/{writer}/accept','ProjectsController@accept');
Route::get('/projects/{project}/writer/{writer}/decline','ProjectsController@decline');
Route::get('/projects/{project}/{writer}/cancelrequest','ProjectsController@cancelRequest');

Route::get('/files/{file}/{project}/delete','ProjectFilesController@destroy');
Route::get('/files','FilesController@index');

Auth::routes(['verify' => true]);

Route::get('/home', 'HomeController@index')->name('home');

Route::post('/user/{user}/updateprofile', 'ProfileController@updateProfile');
Route::post('/user/{user}/changepassword', 'ProfileController@updatePassword');
Route::get('/user/{user}/tours', 'ProfileController@tours');

Route::post('/profile/avatar', 'ProfileController@updateAvatar');


Route::get('/writer/register', 'WriterregisterController@index');
Route::get('/writer/document', 'WriterDocumentsController@index');
Route::post('/writer/document', 'WriterDocumentsController@store');
Route::post('/writer', 'WriterregisterController@store');
Route::get('/writer', 'WriterloginController@index');
Route::get('/writer/login', 'WriterloginController@index');
Route::get('/writer/test', 'WriterquizController@index');
Route::post('/writer/test', 'WriterquizController@store');

Route::get('/writer/notverify', 'WritersController@notverify');
Route::get('/writer/project', 'WriterprojectController@index');

Route::get('/writer/project/details', 'WriterprojectController@show');

Route::get('/writer/profile', 'WriterprofileController@index');
Route::get('/writer/security', 'WriterprofileController@security');
// Route::get('/writer/wallet', 'WriterprofileController@wallet');
Route::get('/wallet', 'WriterprofileController@wallet');

Route::get('/profile/edit', 'ProfileController@view');
Route::get('/profile', 'ProfileController@index');
Route::get('/profile/feedback', 'WriterprofileController@feedback');
Route::get('/profile/security', 'ProfileController@security');
Route::get('/profile/availability', 'ProfileController@availability');
Route::post('/profile/availability', 'ProfileController@saveavailability');
Route::get('/feedback', 'ProfileController@feedback');
// Route::get('/wallet','ProfileController@wallet');

Route::get('/order', 'OrderController@index');
Route::get('/order/show', 'OrderController@show');



Route::get('/admin/login', 'Backend\BackendLoginController@index');
Route::post('/admin/login', 'Backend\BackendLoginController@login');
Route::get('/admin/logout', 'Backend\BackendLoginController@logout');

Route::get('/admin', 'Backend\BackendDashboardController@index');
Route::get('/admin/users', 'Backend\BackendUsersController@index');
Route::get('/admin/users/{user}', 'Backend\BackendUsersController@show');
Route::get('/admin/users/{user}/delete', 'Backend\BackendUsersController@destroy');
Route::get('/admin/users/{user}/project/{project}', 'Backend\BackendUsersController@project');

Route::get('/admin/writers', 'Backend\BackendWritersController@index');
Route::get('/admin/writers/pending', 'Backend\BackendWritersController@pending');
Route::get('/admin/writers/verified', 'Backend\BackendWritersController@verified');
Route::get('/admin/writers/unverified', 'Backend\BackendWritersController@unverified');
Route::get('/admin/writers/{user}/project/{project}', 'Backend\BackendWritersController@project');
Route::get('/admin/writers/{writer}', 'Backend\BackendWritersController@show');
Route::get('/admin/writers/{writer}/delete', 'Backend\BackendWritersController@destroy');
Route::post('/admin/writers/{writer}/verify', 'Backend\BackendWritersController@verify');
Route::get('/admin/writers/{writer}/unverify', 'Backend\BackendWritersController@unverify');
Route::post('/admin/writers/{writer}/rate', 'Backend\BackendWritersController@rate');

Route::get('/admin/payments', 'Backend\BackendPaymentsController@index');
Route::get('/admin/projects/{project}/pay', 'Backend\BackendPaymentsController@store');
Route::resource('/admin/durations', 'Backend\BackendDurationController');


Route::get('/admin/verification/pending', 'Backend\BackendVerificationsController@pending');
Route::get('/admin/verification/writer/{writer}', 'Backend\BackendVerificationsController@writer');

Route::get('/admin/projects', 'Backend\BackendProjectsController@index');
Route::get('/admin/projects/{project}', 'Backend\BackendProjectsController@show');
Route::get('/admin/projects/{project}/delete', 'Backend\BackendProjectsController@destroy');

Route::get('/admin/subjects', 'Backend\BackendSubjectsController@index');
Route::post('/admin/subjects', 'Backend\BackendSubjectsController@store');
Route::post('/admin/subjects/{subject}/update', 'Backend\BackendSubjectsController@update');
Route::get('/admin/subjects/{subject}/delete', 'Backend\BackendSubjectsController@destroy');

Route::get('/admin/papertypes', 'Backend\BackendPaperTypeController@index');
Route::post('/admin/papertypes', 'Backend\BackendPaperTypeController@store');
Route::post('/admin/papertypes/{papertype}/update', 'Backend\BackendPaperTypeController@update');
Route::get('/admin/papertypes/{papertype}/delete', 'Backend\BackendPaperTypeController@destroy');

Route::get('/admin/citations', 'Backend\BackendCitationsController@index');
Route::post('/admin/citations', 'Backend\BackendCitationsController@store');
Route::post('/admin/citations/{citation}/update', 'Backend\BackendCitationsController@update');
Route::get('/admin/citations/{citation}/delete', 'Backend\BackendCitationsController@destroy');

Route::get('/admin/files', 'Backend\BackendFilesController@index');
Route::post('/admin/files/{file}/verify', 'Backend\BackendFilesController@verify');
Route::post('/admin/files/{file}/unverify', 'Backend\BackendFilesController@unverify');


Route::get('/admin/sociallinks', 'Backend\BackendSocialLinksController@index');
Route::post('/admin/sociallinks', 'Backend\BackendSocialLinksController@store');
Route::get('/admin/settings', 'Backend\BackendSettingsController@index');
Route::post('/admin/settings', 'Backend\BackendSettingsController@store');

Route::get('/admin/database', 'Backend\BackendDatabaseController@index');
Route::post('/admin/database/clear', 'Backend\BackendDatabaseController@clear');
Route::post('/admin/database/subjects/clear', 'Backend\BackendDatabaseController@subjectsClear');
Route::post('/admin/database/papertype/clear', 'Backend\BackendDatabaseController@papertypeClear');
Route::post('/admin/database/citation/clear', 'Backend\BackendDatabaseController@citationClear');



Route::get('/maildemo', 'ProjectsController@maildemo');


Route::view('/terms-of-conditions', 'legals.terms');
Route::view('/privacy-policy', 'legals.privacy');
Route::view('/contact-us', 'legals.contact');
Route::post('/contact', 'ContactController@send');


